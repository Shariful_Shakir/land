$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

function ajaxCallCB(url, data, callback) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json'
    }).done(function (response) {
        callback(response);
    });
}

// -------written by shariful-------
// var lang = localStorage.getItem("lang");
// $(document).ready(function(){
//     $('.langEn').hide();
//         if (localStorage.getItem("lang")) {

//         }else{
//             localStorage.setItem("lang", 2);
//             lang = localStorage.getItem("lang");
//         }
// });


$(document).on('keypress', '.eng-input', function (e) {

    var code = e.which || e.keyCode;
    var key = String.fromCharCode(code);
    //console.log(langId);
    if (
        (key >= "a" && key <= "z") || (key >= "A" && key <= "Z") || (key >= "0" && key <= "9") ||
        key == ";" || key == ":" || key == "!" || key == "@" || key == "#" || key == "$" || key == "%" ||
        key == "^" || key == "&" || key == "*" || key == "(" || key == ")" || key == "-" || key == "_" ||
        key == "+" || key == "=" || key == "." || key == "," || key == "?" || key == "<" || key == ">" ||
        key == "/" || key == "\\" || key == "{" || key == "}" || key == "[" || key == "]" || key == "|" ||
        key == " " || key == "	" || key == "\'" || key == "\"" || code == 8 || code == 13 || code == 16
    ) {
        //console.log("ENG");
    } else {
        var msg = (langId == 1) ? 'Bengali Characters are not acceptable.' : 'বাংলা বর্ণমালা গ্রহনযোগ্য নয়।';
        alert(msg);

        //console.log("OTH-eng");
        e.preventDefault();
    }

});

$(document).on('keypress', '.bng-input', function (e) {
    var code = e.which || e.keyCode;
    var key = String.fromCharCode(code);
    var flag = 0;
    //console.log(langId);
    if ((e.which === 97 || e.which === 99 || e.which === 118 || e.which === 120) && e.ctrlKey) {
        flag = 1;
    }
    if (
        ((key >= "a" && key <= "z") || (key >= "A" && key <= "Z") || (key >= "0" && key <= "9") || key == ";" ||
            key == ":" || key == "@" || key == "#" || key == "$" || key == "^" || key == "&") && flag === 0) {

        var msg = (langId == 1) ? 'English Characters are not acceptable.' : 'ইংরেজি বর্ণমালা গ্রহনযোগ্য নয়।';
        alert(msg);

        //console.log("OTH-bng");
        e.preventDefault();
    } else {
        //console.log("BNG");
    }

});

function cv_bn_to_en_js(input) {
    var numbersB = {
        '০': 0,
        '১': 1,
        '২': 2,
        '৩': 3,
        '৪': 4,
        '৫': 5,
        '৬': 6,
        '৭': 7,
        '৮': 8,
        '৯': 9
    };
    var output = [];
    for (var i = 0; i < input.length; ++i) {
        if (numbersB.hasOwnProperty(input[i])) {
            output.push(numbersB[input[i]]);
        } else {
            output.push(input[i]);
        }
    }
    return output.join('');
}

// -------end written by shariful-------

$(document).ready(function () {
    $('.nav2 .nav2-btn').click(function () {
        $('.dropd2').css('display', 'block');
        console.log('click');

    });
});
