 function validateForm(editStatus = '') {
     var trackingNo = $('#tracking_no').val();
     var name = $('#name').val();
     var father_name = $('#father_name').val();
     var mother_name = $('#mother_name').val();
     var contact_no = $('#contact_no').val();
     var contact_no_confirmation = $('#contact_no_confirmation').val();
     var gender = $('#gender').val();
     var date_of_birth = $('#date_of_birth').val();
     var confirm_date_of_birth = $('#confirm_date_of_birth').val();
     var nationality = $('#nationality').val();
     var national_id = $('#national_id').val();
     var national_id_confirmation = $('#national_id_confirmation').val();
     var religion = $('#religion').val();

     var upazila_id = $('#upazila_id').val();
     var union_id = $('#union_id').val();
     var present_village_id = $('#present_village_id').val();
     var present_post_code_id = $('#present_post_code_id').val();

     var permanent_upazila_id = $('#permanent_upazila_id').val();
     var permanent_union_id = $('#permanent_union_id').val();
     var permanent_village_id = $('#permanent_village_id').val();
     var permanent_post_code_id = $('#permanent_post_code_id').val();

     var batch_no = $('#batch_no').val();
     var session_id = $('#session_id').val();
     var educational_qualification = $('#educational_qualification').val();
     var img_file = $('#img_file').val();
     if (trackingNo == "") {
         if (langId == 1) {
             alert("Tracking number must be required. ");
             $('#tracking_no').focus();
         } else {
             alert("ট্রাকিং নম্বর অবশ্যই দিতে হবে ।");
             $('#tracking_no').focus();
         }
         return false;
     }

     if (name == "") {
         if (langId == 1) {
             alert("Name must be required. ");
             $('#name').focus();
         } else {
             alert("নাম অবশ্যই দিতে হবে ।");
             $('#name').focus();
         }
         return false;
     }
     if (father_name == "") {
         if (langId == 1) {
             alert("Father's/Husband's name must be required. ");
             $('#father_name').focus();
         } else {
             alert("পিতা/স্বামীর নাম অবশ্যই দিতে হবে ।");
             $('#father_name').focus();
         }
         return false;
     }
     if (mother_name == "") {
         if (langId == 1) {
             alert("Mother's name must be required. ");
             $('#mother_name').focus();
         } else {
             alert("মায়ের নাম অবশ্যই দিতে হবে ।");
             $('#mother_name').focus();
         }
         return false;
     }
     if (contact_no == "") {
         if (langId == 1) {
             alert("Mobile number must be required. ");
             $('#contact_no').focus();
         } else {
             alert("মোবাইল নং অবশ্যই দিতে হবে ।");
             $('#contact_no').focus();
         }
         return false;
     }
     if (contact_no_confirmation == "") {
         if (langId == 1) {
             alert("Mobile number must be required. ");
             $('#contact_no_confirmation').focus();
         } else {
             alert("পূর্নরায় মোবাইল নং অবশ্যই দিতে হবে ।");
             $('#contact_no_confirmation').focus();
         }
         return false;
     }
     if (contact_no_confirmation != "") {
         var contact_no = $('#contact_no').val();
         var contact_no_confirmation = $('#contact_no_confirmation').val();
         if (contact_no != contact_no_confirmation) {
             if (langId == 1) {
                 alert("Mobile number not match. ");
                 $('#contact_no_confirmation').val('');
                 $('#contact_no').val('');
                 $('#contact_no').focus();
             } else {
                 alert("মোবাইল নং একই হতে হবে।");
                 $('#contact_no_confirmation').val('');
                 $('#contact_no').val('');
                 $('#contact_no').focus();
             }
         }
     }
     if (email_confirmation != "") {
         var email = $('#email').val();
         var email_confirmation = $('#email_confirmation').val();
         if (email != email_confirmation) {
             if (langId == 1) {
                 alert("Email not match. ");
                 $('#email_confirmation').val('');
                 $('#email').val('');
                 $('#email').focus();
             } else {
                 alert("ই-মেইল একই হতে হবে।");
                 $('#email_confirmation').val('');
                 $('#email').val('');
                 $('#email').focus();
             }
         }
     }
     if (gender == "") {
         if (langId == 1) {
             alert("Gender number must be required. ");
             $('#gender').focus();
         } else {
             alert("লিঙ্গ অবশ্যই দিতে হবে ।");
             $('#gender').focus();
         }
         return false;
     }
     if (date_of_birth == "") {
         if (langId == 1) {
             alert("Date of birth must be required. ");
             $('#date_of_birth').focus();
         } else {
             alert("জন্ম তারিখ অবশ্যই দিতে হবে ।");
             $('#date_of_birth').focus();
         }
         return false;
     }
     if (confirm_date_of_birth == "") {
         if (langId == 1) {
             alert("Confirmd date of birth must be required. ");
             $('#confirm_date_of_birth').focus();
         } else {
             alert("পুনরায় জন্ম তারিখ অবশ্যই দিতে হবে ।");
             $('#confirm_date_of_birth').focus();
         }
         return false;
     }
     if (confirm_date_of_birth != "") {
         var date_of_birth = $('#date_of_birth').val();
         var confirm_date_of_birth = $('#confirm_date_of_birth').val();
         if (date_of_birth != confirm_date_of_birth) {
             if (langId == 1) {
                 alert("Date of birth not match. ");
                 $('#confirm_date_of_birth').val('');
                 $('#date_of_birth').val('');
                 $('#date_of_birth').focus();
             } else {
                 alert("জন্ম তারিখ একই হতে হবে।");
                 $('#confirm_date_of_birth').val('');
                 $('#date_of_birth').val('');
                 $('#date_of_birth').focus();
             }
         }
     }
     if (nationality == "") {
         if (langId == 1) {
             alert("Nationality must be required. ");
             $('#nationality').focus();
         } else {
             alert("জাতীয়তা অবশ্যই দিতে হবে ।");
             $('#nationality').focus();
         }
         return false;
     }
     if (national_id == "") {
         if (langId == 1) {
             alert("National ID must be required. ");
             $('#national_id').focus();
         } else {
             alert("জাতীয় পরিচয়পত্র নং অবশ্যই দিতে হবে ।");
             $('#national_id').focus();
         }
         return false;
     }
     if (national_id_confirmation == "") {
         if (langId == 1) {
             alert("Confirmation National ID must be required. ");
             $('#national_id_confirmation').focus();
         } else {
             alert("পুনরায় জাতীয় পরিচয়পত্র নং অবশ্যই দিতে হবে ।");
             $('#natinational_id_confirmationonal_id').focus();
         }
         return false;
     }
     if (national_id_confirmation != "") {
         var national_id = $('#national_id').val();
         var national_id_confirmation = $('#national_id_confirmation').val();
         if (national_id != national_id_confirmation) {
             if (langId == 1) {
                 alert("National ID not match. ");
                 $('#national_id_confirmation').val('');
                 $('#national_id').val('');
                 $('#national_id').focus();
             } else {
                 alert("জাতীয় পরিচয়পত্র নং একই হতে হবে।");
                 $('#national_id_confirmation').val('');
                 $('#national_id').val('');
                 $('#national_id').focus();
             }
         }
     }
     if (religion == "") {
         if (langId == 1) {
             alert("Religion must be required. ");
             $('#religion').focus();
         } else {
             alert("ধর্ম অবশ্যই দিতে হবে ।");
             $('#religion').focus();
         }
         return false;
     }
     if (upazila_id == "") {
         if (langId == 1) {
             alert("Upazila must be required. ");
             $('#upazila_id').focus();
         } else {
             alert("উপজেলার নাম অবশ্যই দিতে হবে ।");
             $('#upazila_id').focus();
         }
         return false;
     }
     if (union_id == "") {
         if (langId == 1) {
             alert("Union must be required. ");
             $('#union_id').focus();
         } else {
             alert("ইউনিয়ন নাম অবশ্যই দিতে হবে ।");
             $('#union_id').focus();
         }
         return false;
     }
     if (present_village_id == "") {
         if (langId == 1) {
             alert("Village/House No must be required. ");
             $('#present_village_id').focus();
         } else {
             alert("গ্রাম/বাসা নং অবশ্যই দিতে হবে ।");
             $('#present_village_id').focus();
         }
         return false;
     }
     if (present_post_code_id == "") {
         if (langId == 1) {
             alert("Post Code must be required. ");
             $('#present_post_code_id').focus();
         } else {
             alert("পোষ্ট কোড অবশ্যই দিতে হবে ।");
             $('#present_post_code_id').focus();
         }
         return false;
     }

     if ($('#address_check').prop("checked") != true) {
         if (permanent_upazila_id == "") {
             if (langId == 1) {
                 alert("Permanent upazila must be required. ");
                 $('#permanent_upazila_id').focus();
             } else {
                 alert("স্থায়ী উপজেলা অবশ্যই দিতে হবে ।");
                 $('#permanent_upazila_id').focus();
             }
             return false;
         }
         if (permanent_union_id == "") {
             if (langId == 1) {
                 alert("Permanent union must be required. ");
                 $('#permanent_union_id').focus();
             } else {
                 alert("স্থায়ী ইউনিয়ন নাম অবশ্যই দিতে হবে ।");
                 $('#permanent_union_id').focus();
             }
             return false;
         }
         if (permanent_village_id == "") {
             if (langId == 1) {
                 alert("Permanent village must be required. ");
                 $('#permanent_village_id').focus();
             } else {
                 alert("স্থায়ী গ্রাম/বাসা নং অবশ্যই দিতে হবে ।");
                 $('#permanent_village_id').focus();
             }
             return false;
         }
         if (permanent_post_code_id == "") {
             if (langId == 1) {
                 alert("Permanent post code must be required. ");
                 $('#permanent_post_code_id').focus();
             } else {
                 alert("স্থায়ী পোষ্ট কোড অবশ্যই দিতে হবে ।");
                 $('#permanent_post_code_id').focus();
             }
             return false;
         }
     }



     if (batch_no == "") {
         if (langId == 1) {
             alert("Batch No. must be required. ");
             $('#batch_no').focus();
         } else {
             alert("ব্যাচ নং অবশ্যই দিতে হবে ।");
             $('#batch_no').focus();
         }
         return false;
     }
     if (session_id == "") {
         if (langId == 1) {
             alert("Session must be required. ");
             $('#session_id').focus();
         } else {
             alert("সেশন  অবশ্যই দিতে হবে ।");
             $('#session_id').focus();
         }
         return false;
     }
     if (educational_qualification == "") {
         if (langId == 1) {
             alert("Educational Qualification must be required. ");
             $('#educational_qualification').focus();
         } else {
             alert("শিক্ষাগত যোগ্যতা অবশ্যই দিতে হবে ।");
             $('#educational_qualification').focus();
         }
         return false;
     }
     if (img_file == "" && editStatus != 'edit') {
         if (langId == 1) {
             alert("Image must be required. ");
             $('#img_file').focus();
             $("#img_file").css("border", "1px solid red");
         } else {
             alert("ছবি আপলোড অবশ্যই করতে হবে ।");
             $('#img_file').focus();
             $("#img_file").css("border", "1px solid red");
         }
         return false;
     }
 }

 $(document).ready(function () {
     $('#file').on('change', function () {
         var file = $('#file').val();
         var file_extension = file.split('.').pop().toUpperCase();
         var file_size = this.files[0].size;
         if (file_size > 10000000) {
             if (langId == 2) {
                 alert("ফাইলের আকার সর্বাধিক ১০ এমবি।");
                 $('#file').val('');
                 $('#file').focus();
             } else {
                 alert("File size maximum 10 mb.");
                 $('#file').val('');
                 $('#file').focus();
             }
         }
         if (file_extension != 'DOC' && file_extension != 'DOCX' && file_extension != 'XLSX' && file_extension != 'PDF') {
             if (langId == 2) {
                 alert("ফাইল এর টাইপ অবশ্যই .doc, .docx, .xlsx, .pdf হতে হবে।");
                 $('#file').val('');
                 $('#file').focus();
                 // $("#file").css("border", "1px solid red");
             } else {
                 alert("File type must be .doc, .docx, .xlsx, .pdf");
                 $('#file').val('');
                 $('#file').focus();
                 // $("#file").css("border", "1px solid red");
             }
         }

         if (file != '') {
             // $("#file").css("border", "1px solid green");
             $('#submitbtn').focus();
         }
     });

     $('#img_file').on('change', function () {
         var img = $('#img_file').val();
         var img_extension = img.split('.').pop().toUpperCase();
         var img_size = this.files[0].size;

         if (img_extension != 'JPG' && img_extension != 'JPEG' && img_extension != 'PNG') {
             if (langId == 2) {
                 alert("ছবি এর টাইপ অবশ্যই .jpg, .png, .jpeg হতে হবে।");
                 $('#img_file').val('');
                 $('#img_file').focus();
                 $("#img_file").css("border", "1px solid red");
             } else {
                 alert("Photo type must be .jpg, .jpeg, .png");
                 $('#img_file').val('');
                 $('#img_file').focus();
                 $("#img_file").css("border", "1px solid red");
             }
         }
         if (img_size > 2000000) {
             if (langId == 2) {
                 alert("ছবি আকার সর্বাধিক ২ এমবি।");
                 $('#img_file').val('');
                 $('#img_file').focus();
                 $("#img_file").css("border", "1px solid red");
             } else {
                 alert("Photo size maximum 2 mb.");
                 $('#img_file').val('');
                 $('#img_file').focus();
                 $("#img_file").css("border", "1px solid red");
             }
         }

         if (img != '') {
             $("#img").css("border", "1px solid green");
         }
     });
 });
