@extends('layouts.app')
@section('content')


        @if(isset($data) && !empty($data))
        <div class="graph-section">
            <div class="container">
                <div class="row">
                    <div class="user-section ">
                        <div class="col-xs-12 col-sm-12 col-md-12 com-lg-12">
                            <h3 class="user-management">
                                @if($langId == 1)
                                    {{"Edit User"}}
                                    @else($langId == 2)
                                     {{"ব্যবহারকারী সম্পাদনা"}}
                                @endif
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="user-section form-section">
                        <form action="{{route('users.update', $user->id)}}" method="POST">
                            {{method_field('PUT')}}
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="name" class="label">
                                                <span class="langBn">নাম</span><span class="langEn">Name</span><span class="text-danger"> *
                                                </span>
                                            </label>
                                            <input type="text" class="form-control" name="name" id="name" value="{{$user->name}}">
                                        </div>
                                    </div>
                                    @role('super_admin')
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="division"><span class="langBn">বিভাগের নাম</span>
                                                <span class="langEn">Division Name</span>
                                                <span class="text-danger"> * </span>
                                            </label>

                                            <select class="custom-select my-1 mr-sm-2" id="division_id" name="division_id">
                                                @if($langId == 1)
                                                    {!! getSelectOptions($data['division_en'], $user->division_id) !!}
                                                    @else($langId == 2)
                                                     {!! getSelectOptions($data['division_bn'], $user->division_id) !!}
                                                @endif
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <label for="district_id"><span class="langBn">জেলার নাম</span>
                                                <span class="langEn">District Name</span>
                                                <span class="text-danger"> * </span>
                                            </label>
                                        <select class="custom-select my-1 mr-sm-2" id="division_id" name="division_id">
                                            @if($langId == 1)
                                                {!! getSelectOptions($data['district_en'], $user->district_id) !!}
                                                @else($langId == 2)
                                                 {!! getSelectOptions($data['district_bn'], $user->district_id) !!}
                                            @endif
                                        </select>
                                        </div>
                                    </div>

                                    @endrole

                                    @role('division_admin')
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <label for="district_id"><span class="langBn">জেলার নাম</span>
                                                <span class="langEn">District Name</span>
                                                <span class="text-danger"> * </span>
                                            </label>
                                            <select class="custom-select my-1 mr-sm-2" id="district_id" name="district_id">
                                                @if($langId == 1)
                                                    {!! getSelectOptions($data['district_en'], $user->district_id) !!}
                                                    @else($langId == 2)
                                                     {!! getSelectOptions($data['district_bn'], $user->district_id) !!}
                                                @endif
                                            </select>
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    @endrole

                                    @role('district_admin')
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <label for="district_id"><span class="langBn">উপজেলার নাম</span>
                                                <span class="langEn">Upazila Name</span><span class="text-danger"> *
                                                </span>
                                            </label>

                                            <select class="custom-select my-1 mr-sm-2" id="upazila_id" name="upazila_id">
                                            @if($langId == 1)
                                                {!! getSelectOptions($data['upazila_en'], $user->upazila_id) !!}
                                                @else($langId == 2)
                                                {!! getSelectOptions($data['upazila_bn'], $user->upazila_id) !!}
                                            @endif
                                        </select>
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    @endrole

                                    @role('upazila_admin')
                                    <div class="col-sm-12 col-md-8">
                                        <div class="form-group">
                                            <label for="district_id"><span class="langBn">উপজেলার নাম</span>
                                                <span class="langEn">Upazila Name</span><span class="text-danger"> *
                                                </span>
                                            </label>

                                            <select class="custom-select mr-sm-2 cust-width" id="upazila_id" name="upazila_id"
                                                required>
                                                @if($langId == 1)
                                                @foreach($data['upazila_en'] as $key=>$upazila)
                                                "<option value="{{$key}}" selected>{!! $upazila !!}</option>"
                                                @endforeach
                                                @endif
                                                @if($langId == 2)
                                                @foreach($data['upazila_bn'] as $key=>$upazila)
                                                "<option value="{{$key}}" selected>{!! $upazila !!}</option>"
                                                @endforeach
                                                @endif
                                            </select>
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    @endrole

                                    @role('super_admin|division_admin|district_admin')
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="upazila_id"><span class="langBn">উপজেলার নাম</span>
                                                <span class="langEn">Upazila Name</span><span class="text-danger"> *
                                                </span>
                                            </label>
                                            <select class="custom-select my-1 mr-sm-2" id="upazila_id" name="upazila_id">
                                            @if($langId == 1)
                                                {!! getSelectOptions($data['upazila_en'], $user->upazila_id) !!}
                                                @else($langId == 2)
                                                {!! getSelectOptions($data['upazila_bn'], $user->upazila_id) !!}
                                            @endif
                                        </select>
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    @endrole

                                    {{--dd($user->toArray())--}}
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="contact_id"><span class="langBn">মোবাইল
                                                    নং</span><span class="langEn">Mobile No</span><span class="text-danger">
                                                    * </span>
                                            </label>

                                            <input type="text" class="form-control" id="contact_no" minlength="11"
                                                maxlength="11" name="contact_no"
                                                onfocusout="this.setAttribute('type','password');"
                                                onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                value="{{ $user->contact_no }}" required oncopy="return false"
                                                oncut="return false" onpaste="return false" autocomplete="off">
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="email"><span class="langBn">ই-মেইল</span><span
                                                    class="langEn">Email</span><span class="text-danger">
                                                    * </span>
                                            </label>

                                            <input type="email" class="form-control eng-input" id="email" name="email" value="{{ $user->email }}"
                                                data-inputmask-alias="email" data-val="true" data-val-required="Required"
                                                onfocusout="this.setAttribute('type','password');"
                                                onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                                                oncopy="return false" oncut="return false" onpaste="return false"
                                                autocomplete="off">
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="password"><span class="langBn">পাসওর্য়াড</span><span
                                                    class="langEn">Password</span><span class="text-danger">
                                                    * </span>
                                            </label>
                                            <!-- <input type="text" class="form-control" name="password" id="password" v-if="!auto_password" placeholder=""> -->

                                            <input type="text" class="form-control eng-input" id="password" name="password">
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-8">
                                            <label for="email"><span class="langBn">পূর্নরায় পাসওর্য়াড</span><span
                                                    class="langEn">Re-Password</span><span class="text-danger">
                                                    * </span>
                                            </label>
                                            <!-- <input type="text" class="form-control" name="password" id="password" v-if="!auto_password" placeholder=""> -->

                                            <input type="text" class="form-control eng-input" id="password_confirmation"
                                                name="password_confirmation" value="{{ old('password_confirmation') }}">
                                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                        </div>
                                        <!-- </div> -->
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="column">

                                        <label for="roles" class="label">
                                            <b>@if($langId == 1)
                                                {{"Roles"}}
                                                @else($langId == 2)
                                                {{"রোল"}}
                                                @endif</b>
                                        </label>
                                        @role('super_admin')

                                        @foreach ($roles as $role)

                                        @php

                                        //dd($role->id);
                                        $ch=$roleUsers->role_id;
                                         if($role->id == $ch)
                                         {
                                           $checked = "checked";
                                          }

                                         else{
                                             $checked="";
                                             }
                                             @endphp
                                        <div class="field">
                                            @if($langId == 1)
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" {{ $checked }} />
                                            {{$role['display_name_en']}}
                                            @else($langId == 2)
                                            {{-- {{$role['display_name_en']}} --}}
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" {{ $checked }} />
                                             {{$role['display_name_bn']}}
                                            @endif
                                        </div>
                                        @endforeach
                                        @endrole

                                        @role('division_admin')
                                        @php
                                        unset($roles[0]);
                                        unset($roles[1]);
                                        @endphp
                                        @foreach ($roles as $role)
                                        @php
                                        $divPermision=$divisionPermision->division_id;
                                          //dd($divPermision);
                                        if($role->id=$divPermision)
                                        {
                                            $checked = "checked";
                                        }
                                        else{
                                            $checked="";
                                        }
                                        @endphp
                                        <div class="field">
                                            @if($langId == 1)
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" {{ $checked }} />
                                            {{$role['display_name_en']}}
                                            @else($langId == 2)
                                            {{$role['display_name_en']}}
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" {{ $checked }} />
                                            @endif
                                        </div>
                                        @endforeach
                                        @endrole

                                        @role('district_admin')
                                        @php
                                        unset($roles[0]);
                                        unset($roles[1]);
                                        unset($roles[2]);
                                        @endphp
                                        @foreach ($roles as $role)
                                        <div class="field">
                                            @if($langId == 1)
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" />
                                            {{$role['display_name_en']}}
                                            @else($langId == 2)
                                            {{$role['display_name_en']}}
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" />
                                            @endif
                                        </div>
                                        @endforeach
                                        @endrole

                                        @role('upazila_admin')
                                        @php
                                        unset($roles[0]);
                                        unset($roles[1]);
                                        unset($roles[2]);
                                        unset($roles[3]);
                                        @endphp
                                        @foreach ($roles as $role)
                                        <div class="field">
                                            @if($langId == 1)
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" />
                                            {{$role['display_name_en']}}
                                            @else($langId == 2)
                                            {{$role['display_name_en']}}
                                            <input type="checkbox" id="inline" name="role[]" value="{{$role->id}}" />
                                            @endif
                                        </div>
                                        @endforeach
                                        @endrole
                                    </div>
                                </div>

                                @role('super_admin')
                                <div class="col-2">
                                    <button type="button" class="btn btn-info">
                                        <b>@if($langId == 1)
                                            {{"Division wise Permission"}}
                                            @else($langId == 2)
                                            {{"বিভাগ অনুযায়ী পারমিশন"}}
                                            @endif</b>
                                    </button>

                                    @if($langId == 1)
                                    <input type="checkbox" class="divisionCheckBox" id="selectAllDivision"  /> Select All
                                    @foreach ($data['division_en'] as $key => $division)
                                     @php

                                        $divPermision=$divisionPermision->division_id;

                                        if($key==$divPermision)
                                        {
                                            $checked = "checked";
                                        }
                                        else{
                                            $checked="";
                                        }

                                        @endphp
                                    <div class="field">
                                        <input type="checkbox" class="divisionCheckBox" name="division[]" value="{{$key}}"
                                            id="checkbox_division" {{ $checked }} /> {{$division}}
                                    </div>
                                    @endforeach
                                    @endif
                                    @if($langId == 2)
                                    <input type="checkbox" class="divisionCheckBox" id="selectAllDivision" /> সব সিলেক্ট
                                    @foreach ($data['division_bn'] as $key=>$division)
                                     @php

                                        $divPermision=$divisionPermision->division_id;

                                        if($key==$divPermision)
                                        {
                                            $checked = "checked";
                                        }
                                        else{
                                            $checked="";
                                        }

                                        @endphp
                                    <div class="field" id="district_show">
                                        <input type="checkbox" class="divisionCheckBox" name="division[]" value="{{$key}}"
                                            id="checkbox_division" {{ $checked }}/> {{$division}}
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                @endrole

                                @role('super_admin|division_admin')
                                <div class="col-2">
                                    <button type="button" class="btn btn-info" data-toggle="" data-target="#district">
                                        <b>@if($langId == 1)
                                            {{"District wise Permission"}}
                                            @else($langId == 2)
                                            {{"জেলা অনুযায়ী পারমিশন"}}
                                            @endif</b>
                                    </button>
                                    <div id="district" class="">
                                        @if($langId == 1)
                                        <!-- <input type="checkbox" id="selectAllDistrict" /> Select All -->
                                        <!-- @foreach ($data['district_en'] as $key=>$district) -->
                                        <div class="field" id="district_show">
                                            <!-- <input type="checkbox" class="districtCheckBox" name="district[]" value="{{$key}}"
                                                id="checkbox_district" /> -->
                                            <!-- {{$district}} -->
                                        </div>
                                        <!-- @endforeach -->
                                        @endif
                                        @if($langId == 2)
                                        <!-- <input type="checkbox" id="selectAllDistrict" /> সব সিলেক্ট -->
                                        <!-- @foreach ($data['district_bn'] as $key=>$district) -->
                                        <div class="field" id="district_show">
                                            <!-- <input type="checkbox" class="districtCheckBox" name="district[]" value="{{$key}}"
                                                id="checkbox_district" />  -->
                                            <!-- {{$district}} -->
                                        </div>
                                        <!-- @endforeach -->
                                        @endif
                                    </div>
                                </div>
                                @endrole

                                @role('super_admin|division_admin|district_admin')
                                <div class="col-2">
                                    <button type="button" class="btn btn-info" data-toggle="" data-target="#upazila">
                                        <b>@if($langId == 1)
                                            {{"Upazila wise Permission"}}
                                            @else($langId == 2)
                                            {{"উপজেলা অনুযায়ী পারমিশন"}}
                                            @endif</b>
                                    </button>

                                    <div id="upazila" class="">
                                        @if($langId == 1)
                                        {{-- {{ dd($data) }} --}}
                                        <!-- <input type="checkbox" id="selectAllUpazila" /> Select All -->
                                        <!-- @foreach ($data['upazila_en'] as $key=>$upazila) -->
                                        <div class="field">
                                            <!-- <input type="checkbox" class="upazilaCheckBox" name="upazila[]" value="{{$key}}" /> -->
                                            <!-- {{$upazila}} -->
                                        </div>
                                        <!-- @endforeach -->
                                        @endif
                                        @if($langId == 2)
                                        <!-- <input type="checkbox" id="selectAllUpazila" /> সব সিলেক্ট -->
                                        <!-- @foreach ($data['upazila_bn'] as $key=>$upazila) -->
                                        <div class="field">
                                            <!-- <input type="checkbox" class="upazilaCheckBox" name="upazila[]" value="{{$key}}" /> -->
                                            <!-- {{$upazila}} -->
                                        </div>
                                        <!-- @endforeach -->
                                        @endif
                                    </div>
                                </div>
                                @endrole
                                <div class="col-12 text-center crt-btn">
                                    <button type="submit" class="btn btn-outline-primary update-buttn">
                                        @if($langId == 1)
                                        {{"Update"}}
                                        @else($langId == 2)
                                        {{"আপডেট"}}
                                        @endif
                                    </button>

                                    <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                                        @if($langId == 1)
                                        {{"Back"}}
                                        @else($langId == 2)
                                        {{" পিছনে"}}
                                        @endif
                                    </a>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endsection
@section('extraJS')

<script>
// $('#inline').prop('checked', true);
// $('#checkbox_division').prop('checked', true);
// $('#district_show').prop('checked', true);

$(document).on('change', '#division_id', function() {
        var DivissionId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DivissionId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getDistrictInfos/' + DivissionId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                if(langId == 1){
                    var district = '<option value="" >Select District</option>';
                }else{
                    var district = '<option value="" >সিলেক্ট জেলা</option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });




$(document).on('change', '#district_id', function() {
        var DistrictId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DistrictId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getUpazliaInfo/' + DistrictId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);

                if(langId == 1){
                    var upazila = '<option value="" >Select Upazila</option>';
                }else{
                    var upazila = '<option value="" >সিলেক্ট উপজেলা</option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });





$(document).ready(function() {
    $('.divisionCheckBox').on('change',loadDistrict);
        alert('h');
        function loadDistrict() {
        var divisionId = [];
        var i = 1;
        $('.divisionCheckBox:checked').each(function() {
            divisionId[i++] = $(this).val();
        });

        $.ajax({
            type: "POST",
            url: "{{url('/getDistrictData')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                divisionId: divisionId
            },
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                var district = "";


                if(langId == 1){
                    district += '<input type="checkbox" class="districtMultiCheck" id="selectAllDistrict" />Select All';
                }else{
                    district += '<input type="checkbox" class="districtMultiCheck" id="selectAllDistrict" /> সব সিলেক্ট';
                }

               // var d=JSON.parse('getDistrictData');
                //alert(d);

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                        //district += '<option value="' + data[i].id + '">' + data[i].district_name_eng + '</option>';
                        district +=
                            "<div class='field'><input type='checkbox' name='district[]' class='districtMultiCheck' value='" +
                            data[i].id + "'/>" + data[i].district_name_eng + "<br>";
                    } else {
                        // district += '<option value="' + data[i].id + '">' + data[i].district_name_bng + '</option>';
                        district +=
                            "<div class='field'><input type='checkbox' name='district[]' class='districtMultiCheck' value='" +
                            data[i].id + "'/>" + data[i].district_name_bng + "<br>";
                    }
                    district += "</div>";
                }
                if ($(".divisionCheckBox:checked").prop("checked") == true) {
                    $("#district").html(district);
                } else {
                    $("#district").html('');
                }

                //alert(district);

            },
            error: function() {
                $("#district").html('');
                $("#upazila").html('');
                //alert('Data Not Found');
            }
        });

    }

      loadDistrict();
   
    $(document).on('change', '.districtMultiCheck', function() {

        var districtId = [];
        var i = 1;
        $('.districtMultiCheck:checked').each(function() {
            districtId[i++] = $(this).val();
        });

        $.ajax({
            type: "POST",
            url: "{{url('/getUpazilaData')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                districtId: districtId
            },
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                var upazila = "";

                if(langId == 1){
                    upazila += '<input type="checkbox" class="upazilaMultiCheck" id="selectAllupazila" /> Select All';
                }else{
                    upazila += '<input type="checkbox" class="upazilaMultiCheck" id="selectAllupazila" /> সব সিলেক্ট';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                        //district += '<option value="' + data[i].id + '">' + data[i].district_name_eng + '</option>';
                        upazila +=
                            "<div class='field'><input type='checkbox' name='upazila[]' class='upazilaMultiCheck' value='" +
                            data[i].id + "'/>" + data[i].upazila_name_eng + "<br>";
                    } else {
                        // district += '<option value="' + data[i].id + '">' + data[i].district_name_bng + '</option>';
                        upazila +=
                            "<div class='field'><input type='checkbox' name='upazila[]' class='upazilaMultiCheck' value='" +
                            data[i].id + "'/>" + data[i].upazila_name_bng + "<br>";
                    }
                    upazila += "</div>";
                }
                if ($(".districtMultiCheck:checked").prop("checked") == true) {
                    $("#upazila").html(upazila);
                } else {
                    $("#upazila").html('');
                }

                //alert(district);

            },
            error: function() {
                $("#upazila").html('');
                //alert('Data Not Found');
            }
        });

    });
});



function nodeToString(node) {
    var tmpNode = document.createElement("div");
    tmpNode.appendChild(node.cloneNode(true));
    var str = tmpNode.innerHTML;
    tmpNode = node = null; // prevent memory leaks in IE
    return str;
}



$(document).ready(function() {
    $(document).on('click', '#selectAllDivision', function() {
        if (this.checked) {
            $('.divisionCheckBox:input:checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $('.divisionCheckBox:input:checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    $(document).on('click', '#selectAllDistrict', function() {
        if (this.checked) {
            $('.districtMultiCheck:input:checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $('.districtMultiCheck:input:checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    $(document).on('click', '#selectAllupazila', function() {
        if (this.checked) {
            $('.upazilaMultiCheck:input:checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $('.upazilaMultiCheck:input:checkbox').each(function() {
                this.checked = false;
            });
        }
    });
    //console.log('upazilaId : ' + upazilaId);
    ajaxCallCB(url, data, function(response) {
        //console.log(response);
        if (langId == 1) {
            var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                response.data.division_name_eng : '';
            var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                response.data.district_name_eng : '';
        } else {
            var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                response.data.division_name_bng : '';
            var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                response.data.district_name_bng : '';
        }
        // $('#division_title').html('<strong>' + division_name + '</strong>');
        // $('#district_title').html('<strong>' + district_name + '</strong>');
        $('#division').val(division_name);
        $('#district').val(district_name);
        //console.log('response : ' + response);
    });
    $('body').on('keydown', '#email', function() {
        if (e.keyCode === 9) {
            var email = $('#email').val();
            email = email.trim();
            // if (email == "") {
            //     e.preventDefault();
            // }
            var re =
                /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    });
    $('#contact_no, #contact_no_confirmation').on('keyup', function() {
        var contact_no = $(this).val();
        if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
            $(this).val('');
        }
        if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
            $(this).val('');
        }
        if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                contact_no[2] == 2)) {
            $(this).val('');
        }
    });
    // New JS By Asad
    var address_check = $('#address_check');
    $(address_check).prop('disabled', false);
    $('.click').click(function() {
        if ($(
                "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
            )
            .prop('disabled')) {
            $(
                    "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                )
                .prop('disabled', false);
        } else {
            $(
                    "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                )
                .prop('disabled', true);
            $('#permanent_division').val('');
            $('#permanent_district').val('');
            $('#permanent_upazila_id').val('');
            $('#permanent_union_id').val('');
            $('#permanent_village_id').val('');
            $('#permanent_post_code_id').val('');
        }
    });
});

function password_set_attribute() {}
$(document).ready(function() {
    $(":input[data-inputmask-mask]").inputmask();
    $(":input[data-inputmask-alias]").inputmask();
    $(":input[data-inputmask-regex]").inputmask("Regex");
});
$(document).ready(function() {
    if (langId == 1) {
        $('.langEn').show();
        $('.langBn').hide();
        $('.BngBtn').hide();
        $('.EngBtn').show();
    } else {
        $('.langBn').show();
        $('.langEn').hide();
        $('.EngBtn').hide();
        $('.BngBtn').show();
    }
    if (langId == 2) {
        $('#name').addClass('bng-input');
        $('#name').removeAttr('oninput');
        $('#father_name').addClass('bng-input');
        $('#father_name').removeAttr('oninput');
        $('#mother_name').addClass('bng-input');
        $('#mother_name').removeAttr('oninput');
        $('#contact_no').addClass('eng-input');
        //$('#contact_no').removeAttr('oninput');
        $('#contact_no_confirmation').addClass('eng-input');
        //$('#contact_no_confirmation').removeAttr('oninput');
        $('#nationality').addClass('bng-input');
        $('#national_id').addClass('eng-input');
        //$('#national_id').removeAttr('oninput');
        $('#national_id_confirmation').addClass('eng-input');
        //$('#national_id_confirmation').removeAttr('oninput');
        $('#training_subject_description').addClass('bng-input');
        $('#it_knowledge').addClass('bng-input');
        $('#communication_skill').addClass('bng-input');
        $('#present_village_id').addClass('bng-input');
        $('#permanent_village_id').addClass('bng-input');
        $('#present_post_code_id').addClass('bng-input');
        $('#permanent_post_code_id').addClass('bng-input');
    }
});
</script>
<script>
// var app = new Vue({
//     el: '#app',
//     data: {
//         auto_password: true,
//         rolesSelected: [{
//             !!old('roles') ? old('roles') : ''!!
//         }],
//         divisionSelected: [{
//             !!old('division_sp_id') ? old('division_sp_id') : ''!!
//         }],
//         districtSelected: [{
//             !!old('district_sp_id') ? old('district_sp_id') : ''!!
//         }],
//         upazilaSelected: [{
//             !!old('upazila_sp_id') ? old('upazila_sp_id') : ''!!
//         }]
//     }
// });

function submit_forms() {
    var x = document.querySelectorAll(".multiForms");
    for (let i = 0; i < x.length; i++) {
        x[i].submit();
    }
}
</script>
@endsection
