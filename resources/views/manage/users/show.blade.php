@extends('layouts.app')
@section('content')


<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">
                <div class="row">
                    <div class="col-6">
                        <h3 class="user-management">
                            @if($langId == 1)
                            {{" View User Details"}}
                            @else($langId == 2)
                            {{"ইউজারের সম্পূর্ণ ডাটা দেখুন"}}
                            @endif
                        </h3>

                    </div>
                    <div class="col-6">
                        <a href="{{route('users.edit', $user->id)}}" class="creat-usrbtn float-right btn mt-0" type="button">
                            @if($langId == 1)
                            {{"Edit User"}}
                            @else($langId == 2)
                            {{"ইডিট ইউজার"}}
                            @endif
                            <span><i class="fas fa-user-plus"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="user-section mt-0">
                <div class="row">
                    <div class="col-6">

                        <div class="table-responsive">
                            <table class="table table-bordered report-table table-hover">
                                <tbody>                                    
                                    <tr>
                                        <th class="user-thead">
                                            @if($langId == 1)
                                            {{"Name"}}
                                            @else($langId == 2)
                                            {{"নাম"}}
                                            @endif
                                        </th>
                                        <td>{{$user->name}}</td>
                                        {{-- dd($data) --}}
                                    </tr>
                                    @if(isset($data['division_en'][$user->division_id]) && !empty($data['division_en'][$user->division_id]))
                                    <tr>                                        
                                        <th class="user-thead">
                                            @if($langId == 1)
                                            {{"Division Name"}}
                                            @else($langId == 2)
                                            {{"বিভাগ নাম"}}                                            
                                        </th>
                                        @endif
                                        <td>                                            
                                            @if($langId == 1)
                                            {{$data['division_en'][$user->division_id]}}
                                            @else($langId == 2)
                                            {{$data['division_bn'][$user->division_id]}}
                                            @endif                                            
                                        </td>
                                    </tr>
                                    @endif
                                    @if(isset($data['district_en'][$user->district_id]) && !empty($data['district_en'][$user->district_id]))
                                    <tr>
                                        <th class="user-thead">
                                            @if($langId == 1)
                                            {{"District Name"}}
                                            @else($langId == 2)
                                            {{"জেলার নাম"}}
                                            @endif
                                        </th>
                                        <td>
                                            @if($langId == 1)
                                            {{$data['district_en'][$user->district_id]}}
                                            @else($langId == 2)
                                            {{$data['district_bn'][$user->district_id]}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if(isset($data['upazila_en'][$user->upazila_id]) && !empty($data['upazila_en'][$user->upazila_id]))
                                    <tr>
                                        <th class="user-thead">
                                            @if($langId == 1)
                                            {{" Upazila Name"}}
                                            @else($langId == 2)
                                            {{"উপজেলার নাম"}}
                                            @endif
                                        </th>
                                        <td>
                                            @if($langId == 1)
                                            {{$data['upazila_en'][$user->upazila_id]}}
                                            @else($langId == 2)
                                            {{$data['upazila_bn'][$user->upazila_id]}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th class="user-thead">
                                            @if($langId == 1)
                                            {{"Email"}}
                                            @else($langId == 2)
                                            {{"ই-মেইল"}}
                                            @endif
                                        </th>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th class="user-thead">
                                            @if($langId == 1)
                                            {{"Mobile No"}}
                                            @else($langId == 2)
                                            {{"মোবাইল নং"}}
                                            @endif
                                        </th>
                                        <td>{{$user->contact_no}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-6">
                        <h3 class="user-management text-center">
                            @if($langId == 1)
                            {{" Role"}}
                            @else($langId == 2)
                            {{"রোল"}}
                            @endif
                        </h3>
                        <div class="text-center">
                            @forelse ($user->roles as $role)
                            <p class="usr-roles">
                                @if($langId == 1)
                                {{$role->display_name_en}}
                                @else($langId == 2)
                                {{$role->display_name_bn}}
                                @endif  
                            </p>
                            @empty
                            <p class="usr-roles">
                                   @if($langId == 1)
                                    {{" This user has not been assigned any roles yet"}}
                                    @else($langId == 2)
                                    {{"কোন রোল সিলেক্টেড নেই"}}
                                    @endif
                           </p>
                            @endforelse
                        </div>
                    </div>
                    <div class="col-12">
                        <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                                @if($langId == 1)
                                {{"Back"}}
                                @else($langId == 2)
                                {{" পিছনে"}}
                                @endif
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection