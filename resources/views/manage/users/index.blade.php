@extends('layouts.app')
@section('content')
@if(isset($users) && !empty($users) && isset($data) && !empty($data))

<div class="graph-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <div class="row">
                        <div class="col-4">
                            <h3 class="user-management">
                                @if($langId == 1)
                                {{" Manage User"}}
                                @else($langId == 2)
                                {{" নতুন ইউজার তৈরী"}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-4">
                            <div class="report-search">
                                @if($langId == 1)
                                <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                                @else($langId == 2)
                                <input class="form-control" type="text" placeholder="সার্চ করুন" aria-label="Search">
                                @endif
                                
                                <i class="fas fa-search" aria-hidden="true"></i>
                            </div>
                        </div>

                        <div class="col-4">
                            <a href="{{ route('users.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">

                                <span><i class="fas fa-user-plus"> </i> </span>
                                @if($langId == 1)
                                {{"User Create"}}
                                @else($langId == 2)
                                {{"নতুন ইউজার তৈরী"}}
                                @endif
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <div class="table-responsive">
                        <table class="table table-bordered report-table table-hover">
                            <thead>
                                <tr>
                                    <th class="user-thead">#</th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"User Name(English)"}}
                                        @else($langId == 2)
                                        {{"ইউজার নাম(ইংরেজী)"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"User Name(Bangla)"}}
                                        @else($langId == 2)
                                        {{"ইউজার নাম(বাংলা)"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"user Role"}}
                                        @else($langId == 2)
                                        {{"ইউজার রোল"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Division"}}
                                        @else($langId == 2)
                                        {{"বিভাগ"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"District"}}
                                        @else($langId == 2)
                                        {{"জেলা"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Email"}}
                                        @else($langId == 2)
                                        {{"ইমেইল"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Date Created"}}
                                        @else($langId == 2)
                                        {{"ডাটা যোগ"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Action"}}
                                        @else($langId == 2)
                                        {{"এ্যাকশন"}}
                                        @endif
                                    </th>

                                </tr>
                            </thead>

                            <tbody>
                                @if(isset($users) && !empty($users))
                                @foreach ($users as $user)
                                <tr>
                                    <td class="user-data">{{$user->id}}</td>
                                    <td class="user-data">{{$user->name_en}}</td>
                                    <td class="user-data">{{$user->name_bn}}</td>
                                    <td class="user-data">{{"ইউজার রোল"}}</td>
                                    <td class="user-data">
                                        @if(!empty($user['division_id']))
                                        @if($langId == 1)
                                        {{ $data['division_en'][$user->division_id] }}
                                        @else($langId == 2)
                                        {{ $data['division_bn'][$user->division_id] }}
                                        @endif
                                        @else
                                        {{ "N/A" }}
                                        @endif
                                    </td>
                                    <td class="user-data">
                                        @if(!empty($user['district_id']))
                                        @if($langId == 1)
                                        {{ $data['district_en'][$user->district_id] }}
                                        @else($langId == 2)
                                        {{ $data['district_bn'][$user->district_id] }}
                                        @endif
                                        @else
                                        {{ "N/A" }}
                                        @endif

                                    </td>
                                    <td class="user-data">{{$user->email}}</td>
                                    <td class="user-data">{{$user->created_at->toFormattedDateString()}}</td>
                                    <td class="has-text-right usr-edt-icon">
                                        <a class="btn is-outlined m-r-5 btn-outline-success cust-usr-btn" title="View" type="button" href="{{route('users.show', $user->id)}}">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" title="Edit" href="{{route('users.edit', $user->id)}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>
                                        <a class="btn is-light btn-outline-danger cust-usr-btn" type="button" href="#" title="Delete" data-toggle="modal" data-target="#exampleModalCenter">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>

                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title confirm" id="exampleModalLongTitle">
                                                            Confirmation Message
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p class="conf-msg">Are You Sure to Delete This Data?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" href="">Yes</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                  <div class="pagignation-sector">
                                <div aria-label="Page navigation example">
                                    <ul class="pagination my-3 justify-content-center">
                                        <li class="page-item custom-item"><a class="page-link" href="#">Prev</a>
                                        </li>
                                        <li class="page-item custom-item active"><a class="page-link" href="#">1</a>
                                        </li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">6</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                </div>

            </div>
        </div>

    </div>
</div>


@endif
@endsection

@section('extraJS')

<script>
    function searchToggle(obj, evt) {
        var container = $(obj).closest('.search-wrapper');
        if (!container.hasClass('active')) {
            container.addClass('active');
            evt.preventDefault();
        } else if (container.hasClass('active') && $(obj).closest('.input-holder').length == 0) {
            container.removeClass('active');
            // clear input
            container.find('.search-input').val('');
        }
    }
</script>

@endsection