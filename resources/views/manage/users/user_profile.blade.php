@extends('layouts.app')
@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">
                                @if($langId == 1)
                                {{" User Profile"}}
                                @else($langId == 2)
                                {{"ইউজার প্রোফাইল"}}
                                @endif
                            </h3>
                        </div>

                        <div class="col-6">
                            <a href="{{route('user.profile.edit', $user->id)}}" class="creat-usrbtn float-right btn mt-0" type="button">

                                <span><i class="fas fa-user-plus"> </i> </span>
                                @if($langId == 1)
                                {{" Edit Profile"}}
                                @else($langId == 2)
                                {{"প্রোফাইল ইডিট "}}
                                @endif
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <div class="row">
                        <div class="col-8">
                            <div class="section-profile-data">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="data-section">
                                            <p class="user-desig">
                                                @if($langId == 1)
                                                <b>{{"User Role:"}}</b>
                                                @else($langId == 2)
                                                <b>{{"ইউজার রোল:"}}</b>
                                                @endif
                                            </p>

                                            <p class="user-desig">
                                                @if($langId == 1)
                                                <b>{{"User Email:"}}</b>
                                                @else($langId == 2)
                                                <b>{{"ইউজার নাম:"}}</b>
                                                @endif
                                            </p>

                                


                                            <p class="user-desig">
                                                @if($langId == 1)
                                                <b>{{"Contact No:"}}</b>
                                                @else($langId == 2)
                                                <b>{{"যোগাযোগের নম্বর:"}}</b>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="data-descript">
                                            <p class="user-desig">
                                                Super Admin
                                            </p>

                                            <p class="user-desig">

                                                @if(isset($user['name']) && !empty($user['name']))
                                                {{$user['name']}}
                                                @endif
                                            </p>

                                            <p class="user-desig">

                                                @if(isset($user['email']) && !empty($user['email']))
                                                {{$user['email']}}
                                                @endif
                                            </p>
                                            <p class="user-desig">
                                                @if(isset($user['contact_no']) && !empty($user['contact_no']))
                                                {{$user['contact_no']}}
                                                @endif
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="section-profile-img">
                                <img src="{{asset('img/profile.jpg')}}" alt="profile pic">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management text-center">
                                @if($langId == 1)
                                {{" Address:"}}
                                @else($langId == 2)
                                {{" ঠিকানা:"}}
                                @endif
                            </h3>
                            <hr>
                            <div class="section-profile-data">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="data-section">
                                            <p class="user-desig">
                                                @if($langId == 1)
                                                <b>{{"Upazila Name:"}}</b>
                                                @endif

                                                @if($langId == 2)
                                                <b>{{"উপজেলার নাম:"}}</b>
                                                @endif
                                            </p>

                                            <p class="user-desig">
                                                @if($langId == 1)
                                                <b>{{"Zila Name:"}}</b>
                                                @endif

                                                @if($langId == 2)
                                                <b>{{"জেলার নাম:"}}</b>
                                                @endif
                                            </p>

                                            <p class="user-desig">
                                                @if($langId == 1)
                                                <b>{{"Division Name:"}}</b>
                                                @endif

                                                @if($langId == 2)
                                                <b>{{"বিভাগের নাম:"}}</b>
                                                @endif
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="data-descript">
                                            <p class="user-desig">
                                                @if($langId == 1)

                                                @if(isset($user['upazila_id']) && !empty($user['upazila_id']))
                                                {{$data['upazilas_en'][$user['upazila_id']]}}
                                                @endif

                                                @else($langId == 2)

                                                @if(isset($user['upazila_id']) && !empty($user['upazila_id']))
                                                {{$data['upazilas_bn'][$user['upazila_id']]}}
                                                @endif
                                                @endif
                                            </p>

                                            <p class="user-desig">
                                                @if($langId == 1)

                                                @if(isset($user['district_id']) && !empty($user['district_id']))
                                                {{$data['district_en'][$user['district_id']]}}
                                                @endif
                                                @else($langId == 2)

                                                @if(isset($user['district_id']) && !empty($user['district_id']))
                                                {{$data['district_bn'][$user['district_id']]}}
                                                @endif
                                                @endif
                                            </p>

                                            <p class="user-desig">
                                                @if($langId == 1)

                                                @if(isset($user['division_id']) && !empty($user['division_id']))
                                                {{$data['division_en'][$user['division_id']]}}
                                                @endif
                                                @else($langId == 2)

                                                @if(isset($user['division_id']) && !empty($user['division_id']))
                                                {{$data['division_bn'][$user['division_id']]}}
                                                @endif
                                                @endif
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>



@endsection