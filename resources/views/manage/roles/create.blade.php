@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">
                <div class="row">
                    <div class="col-12">
                        <h3 class="user-management">
                            @if($langId == 1)
                            {{"Create New Role"}}
                            @else($langId == 2)
                            {{"নতুন রোল তৈরী করুন"}}
                            @endif
                        </h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="user-section form-section">
             <form action="{{route('roles.store')}}" method="POST">
                    {{ csrf_field() }}
                <div class="row">
                    <div class="col-4">
                        <h5 class="multi-role">
                            @if($langId == 1)
                            {{" Role Details:"}}
                            @else($langId == 2)
                            {{" রোলের বিস্তারিত"}}
                            @endif
                            </h5>
                        <hr>

                            <div class="form-group">
                                <label for="display_name" class="label">
                                    @if($langId == 1)
                                    {{"Name (English)"}}
                                    @else($langId == 2)
                                    {{"নাম (ইংরেজী)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="display_name_en" value="{{old('display_name')}}" id="display_name">

                            </div>

                            <div class="form-group">
                                <label for="display_name" class="label">
                                    @if($langId == 1)
                                    {{"Name (Bangla)"}}
                                    @else($langId == 2)
                                    {{"নাম (বাংলা)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="display_name_bn" value="{{old('display_name')}}" id="display_name">

                            </div>

                            <div class="form-group">
                                <label for="name" class="label">
                                    @if($langId == 1)
                                    {{"Slug (Can not be changed)"}}
                                    @else($langId == 2)
                                    {{"স্লাক (পরিবর্তন করা যাবে না)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" id="name">

                            </div>
                            <input type="hidden" name="create_by" value="{{ $user->id }}">                      
                            <input type="hidden" :value="permissionsSelected" name="permissions">                      
                    </div>

                    <div class="col-4">
                        <h5 class="multi-role">
                            @if($langId == 1)
                            {{"Permissions for Role"}}
                            @else($langId == 2)
                            {{"রোলের পারমিশন"}}
                            @endif
                        </h5>
                        <hr>

                        <b-checkbox-group>
                            @foreach ($permissions as $permission)
                            <div class="field">
                                <b-checkbox v-model="permissionsSelected" :native-value="{{$permission->id}}">
                                    @if($langId == 1)
                                    {{$permission->display_name_en}}
                                     @else($langId == 2)
                                    {{$permission->display_name_bn}}
                                    @endif
                                </b-checkbox>
                            </div>
                            @endforeach
                          </b-checkbox-group>
                    </div>

                    <div class="col-12 text-center crt-btn">
                                    <button type="submit" class="btn btn-outline-primary update-buttn">
                                        @if($langId == 1)
                                        {{"Submit"}}
                                        @else($langId == 2)
                                        {{"সাবমিট"}}
                                        @endif
                                    </button>

                                    <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                                        @if($langId == 1)
                                        {{"Back"}}
                                        @else($langId == 2)
                                        {{" পিছনে"}}
                                        @endif
                                    </a>
                                </div>
                    
                </div>
                </form>
            </div>
        </div>

    </div>

</div>

@endsection


@section('scripts')
<script>
    var app = new Vue({
        el: '#app',
        data: {
            permissionsSelected: []
        }
    });
    var app = new Vue({
        el: '#app',
        data: {
            divisionPermissionsSelected: []
        }
    });
</script>
@endsection