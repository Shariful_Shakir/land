@extends('layouts.app')
@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <div class="row">
                        <div class="col-8">
                        <h3 class="user-management">
                        @if($langId == 1)
                        {{"Manage Roles"}}
                        @else($langId == 2)
                        {{"ম্যানেজ রোল"}}
                        @endif
                    </h3>
                    </div>

                     <div class="col-4">
                <a href="{{ route('roles.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">
                    <span><i class="fas fa-user-plus"> </i> </span>
                    @if($langId == 1)
                    {{"New Role"}}
                    @else($langId == 2)
                    {{" নতুন রোল"}}
                    @endif
                </a>
            </div>
            
                    </div>
                    
           
                </div>

            </div>
            
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <div class="table-responsive">
                        <table class="table table-bordered report-table table-hover">
                            <thead>
                                <tr>

                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Name of Role"}}
                                        @else($langId == 2)
                                        {{"রোলের নাম"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Created By"}}
                                        @else($langId == 2)
                                        {{"প্রস্তুতকারক"}}
                                        @endif
                                    </th>
                                    <th class="user-thead">
                                        @if($langId == 1)
                                        {{"Action"}}
                                        @else($langId == 2)
                                        {{"কার্যক্রম"}}
                                        @endif
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @role('super_admin')
                                @foreach ($roles as $role)
                                <tr>
                                    <td>
                                        @if($langId == 1)
                                        {{$role->display_name_en}}
                                        @else($langId == 2)
                                        {{$role->display_name_bn}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($langId == 1 && !empty($role->create_by))
                                        {{$allUser['name_en'][$role->create_by]}}
                                        @elseif($langId == 2 && !empty($role->create_by))
                                        {{$allUser['name_bn'][$role->create_by]}}
                                        @endif
                                    </td>
                                    <td class="usr-edt-icon">
                                        <a class="btn is-outlined m-r-5 btn-outline-success cust-usr-btn" title="View" type="button" href="{{route('roles.show', $role->id)}}">
                                            <i class="fas fa-eye"></i>
                                        </a>

                                        <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" href="{{route('roles.edit', $role->id)}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            @endrole


                            @php
                                unset($roles[0]);
                                unset($roles[1]);
                                unset($roles[2]);
                                unset($roles[3]);
                                unset($roles[4]);
                            @endphp
                            {{-- dd($roles->toArray()) --}}
                            
                            @foreach ($roles as $role)
                                @if($user->id == $role->create_by)
                                <tr>
                                    <td>
                                        @if($langId == 1)
                                        {{$role->display_name_en}}
                                        @else($langId == 2)
                                        {{$role->display_name_bn}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($langId == 1 && !empty($role->create_by))
                                        {{$allUser['name_en'][$role->create_by]}}
                                        @elseif($langId == 2 && !empty($role->create_by))
                                        {{$allUser['name_bn'][$role->create_by]}}
                                        @endif
                                    </td> 
                                    <td class="usr-edt-icon">
                                        <a class="btn is-outlined m-r-5 btn-outline-success cust-usr-btn" title="View" type="button" href="{{route('roles.show', $role->id)}}">
                                            <i class="fas fa-eye"></i>
                                        </a>

                                        <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" href="{{route('roles.edit', $role->id)}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endif
                                @endforeach                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection