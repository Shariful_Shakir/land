@extends('layouts.app')
@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">
                <div class="row">
                    <div class="col-12">
                        <h3 class="user-management">
                          @if($langId == 1)
                          {!! $role->display_name_en !!}
                          @else($langId == 2)
                          {!! $role->display_name_bn !!}
                          @endif
                        </h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="user-section form-section">
             <form action="{{route('roles.update', $role->id)}}" method="POST">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
                <div class="row">
                    <div class="col-4">
                        <h5 class="multi-role">Role Details:</h5>
                        <hr>

                            <div class="form-group">
                                <label for="display_name" class="label">Name (English)</label>
                                <input type="text" class="form-control" name="display_name_en" value="{{$role->display_name_en}}" id="display_name_en">
                            </div>

                            <div class="form-group">
                                <label for="display_name" class="label">Name (Bangla)</label>
                                <input type="text" class="form-control" name="display_name_bn" value="{{$role->display_name_bn}}" id="display_name_bn">
                            </div>

                            <div class="form-group">
                                <label for="name" class="label">Slug (Can not be changed)</label>
                                <input type="text" class="form-control" name="name" value="{{$role->name}}" id="name" disabled>
                            </div>

                            <input type="hidden" :value="permissionsSelected" name="permissions">                      
                    </div>

                    <div class="col-4">
                        <h5 class="multi-role">
                          @if($langId == 1)
                            {{"Permissions:"}}
                            @else($langId == 2)
                            {{"অনুমতি সমূহ"}}
                            @endif
                        </h5>
                        <hr>

                        <b-checkbox-group>
                            @foreach ($permissions as $permission)
                            <div class="field">
                                <b-checkbox v-model="permissionsSelected" :native-value="{{$permission->id}}">
                                    @if($langId == 1)
                                        {{$permission->display_name_en}}
                                    @else($langId == 2)
                                        {{$permission->display_name_bn}}
                                    @endif                                    
                                </b-checkbox>
                            </div>
                            @endforeach
                          </b-checkbox-group>
                    </div>

                    <div class="col-12 crt-btn text-center">
                        <button type="submit" class="btn crt-userbtn">
                         @if($langId == 1)
                            {{"Update"}}
                            @else($langId == 2)
                            {{"আপডেট"}}
                            @endif
                        </button>

                        <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                            @if($langId == 1)
                            {{"Back"}}
                            @else($langId == 2)
                            {{" পিছনে"}}
                            @endif
                        </a>
                    </div>
                    
                </div>
                </form>
            </div>
        </div>

    </div>

</div>

@endsection


@section('scripts')
  <script>

  var app = new Vue({
    el: '#app',
    data: {
      permissionsSelected: {!!$role->permissions->pluck('id')!!}
    }
  });

  </script>
@endsection
