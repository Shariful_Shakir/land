@extends('layouts.app')
@section('content')

<div class="graph-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">
                                @if($langId == 1)
                                {!! $role->display_name_en !!}
                                @else($langId == 2)
                                {!! $role->display_name_bn !!}
                                @endif
                            </h3>
                        </div>

                        <div class="col-6">
                            <a href="{{route('roles.edit', $role->id)}}" class="creat-usrbtn float-right btn mt-0" type="button">
                                <span><i class="fas fa-user-plus"> </i> </span>
                                @if($langId == 1)
                                {{" Edit this Role"}}
                                @else($langId == 2)
                                {{" রোল ইডিট করুন"}}
                                @endif
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">

                    <div class="container">

                <div class="row">
                    <div class="col-4">
                        <h5 class="multi-role">
                            @if($langId == 1)
                            {{"Role Details:"}}
                            @else($langId == 2)
                            {{"রোল ডিটেলস"}}
                            @endif
                        </h5>
                        <hr>
                            <div class="form-group">
                                <label for="display_name" class="label">
                                    @if($langId == 1)
                                    {{"Name"}}
                                    @else($langId == 2)
                                    {{"নাম"}}
                                    @endif
                                </label> : 
                                @if($langId == 1)
                                {{$role->display_name_en}}
                                @else($langId == 2)
                                {{$role->display_name_bn}}
                                @endif
                                
                            </div>

                            <div class="form-group">
                                <label for="name" class="label">
                                    @if($langId == 1)
                                    {{"Slug (Can not be changed)"}}
                                    @else($langId == 2)
                                    {{"স্লাক (পরিবর্তন করা যাবে না)"}}
                                    @endif
                                </label> :
                                {{$role->name}}
                            </div>

                                                  
                    </div>

                    <div class="col-4">
                        <h5 class="multi-role">
                          @if($langId == 1)
                            {{"Permissions:"}}
                            @else($langId == 2)
                            {{"অনুমতি সমূহ"}}
                            @endif
                        </h5>
                        <hr>

                        <b-checkbox-group>
                            @foreach ($role->permissions as $r)
                                    <li>{{$r->display_name_en}} </li>
                                    @endforeach
                          </b-checkbox-group>


                    </div>

                    <div class="col-12 crt-btn text-center">
                        <button type="submit" class="btn crt-userbtn">
                         @if($langId == 1)
                            {{"Update"}}
                            @else($langId == 2)
                            {{"আপডেট"}}
                            @endif
                        </button>

                        <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                            @if($langId == 1)
                            {{"Back"}}
                            @else($langId == 2)
                            {{" পিছনে"}}
                            @endif
                        </a>
                    </div>
                    
                </div>





                </div>
            </div>
        </div>
    </div>
</div>

@endsection