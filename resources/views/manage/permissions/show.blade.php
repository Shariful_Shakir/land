@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">
                <div class="row">
                    <div class="col-6">
                        <h3 class="user-management">View Permission Details</h3>
                    </div>
                    <div class="col-6">
                        <a href="{{route('permissions.edit', $permission->id)}}" class="creat-usrbtn float-right btn" type="button"> Edit Permission
                    <span><i class="fas fa-user-plus"></i></span>
                  </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="flex-container">
    <div class="columns m-t-10">
        <div class="column">
            <h1 class="title">View Permission Details</h1>
        </div> <!-- end of column -->

        <div class="column">
            <a href="{{route('permissions.edit', $permission->id)}}" class="button is-primary is-pulled-right"><i class="fa fa-edit m-r-10"></i> Edit Permission</a>
        </div>
    </div>
    <hr class="m-t-0">

    <div class="columns">
        <div class="column">
            <div class="box">
                <article class="media">
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>{{$permission->display_name}}</strong> <small>{{$permission->name}}</small>
                                <br>
                                {{$permission->description}}
                            </p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
@endsection