@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">

            <div class="user-section">
                <div class="row">
                    <div class="col-6">
                        <h3 class="user-management">Manage Permissions</h3>
                    </div>
                    <div class="col-6">
                        <a href="{{route('permissions.create')}}" class="creat-usrbtn float-right btn" type="button"> Create
                            <span><i class="fas fa-user-plus"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="card">
                <div class="card-content">
                    <table class="table is-narrow cust-user-tble table-bordered">
                        <thead>
                            <tr>
                                <th>Name(English)</th>
                                <th>Name(Bangla)</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($permissions as $permission)
                            <tr>
                                <th>{{$permission->display_name_en}}</th>
                                <th>{{$permission->display_name_bn}}</th>
                                <td>{{$permission->name}}</td>
                                <td class="has-text-right usr-edt-icon">
                                    <a class="btn is-outlined m-r-5 btn-outline-success cust-usr-btn" type="button" href="{{route('permissions.show', $permission->id)}}">
                                        <i class="fas fa-eye"></i>
                                    </a>

                                    <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" href="{{route('permissions.edit', $permission->id)}}">
                                        <i class="fas fa-user-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>


</div>
@endsection