@extends('layouts.app')

@section('content')
<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">
                <div class="row">
                    <div class="col-6">
                        <h3 class="user-management">View Permission Details</h3>
                    </div>
                       <div class="col-6">
                        <a href="{{route('permissions.edit', $permission->id)}}" class="creat-usrbtn float-right btn" type="button"> Edit Permission
                            <span><i class="fas fa-user-plus"></i></span>
                        </a>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row">
                    <div class="user-section form-section">
                        <form action="{{route('permissions.update', $permission->id)}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('PUT')}}

                            <div class="row">
                                <div class="col-6">

                                     <div class="form-group">
                                       <label for="display_name" class="label">Name (English)</label>
                                        <input type="text" class="form-control" name="display_name_en" id="display_name" value="{{$permission->display_name_en}}">
                                    </div>
                                    <div class="form-group">
                                       <label for="display_name" class="label">Name (Bangla)</label>
                                        <input type="text" class="form-control" name="display_name_bn" id="display_name" value="{{$permission->display_name_bn}}">
                                    </div>
                                   
                                </div>
                                <div class="col-6">
                                 <div class="form-group">
                                        <label for="name" class="label">Slug </label>
                                        <input type="text" class="form-control" name="name" id="name" value="{{$permission->name}}">
                                    </div>
                                     
                                </div>
                               <div class="col-12 text-left">
                                    <button type="submit" class="btn crt-userbtn mt-3">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    </div>
</div>

<!-- <div class="flex-container">
    <div class="columns m-t-10">
        <div class="column">
            <h1 class="title">View Permission Details</h1>
        </div> 

        <div class="column">
            <a href="{{route('permissions.edit', $permission->id)}}" class="button is-primary is-pulled-right"><i class="fa fa-edit m-r-10"></i> Edit Permission</a>
        </div>
    </div>
    <hr class="m-t-0">

    <form action="{{route('permissions.update', $permission->id)}}" method="POST">
        {{csrf_field()}}
        {{method_field('PUT')}}

        <div class="field">
            <label for="display_name" class="label">Name (Display Name)</label>
            <p class="control">
                <input type="text" class="input" name="display_name" id="display_name" value="{{$permission->display_name}}">
            </p>
        </div>

        <div class="field">
            <label for="name" class="label">Slug <small>(Cannot be changed)</small></label>
            <p class="control">
                <input type="text" class="input" name="name" id="name" value="{{$permission->name}}" disabled>
            </p>
        </div>

        <div class="field">
            <label for="description" class="label">Description</label>
            <p class="control">
                <input type="text" class="input" name="description" id="description" placeholder="Describe what this permission does" value="{{$permission->description}}">
            </p>
        </div>

        <button class="button is-primary">Save Changes</button>
    </form>
</div> -->
@endsection