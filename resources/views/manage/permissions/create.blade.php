@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">
                <div class="row">
                    <div class="col-12">
                        <h3 class="user-management">Create New Permission</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="user-section form-section">

                <form action="{{route('permissions.store')}}" method="POST">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-6">

                            <div class="form-group" v-if="permissionType == 'basic'">
                                <label for="display_name" class="label">Name (English)</label>
                                <input type="text" class="form-control" name="display_name_en" id="display_name">
                            </div>

                            <div class="form-group" v-if="permissionType == 'basic'">
                                <label for="display_name" class="label">Name (Bangla)</label>
                                <input type="text" class="form-control" name="display_name_bn" id="display_name">
                            </div>

                            <div class="form-group" v-if="permissionType == 'basic'">
                                <label for="name" class="label">Slug (Controller@method)</label>
                                <input type="text" class="form-control" name="name" id="name">
                            </div>

                        </div>


                        <div class="col-12 crt-btn text-center">
                            <button type="submit" class="btn crt-userbtn mt-3">Create Permission</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


@endsection
