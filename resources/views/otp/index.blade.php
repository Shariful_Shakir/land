
 @include('layouts.css')
 @include('layouts.js')
@php

 $langId = Cookie::get('langId');

 @endphp
 <style>
 .login-box{
    grid-template-columns: 1fr 0fr !important;
 }
 </style>
<div class="log-box-full">
    <div class="container-fluid log-min">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="log-box">

                        <div class="login-box">

                            <div class="box bg-white p-5">

                                <form action="" method="POST" role="form">
                                    @csrf
                                    <h4 class="log-text">
                                        Mobile Verify
                                    </h4>
                                    <div id="otp_verify_temp">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input style="text-align:center"
                                                class="form-control cust-log-cnt input {{$errors->has('contact_no') ? 'is-danger' : ''}}"
                                                type="text" id="contact_no" name="contact_no" value="{{old('contact_no')}}"
                                                placeholder="Enter Mobile Number" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12 log-btn mt-4">
                                            <span id="sand_otp" class="btn log-userbtn">
                                            Sand
                                            </span>
                                        </div>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




    <script>
    $(document).ready(function(){

        $('#sand_otp').on('click',function(){
            var contact_no = $('#contact_no').val();
            var data = {
                "_token": "{{ csrf_token() }}",
                contact_no: contact_no
            };
            $.ajax({
                type:'POST',
                url: "{{ url('otp-send') }}",
                data:data,
                success: function(res){
                    var al = "Verify Code Send To "+ contact_no +" !\n Your Code: "+ res;
                    alert(al);
                    var text = '';
                text += '<div class="form-group row">'
                        +'<div class="col-sm-12">'
                        +'<input style="text-align:center" class="form-control cust-log-cnt input {{$errors->has("contact_no") ? "is-danger" : "" }}" type="text" id="otp_code" name="otp_code" placeholder="Enter Verify Code" required autocomplete="off">'
                        +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                        +'<div class="col-12 log-btn mt-4">'
                        +'<span id="verify_otp" class="btn log-userbtn">'
                        +'Verify'
                        +'</span>'
                        +'</div>'
                        +'</div>';
                $('#otp_verify_temp').html(text);
                },
                error: function(){
                    alert('Your Mobile Number not Found !');

                }
            });

        });

        $(document).on('click', '#verify_otp', function(){
            var otp_code = $('#otp_code').val();
            var data = {
                "_token": "{{ csrf_token() }}",
                otp_code: otp_code
            };
            $.ajax({
                type:'POST',
                url: "{{ url('otp-verify') }}",
                data:data,
                success: function(res){
                    alert('verified');
                    top.location.href = 'http://localhost:8080/land_f/public/home';
                },
                error: function(){
                    alert('Not Verifyed !');
                    location . reload();
                }
            });
        });


});
    </script>
