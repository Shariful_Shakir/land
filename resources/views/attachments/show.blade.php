@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Show Applications</div>

                <div class="card-body">
                    <div class="row"> <div class="col-3 font-weight-bold"> ট্রাকিং নম্বর </div> <div class="col-9"> {{ $application->tracking_no }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> বিভাগের নাম </div> <div class="col-9"> {{ isset($application->division->name_en) ? $application->division->name_en : '' }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> জেলার নাম </div> <div class="col-9"> {{ isset($application->district->name_en) ? $application->district->name_en : '' }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> উপজেলার নাম </div> <div class="col-9"> {{ isset($application->upazila->name_en) ? $application->upazila->name_en : '' }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> সংযুক্তি প্রাপ্ত যুব ও যুব মহিলার নাম </div> <div class="col-9"> {{ $application->name }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> পিতা/স্বামীর নাম </div> <div class="col-9"> {{ $application->father_name }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> বর্তমান ঠিকানা </div> <div class="col-9"> {{ $application->present_address }} </div> </div>
                    
                    <div class="row"> <div class="col-3 font-weight-bold"> সংযুক্তি প্রাপ্ত দপ্তরের নাম </div> <div class="col-9"> {{ $application->permanent_address }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> দপ্তরে যোগদানের তারিখ </div> <div class="col-9"> {{ $application->permanent_address }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> সংযুত্কির শেষ তারিথ </div> <div class="col-9"> {{ $application->permanent_address }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> মোট আহরিত/গ্রহণকৃত অর্থের পরিমান </div> <div class="col-9"> {{ $application->permanent_address }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold"> ড্রপ আউট আছে কিনা? </div> <div class="col-9"> {{ $application->permanent_address }} </div> </div>
					
					<a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                        @if($langId == 1)
                        {{"Back"}}
                        @else($langId == 2)
                        {{" পিছনে"}}
                        @endif
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection