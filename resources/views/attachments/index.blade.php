@extends('layouts.app')

@section('title', 'Attachment')

@section('content')

	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $title }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					<a href="{{ route('attachments.create') }}" class="btn btn-info float-right mr-1 mb-1"><span class="langBn">সৃষ্টি</span> <span class="langEn">Create</span></a>
					<div class="table-responsive">
						<table class="table table-striped">
						  <thead class="thead-dark">
							<tr>
							  <th scope="col">#</th>
							  <th scope="col">ট্রাকিং নম্বর</th>
							  <th scope="col">উপজেলা</th>
							  <th scope="col">সংযুক্তি প্রাপ্তীর নাম</th>
							  <th scope="col">সংযুক্তি দপ্তরের নাম</th>
							  <th scope="col">গ্রহণকৃত অর্থের পরিমান</th>
							  <th scope="col"> ক্রিয়াকলাপ </th>
							</tr>
						  </thead>
						  <tbody>
							@php $i= 0; @endphp
							@forelse($attachments as $row)
							<tr>
							  <th scope="row">{{ ++$i }}</th>
							  <td> {{ $row->tracking_no }} </td>
							  <td> {{ isset($row->upazila->name) ? $row->upazila->name : '' }} </td>
							  <td> {{ $row->name }} </td>
							  <td> {{ $row->attached_office_name }} </td>
							  <td> {{ $row->amount }} </td>
							  <td>
									<form class="delete" action="{{ route('attachments.destroy', $row->id)}}" method="post">

										<a href="{{ route('attachments.show', $row->id) }}" target="_blank" class="btn btn-info float-left mr-1">দেখুন</a>

										<a href="{{ route('attachments.edit', $row->id) }}" class="btn btn-info float-left mr-1">এডিট</a>


										@csrf
										@method('DELETE')
										<button class="btn btn-danger" type="submit">ডিলিট</button>
									</form>
							  </td>
							</tr>
							@empty
							<tr> <td colspan="7" class="text-center"> কোন ডাটা নাই </td> </tr>
							@endforelse

						  </tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){
    if (langId == 1) {
        $('.langEn').show();
        $('.langBn').hide();
        $('.BngBtn').hide();
        $('.EngBtn').show();
    } else {
        $('.langBn').show();
        $('.langEn').hide();
        $('.EngBtn').hide();
        $('.BngBtn').show();
    }
});
</script>
@endsection
