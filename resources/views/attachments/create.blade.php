@extends('layouts.app')
@section('title', 'Appications')

@section('content')
@if (session('success'))
    <div class="alert alert-success alert-dismissible text-center fade show " role="alert">
        <strong>@if($langId == 1) Success @else সফল @endif !</strong> {{ \Session::get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif


<!-- @if ($errors->any())
    <div class="alert alert-danger alert-dismissible text-center fade show " role="alert">
        @if($langId == 1) <strong>Failed ! </strong>Please fill up the form very carefully.@else <strong>ব্যর্থ হয়েছেন !</strong> অনুগ্রহ করে ফর্ম টি সঠিক ভাবে পূরণ করুন @endif
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif -->


@if(isset($data) && !empty($data))
<form name="myForm" action="{{ route('attachments.store') }}" class="needs-validation" method="POST" onsubmit="return validateCheck();" enctype="multipart/form-data"
    novalidate>
    @csrf
    <div class="container-fluid cust-form-bg entry-form py-2" style="background: #cdf6e8;">
        <br/><br/>

        <div class="form section-1">
            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ট্রাকিং নম্বর</span><span class="langEn">Tracking No</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" maxlength="20" onkeydown="return event.keyCode !== 69" class="form-control @error('tracking_no') is-invalid @enderror"
                                id="tracking_no" name="tracking_no" value="{{ old('tracking_no') }}"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                required autocomplete="off" oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            @error('tracking_no')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('tracking_no') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পিতা/স্বামীর
                                নাম</span><span class="langEn">Father's/Husband's Name</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control @error('father_name') is-invalid @enderror" required id="father_name"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="father_name"
                                value="{{ old('father_name') }}" required autocomplete="off" oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            @error('father_name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('father_name') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পুরুষ এবং মহিলার সংযুক্তি</span><span class="langEn">Male/Female Attachment</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                           <input type="text" class="form-control @error('man_woman_attachment') is-invalid @enderror" required id="man_woman_attachment"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="man_woman_attachment"
                                value="{{ old('man_woman_attachment') }}" required autocomplete="off" oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            @error('man_woman_attachment')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('man_woman_attachment') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <div class="form section-2 ">
            <h6 class="langBn">বর্তমান ঠিকানাঃ</h6>
            <h6 class="langEn">Present Address</h6>
            <div class="divider"></div>


            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">বিভাগের
                                নাম</span><span class="langEn">Division Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <div id="division_title">
                                <input type="text" id="division" required name="present_division_id"
                                    class="form-control" value="{{ old('division_id') }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জেলার
                                নাম</span><span class="langEn">District Name</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <div id="district_title">
                                <input type="text" id="district" name="present_district_id" class="form-control @error('present_district_id') is-invalid @enderror"
                                    required value="{{ old('district_id') }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                @role('super_admin|division_admin|district_admin')

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">উপজেলার
                                নাম</span><span class="langEn">Upazila Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2 cust-width @error('present_upazila_id') is-invalid @enderror" id="upazila_id" name="present_upazila_id"
                                required>
                                @if($langId == 1)
                                {!! getSelectOptions($data['upazilas_en'], old('upazila_id')) !!}
                                @endif
                                @if($langId == 2)
                                {!! getSelectOptions($data['upazilas_bn'], old('upazila_id')) !!}
                                @endif
                            </select>
                            @error('present_upazila_id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('present_upazila_id') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div>

                @endrole
                @role('data_entry_user')

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">উপজেলার
                                নাম</span><span class="langEn">Upazila Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2 cust-width @error('present_upazila_id') is-invalid @enderror" id="upazila_id_data_entry"
                                name="present_upazila_id" required>
                                @if($langId == 1)
                                @foreach($data['upazilas_en'] as $key=>$upazila)
                                "<option value="{{$key}}" selected>{!! $upazila !!}</option>"
                                @endforeach
                                @endif
                                @if($langId == 2)
                                @foreach($data['upazilas_bn'] as $key=>$upazila)
                                "<option value="{{$key}}" selected>{!! $upazila !!}</option>"
                                @endforeach
                                @endif
                            </select>
                            @error('present_upazila_id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('present_upazila_id') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div>
                @endrole

            </div>

            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ইউনিয়ন
                                নাম</span><span class="langEn">Union Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2 @error('present_union_id') is-invalid @enderror" id="union_id" name="present_union_id"
                                value="{{ old('present_union_id') }}" required oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            </select>
                            @error('present_union_id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('present_union_id') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">গ্রাম/বাসা
                                নং</span><span class="langEn">Village/House No</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input class="form-control @error('present_village_id') is-invalid @enderror" id="present_village_id" type="text" name="present_village_id"
                                  name="man_woman_attachment"
                                value="{{ old('present_village') }}" required autocomplete="off" required oncopy="return false" oncut="return false"
                                onpaste="return false" maxlength="70" ondrop="return false">
                            @error('present_village_id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('present_village_id') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পোষ্ট
                                কোড</span><span class="langEn">Post Code</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input class="form-control @error('present_post_code_id') is-invalid @enderror" id="present_post_code_id" type="text"
                                name="present_post_code_id" value="{{old('present_post_code_id')}}" autocomplete="off"
                                required
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false" maxlength="6">
                            @error('present_post_code_id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('present_post_code_id') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>




            {{-- <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">সংযৃক্তি অফিস</span><span class="langEn">Office Attachement</span><span
                                class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control @error('attached_office') is-invalid @enderror" required id="attached_office" name="attached_office"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" value="{{old('attached_office')}}" autocomplete="off" required oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            @error('attached_office')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('attached_office') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">সংযৃক্তি অফিস</span><span class="langEn">Office Attachement</span><span
                                class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="file" class="cust-file-type" required id="attached_office" name="attached_office"
                                value="{{old('attached_office')}}" autocomplete="off" required oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            @error('attached_office')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{$errors->first('attached_office') }}</strong>
                                  </span>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">যোগদানের তারিখ</span><span class="langEn">Joining Date</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control @error('joining_date') is-invalid @enderror" id="joining_date" required name="joining_date"
                                <!-- onfocusout="this.setAttribute('type','password'); -->"
                                onfocus="this.setAttribute('type','text');" data-inputmask-alias="dd-mm-yyyy"
                                data-inputmask="'yearrange': { 'minyear': '1971'}" data-val="true"
                                data-val-required="Required" oncopy="return false" oncut="return false"
                                onpaste="return false" value="{{ old('joining_date') }}" required autocomplete="off">

                            @error('joining_date')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('joining_date') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">যোগদানের শেষ তারিখ</span><span class="langEn">Last Joining  Date</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control @error('last_joining_date') is-invalid @enderror" id="last_joining_date" required
                                name="last_joining_date" data-inputmask-alias="dd-mm-yyyy"
                                data-inputmask="'yearrange': { 'minyear': '1971'}" data-val="true"
                                data-val-required="Required" value="{{ old('last_joining_date') }}" required
                                autocomplete="off">
                            @error('last_joining_date')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('last_joining_date') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">জমাকৃত টাকা</span><span class="langEn">Recived Money</span><span
                                class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" maxlength="8" onkeydown="return event.keyCode !== 69" class="form-control @error('recived_money') is-invalid @enderror"
                                id="recived_money" name="recived_money" value="{{ old('recived_money') }}"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                required autocomplete="off" oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">

                            @error('recived_money')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('recived_money') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div>


                {{-- <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ডপ-আউট?</span><span class="langEn">Drop-Out?</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control @error('drop_out') is-invalid @enderror" required id="drop_out"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="drop_out"
                                value="{{ old('drop_out') }}" required autocomplete="off" oncopy="return false" oncut="return false"
                                onpaste="return false" ondrop="return false">
                            @error('drop_out')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{$errors->first('drop_out') }}</strong>
                              </span>
                           @enderror
                        </div>
                    </div>
                </div> --}}
                 <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ডপ-আউট?</span><span class="langEn">Drop-Out?</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                           <select class="form-control" name="drop_out" value="{{ old('drop_out') }}">
                               <option style="display:none" value="0"></option>
                               @if($langId==1){
                                <option value="1">Yes</option>
                               <option value="2">No</option>
                               }
                               @endif
                               @if($langId==2){
                                 <option value="1">হ্যাঁ</option>
                               <option value="2">না</option>   
                               }
                               @endif
                           </select>
                        </div>
                    </div>
                </div>

            </div>

        </div>



        <br/>
        <div class="form section-4">
            <div class="row">
                <div class="col-6 btns text-center">
                    <button class="btn btn-primary btn-sm"> Submit </button>
                    <button type="reset" class="btn btn-warning btn-sm text-white" onClick="window.location.reload();"> Reset </button>
                </div>
            </div>
        </div>

        <br/><br/><br/><br/>
    </div>
</form>
@endif
@endsection
@section('extraJS')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
    integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="{{ asset('assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
    rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
<link rel="stylesheet" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
<script src="{{ asset('js/form-validation.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script src="{{ asset('js/dataEntryFormValidation.js') }}"></script>


<script>


    $(document).ready(function() {


    $('#upazila_id_data_entry').on('keydown', function() {
        var url = "{{ url('/getUpazliaInfos') }}";
        var upazilaId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        //console.log('upazilaId : ' + upazilaId);
        ajaxCallCB(url, data, function(response) {
            //console.log(response);
            if (langId == 1) {
                var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                    response.data.division_name_eng : '';
                var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                    response.data.district_name_eng : '';
            } else {
                var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                    response.data.division_name_bng : '';
                var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                    response.data.district_name_bng : '';
            }
            // $('#division_title').html('<strong>' + division_name + '</strong>');
            // $('#district_title').html('<strong>' + district_name + '</strong>');
            $('#division').val(division_name);
            $('#district').val(district_name);
            //console.log('response : ' + response);
        });
    });


    $('#upazila_id_data_entry').on('keydown', function() {
            var url = "{{ url('/getUnionInfos') }}";
            var upazilaId = $(this).val();
            var union_name = '';
            var data = {
                "_token": "{{ csrf_token() }}",
                upazila_id: upazilaId
            };

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'json'
            }).done(function(response) {
                //console.log(response);
                var union = '';
                union += '<option value=""></option>';
                //console.log(union_name);
                $.each(response, function(i, item) {
                    if (langId == 1) {
                        union += '<option value="' + response[i].id + '">' + response[i]
                            .union_name_eng + '</option>';
                    } else {
                        union += '<option value="' + response[i].id + '">' + response[i]
                            .union_name_bng + '</option>';
                    }
                });
                $('#union_id').html(union);
            });
        });


    $('#upazila_id').on('change', function() {
        var url = "{{ url('/getUpazliaInfos') }}";
        var upazilaId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        //console.log('upazilaId : ' + upazilaId);
        ajaxCallCB(url, data, function(response) {
            //console.log(response);
            if (langId == 1) {
                var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                    response.data.division_name_eng : '';
                var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                    response.data.district_name_eng : '';
            } else {
                var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                    response.data.division_name_bng : '';
                var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                    response.data.district_name_bng : '';
            }
            // $('#division_title').html('<strong>' + division_name + '</strong>');
            // $('#district_title').html('<strong>' + district_name + '</strong>');
            $('#division').val(division_name);
            $('#district').val(district_name);
            //console.log('response : ' + response);
        });
    });

    $('#upazila_id').on('change', function() {
        var url = "{{ url('/getUnionInfos') }}";
        var upazilaId = $(this).val();
        var union_name = '';
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            //console.log(response);
            var union = '';
            union += '<option value=""></option>';
            //console.log(union_name);
            $.each(response, function(i, item) {
                if (langId == 1) {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#union_id').html(union);
        });
    });

    $('#union_id ').on('change', function() {
        var url = "{{ url('/getPostalCode') }}";
        var union_id = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            union_id: union_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var post_code = (typeof response.data[0].postcode != 'undefined') ? response.data[0]
                .postcode : '';
            if (post_code) {
                $('#present_post_code_id').val(post_code);
            } else {
                $('#present_post_code_id').val('');
            }
        });
    });
    // ------------permanent--------------
    $('#permanent_upazila_id').on('change', function() {
        var url = "{{ url('/getUpazliaInfos') }}";
        var upazilaId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        //console.log('upazilaId : ' + upazilaId);
        ajaxCallCB(url, data, function(response) {
            // var division_name = (typeof response.data.division_name_eng != 'undefined') ?
            //     response.data.division_name_eng : '';
            // var district_name = (typeof response.data.district_name_eng != 'undefined') ?
            //     response.data.district_name_eng : '';
            if (langId == 1) {
                var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                    response.data.division_name_eng : '';
                var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                    response.data.district_name_eng : '';
            } else {
                var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                    response.data.division_name_bng : '';
                var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                    response.data.district_name_bng : '';
            }
            // $('#division_title').html('<strong>' + division_name + '</strong>');
            // $('#district_title').html('<strong>' + district_name + '</strong>');
            $('#permanent_division').val(division_name);
            $('#permanent_district').val(district_name);
            //console.log('response : ' + response);
        });
    });
    $('#permanent_upazila_id').on('change', function() {
        var url = "{{ url('/getUnionInfos') }}";
        var upazilaId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var union = '';
            union += '<option value=""></option>';
            $.each(response, function(i, item) {
                // union += '<option value="' + response[i].id + '">' + response[i]
                //.union_name_bng + '</option>';
                if (langId == 1) {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#permanent_union_id').html(union);
        });
    });
    $('#permanent_union_id ').on('change', function() {
        var url = "{{ url('/getPostalCode') }}";
        var union_id = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            union_id: union_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var post_code = (typeof response.data[0].postcode != 'undefined') ? response.data[0]
                .postcode : '';
            if (post_code) {
                $('#permanent_post_code_id').val(post_code);
            } else {
                $('#permanent_post_code_id').val('');
            }
        });
    });
    //------------permanent---------------
    $('body').on('keydown', '#tracking_no', function(e) {
        if (e.keyCode === 9) {
            var tracking_no = $('#tracking_no').val();
            if (tracking_no == "") {
                e.preventDefault();
            }
        }
    });


    $('body').on('keydown', '#recived_money', function(e) {
        if (e.keyCode === 9) {
            var recived_money = $('#recived_money').val();
            if (recived_money == "") {
                e.preventDefault();
            }
        }
    });


     $('body').on('keydown', '#present_post_code_id', function(e) {
        if (e.keyCode === 9) {
            var present_post_code_id = $('#present_post_code_id').val();
            if (present_post_code_id == "") {
                e.preventDefault();
            }
        }
    });



    $('body').on('keydown', '#name', function(e) {
        if (e.keyCode === 9) {
            var name = $('#name').val();
            name = name.trim();
            if (name == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#union_id', function(e) {
        if (e.keyCode === 9) {
            var union_id = $('#union_id').val();
            if (union_id == "") {
                e.preventDefault();
            }
        }
    });





    $('body').on('keydown', '#permanent_union_id', function(e) {
        if (e.keyCode === 9) {
            var permanent_union_id = $('#permanent_union_id').val();
            if (permanent_union_id == "") {
                e.preventDefault();
            }
        }
    });

    $('#name').on('keyup', function() {
        var name = $(this).val();
        if (typeof name[0] != 'undefined' && (name[0] == (event.charCode == 32) || name[0] == '.' ||
                name[0] == '-')) {
            $(this).val('');
        }
    });
    //onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'
    $('body').on('keydown', '#father_name', function(e) {
        if (e.keyCode === 9) {
            var father_name = $('#father_name').val();
            father_name = father_name.trim();
            if (father_name == "") {
                e.preventDefault();
            }
        }
    });
    $('#father_name').on('keyup', function() {
        var father_name = $(this).val();
        if (typeof father_name[0] != 'undefined' && (father_name[0] == (event.charCode == 32) ||
                father_name[0] == '.' || father_name[0] == '-')) {
            $(this).val('');
        }
    });


    $('body').on('keydown', '#man_woman_attachment', function(e) {
        if (e.keyCode === 9) {
            var man_woman_attachment = $('#man_woman_attachment').val();
            man_woman_attachment = man_woman_attachment.trim();
            if (man_woman_attachment == "") {
                e.preventDefault();
            }
        }
    });
    $('#man_woman_attachment').on('keyup', function() {
        var man_woman_attachment = $(this).val();
        if (typeof man_woman_attachment[0] != 'undefined' && (man_woman_attachment[0] == (event.charCode == 32) ||
                man_woman_attachment[0] == '.' || man_woman_attachment[0] == '-')) {
            $(this).val('');
        }
    });



    $('body').on('keydown', '#upazila_id', function(e) {
        if (e.keyCode === 9) {
            var upazila_id = $('#upazila_id').val();
            if (upazila_id == "") {
                e.preventDefault();
            }
        }
    });




    $('body').on('keydown', '#present_village_id', function(e) {
        if (e.keyCode === 9) {
            var present_village_id = $('#present_village_id').val();
            present_village_id = present_village_id.trim();
            if (present_village_id == "") {
                e.preventDefault();
            }
        }
    });
    $('#present_village_id').on('keyup', function() {
        var present_village_id = $(this).val();
        if (typeof present_village_id[0] != 'undefined' && (present_village_id[0] == (event.charCode == 32) ||
                present_village_id[0] == '.' || present_village_id[0] == '-')) {
            $(this).val('');
        }
    });




    $('body').on('keydown', '#attached_office', function(e) {
        if (e.keyCode === 9) {
            var attached_office = $('#attached_office').val();
            attached_office = attached_office.trim();
            if (attached_office == "") {
                e.preventDefault();
            }
        }
    });
    $('#attached_office').on('keyup', function() {
        var attached_office = $(this).val();
        if (typeof attached_office[0] != 'undefined' && (attached_office[0] == (event.charCode == 32) ||
                attached_office[0] == '.' || attached_office[0] == '-')) {
            $(this).val('');
        }
    });



    /*$('body').on('keydown', '#joining_date', function(e) {
        var joining_date = $('#joining_date').val();
        if(e.keyCode >47 && e.keyCode <=58){
            joining_date.trim();
        }else{
            alert('Number only alowed');
            e.preventDefault();
        }
    });*/


    $('body').on('keydown', '#joining_date', function(e) {
        var joining_date = $('#joining_date').val();
        if (langId == 1) {
              if( (joining_date.charAt(0) != undefined)  && (e.keyCode > 95) && (e.keyCode <= 105) || (e.keyCode > 47) && (e.keyCode <= 57)  || (e.keyCode == 8) || (e.keyCode == 9) || (e.keyCode == 114) ){
                   //alert('Re');
                joining_date.trim();
              }else{
                alert('Please enter english number.');
                e.preventDefault();
              }
          }else{
            //alert(joining_date[0]);
             if( (joining_date.charAt(0) != undefined)  && (e.keyCode > 95) && (e.keyCode <= 105) || (e.keyCode > 47) && (e.keyCode <= 57)  || (e.keyCode == 8) || (e.keyCode == 9) || (e.keyCode == 114) ){
                     //alert('Re');
                joining_date.trim();
              }else{
                alert('অনুগ্রহ করে ইংরেজি সংখা প্রবেশ করান । ');
                e.preventDefault();
              }
          }

    });



     $('body').on('keydown', '#last_joining_date', function(e) {
        var last_joining_date = $('#last_joining_date').val();
        if (langId == 1) {
              if( (last_joining_date.charAt(0) != undefined)  && (e.keyCode > 95) && (e.keyCode <= 105) || (e.keyCode > 47) && (e.keyCode <= 57)  || (e.keyCode == 8) || (e.keyCode == 9) || (e.keyCode == 114) ){
                   //alert('Re');
                last_joining_date.trim();
              }else{
                alert('Please enter english number.');
                e.preventDefault();
              }
          }else{
            //alert(last_joining_date[0]);
             if( (last_joining_date.charAt(0) != undefined)  && (e.keyCode > 95) && (e.keyCode <= 105) || (e.keyCode > 47) && (e.keyCode <= 57)  || (e.keyCode == 8) || (e.keyCode == 9) || (e.keyCode == 114) ){
                     //alert('Re');
                last_joining_date.trim();
              }else{
                alert('অনুগ্রহ করে ইংরেজি সংখা প্রবেশ করান । ');
                e.preventDefault();
              }
          }

    });


    $('body').on('keydown', '#drop_out', function(e) {
        if (e.keyCode === 9) {
            var drop_out = $('#drop_out').val();
            drop_out = drop_out.trim();
            if (drop_out == "") {
                e.preventDefault();
            }
        }
    });
    $('#drop_out').on('keyup', function() {
        var drop_out = $(this).val();
        if (typeof drop_out[0] != 'undefined' && (drop_out[0] == (event.charCode == 32) ||
                drop_out[0] == '.' || drop_out[0] == '-')) {
            $(this).val('');
        }
    });



    $(document).on('keydown', function(e){
        if (e.keyCode === 9) {
            $("#file").focus(function(){
                $("#submitbtn").removeAttr('disabled');
            });
        }
    });
    $("#btndiv").mouseover(function(){
        $("#submitbtn").removeAttr('disabled');
    });



});

function password_set_attribute() {}
$(document).ready(function() {
    $(":input[data-inputmask-mask]").inputmask();
    $(":input[data-inputmask-alias]").inputmask();
    $(":input[data-inputmask-regex]").inputmask("Regex");
});
$(document).ready(function() {
    if (langId == 1) {
        $('.langEn').show();
        $('.langBn').hide();
        $('.BngBtn').hide();
        $('.EngBtn').show();
    } else {
        $('.langBn').show();
        $('.langEn').hide();
        $('.EngBtn').hide();
        $('.BngBtn').show();
    }
    if (langId == 2) {
        $('#name').addClass('bng-input');
        $('#name').removeAttr('oninput');

        $('#tracking_no').removeAttr('oninput');
        $('#tracking_no').attr('type', 'text');
        $('#tracking_no').addClass('bng-input');


        $('#recived_money').removeAttr('oninput');
        $('#recived_money').attr('type', 'text');
        $('#recived_money').addClass('bng-input');

        $('#present_post_code_id').removeAttr('oninput');
        $('#present_post_code_id').attr('type', 'text');
        $('#present_post_code_id').addClass('bng-input');

        $('#father_name').addClass('bng-input');
        $('#father_name').removeAttr('oninput');

        $('#man_woman_attachment').addClass('bng-input');
        $('#man_woman_attachment').removeAttr('oninput');


        $('#present_village_id').addClass('bng-input');
        $('#present_village_id').removeAttr('oninput');

        $('#drop_out').addClass('bng-input');
        $('#drop_out').removeAttr('oninput');

        $('#attached_office').addClass('bng-input');
        $('#attached_office').removeAttr('oninput');


    }
});
</script>


<script>
    function validateCheck() {

    var trackingNo = document.forms["myForm"]["tracking_no"].value;
    if (trackingNo == "") {
        $('#tracking_no').css('border','1px solid red');
        if (langId == 1) {
            alert("Tracking number must be required. ");
        }else{
           alert("ট্রাকিং নম্বর অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#tracking_no').css('border','1px solid green');
      }

    var fatherName = document.forms["myForm"]["father_name"].value;
      if (fatherName == "") {
        $('#father_name').css('border','1px solid red');
        if (langId == 1) {
            alert("Father's/Husband name must be required. ");
        }else{
           alert("পিতা/স্বামীর নাম অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#father_name').css('border','1px solid green');
      }

    var manWomanAttachment = document.forms["myForm"]["man_woman_attachment"].value;
      if (manWomanAttachment == "") {
        $('#man_woman_attachment').css('border','1px solid red');
        if (langId == 1) {
            alert("Male or female attachment must be required. ");
        }else{
           alert("পুরুষ এবং মহিলার সংযুক্তি অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#man_woman_attachment').css('border','1px solid green');
      }



    var presentUpazilaId = document.forms["myForm"]["present_upazila_id"].value;
      if (presentUpazilaId == "") {
        $('#upazila_id').css('border','1px solid red');
        if (langId == 1) {
            alert("Upazila name must be required. ");
        }else{
           alert("উপজেলার নাম অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#upazila_id').css('border','1px solid green');
      }

    var presentUnionId = document.forms["myForm"]["union_id"].value;
      if (presentUnionId == "") {
        $('#union_id').css('border','1px solid red');
        if (langId == 1) {
            alert("Union name must be required. ");
        }else{
           alert("ইউনিয়ন নাম অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#union_id').css('border','1px solid green');
      }


    var presentVillageId = document.forms["myForm"]["present_village_id"].value;
      if (presentVillageId == "") {
        $('#present_village_id').css('border','1px solid red');
        if (langId == 1) {
            alert("Village/House number must be required. ");
        }else{
           alert("গ্রাম/বাসা নং অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#present_village_id').css('border','1px solid green');
      }



    var presentPostCodeId = document.forms["myForm"]["present_post_code_id"].value;
      if (presentPostCodeId == "") {
        $('#present_post_code_id').css('border','1px solid red');
        if (langId == 1) {
            alert("Post code must be required. ");
        }else{
           alert("পোষ্ট কোড অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#present_post_code_id').css('border','1px solid green');
      }


    var attachedOffice = document.forms["myForm"]["attached_office"].value;
      if (attachedOffice == "") {
        $('#attached_office').css('border','1px solid red');
        if (langId == 1) {
            alert("Attached office must be required. ");
        }else{
           alert("সংযৃক্তি অফিস অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#attached_office').css('border','1px solid green');
      }

    var joiningDate = document.forms["myForm"]["joining_date"].value;
      if (joiningDate == "") {
        $('#joining_date').css('border','1px solid red');
        if (langId == 1) {
            alert("Joining date must be required. ");
        }else{
           alert("যোগদানের তারিখ অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#joining_date').css('border','1px solid green');
      }


    var lastJoiningDate = document.forms["myForm"]["last_joining_date"].value;
      if (lastJoiningDate == "") {
        $('#last_joining_date').css('border','1px solid red');
        if (langId == 1) {
            alert("Last joining date must be required. ");
        }else{
           alert("যোগদানের শেষ তারিখ অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#last_joining_date').css('border','1px solid green');
      }


    var recivedMoney = document.forms["myForm"]["recived_money"].value;
      if (recivedMoney == "") {
        $('#recived_money').css('border','1px solid red');
        if (langId == 1) {
            alert("Recived money must be required. ");
        }else{
           alert("জমাকৃত টাকা অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#recived_money').css('border','1px solid green');
      }

    var dropOut = document.forms["myForm"]["drop_out"].value;
      if (dropOut == "") {
        $('#drop_out').css('border','1px solid red');
        if (langId == 1) {
            alert("Drop out must be required. ");
        }else{
           alert("ডপ আউট অবশ্যই দিতে হবে ।");
        }
        return false;
      }else{
        $('#drop_out').css('border','1px solid green');
      }

}

</script>
</script>
@endsection
