@extends('layouts.app')

@section('title', 'Appications')

@section('content')

	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Add Data</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

					@if ($errors->any())
					  <div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							  <li>{{ $error }}</li>
							@endforeach
						</ul>
					  </div><br />
					@endif

					<form action="{{ route('attachments.update', $attachments->id) }}" method="post" >
						@method('PATCH')
                         @csrf
						<div class="row">
							<div class="col-3">
								<div class="form-group">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <span class="langBn"> ট্রাকিং নম্বর </span>
                                        <span class="langEn"> Tracking No. </span>
                                        <span class="text-danger"> * </span>
                                    </label>
								  <input type="number" class="form-control" id="tracking_no"  name="tracking_no" value="{{ $attachments->tracking_no }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
									<label for="division_id">বিভাগের নাম</label>
									<div id="division_title"> </div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
									<label for="district_id">জেলার নাম</label>
									<div id="district_title"> </div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group">
								  <label for="upazila_id">উপজেলার নাম</label>
								  {{ old('upazila_id') }}
								  <select class="form-control" id="upazila_id" data-placeholder="উপজেলার নাম" name="upazila_id">
									{!! getSelectOptions($data['upazilas_bn'], old('upazila_id')) !!}
								  </select>

								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                  <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <span class="langBn"> সংযুক্তি প্রাপ্ত যুব ও যুব মহিলার নাম </span>
                                        <span class="langEn">young and the young woman received the attachment. </span>
                                        <span class="text-danger"> * </span>
                                  </label>

								  <input type="text" class="form-control" id="name"  name="name"  value="{{ $attachments->name }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
							<div class="col-4">
								<div class="form-group required">
                                 <label class="col-sm-12 col-md-4 col-form-label text-right">
                                 <span class="langBn"> পিতা/স্বামীর নাম </span>
                                 <span class="langEn"> Husband/Wife. </span>
                                 <span class="text-danger"> * </span>
                                 </label>

								  <input type="text" class="form-control" id="father_name"  name="father_name"  value="{{ $attachments->father_name }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                    <span class="langBn"> বর্তমান ঠিকানা </span>
                                    <span class="langEn"> Present Adress </span>
                                    <span class="text-danger"> * </span>
                                    </label>

								  <input type="text" class="form-control" id="present_address"  name="present_address" value="{{ $attachments->present_address }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                <label class="col-sm-12 col-md-4 col-form-label text-right">
                                <span class="langBn"> সংযুক্তি প্রাপ্ত দপ্তরের নাম </span>
                                <span class="langEn"> Attached Office </span>
                                <span class="text-danger"> * </span>
                                </label>

								 <input type="text" class="form-control" id="attached_office_name"  name="attached_office_name"  value="{{ $attachments->present_address }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                <label class="col-sm-12 col-md-4 col-form-label text-right">
                                    <span class="langBn"> দপ্তরে যোগদানের তারিখ </span>
                                    <span class="langEn"> Joining Date at Office </span>
                                <span class="text-danger"> * </span>
                                </label>

								  <!-- <textarea class="form-control" id="office_joined_date" placeholder="বর্তমান ঠিকানা লিখুন" name="office_joined_date" required>{{ old('office_joined_date') }}</textarea> -->
								  <input type="text" class="form-control" id="office_joined_date"  name="office_joined_date"  value="{{ $attachments->office_joined_date }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                 <label class="col-sm-12 col-md-4 col-form-label text-right">
                                    <span class="langBn"> সংযুত্কির শেষ তারিথ </span>
                                    <span class="langEn">Last Joining Date </span>
                                    <span class="text-danger"> * </span>
                                  </label>

								  <input type="text" class="form-control" id="office_end_date"  name="office_end_date"  value="{{ $attachments->office_end_date }}" required>
								  <!-- <textarea class="form-control" id="office_end_date" placeholder="বর্তমান ঠিকানা লিখুন" name="office_end_date" required>{{ old('office_end_date') }}</textarea> -->
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                 <label class="col-sm-12 col-md-4 col-form-label text-right">
                                 <span class="langBn"> মোট আহরিত/গ্রহণকৃত অর্থের পরিমান </span>
                                 <span class="langEn">Total Collected/Received Money </span>
                                 <span class="text-danger"> * </span>
                                 </label>

								  <input type="text" class="form-control" id="amount"  name="amount"  value="{{ $attachments->amount }}" required>
								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-4">
								<div class="form-group required">
                                 <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <span class="langBn">ড্রপ আউট আছে কিনা ? </span>
                                        <span class="langEn">Have Dropped ?</span>
                                        <span class="text-danger"> * </span>
                                </label>
                                {{-- <select class="custom-select my-1 mr-sm-2" name="is_dropout" id="is_dropout">
                                    {!! getSelectOptions($data['is_drop'], $attachments->is_dropout) !!}
                                </select> --}}

								  <div class="valid-feedback">বৈধ হয়েছে</div>
								  <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-primary mt-3"><span class="langBn">সাবমিট</span><span class="langEn">Submit</span></button>
						</span>
					</form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('extraJS')

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="{{ asset('assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
<link rel="stylesheet" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
<script src="{{ asset('js/form-validation.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script>

$(document).ready(function() {
	$("#upazila_id").select2({
		theme: "bootstrap",
		//theme: "bootstrap4"
		allowClear: true
	});

	$("#session_id").select2({
		theme: "bootstrap",
		allowClear: true
	});

	$("#blood_group").select2({
		theme: "bootstrap",
		allowClear: true
	});

	$("#religion").select2({
		theme: "bootstrap",
		allowClear: true
	});

	$(".datepicker").datetimepicker({
        //format: 'yyyy-mm-dd',
        //minView: 2,
        format: 'DD-MM-YYYY',
		// autoclose:true,
		// endDate: "today",
		// maxDate: today
    });


	$('#upazila_id').on('change', function() {

		var url = "{{ url('/getUpazliaInfos') }}";
		var upazilaId = $(this).val();
		var data = {
			upazila_id : upazilaId
		};

		//console.log('upazilaId : ' + upazilaId);
		ajaxCallCB(url, data, function(response) {
			var division_name = (typeof response.data.division_name != 'undefined') ? response.data.division_name : '';
			var district_name = (typeof response.data.district_name != 'undefined') ? response.data.district_name : '';
			$('#division_title').html('<strong>' + division_name + '</strong>');
			$('#district_title').html('<strong>' + district_name + '</strong>');
			//console.log('response : ' + response);
		});
	});
});
$(document).ready(function(){
    if (langId == 1) {
        $('.langEn').show();
        $('.langBn').hide();
        $('.BngBtn').hide();
        $('.EngBtn').show();
    } else {
        $('.langBn').show();
        $('.langEn').hide();
        $('.EngBtn').hide();
        $('.BngBtn').show();
    }

    if(langId == 2){
        $('#name').addClass('bng-input');
        $('#name').removeAttr('oninput');
        $('#present_address').addClass('bng-input');
        $('#present_address').removeAttr('oninput');
        $('#attached_office_name').addClass('bng-input');
        $('#attached_office_name').removeAttr('oninput');
        $('#office_joined_date').addClass('bng-input');
        $('#office_joined_date').removeAttr('oninput');
        $('#amount').addClass('bng-input');
        $('#amount').removeAttr('oninput');
        $('#office_end_date').addClass('bng-input');
        $('#office_end_date').addClass('bng-input');
        $('#father_name').removeAttr('oninput');
        $('#father_name').addClass('bng-input');





    }
});
</script>
@endsection
