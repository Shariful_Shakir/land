@extends('layouts.app')

@section('title', 'Appications')

@section('content')

	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					<a href="{{ url('work-status/create') }}" class="btn btn-info float-right mr-1 mb-1">ডাটা যোগ</a>
					<div class="table-responsive">
						<table class="table table-striped">
						  <thead class="thead-dark">
							<tr>
							  <th scope="col">#</th>
							  <th scope="col">ট্রাকিং নম্বর</th>
							  <th scope="col">উপজেলা</th>
							  <th scope="col">আবেদনকারীর নাম</th>
							  <th scope="col">ই-মেইল</th>
							  <th scope="col">মোবাইল নং</th>
							  <th scope="col"> ক্রিয়াকলাপ </th>
							</tr>
						  </thead>
						  <tbody>
						
							
							<tr>
							  <th scope="row"></th>
							  <td>  </td>
							  <td> </td>
							  <td>  </td>
							  <td>  </td>
							  <td>  </td>
							 
							</tr>
							
							<tr> <td colspan="7" class="text-center"> কোন ডাটা নাই </td> </tr>
						
							
						  </tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>

@endsection 
