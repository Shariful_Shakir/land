@extends('layouts.app')
@section('title', 'Appications')
@section('content')


@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif

<div class="container-fluid my-3 ">
        <div class="form section-1 p-2">
            <div class="row">
                <div class="offset-1 col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">ট্রাকিং নম্বর
                            <span class="text-danger"> * </span>
                        </label>
                        <div class="col-sm-12 col-md-4">
                            <input type="text" class="form-control" placeholder="ট্রাকিং নম্বর">
                        </div>
                    </div>
                </div>
            </div>
            <h6>পূর্ববর্তী অবস্থা</h6>
            <div class="row">

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">বিভাগের নাম<span
                                class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <select class="custom-select" id="inlineFormCustomSelectPref">
                                <option selected>বিভাগের নাম...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">জেলার নাম<span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <select class="custom-select" id="inlineFormCustomSelectPref">
                                <option selected>বিভাগের নাম...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">উপজেলার নাম<span
                                class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <select class="custom-select" id="inlineFormCustomSelectPref">
                                <option selected>বিভাগের নাম...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">সংযুক্তি প্রাপ্ত যুব ও যুব মহিলার
                            নাম <span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">সর্বোচ্চ প্রাতিষ্ঠানিক শিক্ষা ‍
                            <spaan class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">কোনরুপ আয়ের উৎস ছিল কি না ? <span
                                class="text-danger"> * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                    value="option1">
                                <label class="form-check-label" for="inlineRadio1"> হ্যাঁ </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                    value="option2">
                                <label class="form-check-label" for="inlineRadio2">না</label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">কোন স্কিল ছিল কি না? <span
                                class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-8">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                    value="option1">
                                <label class="form-check-label" for="inlineRadio1"> হ্যাঁ </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                    value="option2">
                                <label class="form-check-label" for="inlineRadio2">না</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">বয়স ও বৈবাহিক অবস্থা: বিবাহিত হলে
                            স্বামী /স্ত্রীর শিক্ষাগত যোগ্যতা ও সন্তান সংখ্যা <span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">নিজের উপর আস্থা <span
                                class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>


            <div class="form section-2">
                <h6>পরবর্তী অবস্থা</h6>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">ন্যাশনাল সার্ভিসে অর্ন্তভুক্তির
                                ফলে অর্জিত বিশেষ দক্ষতা <span class="text-danger">
                                    *
                                </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">অস্থায়ী চাকুরী শেষে প্রাপ্ত টাকা
                                কোন কাজে ব্যবহার করা হয়েছে? <span class="text-danger"> * </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">কর্মসূচি চলাকালিন সময়ে প্রাপ্ত
                                টাকার ব্যবহার <span class="text-danger">
                                    * </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form section-3">

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right boltu">বর্তমান পেশার সংগে অস্থায়ী
                                চাকুরীর সম্পর্ক/প্রাসংঙ্গিকতা <span class="text-danger"> * </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">নিজের উপর আস্থা পূর্বের চেয়ে
                                বেড়েছে কিনা </label>
                            <div class="col-sm-12 col-md-8">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                        id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1"> হ্যাঁ </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                        id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">না</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">বয়স ও বৈবাহিক অবস্থা: বিবাহিত
                                হলে
                                স্বামী /স্ত্রীর শিক্ষাগত যোগ্যতা ও সন্তান সংখ্যা <span class="text-danger"> *
                                </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right"> আত্নকর্মসংস্থানের জন্য ব্যাংক
                                বা অন্য কোন উৎস হতে ঋন নেওয়া হয়েছে কি না <span class="text-danger">
                                    *
                                </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">কর্মসংস্থানের জন্য কোন নিয়োগ
                                পরীক্ষায় অংশগ্রহন করেছে কি না এবং ফলাফল <span class="text-danger"> * </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">ন্যাশনাল সার্ভিসে কর্মসূচিতে
                                অংশগ্রহন কালে উপকারভোগীদের বয়স <span class="text-danger">
                                    * </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form section-4">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right">ন্যাশনাল সার্ভিসে কর্মসূচিতে
                                অংশগ্রহন শেষে যুবাদের জন্য যুব উন্নয়ন অধিদপ্তর ব্যাতীত অন্যান্য প্রাতিষ্ঠানিক
                                (যেমন-ব্যাংক ঋণ) ঋণ প্রাপ্তি সুযোগ পেয়েছে কিনা? <span class="text-danger"> *
                                </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right"> কোন ঋণ গ্রহণ করলে তা কি কাজে
                                ব্যবহার করা হয়েছে <span class="text-danger"> *
                                </span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-4 btns text-center">
                        <button class="btn btn-primary btn-sm"> Submit </button>
                        <button class="btn btn-warning btn-sm text-white"> Reset </button>
                    </div>
                </div>
            </div>

        </div>


@endsection
@section('extraJS')

@endsection