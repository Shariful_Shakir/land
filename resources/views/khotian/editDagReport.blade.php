@extends('layouts.app')

@section('content')
@php

 $langId = Cookie::get('langId');


 @endphp

   <div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Edit Information"}}
                        @else($langId == 2)
                        {{""}}
                        @endif
                    </h3>
                    <br>
                    @if (Session::has('errors'))
                    <hr>
                    <div class="alert alert-danger">{{ Session::get('errors') }}</div>
                    <hr>
                    @endif
                    @if (Session::has('success'))
                    <hr>
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                    <hr>
                    @endif


                    <form method="post" action="{{ url('/store-dag-report') }}">
                        @csrf

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="dag_no">Dag No</label>
                                <input type="text" class="form-control" name="dag_no" value="{{$data->dag_no}}"/>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="class"> Class </label>
                                <input type="text" class="form-control" name="class" value="{{$data->type}}"/>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="area_of_land">Area of land</label>
                                <input type="text" class="form-control" name="area_of_land" value="{{$data->land_part}}"/>
                                <input type="hidden" class="form-control" name="id" value="{{$data->id}}"/>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-outline-primary update-buttn">
                            Submit
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extraJS')
<script>

</script>
@endsection
