@extends('layouts.app')

@section('content')
@php

 $langId = Cookie::get('langId');

$data['division_en'] = DB::table('geo_divisions')->pluck('division_name_eng', 'id')->toArray();
$data['division_bn'] = DB::table('geo_divisions')->pluck('division_name_bng', 'id')->toArray();
$data['district_en'] = DB::table('geo_districts')->pluck('district_name_eng', 'id')->toArray();
$data['district_bn'] = DB::table('geo_districts')->pluck('district_name_bng', 'id')->toArray();
$data['upazilas_en'] = DB::table('geo_upazilas')->pluck('upazila_name_eng', 'id')->toArray();
$data['upazilas_bn'] = DB::table('geo_upazilas')->pluck('upazila_name_bng', 'id')->toArray();
$super = Cookie::get('super');

 @endphp
<style>
    .edit{
        float: left;
        position: absolute;
        left: 81%;
    }
    .edit2{
        float: left;
        position: absolute;
        left: 87%;
    }
</style>

   <div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Search Khotian"}}
                        @else($langId == 2)
                        {{""}}
                        @endif
                    </h3>
                    <br>
                    @if (Session::has('errors'))
                    <hr>
                    <div class="alert alert-danger">{{ Session::get('errors') }}</div>
                    <hr>
                    @endif


                    <form method="post" action="{{ url('khotian/report') }}">
                        @csrf
                        <div class="row">
                        <div class="form-group col-md-6">
                            <label for="">
                                @if($langId == 1)
                                {{"Division Name"}}
                                @else($langId == 2)
                                {{"Division Name"}}
                                @endif
                                <span class="text-danger"> *
                                </span>
                            </label>
                            <select class="custom-select mr-sm-2 cust-width" id="division_id" name="division_id" required>
                                @if($langId == 1)
                                {!! getSelectOptions($data['division_en'], old('division_id')) !!}
                                @endif
                                @if($langId == 2)
                                {!! getSelectOptions($data['division_bn'], old('division_id')) !!}
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                             <label for="">
                                @if($langId == 1)
                                {{"District Name"}}
                                @else($langId == 2)
                                {{"District Name"}}
                                @endif
                                <span class="text-danger"> *
                                </span>
                            </label>
                            <select class="custom-select mr-sm-2" id="district_id" name="district_id"
                                value="{{ old('district_id') }}" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">
                                @if($langId == 1)
                                {{"Upazila Name"}}
                                @else($langId == 2)
                                {{"Upazila Name"}}
                                @endif
                                <span class="text-danger"> *
                                </span>
                            </label>
                            <select class="custom-select mr-sm-2" id="upazila_id" name="upazila_id"
                                value="{{ old('upazila_id') }}" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Union Name<span class="text-danger"> *
                            </span></label>
                            <select class="custom-select mr-sm-2" id="union_id" name="union_id" required>
                            </select>
                        </div>
                    </div>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                    <label for="khotian_no">Khotian No:</label>
                                    <input type="text" class="form-control" name="khotian_no" value="{{old('khotian_no')}}"/>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-outline-primary update-buttn">
                            Search
                        </button>

                    </form>
<br>
        @if(!empty($khotians))
            <div class="row"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="col-6">Total Dag: {{$total_dag}}</span> <span class="col-4 float-right">Total Land: {{$total_part}} (acre)</span> <span class="col-4"></span> </div>
            <div class="col-12">
                <div class="user-section mt-0">
                    <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>
                                <th class="user-thead">
                                    Dag No
                                </th>
                                <th class="user-thead">
                                    Class
                                </th>
                                <th class="user-thead">
                                    Part
                                </th>
                               @if($super == 1)
                                <th class="user-thead">
                                    Status
                                </th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($khotians as $key=>$khotian)
                            <tr>
                                <td>{{$khotian['dag_no']}}</td>
                                <td>{{$khotian['type']}}</td>
                                <td>{{$khotian['land_part']}}</td>
                                @if($super == 1)
                                <td>
                                    <a class="btn is-light btn-outline-warning cust-usr-btn edit" type="button" title="Edit" href="{{url('edit-dag-report', $khotian['id'])}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>
                                    <form action="" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>

                                    </form>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

            <!-- Khotial Modal  -->
            <!-- <div id="modalSubscriptionForm_s"> -->
                <!-- <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Information</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-4">
                        <label data-error="wrong" data-success="right" for="form2">Dag No</label>
                        <input type="number" id="form3" class="form-control validate">
                        </div>

                        <div class="md-form mb-4">
                        <label data-error="wrong" data-success="right" for="form2">Class</label>
                        <input type="text" id="form2" class="form-control validate">
                        </div>

                        <div class="md-form mb-4">
                        <label data-error="wrong" data-success="right" for="form2">Part</label>
                        <input type="text" id="form2" class="form-control validate">
                        </div>

                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-indigo">Submit<i class="fas fa-paper-plane-o ml-1"></i></button>
                    </div>
                    </div>
                </div> -->
            <!-- </div> -->

            <!-- End Khotial Modal  -->


            <div class="col-12">
                <div class="user-section mt-0">
                    <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>
                                <th class="user-thead">
                                    Serial
                                </th>
                                <th class="user-thead">
                                    Owner Name
                                </th>
                                <th class="user-thead">
                                    Father's Name
                                </th>
                                <th class="user-thead">
                                    Part (Let Total 1)
                                </th>
                                @if($super == 1)
                                <th class="user-thead">
                                    Status
                                </th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @php $serial = 1; @endphp
                        @foreach($owners as $key=>$owner)
                            <tr>
                                <td>{{ $serial++ }}</td>
                                <td>{{ $owner['owner_name'] }}</td>
                                <td>{{ $owner['father_name'] }}</td>
                                <td>{{ $owner['part'] }}</td>
                                @if($super == 1)
                                <td>
                                    <a class="btn is-light btn-outline-warning cust-usr-btn edit2" type="button" title="Edit" href="{{url('edit-owner-report', $owner['id'])}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>
                                    <form action="" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>

                                    </form>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extraJS')
<script>
$(document).on('click', '#edit', function(){
    var modal = '<div class="modal fade" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
                +'<div class="modal-dialog" role="document">'
                    +'<div class="modal-content">'
                    +'<div class="modal-header text-center">'
                        +'<h4 class="modal-title w-100 font-weight-bold">Edit Information</h4>'
                        +'<button type="button" class="close" aria-label="Close">'
                        +'<span aria-hidden="true">&times;</span>'
                        +'</button>'
                    +'</div>'
                    +'<div class="modal-body mx-3">'
                        +'<div class="md-form mb-4">'
                        +'<label data-error="wrong" data-success="right" for="form2">Dag No</label>'
                        +'<input type="number" id="form3" class="form-control validate">'
                        +'</div>'

                        +'<div class="md-form mb-4">'
                        +'<label data-error="wrong" data-success="right" for="form2">Class</label>'
                        +'<input type="text" id="form2" class="form-control validate">'
                        +'</div>'

                        +'<div class="md-form mb-4">'
                        +'<label data-error="wrong" data-success="right" for="form2">Part</label>'
                        +'<input type="text" id="form2" class="form-control validate">'
                        +'</div>'

                    +'</div>'
                    +'<div class="modal-footer d-flex justify-content-center">'
                        +'<button class="btn btn-indigo">Submit<i class="fas fa-paper-plane-o ml-1"></i></button>'
                    +'</div>'
                    +'</div>'
                +'</div></div>';
    $('#modalSubscriptionForm_s').html(modal);
});

$(document).on('change', '.close', function() {
    console.log("OK");
    $('#modalSubscriptionForm_s').parent().remove();
});


$(document).on('change', '#division_id', function() {
        var DivissionId = $(this).val();
         var data = {
            "_token": "{{ csrf_token() }}",
            divissionId: DivissionId
        };
        //alert(DivissionId);
        $.ajax({
            type: "POST",
            url: "{{ url('getDistrictInfos') }}",
            data: data,
            success: function(data) {
                //console.log(data);
                if(langId == 1){
                    var district = '<option value="" ></option>';
                }else{
                    var district = '<option value="" ></option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });

    $(document).on('change', '#district_id', function() {
        var DistrictId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            districtId: DistrictId
        };
        //alert(DistrictId);
        $.ajax({
            type: "post",
            url: "{{ url('getUpazliaInfo') }}",
            data: data,
            success: function(data) {
                //console.log(data);

                if(langId == 1){
                    var upazila = '<option value="" ></option>';
                }else{
                    var upazila = '<option value="" ></option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });

    $(document).on('change', '#upazila_id', function(){
        //--############ FOR Load Union Data---------------------------
        var upazilaId = $(this).val();
        var url = "{{ url('/getUnionInfos') }}";
        var union_name = '';
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        console.log('upazila_id' + upazila_id);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var union = '';
            var selected = '';
            union += '<option value=""></option>';

            $.each(response, function(i, item) {

                if (langId == 1) {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#union_id').html(union);
        });
    });
</script>
@endsection
