@extends('layouts.app')

@section('content')
@php

 $langId = Cookie::get('langId');

$data['division_en'] = DB::table('geo_divisions')->pluck('division_name_eng', 'id')->toArray();
$data['division_bn'] = DB::table('geo_divisions')->pluck('division_name_bng', 'id')->toArray();
$data['district_en'] = DB::table('geo_districts')->pluck('district_name_eng', 'id')->toArray();
$data['district_bn'] = DB::table('geo_districts')->pluck('district_name_bng', 'id')->toArray();
$data['upazilas_en'] = DB::table('geo_upazilas')->pluck('upazila_name_eng', 'id')->toArray();
$data['upazilas_bn'] = DB::table('geo_upazilas')->pluck('upazila_name_bng', 'id')->toArray();

 @endphp

   <div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Add Land"}}
                        @else($langId == 2)
                        {{""}}
                        @endif
                    </h3>
                    <br>
                    @if (Session::has('errors'))
                    <hr>
                    <div class="alert alert-danger">{{ Session::get('errors') }}</div>
                    <hr>
                    @endif
                    @if (Session::has('success'))
                    <hr>
                    <div class="alert alert-info">{{ Session::get('success') }}</div>
                    <hr>
                    @endif


                    <form method="post" action="{{ url('khotian/store') }}">
                        @csrf
                        <div class="row">
                        <div class="form-group col-md-6">
                            <label for="">
                                @if($langId == 1)
                                {{"Division Name"}}
                                @else($langId == 2)
                                {{"Division Name"}}
                                @endif
                                <span class="text-danger"> *
                                </span>
                            </label>
                            <select class="custom-select mr-sm-2 cust-width" id="division_id" name="division_id" required>
                                @if($langId == 1)
                                {!! getSelectOptions($data['division_en'], old('division_id')) !!}
                                @endif
                                @if($langId == 2)
                                {!! getSelectOptions($data['division_bn'], old('division_id')) !!}
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                             <label for="">
                                @if($langId == 1)
                                {{"District Name"}}
                                @else($langId == 2)
                                {{"District Name"}}
                                @endif
                                <span class="text-danger"> *
                                </span>
                            </label>
                            <select class="custom-select mr-sm-2" id="district_id" name="district_id"
                                value="{{ old('district_id') }}" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">
                                @if($langId == 1)
                                {{"Upazila Name"}}
                                @else($langId == 2)
                                {{"Upazila Name"}}
                                @endif
                                <span class="text-danger"> *
                                </span>
                            </label>
                            <select class="custom-select mr-sm-2" id="upazila_id" name="upazila_id"
                                value="{{ old('upazila_id') }}" required>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Union Name<span class="text-danger"> *
                            </span></label>
                            <select class="custom-select mr-sm-2" id="union_id" name="union_id" required>
                            </select>
                        </div>
                    </div>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                    <label for="khotian_no">Khotian No:</label>
                                    <input type="text" class="form-control" name="khotian_no" />
                            </div>
                        </div>
                        <span class="btn btn-info" id="addLandInfo">Add Land info</span>

                        <div id="landInfosArea">

                        </div>
                        <span class="btn btn-info" id="addLandOwner">Add Land Owner</span>
                        <div id="landOwnersArea">

                        </div>

                        <button type="submit" class="btn btn-outline-primary update-buttn">
                            Submit
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extraJS')
<script>
    $(document).ready(function(){
        $('#addLandInfo').on('click', function(){
            var text = '';
            text += '<div class="form-row">'
                    +'<div class="form-group col-md-3">'
                    +'<label for="dag_no">Dag No</label>'
                    +'<input type="text" class="form-control" name="dag_no[]" />'
                    +'</div>'
                    +'<div class="form-group col-md-3">'
                    +'<label for="class"> Class </label>'
                    +'<input type="text" class="form-control" name="class[]" />'
                    +'</div>'
                    +'<div class="form-group col-md-3">'
                    +'<label for="area_of_land">Area of land</label>'
                    +'<input type="text" class="form-control" name="area_of_land[]" />'
                    +'</div>';
            text += '<div class="form-group col-md-3"><label for="">&nbsp;</label><span class="form-control btn btn-danger" id="removeInfo">Remove</span></div>';

            $('#landInfosArea').append(text);
        });

        $('#addLandOwner').on('click', function(){
            var text = '';
            text += '<div class="form-row rm">'
                    +'<div class="form-group col-md-3">'
                    +'<label for="owner_name">Owner Name</label>'
                    +'<input type="text" class="form-control" name="owner_name[]" />'
                    +'</div>'
                    +'<div class="form-group col-md-3">'
                    +'<label for="father_name"> Father\'s Name</label>'
                    +'<input type="text" class="form-control" name="father_name[]" />'
                    +'</div>'
                    +'<div class="form-group col-md-3">'
                    +'<label for="part_of_land">Part of land</label>'
                    +'<input type="text" class="form-control" name="part_of_land[]" />'
                    +'</div>';
            text += '<div class="form-group col-md-3"><label for="">&nbsp;</label><span class="form-control btn btn-danger" id="removeOwner">Remove</span></div>';

            $('#landOwnersArea').append(text);
        });

        $(document).on('click', '#removeOwner', function(){
            $(this).parent().parent().remove();
        });
        $(document).on('click', '#removeInfo', function(){
            $(this).parent().parent().remove();
        });


        $(document).on('change', '#division_id', function() {
        var DivissionId = $(this).val();
         var data = {
            "_token": "{{ csrf_token() }}",
            divissionId: DivissionId
        };
        //alert(DivissionId);
        $.ajax({
            type: "POST",
            url: "{{ url('getDistrictInfos') }}",
            data: data,
            success: function(data) {
                //console.log(data);
                if(langId == 1){
                    var district = '<option value="" ></option>';
                }else{
                    var district = '<option value="" ></option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });

    $(document).on('change', '#district_id', function() {
        var DistrictId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            districtId: DistrictId
        };
        //alert(DistrictId);
        $.ajax({
            type: "post",
            url: "{{ url('getUpazliaInfo') }}",
            data: data,
            success: function(data) {
                //console.log(data);

                if(langId == 1){
                    var upazila = '<option value="" ></option>';
                }else{
                    var upazila = '<option value="" ></option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });

    $(document).on('change', '#upazila_id', function(){
        //--############ FOR Load Union Data---------------------------
        var upazilaId = $(this).val();
        var url = "{{ url('/getUnionInfos') }}";
        var union_name = '';
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        console.log('upazila_id' + upazila_id);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var union = '';
            var selected = '';
            union += '<option value=""></option>';

            $.each(response, function(i, item) {

                if (langId == 1) {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#union_id').html(union);
        });
    });

    });
</script>
@endsection
