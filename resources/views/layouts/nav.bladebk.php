<nav class="navbar navbar-expand-lg navbar-light">
<div class="container-fluid">
<a class="navbar-brand" href="#">
    <img src="{{asset('theme/./images/youth-logo.png')}}" alt="logo" class="img-fluid">
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <div class="nav-heading">
        <h3 class="bd">Government of the People's Republic of Bangladesh
        </h3>
        <p class="mb-0 dept">Department of Youth Development
        </p>
        <p class="mb-0 nav-data">National Service Programme (NSP)</p>
    </div>
    <ul class="navbar-nav ml-auto second-nav">
        <li class="nav-item dropdown">
            <a class="nav-link text-purple" href="#" id="navbarDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <!--  ইউজার ম্যানেজমেন্ট --> Users & Role <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
            </a>
            <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                <a class="dropdown-item text-purple" href="{{route('users.index')}}"><!-- ইউজার ম্যানেজমেন্ট 1 --> User Manage</a>
                <a class="dropdown-item text-purple" href="{{route('roles.index')}}"><!-- ইউজার ম্যানেজমেন্ট --> User Roles</a>
                <a class="dropdown-item text-purple" href="{{route('permissions.index')}}"><!-- ইউজার ম্যানেজমেন্ট --> User Permissions</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link text-purple" href="#" id="navbarDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <!-- ডাটা এন্ট্রি --> Data Entry<span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
            </a>
            <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                <a class="dropdown-item text-purple" href="{{route('applications.index')}}"><!-- ডাটা এন্ট্রি 1 --> Application</a>
                <a class="dropdown-item text-purple" href="#"><!-- ডাটা এন্ট্রি 1 --> Attactasment</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link text-purple" href="#" id="navbarDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                রিপোর্ট <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
            </a>
            <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                <a class="dropdown-item text-purple" href="#">রিপোর্ট 1</a>
                <a class="dropdown-item text-purple" href="#">রিপোর্ট 2</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link text-purple" href="#" id="navbarDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="changeIcon()">
                সেটিংস <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
            </a>
            <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                <a class="dropdown-item text-purple" href="#"><span><i class="fas fa-cog"></i></span> সেটিংস
            1</a>
            <a class="dropdown-item text-purple" href="#"><span><i class="fas fa-cog"></i></span>সেটিংস
        2</a>
    </div>
</li>

<li class="nav-item dropdown">
    <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
        <!-- Authentication Links -->
        @guest
        <li class="nav-item">
            <a class="nav-link" href="{{route('login')}}">{{ ucfirst(config('multiauth.prefix')) }} Login</a>
        </li>
        @else
        <li class="nav-item dropdown">
            <a class="nav-link text-purple dropdown-toggle custom-small-pic " href="#" id="navbarDropdown"
                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="{{asset('theme/images/profile.jpg')}}" alt="profile pic">
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <hr class="navbar-divider">
                <a href="{{route('logout')}}" class="navbar-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <span class="icon">
                        <i class="fa fa-fw fa-sign-out m-r-5"></i>
                    </span>
                    Logout
                </a>
                @include('_includes.forms.logout')
            </div>
        </li>
        @endguest
    </div>
</li>
</ul>
</div>
</div>
</nav>