<style type="text/css">
    .langSelect{background:  #662D91 ;}

.text {
  position: relative;
  font-family: sans-serif;
  text-transform: uppercase;
  /* font-size: 4em;
  letter-spacing: 4px;
  overflow: hidden;
  background: linear-gradient(90deg, #000, #fff, #000);
  background-repeat: no-repeat;
  background-size: 80%;
  animation: animate 1s linear infinite;
  -webkit-background-clip: text;
  -webkit-text-fill-color: rgba(255, 255, 255, 0); */
}

@keyframes animate {
  0% {
    background-position: -500%;
  }
  100% {
    background-position: 500%;
  }
}
</style>

<div class="green-bg">
  <!-- <div class="container-fluid"> -->
    <div class="container-fluid text-white">
      <div class="row">
        <div class="col-6">
          <p class="text">
            @if(isset($langId))
            @if($langId == 1)
              {{"Bangladesh Land Information"}}
            @else($langId == 2)
              {{"Bangladesh Land Information"}}
            @endif
            @else
              {{"Bangladesh Land Information"}}
            @endif

          </p>
        </div>

        <div class="col-6">
          <div class="top-left-content">
            <p class="bangla-date">
            @if(isset($langId))
              @if($langId == 1)
                @php
                  $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                @endphp
                {{ $dt->format('F j, Y, g:i A') }}
              @else($langId != 1)
                @php
                  $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                @endphp
                {{ $dt->format('F j, Y, g:i A') }}
              @endif
            @endif
          </p>

            <!-- <a href="{{url('change-language/' . 1)}}" @if($langId == 1) class="langSelect" @endif><button class="btn cust-lang-btn stylebutton" >English</button></a> -->
            <!-- <a href="{{url('change-language/' . 2)}}" @if($langId == 2) class="langSelect" @endif><button class="btn cust-lang-btn stylebutton" >বাংলা</button></a> -->
          </div>
        </div>
      </div>
    </div>
  <!-- </div> -->
</div>

<script type="text/javascript">
  var langId = "{{ $langId }}";
  if(langId == 1){
   document.getElementById("engBtn").setAttribute("style", "background-color: #662d91;");
  }else{
    document.getElementById("bngBtn").setAttribute("style", "background-color: #662d91;");
  }
</script>
