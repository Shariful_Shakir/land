<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.css')
    <body>
        @php
            $url = request()->segment(count(request()->segments()))
        @endphp

        @include('layouts.header')

        @include('layouts.nav')

        <div class="management-area" id="app">
            <div id="loading">
                <img id="loading-image" src="{{ asset('img/loader.gif') }}" alt="Loading..." />
            </div>
          @yield('content')
        </div>
        @include('layouts.footer')
        @include('layouts.js')
        <!-- =========== ============================= ================ -->
        @yield('extraJS')
        @php
            $langId = Cookie::get('langId');
            if(empty($langId)){
                Cookie::queue(Cookie::make('langId', 1, 3600));
            }
        @endphp

        <script>
        var langId = '<?=$langId?>'
        $(".delete").on("submit", function(){
			return confirm("Are you sure?");
        });

        $(document).ready(function() {
            $('#loading').hide();
        });
        </script>

        <!-- For Laratrust -->
		<script src="{{ asset('laratrust/js/app.js') }}"></script>

    @include('_includes.notifications.toast')
    @yield('scripts')
    </body>
</html>