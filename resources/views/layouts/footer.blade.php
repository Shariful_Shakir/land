<footer class="footer-bg">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="copyright text-white">
          <span class="footerFont"><i class="far fa-copyright"></i>
            @if(isset($langId))
            @if($langId == 1)
            {{"All right reserved to Department of Land Information"}}
            @else($langId == 2)
            {{"All right reserved to Department of Land Information"}}
            @endif
            @else
              {{"All right reserved to Department of Land Information"}}
            @endif
          </span>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="copyright text-white">
          <p class="copyright-right-text mb-0 footerFont">
            @if(isset($langId))
            @if($langId == 1)
            {{"Developed & maintanance by "}}<a href='' target=''>Sohag Roy</a>
            @else($langId == 2)
            {{" by "}}<a href='' target=''>Sohag Roy</a>
            @endif
            @else
              {{"Developed & maintanance by "}}<a href='' target=''>Sohag Roy</a>
            @endif
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
