<head>
  <!-- Start New Them by Asad -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


  {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">--}}

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
    integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">

  <link rel="stylesheet" href="{{asset('theme/css/animate.css')}}">

  <link rel="stylesheet" href="{{asset('theme/css/profile.css')}}">

  <link rel="stylesheet" href="{{asset('css/common.css')}}">

  <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- End Theme by Asad -->

  <meta name="_token" content="{{ csrf_token() }}" />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
  @php
    $langId = Cookie::get('langId');
    if($langId == 2){
     echo 'Land';
    }else{
      echo 'Land';
    }
  @endphp
        </title>
  <!-- <title>{{ config('app.name', 'Youth Development') }}</title> -->
  <!-- Scripts -->
  <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('fonts/solaimanLipi/fonts.css') }}">

  <!-- Laratrust -->

  <!-- <link href="{{-- asset('laratrust/css/app.css') --}}" rel="stylesheet"> -->
  <link rel="icon" href="" type="image/gif" sizes="16x16">
  @yield('styles')

  <!-- End Laratrust -->
</head>
