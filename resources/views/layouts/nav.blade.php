@guest

@else
@php
$super = Cookie::get('super');
$role = Cookie::get('role');
$name = Cookie::get('name');
 @endphp
<nav class="navbar navbar-expand-lg navbar-light pt-0 pb-0">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="{{asset('theme/images/youth-logo.png')}}" alt="profile pic">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="nav-heading">
                <h3 class="bd">
                @if($langId == 1)
                {{"Bangladesh Land Information Service"}}
                @else($langId == 2)
                {{""}}
                @endif
                </h3>
                <p class="mb-0 dept">
                    @if($langId == 1)
                    {{"We can find our land info    "}}
                    @else($langId == 2)
                    {{""}}
                    @endif
                </p>
            </div>
            <ul class="navbar-nav ml-auto second-nav">
                <a class="nav-link text-purple @if(Request::segment(1) == 'home') nav-select @endif" href="{{Route('home')}}" >
                    @if($langId == 1)
                    {{"Home"}}
                    @else($langId == 2)
                    {{""}}
                    @endif
                </a>
                <li class="nav-item dropdown">
                    <a class="nav-link text-purple @if(Request::segment(1) == 'khotian-data-entry') nav-select @endif" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" >
                        @if($langId == 1)
                        {{"More"}}
                        @else($langId == 2)
                        {{""}}
                        @endif
                        <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
                    </a>
                    <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                    @if($super != 1)
                        <a class="dropdown-item text-purple" href="{{url('user-khotian-info')}}">
                            @if($langId == 1)
                            {{"Khotian"}}
                            @else($langId == 2)
                            {{"Khotian"}}
                            @endif
                        </a>
                        <a class="dropdown-item text-purple" href="{{url('khotian/report')}}">
                            @if($langId == 1)
                            {{"Search Global Khotian"}}
                            @else($langId == 2)
                            {{"Search Global Khotian"}}
                            @endif
                        </a>
                    @else
                        <a class="dropdown-item text-purple" href="{{url('khotian-data-entry')}}">
                            @if($langId == 1)
                            {{"Data Entry"}}
                            @else($langId == 2)
                            {{"Data Entry"}}
                            @endif
                        </a>
                    @endif
                        <!-- <a class="dropdown-item text-purple" href="{{route('roles.index')}}">
                            @if($langId == 1)
                            {{"Roles Management"}}
                            @else($langId == 2)
                            {{"রোল ম্যানেজমেন্ট"}}
                            @endif
                        </a>
                        <a class="dropdown-item text-purple" href="{{route('permissions.index')}}">
                            @if($langId == 1)
                            {{"Permissions"}}
                            @else($langId == 2)
                            {{"পারমিশন ম্যানেজমেন্ট"}}
                            @endif
                        </a> -->

                    </div>
                </li>
                <li class="nav-item dropdown">

                    <!-- <a class="nav-link text-purple @if(Request::segment(1) == 'applications' || Request::segment(1) == 'attachments') nav-select @endif" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        @if($langId == 1)
                        {{"Data Entry"}}
                        @else($langId == 2)
                        {{"ডাটা এন্ট্রি "}}
                        @endif
                        <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
                    </a>
                    <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-purple" href="{{ route('applications.create') }}">
                            @if($langId == 1)
                            {{"Application"}}
                            @else($langId == 2)
                            {{"আবেদন "}}
                            @endif
                        </a>
                        <a class="dropdown-item text-purple" href="{{ route('attachments.create') }}">
                            @if($langId == 1)
                            {{"Attachment"}}
                            @else($langId == 2)
                            {{"সংযুক্তির তথ্য"}}
                            @endif
                        </a>
                    </div> -->
                </li>
                @if(!empty($super))
                <li class="nav-item dropdown">
                    <a class="nav-link text-purple @if(Request::segment(2) == 'report') nav-select @endif" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        @if($langId == 1)
                        {{"Report"}}
                        @else($langId == 2)
                        {{""}}
                        @endif
                        <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
                    </a>
                    <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-purple" href="{{url('khotian/report')}}">
                            @if($langId == 1)
                            {{"Search Khotian"}}
                            @else($langId == 2)
                            {{""}}
                            @endif
                        </a>

                        <a class="dropdown-item text-purple" href="{{route('attached.report')}}">
                            <!-- @if($langId == 1)
                            {{""}}
                            @else($langId == 2)
                            {{""}}
                            @endif -->
                        </a>

                    </div>
                </li>
                @endif
                <li class="nav-item dropdown">
                @if($super == 1)
                    <a class="nav-link text-purple @if(Request::segment(1) != 'home' && Request::segment(1) != 'khotian-data-entry' && Request::segment(1) != 'applications' && Request::segment(2) != 'report' && Request::segment(1) != 'attachments') nav-select @endif" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" onclick="changeIcon()">
                        @if($langId == 1)
                        {{"Setting"}}
                        @else($langId == 2)
                        {{"সেটিংস"}}
                        @endif
                        <span class="cust-icon"><i class="fas fa-chevron-down"></i></span>
                    </a>
                    <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-purple" href="{{route('divisions.index')}}"><span><i class="fas fa-cog"></i></span>
                        @if($langId == 1)
                        {{"Division"}}
                        @else($langId == 2)
                        {{"বিভাগ"}}
                        @endif
                        </a>
                    <a class="dropdown-item text-purple" href="{{route('districts.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"District"}}
                    @else($langId == 2)
                    {{"জেলা"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('upazilas.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Upazila"}}
                    @else($langId == 2)
                    {{"উপজেলা"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('unions.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Union"}}
                    @else($langId == 2)
                    {{"ইউনিয়ন"}}
                    @endif
                    </a>
                    <!-- <a class="dropdown-item text-purple" href="{{route('bloodGroups.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Blood Group"}}
                    @else($langId == 2)
                    {{"রক্তের গ্রুপ"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('genders.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Gender"}}
                    @else($langId == 2)
                    {{"লিঙ্গ"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('religions.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Religion"}}
                    @else($langId == 2)
                    {{"ধর্ম"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('batchs.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Batch No"}}
                    @else($langId == 2)
                    {{"ব্যাচ নং"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('sessions.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Session"}}
                    @else($langId == 2)
                    {{"সেশন"}}
                    @endif
                    </a>
                    <a class="dropdown-item text-purple" href="{{route('educationalQualifications.index')}}"><span><i class="fas fa-cog"></i></span>
                    @if($langId == 1)
                    {{"Educational Qualification"}}
                    @else($langId == 2)
                    {{"শিক্ষাগত যোগ্যতা"}}
                    @endif
                    </a> -->
            </div>
            @endif
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link text-purple custom-small-pic" href="#" id="navbarDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <img src="{{asset('theme/images/profile.jpg')}}" alt="profile pic">
                <!-- <span class="user-name"></span> -->
                <i class="fas fa-chevron-down"></i>
            </a>
            <!-- <hr class="navbar-divider"> -->

            <div class="dropdown-menu animated fadeInDown" aria-labelledby="navbarDropdown">
                <p style="text-align:center">{{$role.': '.$name}}</p>
                <a class="dropdown-item text-purple" href="{{Route('user.profile')}}"><span><i class="fas fa-cog"></i></span>
                @if($langId == 1)
                {{"User Infomation"}}
                @else($langId == 2)
                {{"ইউজারের তথ্য"}}
                @endif
            </a>
            <!-- <a class="dropdown-item text-purple" href="{{route('logout')}}"><span><i
            class="fas fa-sign-out-alt"></i></span>লগ আউট</a> -->
            <!--  <hr class="navbar-divider"> -->
            <a href="{{route('logout')}}" class="dropdown-item text-purple" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <span><i
                class="fas fa-sign-out-alt"></i></span>
                @if($langId == 1)
                {{"Log Out"}}
                @else($langId == 2)
                {{"লগ আউট"}}
                @endif
            </a>
            @include('_includes.forms.logout')
        </div>
        @endguest
    </li>

</ul>
</div>
</div>
</nav>
