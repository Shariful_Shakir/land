@extends('layouts.app')
@section('content')

<div class="graph-section">
    <div class="container-fluid">

                <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 com-lg-12">
                    <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">
                                @if($langId == 1)
                                {{"Beneficiaries personal information"}}
                                @else($langId == 2)
                                {{"সুবিধাভোগীদের ব্যাক্তিগত তথ্য"}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-6">
                            <div class="float-right">
                                <div class="d-inline pdf-btn">
                                        <form class="d-inline" action="{{route('application.print', 'excel')}}" method="post" enctype="multipart/form-data" target="_black">
                                        @csrf
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_division_id']))
                                            {{$conditions['present_division_id']}}
                                            @endif
                                            " name="present_division_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_district_id']))
                                            {{$conditions['present_district_id']}}
                                            @endif" name="present_district_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_upazila_id']))
                                            {{$conditions['present_upazila_id']}}
                                            @endif" name="present_upazila_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['batch_no']))
                                            {{$conditions['batch_no']}}
                                            @endif" name="batch_no" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['session_id']))
                                            {{$conditions['session_id']}}
                                            @endif" name="session_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['contact_no']))
                                            {{$conditions['contact_no']}}
                                            @endif" name="contact_no" />
                                        <input type="hidden" name="filter">

                                        <button tupe="submit" class="report-icn btn-sm"><span><i class="fas fa-file-pdf"></i></span>
                                             @if($langId == 1)
                                                {{"Excel"}}
                                                @else($langId == 2)
												{{"এক্সেল"}}
                                            @endif
                                        </button>
                                    </form>
                                </div>
								<div class="d-inline pdf-btn">
                                        <form class="d-inline" action="{{route('application.pdf', 'pdf')}}" method="post" enctype="multipart/form-data" target="_black">
                                        @csrf
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_division_id']))
                                            {{$conditions['present_division_id']}}
                                            @endif
                                            " name="present_division_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_district_id']))
                                            {{$conditions['present_district_id']}}
                                            @endif" name="present_district_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_upazila_id']))
                                            {{$conditions['present_upazila_id']}}
                                            @endif" name="present_upazila_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['batch_no']))
                                            {{$conditions['batch_no']}}
                                            @endif" name="batch_no" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['session_id']))
                                            {{$conditions['session_id']}}
                                            @endif" name="session_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['contact_no']))
                                            {{$conditions['contact_no']}}
                                            @endif" name="contact_no" />
                                        <input type="hidden" name="filter">

                                        <button tupe="submit" class="report-icn btn-sm" target="_black"><span><i class="fas fa-file-pdf"></i></span>
                                             @if($langId == 1)
                                                {{"PDF"}}
                                                @else($langId == 2)
												{{"পিডিএফ"}}
                                            @endif
                                        </button>
                                    </form>
                                </div>
                                <div class="d-inline pdf-btn">
                                    <form class="d-inline" action="{{route('application.print', 'print')}}" method="post" enctype="multipart/form-data" target="black">
                                        @csrf
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_division_id']))
                                            {{$conditions['present_division_id']}}
                                            @endif
                                            " name="present_division_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_district_id']))
                                            {{$conditions['present_district_id']}}
                                            @endif" name="present_district_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['present_upazila_id']))
                                            {{$conditions['present_upazila_id']}}
                                            @endif" name="present_upazila_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['batch_no']))
                                            {{$conditions['batch_no']}}
                                            @endif" name="batch_no" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['session_id']))
                                            {{$conditions['session_id']}}
                                            @endif" name="session_id" />
                                            <input type="hidden" value="
                                            @if(!empty($conditions['contact_no']))
                                            {{$conditions['contact_no']}}
                                            @endif" name="contact_no" />
                                        <input type="hidden" name="filter">
                                        <input type="hidden" name="printStatus" value="2">
                                            <button class="report-icn btn-sm">
                                                <i class="fas fa-print"></i> <span>
                                                     @if($langId == 1)
                                                        {{"Print"}}
                                                        @else($langId == 2)
                                                        {{"প্রিন্ট"}}
                                                    @endif
                                                <span>
                                            </button>
                                    </form>
                                            <!-- <button class="report-icn btn-sm">
                                                <i class="fas fa-file-excel"></i> <span>
                                                     @if($langId == 1)
                                                        {{"Excel"}}
                                                        @else($langId == 2)
                                                        {{"এক্সেল"}}
                                                    @endif
                                                </span>
                                            </button> -->
                                </div>
                             </div>

                        </div>
                    </div>
                </div>
                </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <div class="table-responsive">
                        <table class="table table-bordered report-table" id="dataTable">
                            <thead>

                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Traking No"}}
                                        @else($langId == 2)
                                        {{"ট্রাকিং নং"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Applicant Name"}}
                                        @else($langId == 2)
                                        {{"উপকারভোগীর নাম"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Father/ Husband Name"}}
                                        @else($langId == 2)
                                        {{"পিতা/স্বামীর নাম"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Present Address"}}
                                        @else($langId == 2)
                                        {{"বর্তমান ঠিকানা"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Educational Qualification"}}
                                        @else($langId == 2)
                                        {{"শিক্ষাগত যোগ্যতা"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Birth-Date"}}
                                        @else($langId == 2)
                                        {{"জন্ম তারিখ"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Traning Subjects"}}
                                        @else($langId == 2)
                                        {{"প্রশিক্ষনের বিষয়সমূহ"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"National Id No"}}
                                        @else($langId == 2)
                                        {{"জাতীয় পরিচয়পত্র নং"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Mobile No"}}
                                        @else($langId == 2)
                                        {{"মোবাইল নং"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"E-mail"}}
                                        @else($langId == 2)
                                        {{"ই-মেইল"}}
                                        @endif
                                    </th>
                                    <th scope="col">
                                        @if($langId == 1)
                                        {{"Blood Group"}}
                                        @else($langId == 2)
                                        {{"রক্তের গ্রুপ"}}
                                        @endif
                                    </th>
                                </tr>
                            </thead>

                            <tbody>



                                @php $i= 0; @endphp
                                @forelse($applicationsReport as $row)

                                @if($langId == 1)
                                <tr>
                                    <th scope="row">{{ ++$i }}</th>
                                    <td> {{ $row->tracking_no }} </td>
                                    <td> {{ $row->name }} </td>
                                    <td> {{ $row->father_name }} </td>
                                    <td>
                                        {{ $row->present_village_id }},
                                        {{ $data['union_en'][$row->present_union_id] }},
                                        {{ $data['upazilas_en'][$row->present_upazila_id] }},
                                        {{ $data['district_en'][$row->present_district_id] }},
                                        {{ $data['division_en'][$row->present_division_id] }}
                                    </td>
                                    <td> {{ $data['educational_qualification_en'][$row->educational_qualification] }} </td>
                                    <td> {{ $row->date_of_birth }} </td>
                                    <td> {{ $row->training_subject_description }} </td>
                                    <td> {{ $row->national_id }} </td>
                                    <td> {{ $row->contact_no }} </td>
                                    <td> {{ $row->email }} </td>
                                    <td> {{ $data['blood_groups_en'][$row->blood_group] }} </td>
                                </tr>
                                @else($langId == 2)
                                <tr>
                                    <th scope="row">{{ ++$i }}</th>
                                    <td> {{ $row->tracking_no }} </td>
                                    <td> {{ $row->name }} </td>
                                    <td> {{ $row->father_name }} </td>
                                    <td>
                                        {{ $row->present_village_id }},
                                        {{ $data['union_bn'][$row->present_union_id] }},
                                        {{ $data['upazilas_bn'][$row->present_upazila_id] }},
                                        {{ $data['district_bn'][$row->present_district_id] }},
                                        {{ $data['division_bn'][$row->present_division_id] }}
                                    </td>
                                    <td> {{ $data['educational_qualification_bn'][$row->educational_qualification] }} </td>
                                    <td> {{ $row->date_of_birth }} </td>
                                    <td> {{ $row->training_subject_description }} </td>
                                    <td> {{ $row->national_id }} </td>
                                    <td> {{ $row->contact_no }} </td>
                                    <td> {{ $row->email }} </td>
                                    <td> {{ $data['blood_groups_bn'][$row->blood_group] }} </td>
                                </tr>
                                @endif
                                @empty
                                <tr>
                                    <td colspan="12" class="text-center">
                                         @if($langId == 1)
                                        {{"No Data Available"}}
                                        @else($langId == 2)
                                        {{"কোন ডাটা নাই "}}
                                        @endif
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>

                        </table>

                    </div>
                    <br>
                    <div class="col-sm-12 text-right text-center-xs">
                        <div class="pagination-sm m-t-none m-b-none" style="position:absolute;">
                            {{$applicationsReport->links()}}
                        </div>
                    </div>
                     <!-- <div class="row">
                         <div class="pagignation-sector">
                                <div aria-label="Page navigation example">
                                    <ul class="pagination my-3 justify-content-center">
                                        <li class="page-item custom-item"><a class="page-link" href="#">Prev</a>
                                        </li>
                                        <li class="page-item custom-item active"><a class="page-link" href="#">1</a>
                                        </li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">6</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                     </div> -->
                  <div class="row">
                    <div class="col-12 text-right">
                          <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                @if($langId == 1)
                                {{"Back"}}
                                @else($langId == 2)
                                {{" পিছনে"}}
                                @endif
                            </a>
                  </div>
                  </div>


                </div>


                </div>
            </div>
        </div>

    </div>
</div>


@endsection
<script>
function excel(){
    window.print();
}
</script>
