@extends('layouts.app')
@section('content')

<div class="main-panel">
  <div class="content-wrapper">

    <div class="card">
      <div class="card-header">
        Manage User
      </div>
      <div class="card-body">
        <table class="table table-hover table-striped"  id="dataTable">
            <thead>

                <tr>
                        <th scope="col">#</th>
                        <th scope="col">
                            <span class="langBn">ট্রাকিং নং</span>
                            <span class="langEn">Traking No</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">উপকারভোগীর নাম</span>
                            <span class="langEn">Applicant Name</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">পিতা/স্বামীর নাম</span>
                            <span class="langEn">Father/ Husband Name</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">বর্তমান ঠিকানা</span>
                            <span class="langEn">Present Address</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">শিক্ষাগত যোগ্যতা</span>
                            <span class="langEn">Educational Qualification</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">জন্ম তারিখ</span>
                            <span class="langEn">Birth-Date</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">প্রশিক্ষনের বিষয়সমূহ</span>
                            <span class="langEn">Traning Subjects</span>
                        </th>
                        <th scope="col">
                            <span class="langBn">জাতীয় পরিচয়পত্র নং</span>
                            <span class="langEn">National Id No</span>
                        </th>
                        <th scope="col"><span class="langBn">মোবাইল নং</span>
                        <span class="langEn">Mobile</span>
                    </th>
                    <th scope="col">
                        <span class="langBn">ই-মেইল</span>
                        <span class="langEn">E-mail</span>
                    </th>
                    <th scope="col">
                        <span class="langBn">রক্তের গ্রুপ</span>
                        <span class="langEn">Blood Group</span>
                    </th>
                </tr>
            </thead>

          <tbody>

            

                        @php $i= 0; @endphp
                        @forelse($applicationsReport as $row)
                        <tr class="langEn">
                            <th scope="row">{{ ++$i }}</th>
                            <td> {{ $row->tracking_no }} </td>
                            <td> {{ $row->name }} </td>
                            <td> {{ $row->father_name }} </td>
                            <td>
                                {{ $row->present_village_id }},
                                {{ $data['union_en'][$row->present_union_id] }},
                                {{ $data['upazilas_en'][$row->present_upazila_id] }},
                                {{ $data['district_en'][$row->present_district_id] }},
                                {{ $data['division_en'][$row->present_division_id] }}
                            </td>
                            <td> {{ $data['educational_qualification_en'][$row->educational_qualification] }} </td>
                            <td> {{ $row->date_of_birth }} </td>
                            <td> {{ $row->training_subject_description }} </td>
                            <td> {{ $row->national_id }} </td>
                            <td> {{ $row->contact_no }} </td>
                            <td> {{ $row->email }} </td>
                            <td> {{ $data['blood_groups_en'][$row->blood_group] }} </td>
                        </tr>
                        <tr class="langBn">
                            <th scope="row">{{ ++$i }}</th>
                            <td> {{ $row->tracking_no }} </td>
                            <td> {{ $row->name }} </td>
                            <td> {{ $row->father_name }} </td>
                            <td>
                                {{ $row->present_village_id }},
                                {{ $data['union_bn'][$row->present_union_id] }},
                                {{ $data['upazilas_bn'][$row->present_upazila_id] }},
                                {{ $data['district_bn'][$row->present_district_id] }},
                                {{ $data['division_bn'][$row->present_division_id] }}
                            </td>
                            <td> {{ $data['educational_qualification_bn'][$row->educational_qualification] }} </td>
                            <td> {{ $row->date_of_birth }} </td>
                            <td> {{ $row->training_subject_description }} </td>
                            <td> {{ $row->national_id }} </td>
                            <td> {{ $row->contact_no }} </td>
                            <td> {{ $row->email }} </td>
                            <td> {{ $data['blood_groups_bn'][$row->blood_group] }} </td>
                        </tr>
                        
                        @empty
                        <tr> <td colspan="7" class="text-center"> কোন ডাটা নাই </td> </tr>
                        @endforelse
          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>
@endsection
