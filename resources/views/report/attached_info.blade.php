@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container">
        <div class="row">
            <div class="user-section">

                <div class="row">
                    <div class="col-6">
                        <h3 class="user-management">সংযুক্তির তথ্য
                            
                        </h3>
                    </div>
                    <div class="col-6">
                        <a href="{{route('users.create')}}" class="creat-usrbtn float-right btn" type="button"> Create
                            <span><i class="fas fa-user-plus"></i></span>
                        </a>
                    </div>
                </div>

            </div>
        </div>

       <div class="row">
             <div class="user-section form-section">
 
            <form>
                <div class="form-row">
                    <div class="col-3">
                        <!-- <input type="text" class="form-control" placeholder="জেলার নাম"> -->
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                        <option selected>জেলার নাম</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <!-- <input type="text" class="form-control" placeholder="উপজেলার নাম"> -->
                           <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option selected>উপজেলার নাম</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                    </div>
                     <div class="col-3">
                        <!-- <input type="text" class="form-control" placeholder="ব্যাচ নং"> -->
                           <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option selected>ব্যাচ নং</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                    </div>
                    <div class="col-3">
                        <!-- <input type="text" class="form-control" placeholder="সেশন"> -->
                           <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option selected>সেশন</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                    </div>
                </div>
            </form>

        </div>
       </div>

        <div class="row">
            <div class="card">
                <div class="card-content">
                    <table class="table table-bordered">
                        <thead>
                            <tr>

                                <th scope="col">ক্রমিক নং</th>
                                <th scope="col">ট্রাকিং নং</th>
                                <th scope="col">উপকারভোগীর নাম</th>
                                <th scope="col">পিতার নাম</th>
                                <th scope="col">সংযুক্তি প্রাপ্ত দপ্তরের নাম</th>
                                <th scope="col">দপ্তরে যোগদানের তারিখ</th>
                                <th scope="col">সংযুক্তির শেষ তারিখ</th>
                                <th scope="col">মোট আহরিত অথর্ের ‍পরিমান</th>
                                <th scope="col">বর্তমান অবস্থান</th>
                                <th scope="col">মোবাইল নং</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Mark</td>
                                <td>Admin</td>
                                <td>Active</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                               
                            </tr>
                            <tr>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                              
                            </tr>
                            <tr>
                                <td>@mdo</td>
                                <td>Larry the Bird</td>
                                <td>@twitter</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                                <td>@mdo</td>
                               
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>

@endsection