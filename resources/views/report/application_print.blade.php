@extends('layouts.css')

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('img/fabicon.png') }}" type="image/gif" sizes="16x16">

    <!-- Bootstrap CSS -->

<style>
    @media print
    {
        html
        {
            transform: scale(1);transform-origin: 0 0;
        }
    }
    @media print
    {
        html
        {
            zoom: 70%;
        }
    }

    @font-face {
    font-family: "Nikosh";
    font-style: normal;
    font-weight: normal;
    src: url(fonts/Nikosh.ttf) format('truetype');
}
* {
    font-family: "Nikosh";
}

.user-section th{
font-size:14px;
/* color:red; */
text-align:center;
}
.user-section td{
font-size:14px;
/* color:red; */
text-align:center;
}

.head{
width:100%;
position: absolute;
top:0;
left:0;
/* background: #008800; */
padding:20px 10px;
border-bottom:1px solid #f2f2f2;
}

.head-left{
float:left;
width:180px;
}
.head-left img{
text-align:center;
}
.head-right{
/* float:right; */
text-align:left;
}

.head-right h2{
text-align:center;
margin-bottom:0;
/* color:#662d91; */
/* font-weight:bold; */
}
.head-right h4{
text-align:center;
margin-bottom:0;
}
.head-right p{
text-align:center;
}


</style>

 </head>
  <body>

    <div class="head-left">
        <img src="{{asset('theme/images/youth-logo.png')}}" alt="profile pic">
    </div>
    <div class="head">
        <div class="head-right">
            <h2>
                @if($langId == 1)
                {{"Government of the People's Republic of Bangladesh"}}
                @else($langId == 2)
                {{"গণপ্রজাতন্ত্রী বাংলাদেশ সরকার"}}
                @endif
            </h2>
            <h4>
                @if($langId == 1)
                {{"Department of Youth Development"}}
                @else($langId == 2)
                {{"যুব উন্নয়ন অধিদপ্তর"}}
                @endif</h4>

            <h4>
                    @if($langId == 1)
                    {{"National Service Programme (NSP)"}}
                    @else($langId == 2)
                    {{"ন্যাশনাল সার্ভিস কর্মসূচী"}}
                    @endif
                </h4>
        </div>
    </div>
<br>
<div class="user-section" style="margin-top:160px">
	<div class="col-6">
        <h3 class="user-management">
            @if($langId == 1)
            {{"Beneficiaries personal information"}}
            @else($langId == 2)
            {{"সুবিধাভোগীদের ব্যাক্তিগত তথ্য"}}
            @endif
        </h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" >
            <thead>

                <tr>
                    <th scope="col"> @if($langId == 1)
                        {{"Serial No"}}
                        @else($langId == 2)
                        {{"ক্রমিক নং"}}
                        @endif</th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Traking No"}}
                        @else($langId == 2)
                        {{"ট্রাকিং নং"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Applicant Name"}}
                        @else($langId == 2)
                        {{"উপকারভোগীর নাম"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Father/ Husband Name"}}
                        @else($langId == 2)
                        {{"পিতা/স্বামীর নাম"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Present Address"}}
                        @else($langId == 2)
                        {{"বর্তমান ঠিকানা"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Educational Qualification"}}
                        @else($langId == 2)
                        {{"শিক্ষাগত যোগ্যতা"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Birth-Date"}}
                        @else($langId == 2)
                        {{"জন্ম তারিখ"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Traning Subjects"}}
                        @else($langId == 2)
                        {{"প্রশিক্ষনের বিষয়সমূহ"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"National Id No"}}
                        @else($langId == 2)
                        {{"জাতীয় পরিচয়পত্র নং"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Mobile No"}}
                        @else($langId == 2)
                        {{"মোবাইল নং"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"E-mail"}}
                        @else($langId == 2)
                        {{"ই-মেইল"}}
                        @endif
                    </th>
                    <th scope="col">
                        @if($langId == 1)
                        {{"Blood Group"}}
                        @else($langId == 2)
                        {{"রক্তের গ্রুপ"}}
                        @endif
                    </th>
                </tr>
            </thead>

            <tbody>



                @php $i= 0; @endphp
                @forelse($applicationsReport as $row)

                @if($langId == 1)
                <tr>
                    <th scope="row">{{ ++$i }}</th>
                    <td> {{ $row->tracking_no }} </td>
                    <td> {{ $row->name }} </td>
                    <td> {{ $row->father_name }} </td>
                    <td>
                        {{ $row->present_village_id }},
                        {{ $data['union_en'][$row->present_union_id] }},
                        {{ $data['upazilas_en'][$row->present_upazila_id] }},
                        {{ $data['district_en'][$row->present_district_id] }},
                        {{ $data['division_en'][$row->present_division_id] }}
                    </td>
                    <td> {{ $data['educational_qualification_en'][$row->educational_qualification] }} </td>
                    <td> {{ $row->date_of_birth }} </td>
                    <td> {{ $row->training_subject_description }} </td>
                    <td> {{ $row->national_id }} </td>
                    <td> {{ $row->contact_no }} </td>
                    <td> {{ $row->email }} </td>
                    <td> {{ $data['blood_groups_en'][$row->blood_group] }} </td>
                </tr>
                @else($langId == 2)
                <tr>
                    <th scope="row">{{ ++$i }}</th>
                    <td> {{ $row->tracking_no }} </td>
                    <td> {{ $row->name }} </td>
                    <td> {{ $row->father_name }} </td>
                    <td>
                        {{ $row->present_village_id }},
                        {{ $data['union_bn'][$row->present_union_id] }},
                        {{ $data['upazilas_bn'][$row->present_upazila_id] }},
                        {{ $data['district_bn'][$row->present_district_id] }},
                        {{ $data['division_bn'][$row->present_division_id] }}
                    </td>
                    <td> {{ $data['educational_qualification_bn'][$row->educational_qualification] }} </td>
                    <td> {{ $row->date_of_birth }} </td>
                    <td> {{ $row->training_subject_description }} </td>
                    <td> {{ $row->national_id }} </td>
                    <td> {{ $row->contact_no }} </td>
                    <td> {{ $row->email }} </td>
                    <td> {{ $data['blood_groups_bn'][$row->blood_group] }} </td>
                </tr>
                @endif
                @empty
                <tr>
                    <td colspan="12" class="text-center">

                         @if($langId == 1)
                        {{"No Data Available"}}
                        @else($langId == 2)
                        {{" কোন ডাটা নাই "}}
                        @endif
                        </td>
                </tr>
                @endforelse
            </tbody>

        </table>
    </div>
</div>
<script>
  window.print();
</script>

</body>
</html>
