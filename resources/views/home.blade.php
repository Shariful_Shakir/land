@extends('layouts.app')
@section('content')

@php $super = Cookie::get('super'); @endphp
<div class="graph-section">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <!-- <div class="col-xs-12 col-sm-12 com-md-6 col-lg-6">
                    <div class="graph-box">
                        <div class="text-center" id="container">

                        </div>
                    </div>
                </div> -->
                <div class="col-xs-12 col-sm-12 com-md-12 col-lg-12">
                    <div class="graph-box">
                @if(!empty($super) && $super == 1)
                    <br>
                    <h3 style="text-align:center" class="bd">Welcome  To Land Service</h3><br>
                    <div class="map text-center" id="changeSvgMap"></div>
                @else
                    <div class="row">
                        <div class="col">
                            <div class="panel">
                            <h3>খতিয়ান</h3>
                            মৌজা ভিত্তিক এক বা একাধিক ভূমি
                            মালিকের ভূ-সম্পত্তির বিবরণ সহ যে
                            ভূমি রেকর্ড জরিপকালে প্রস্ত্তত করা
                            হয় তাকে খতিয়ান বলে। এতে
                            ভূমধ্যাধিকারীর নাম ও প্রজার নাম,
                            জমির দাগ নং, পরিমাণ, প্রকৃতি,
                            খাজনার হার ইত্যাদি লিপিবদ্ধ
                            থাকে। আমাদের দেশে বিভিন্ন
                            ধরনের খতিয়ানের উপস্থিতি লক্ষ্য
                            করা যায়। তন্মধ্যে সিএস, এসএ এবং
                            আরএস উল্লেখযোগ্য। ভূমি জরিপকালে
                            ভূমি মালিকের মালিকানা নিয়ে
                            যে বিবরণ প্রস্তুত করা হয় তাকে
                            “থতিয়ান” বলে। খতিয়ান প্রস্তত করা হয়
                            মৌজা ভিত্তিক।
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="panel">
                            <h3>নামজারী</h3>
                            ভূমি ব্যবস্থাপনায় মিউটেশন বা নামজারী একটি অতীব গুরুত্বপূর্ণ প্রক্রিয়া। জমি ক্রয় বা অন্য কোন উপায়ে জমির মালিক হয়ে থাকলে হাল নাগাদ রেকর্ড সংশোধন করার ক্ষেত্রে মিউটেশন একটি অপরিহার্য নাম। ইংরেজী মিউটেশন (Mutation) শব্দের বাংলা অর্থ হলো পরিবর্তন। আইনের ভাষায় এই মিউটেশন শব্দটির অর্থই হলো নামজারী। নামজারী বা নাম খারিজ বলতে নতুন মালিকের নামে জমি রেকর্ড করা বুঝায়। অর্থাত্ পুরনো মালিকের নাম বাদ দিয়ে নতুন মালিকের নামে জমি রেকর্ড করাকে নামজারী/নাম খারিজ বলে। ভূমি মালিকানার রেকর্ড বা খতিয়ান বা স্বত্বলিপি হালকরণের জন্য জরিপ কার্যক্রম চূড়ান্ত করতে দীর্ঘ সময়ের প্রয়োজন হয়। যে সময়ের মধ্যে উত্তরাধিকার সূত্রে, এওয়াজ সূত্রে বিক্রয়, দান, খাস জমি বন্দোবস্ত ইত্যাদি ভূমি মালিকানার পরিবর্তন প্রতিনিয়ত ঘটতে থাকে। যে কারণে প্রতিনিয়ত পরিবর্তনশীল ভূমি মালিকানার রেকর্ড হালকরণের সুবিধার্থে জমিদারী অধিগ্রহণ ও প্রজাস্বত্ব আইন ১৯৫০ এর ১৪৩ ধারায় কালেক্টরকে (জেলা প্রশাসক) ক্ষমতা দেওয়া হয়েছে। এই ক্ষমতা বলে জমা, খারিজ ও নামজারী এবং জমা একত্রিকরণের মাধ্যমে রেকর্ড হাল নাগাদ সংরক্ষণ করা হয়।
                            </div>
                        </div>
                        <div class="col">
                            <div class="panel">
                            <h3>খাস জমি কি এবং কিভাবে বন্দোবস্ত নিবেন।</h3>
                            কোনো জমি যদি সরকারের হাতে ন্যস্ত হয় এবং সেই জমিগুলি যদি সম্পূর্ণ সরকারের নিয়ন্ত্রণাধীন এবং সরকার,এই জমিগুলি সরকার কর্তৃক প্রণীত পদ্ধতি অনুযায়ী বন্দোবস্ত দিতে পারেন অথবা অন্য কোনো ভাবে ব্যবহার করতে পারেন তাহলে উক্ত ভূমিগুলিকে খাস জমি বলে।
                            ১৯৫০ সালের স্টেট একুইজিশন এন্ড টেনান্সি এক্টের ৭৬ ধারার ১ উপধারায় খাস জমি সম্বন্ধে বলা হয়েছে।
                            উক্ত ধারায় বলা হয়েছে যে,কোনো ভূমি যদি সরকারের হাতে ন্যস্ত হয় এবং সেই জমিগুলি যদি সম্পূর্ণ সরকারের নিয়ন্ত্রণাধীন থাকে তাহলে সরকার,এই ভূমিগুলি সরকার কর্তৃক প্রণীত পদ্ধতি অনুযায়ী বন্দোবস্ত দিতে পারেন,অথবা অন্য কোনো ভাবে ব্যবহার করতে পারেন, সরকারের নিয়ন্ত্রণাধীন উপরোক্ত ভূমিগুলিকে খাস জমি হিসাবে বুঝাবে।
                            তবে অন্যান্য মন্ত্রণালয়ের যথা বন বা পূর্ত কিংবা সড়ক ও জনপথ এর স্বত্বাধীন বা মালিকানাধীন বা নিয়ন্ত্রণাধীন ভূমিকে সরকারের খাস জমি হিসাবে গন্য করা যাবে না।
                            </div>
                        </div>
                    </div>

                @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extraJS')
<link rel="stylesheet" href="bangladesh-map/bd-map.css">
<script src="bangladesh-map/bd-svg.js"></script>
<script src="bangladesh-map/bd-map.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script>
$(document).ready(function() {
    var mapData = '<?=json_encode($mapDataRs);?>';
    var mapDataObj = JSON.parse(mapData);

   // var regionsOfBangladeshDemo = mapDataObj;
    var regionsOfBangladeshDemo = {
        "bangladesh": [{
                "region_name": "Rangpur",
                "region_id": "rangpurDivision",
                "total": 1
            },
            {
                "region_name": "Dhaka",
                "region_id": "dhakaDivision",
                "total": 2
            },
            {
                "region_name": "Rajshahi",
                "region_id": "rajshahiDivision",
                "total": 3
            },
            {
                "region_name": "Mymensingh",
                "region_id": "mymensinghDivision",
                "total": 4
            },
            {
                "region_name": "Sylhet",
                "region_id": "sylhetDivision",
                "total": 5
            },
            {
                "region_name": "Khulna",
                "region_id": "khulnaDivision",
                "total": 6
            },
            {
                "region_name": "Chittagong",
                "region_id": "chittagongDivision",
                "total": 7
            },
            {
                "region_name": "Barisal",
                "region_id": "barisalDivision",
                "total": 8
            },
        ],
        "rangpurDivision": [{
            "parent_region_id": "bangladesh",
            "region_name": "Panchagarh",
            "region_id": "panchagarhDistrict",
            "total": 8051
        },
        {
            "parent_region_id": "bangladesh",
            "region_name": "Nilphamari",
            "region_id": "nilphamariDistrict",
            "total": 80
        }],
        "rangpurDistrict": [{
            "parent_region_id": "rangpurDivision",
            "region_name": "Gangachhara",
            "region_id": "gangachharaUpazila",
            "total": 5001
        }]
    };
    var selectedAllDistrict = selectedDivision = selectedDistrict = 'bargunaDistrict';
    var regionsOfBangladesh = regionsOfBangladeshDemo;
    var regionKey = 'bangladesh';
    var title = 'Bangladesh MAP';
    if (selectedAllDistrict == 'allDistrict') {
        regionKey = selectedAllDistrict;
    }
    if (selectedDivision != '') {
        regionKey = selectedDivision;
    }
    if (selectedDistrict != '') {
        regionKey = selectedDistrict;
    }
    $("#changeSvgMap").generateMap({
        'title': title,
        'width': '580px',
        'height': '680px',
        'regionKey': regionKey,
        'regionData': regionsOfBangladesh
    });
});
</script>

<script>
var highchartObj = '<?=json_encode($chartMainData);?>';
	highchartObj = JSON.parse(highchartObj);

console.log('highchartObj.series : ' + JSON.stringify(highchartObj.series));
console.log('highchartObj.drilldown : ' + JSON.stringify(highchartObj.drilldown));

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    credits: {
        enabled: false
    },
    title: {
        text: 'Bangladesh'
    },
    xAxis: {
        type: 'category'
    },
    legend: {
        enabled: true
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true
            }
        }
    },
	tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"> \u25CF </span> {point.name} : <b>{point.y}</b><br/>'
        //pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
    },
    series: [highchartObj.series],
	/* series: [{
        name: 'Division',
        colorByPoint: true,
        data: [{
            name: 'Dhaka',
            y: 105,
            drilldown: 'dhaka'
        }, {
            name: 'Barishal',
            y: 80,
            drilldown: 'barishal'
        }, {
            name: 'Chittagong',
            y: 90,
            drilldown: 'chittagong'
        }, {
            name: 'Khulna',
            y: 55,
            drilldown: 'khulna'
        }, {
            name: 'Mymensingh',
            y: 90,
            drilldown: 'mymensingh'
        }, {
            name: 'Rajshahi',
            y: 65,
            drilldown: 'rajshahi'
        }, {
            name: 'Rangpur',
            y: 70,
            drilldown: 'rangpur'
        }, {
            name: 'Sylhet',
            y: 40,
            drilldown: 'sylhet'
        }]
    }],   */

    drilldown: highchartObj.drilldown

	/* drilldown: {
        series: [/* {
					id: 'dhaka',
					data: [{
						name: 'Dhaka',
						y: 4,
						drilldown: 'dhaka'
					}]
					}, * / {
					id: 'dhaka',
					data: [{
						name: 'Dhaka District',
						y: 3,
						drilldown: 'dhaka_dist'
					}, {
						name: 'Gazipur District',
						y: 3,
						drilldown: 'gazipur_dist'
					}, {
						name: 'Kishoreganj District',
						y: 3,
						drilldown: 'kishoreganj'
					}, {
						name: 'Manikganj District',
						y: 3,
						drilldown: 'manikganj'
					}]
				}, {
                id: 'dhaka_dist',
                data: [
                    ['Dhamrai Upazila', 1],
                    ['Dohar Upazila', 1],
                    ['Keraniganj Upazila', 1]
                ]
            },
            {
                id: 'gazipur_dist',
                data: [
                    ['Gazipur Sadar upazila', 1],
                    ['Kaliakair upazila', 1],
                    ['Kaliganj upazila', 1]
                ]
            },
            {
                id: 'kishoreganj',
                data: [
                    ['Gazipur Sadar', 1],
                    ['Kaliakair', 1],
                    ['Kaliganj', 1]
                ]
            },
            {
                id: 'manikganj',
                data: [
                    ['Gazipur Sadar', 1],
                    ['Kaliakair', 1],
                    ['Kaliganj', 1]
                ]
            },
            {
                id: 'barishal',
                data: [{
                    name: 'Barishal',
                    y: 4,
                    drilldown: 'barishal'
                }, ]
            },
            {
                id: 'barishal',
                data: [{
                        name: 'Barguna',
                        y: 3,
                        drilldown: 'barguna'
                    },
                    ['Agailjhara', 1],
                    ['Babuganj', 1]
                ]
            },
            {
                id: 'barguna',
                data: [
                    ['Mehendiganj', 1],
                    ['Agailjhara', 1],
                    ['Babuganj', 1]
                ]
            },
            {
                id: 'chittagong',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            }, {
                id: 'khulna',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            }, {
                id: 'mymensingh',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            }, {
                id: 'rajshahi',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            }, {
                id: 'rangpur',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            }, {
                id: 'sylhet',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            }
        ]
    } */
});
</script>


@endsection