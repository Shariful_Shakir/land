@extends('layouts.app')

@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management">

                        @if($langId == 1)
                        {{"Update Gender"}}
                        @else($langId == 2)
                        {{"লিঙ্গ আপডেট করুন"}}
                        @endif
                    </h3>
                    <hr>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                    @endif

                    <form method="post" action="{{ route('genders.update', $Gender->id) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">

                                <label for="title_eng">
                                       @if($langId == 1)
                                    {{"Gender Name (English):"}}
                                    @else($langId == 2)
                                    {{"লিঙ্গ (ইংরেজী)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="title_eng" value={{ $Gender->title_eng }} />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="title_bng">
                                      @if($langId == 1)
                                    {{"Gender Name (Bangla)"}}
                                    @else($langId == 2)
                                    {{"লিঙ্গ (বাংলা)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="title_bng" value={{ $Gender->title_bng }} />
                            </div>

                             <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-outline-primary update-buttn">
                                    @if($langId == 1)
                                    {{"Update"}}
                                    @else($langId == 2)
                                    {{"আপডেট"}}
                                    @endif
                                </button>
                                   <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                    @if($langId == 1)
                                    {{"Back"}}
                                    @else($langId == 2)
                                    {{" পিছনে"}}
                                    @endif
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection