@extends('layouts.app')

@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {!! "Update a Union" !!}
                        @else($langId == 2)
                        {!! "আপডেট ইউনিয়ন" !!}
                        @endif
                    </h3>
                    <hr>

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="post" action="{{ route('unions.update', $unions->id) }}">
                        @method('PATCH')
                        @csrf

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="union_name_bng">
                                    @if($langId == 1)
                                    {!! "Division Name" !!}
                                    @else($langId == 2)
                                    {!! "বিভাগের নাম :" !!}
                                    @endif
                                </label>

                                <select class="custom-select mr-sm-2 cust-width" id="division_id" name="division_id" required>
                                    @if($langId == 1)
                                    {!! getSelectOptions($data['division_en'], $unions->geo_division_id) !!}
                                    @else($langId == 2)
                                    {!! getSelectOptions($data['division_bn'], $unions->geo_division_id) !!}
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="union_name_bng">
                                    @if($langId == 1)
                                    {!! "Zila Name" !!}
                                    @else($langId == 2)
                                    {!! "জেলার নাম:" !!}
                                    @endif
                                </label>

                                <select class="custom-select mr-sm-2 cust-width" id="district_id" name="district_id" required>
                                    @if($langId == 1)
                                    {!! getSelectOptions($data['district_en'], $unions->geo_district_id) !!}
                                    @else($langId == 2)
                                    {!! getSelectOptions($data['district_bn'], $unions->geo_district_id) !!}
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="union_name_bng">
                                    @if($langId == 1)
                                    {!! "Upazila Name" !!}
                                    @else($langId == 2)
                                    {!! "উপজেলা" !!}
                                    @endif
                                </label>
                                <select class="custom-select mr-sm-2 cust-width" id="upazila_id" name="upazila_id" required>
                                    @if($langId == 1)
                                    {!! getSelectOptions($data['upazila_en'], $unions->geo_upazila_id) !!}
                                    @else($langId == 2)
                                    {!! getSelectOptions($data['upazila_bn'], $unions->geo_upazila_id) !!}
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="union_name_eng">
                                    @if($langId == 1)
                                    {!! "Union Name (English):" !!}
                                    @else($langId == 2)
                                    {!! "ইউনিয়নের নাম (ইংরেজী)" !!}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="union_name_eng" value={{ $unions->union_name_eng }} />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="union_name_bng">
                                    @if($langId == 1)
                                    {!! "Union Name (Bangla):" !!}
                                    @else($langId == 2)
                                    {!! "ইউনিয়নের নাম (বাংলা)" !!}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="union_name_bng" value={{ $unions->union_name_bng }} />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="bbs_code">
                                    @if($langId == 1)
                                    {{"Bbs_code:"}}
                                    @else($langId == 2)
                                    {{"বি বি এস কোড"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="bbs_code" value={{ $unions->bbs_code }} />
                            </div>

                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-outline-primary update-buttn">
                                    @if($langId == 1)
                                    {{"Update"}}
                                    @else($langId == 2)
                                    {{"আপডেট"}}
                                    @endif
                                </button>

                                <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                    @if($langId == 1)
                                    {{"Back"}}
                                    @else($langId == 2)
                                    {{" পিছনে"}}
                                    @endif
                                </a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection