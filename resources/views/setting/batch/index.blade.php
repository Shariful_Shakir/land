@extends('layouts.app')

@section('content')
<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif
                <div class="user-section">

                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">

                                @if($langId == 1)
                                {{"Batch No"}}
                                @else($langId == 2)
                                {{"ব্যাচ নং"}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('batchs.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">
                              <span><i class="fas fa-plus"> </i> </span>
                                @if($langId == 1)
                                {{"New District"}}
                                @else($langId == 2)
                                {{"নতুন ব্যাচ নং"}}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>
                                <th class="user-thead">
                                @if($langId == 1)
                                {{"ID"}}
                                @else($langId == 2)
                                {{"আইডি"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                              @if($langId == 1)
                                {{"Religion (English)"}}
                                @else($langId == 2)
                                {{"ব্যাচ নং (ইংরেজী)"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                       @if($langId == 1)
                                {{"Batch No (Bangla)"}}
                                @else($langId == 2)
                                {{"ব্যাচ নং (বাংলা)"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                      @if($langId == 1)
                                {{"Actions"}}
                                @else($langId == 2)
                                {{"কার্যক্রম"}}
                                @endif
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Batchs as $Batch)
                            <tr>
                                <td>{{$Batch->id}}</td>
                                <td>{{$Batch->title_eng}}</td>
                                <td>{{$Batch->title_bng}}</td>
                                <td>
                                    <form action="{{ route('batchs.destroy', $Batch->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" title="Edit" href="{{ route('batchs.edit', $Batch->id)}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>

                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>

                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection