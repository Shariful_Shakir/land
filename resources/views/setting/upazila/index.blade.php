@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif

                <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">

                                @if($langId == 1)
                                {{"Upazila"}}
                                @else($langId == 2)
                                {{"উপজেলা"}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('upazilas.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">
                              <span><i class="fas fa-plus"> </i> </span>
                                @if($langId == 1)
                                {{"New Upazila"}}
                                @else($langId == 2)
                                {{"নতুন উপজেলা"}}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                   <div class="table-responsive">
                         <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>
                                <th class="user-thead">
                                        @if($langId == 1)
                                {{"ID"}}
                                @else($langId == 2)
                                {{"আইডি"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                                                   
                                          @if($langId == 1)
                                {{" Upazila Name (English)"}}
                                @else($langId == 2)
                                {{"উপজেলার নাম(ইংরেজী)"}}
                                @endif
                            
                            </th>
                                <th class="user-thead">
                                               @if($langId == 1)
                                {{" Upazila Name (Bangla)"}}
                                @else($langId == 2)
                                {{"উপজেলার নাম(বাংলা)"}}
                                @endif
                                </th>
                                <!-- <th class="user-thead">

                                            @if($langId == 1)
                                {{"Bbs_code"}}
                                @else($langId == 2)
                                {{"বি বি এস কোড"}}
                                @endif
                                </th> -->
                                <th class="user-thead">
                                              @if($langId == 1)
                                {{"Actions"}}
                                @else($langId == 2)
                                {{"কার্যক্রম"}}
                                @endif
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($upazilas as $upazila)
                            <tr>
                                <td>{{$upazila->id}}</td>
                                <td>{{$upazila->upazila_name_eng}}</td>
                                <td>{{$upazila->upazila_name_bng}}</td>
                                <!-- <td>{{$upazila->bbs_code}}</td> -->

                                <td>
                                    <form action="{{ route('upazilas.destroy', $upazila->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('upazilas.edit', $upazila->id)}}" class="btn is-light btn-outline-warning cust-usr-btn">

                                            <i class="fas fa-user-edit"></i>

                                        </a>

                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                   </div>
                  
                      <div class="pagignation-sector">
                            <div aria-label="Page navigation example">
                                    <ul class="pagination my-3 justify-content-center">
                                        <li class="page-item custom-item"><a class="page-link" href="#">Prev</a>
                                        </li>
                                        <li class="page-item custom-item active"><a class="page-link" href="#">1</a>
                                        </li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">6</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


        @endsection