@extends('layouts.app')

@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Add a Upazila"}}
                        @else($langId == 2)
                        {{"উপজেলা যোগ করুন"}}
                        @endif
                    </h3>
                    <hr>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="post" action="{{ route('upazilas.store') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="district_name_eng">
                                         @if($langId == 1)
                                    {{"Division Name:"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম"}}
									 @endif
                                </label>
                                <select class="custom-select mr-sm-2 cust-width" id="division_id" name="geo_division_id" required>
                                    @if($langId == 1)
                                    {!! getSelectOptions($data['division_en'], old('geo_division_id')) !!}
                                    @endif
                                    @if($langId == 2)
                                    {!! getSelectOptions($data['division_bn'], old('geo_division_id')) !!}
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="district_name_eng">
                                      @if($langId == 1)
                                    {{"District Name:"}}
                                    @else($langId == 2)
                                    {{"জেলার নাম"}}
									 @endif
                                </label>
                                <select class="custom-select mr-sm-2" id="district_id" name="geo_district_id" value="{{ old('geo_district_id') }}" required>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="upazila_name_eng">
                                     @if($langId == 1)
                                    {{"Upazila Name (English):"}}
                                    @else($langId == 2)
                                    {{"উপজেলার নাম (ইংরেজী)"}}
									 @endif
                                </label>
                                <input type="text" class="form-control" name="upazila_name_eng" />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="upazila_name_bng">
                                    
                                      @if($langId == 1)
                                    {{"Upazila Name (Bangla):"}}
                                    @else($langId == 2)
                                    {{"উপজেলার নাম (বাংলা)"}}
									 @endif
                                </label>
                                <input type="text" class="form-control" name="upazila_name_bng" />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="bbs_code">
                                    @if($langId == 1)
                                    {{"Bbs_code:"}}
                                    @else($langId == 2)
                                    {{"বি বি এস কোড"}}
									 @endif
                                </label>
                                <input type="text" class="form-control" name="bbs_code" />
                            </div>

                            <div class="col-md-4  text-center">
                                <button type="submit" class="btn btn-outline-primary update-buttn mt-tweenty">
                                    @if($langId == 1)
                                    {{"Add District"}}
                                    @else($langId == 2)
                                    {{"উপজেলা যোগ করুন"}}
                                    @endif
                                </button>

                                <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                    @if($langId == 1)
                                    {{"Back"}}
                                    @else($langId == 2)
                                    {{" পিছনে"}}
                                    @endif
                                </a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
$(document).on('change', '#division_id', function() {   
        var DivissionId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DivissionId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getDistrictInfos/' + DivissionId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                if(langId == 1){
                    var district = '<option value="" >Select District</option>';
                }else{
                    var district = '<option value="" >সিলেক্ট জেলা</option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });




$(document).on('change', '#district_id', function() {   
        var DistrictId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DistrictId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getUpazliaInfo/' + DistrictId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                
                if(langId == 1){
                    var upazila = '<option value="" >Select Upazila</option>';
                }else{
                    var upazila = '<option value="" >সিলেক্ট উপজেলা</option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });







$(document).ready(function() {
  
  $('#selectAllDivision').on('click',function(){
    if(this.checked) {
        $('.divisionCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.divisionCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });
  $('#selectAllDistrict').on('click',function(){
    if(this.checked) {
        $('.districtCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.districtCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });
  $('#selectAllUpazila').on('click',function(){
    if(this.checked) {
        $('.upazilaCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.upazilaCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });




//console.log('upazilaId : ' + upazilaId);
ajaxCallCB(url, data, function(response) {
//console.log(response);
if (langId == 1) {
var division_name = (typeof response.data.division_name_eng != 'undefined') ?
response.data.division_name_eng : '';
var district_name = (typeof response.data.district_name_eng != 'undefined') ?
response.data.district_name_eng : '';
} else {
var division_name = (typeof response.data.division_name_bng != 'undefined') ?
response.data.division_name_bng : '';
var district_name = (typeof response.data.district_name_bng != 'undefined') ?
response.data.district_name_bng : '';
}
// $('#division_title').html('<strong>' + division_name + '</strong>');
// $('#district_title').html('<strong>' + district_name + '</strong>');
$('#division').val(division_name);
$('#district').val(district_name);
//console.log('response : ' + response);
});
$('body').on('keydown', '#email', function() {
if (e.keyCode === 9) {
var email = $('#email').val();
email = email.trim();
// if (email == "") {
//     e.preventDefault();
// }
var re =/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
return re.test(email);
}
});

$('#contact_no, #contact_no_confirmation').on('keyup', function() {
var contact_no = $(this).val();
if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
$(this).val('');
}
if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
$(this).val('');
}
if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
contact_no[2] == 2)) {
$(this).val('');
}
});

// New JS By Asad
var address_check = $('#address_check');
$(address_check).prop('disabled', false);
$('.click').click(function() {
if ($(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled')) {
$(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled', false);
} else {
$(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled', true);
$('#permanent_division').val('');
$('#permanent_district').val('');
$('#permanent_upazila_id').val('');
$('#permanent_union_id').val('');
$('#permanent_village_id').val('');
$('#permanent_post_code_id').val('');
}
});
});
function password_set_attribute() {
}
$(document).ready(function() {
$(":input[data-inputmask-mask]").inputmask();
$(":input[data-inputmask-alias]").inputmask();
$(":input[data-inputmask-regex]").inputmask("Regex");
});
$(document).ready(function() {
if (langId == 1) {
$('.langEn').show();
$('.langBn').hide();
$('.BngBtn').hide();
$('.EngBtn').show();
} else {
$('.langBn').show();
$('.langEn').hide();
$('.EngBtn').hide();
$('.BngBtn').show();
}
if (langId == 2) {
$('#name').addClass('bng-input');
$('#name').removeAttr('oninput');
$('#father_name').addClass('bng-input');
$('#father_name').removeAttr('oninput');
$('#mother_name').addClass('bng-input');
$('#mother_name').removeAttr('oninput');
$('#contact_no').addClass('eng-input');
//$('#contact_no').removeAttr('oninput');
$('#contact_no_confirmation').addClass('eng-input');
//$('#contact_no_confirmation').removeAttr('oninput');
$('#nationality').addClass('bng-input');
$('#national_id').addClass('eng-input');
//$('#national_id').removeAttr('oninput');
$('#national_id_confirmation').addClass('eng-input');
//$('#national_id_confirmation').removeAttr('oninput');
$('#training_subject_description').addClass('bng-input');
$('#it_knowledge').addClass('bng-input');
$('#communication_skill').addClass('bng-input');
$('#present_village_id').addClass('bng-input');
$('#permanent_village_id').addClass('bng-input');
$('#present_post_code_id').addClass('bng-input');
$('#permanent_post_code_id').addClass('bng-input');
}
});
</script>
@endsection