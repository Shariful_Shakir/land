@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif

                <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">

                                @if($langId == 1)
                                {{"District"}}
                                @else($langId == 2)
                                {{"জেলা"}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('districts.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">
                             <span><i class="fas fa-plus"> </i> </span>
                                @if($langId == 1)
                                {{"New District"}}
                                @else($langId == 2)
                                {{"নতুন জেলা"}}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <div class="table-responsive">
                        <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>
                                <th class="user-thead">
                                     @if($langId == 1)
                                          @if($langId == 1)
                                    {{"ID"}}
                                    @else($langId == 2)
                                    {{"আইডি"}}
                                    @endif
                                    @endif
                                </th>
                                <th class="user-thead">
                                       @if($langId == 1)
                                    {{"Division Name (English)"}}
                                    @else($langId == 2)
                                    {{"জেলার নাম (ইংরেজী)"}}
                                    @endif
                                </th>
                                <th class="user-thead">
                                        @if($langId == 1)
                                    {{"Division Name (Bangla)"}}
                                    @else($langId == 2)
                                    {{"জেলার নাম (বাংলা)"}}
                                    @endif
                                </th>
                               <!--  <th class="user-thead">
                                      @if($langId == 1)
                                    {{"Division Bbs_code"}}
                                    @else($langId == 2)
                                    {{"বিভাগের বি বি এস কোড"}}
                                    @endif
                                </th>
                                <th class="user-thead">
                                      @if($langId == 1)
                                    {{"District Bbs_code"}}
                                    @else($langId == 2)
                                    {{"জেলার বি বি এস কোড"}}
                                    @endif
                                </th> -->
                                <th class="user-thead">
                                     @if($langId == 1)
                                    {{"Actions"}}
                                    @else($langId == 2)
                                    {{"কার্যক্রম"}}
                                    @endif
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($districts as $district)
                            <tr>
                                <td>{{$district->id}}</td>
                                <td>{{$district->district_name_eng}}</td>
                                <td>{{$district->district_name_bng}}</td>
                                <!-- <td>{{$district->division_bbs_code}}</td>
                                <td>{{$district->bbs_code}}</td> -->

                                <td>
                                    <form action="{{ route('districts.destroy', $district->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" title="Edit" href="{{ route('districts.edit', $district->id)}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>

                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>

                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>

                      <div class="pagignation-sector">
                            <div aria-label="Page navigation example">
                                    <ul class="pagination my-3 justify-content-center">
                                        <li class="page-item custom-item"><a class="page-link" href="#">Prev</a>
                                        </li>
                                        <li class="page-item custom-item active"><a class="page-link" href="#">1</a>
                                        </li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">6</a></li>
                                        <li class="page-item custom-item"><a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>

                            </div>
                        </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection