@extends('layouts.app')

@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Add a District"}}
                        @else($langId == 2)
                        {{"জেলা যোগ করুন"}}
                        @endif
                    </h3>
                    <hr>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="post" action="{{ route('districts.store') }}">
                        @csrf
                        <div class="form-row">

                            <div class="form-group col-md-3">
                                <label for="district_name_eng">
                                    @if($langId == 1)
                                    {{"Division Name:"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম"}}
                                    @endif
                                </label>
                                <select class="custom-select mr-sm-2 cust-width" id="division_id" name="geo_division_id" required>
                                    @if($langId == 1)
                                    {!! getSelectOptions($data['division_en'], old('geo_division_id')) !!}
                                    @endif
                                    @if($langId == 2)
                                    {!! getSelectOptions($data['division_bn'], old('geo_division_id')) !!}
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="district_name_eng">

                                       @if($langId == 1)
                                    {{"District Name (English):"}}
                                    @else($langId == 2)
                                    {{"জেলার নাম (ইংরেজী)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="district_name_eng" />
                            </div>

                            <div class="form-group col-md-3">
                                <label for="district_name_bng">
                                         @if($langId == 1)
                                    {{"District Name (Bangla):"}}
                                    @else($langId == 2)
                                    {{"জেলার নাম (বাংলা)"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="district_name_bng" />
                            </div>

                            <div class="form-group col-md-3">

                                <label for="bbs_code">
                                   @if($langId == 1)
                                    {{"Bbs_code:"}}
                                    @else($langId == 2)
                                    {{"বি বি এস কোড"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="bbs_code" />
                            </div>

                              <div class="col-12 text-center">
                                <button type="submit" class="btn btn-outline-primary update-buttn">
                                    @if($langId == 1)
                                    {{"Add District"}}
                                    @else($langId == 2)
                                    {{"জেলা যোগ করুন"}}
                                    @endif
                                </button>
                                
                                <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                    @if($langId == 1)
                                    {{"Back"}}
                                    @else($langId == 2)
                                    {{" পিছনে"}}
                                    @endif
                                </a>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

