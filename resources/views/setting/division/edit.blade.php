@extends('layouts.app')

@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">

                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Update a Division"}}
                        @else($langId == 2)
                        {{"বিভাগ আপডেট করুন"}}
                        @endif
                    </h3>
                    <hr>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" action="{{ route('divisions.update', $division->id) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-row">

                            <div class="form-group col-md-4">
                                <label for="division_name_eng">
                                      @if($langId == 1)
                                    {{"Division Name (English):"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম(ইংরেজী)"}}
									 @endif
                                  
                                </label>
                                <input type="text" class="form-control" name="division_name_eng" value={{ $division->division_name_eng }} />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="division_name_bng">
                               					  @if($langId == 1)
                                    {{"Division Name (Bangla):"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম(বাংলা)"}}
                                @endif
                                </label>
                                <input type="text" class="form-control" name="division_name_bng" value={{ $division->division_name_bng }} />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="bbs_code">
                             					     @if($langId == 1)
                                    {{"Bbs_code:"}}
                                    @else($langId == 2)
                                    {{"বি বি এস কোড"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="bbs_code" value={{ $division->bbs_code }} />
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-outline-primary update-buttn">
                                    @if($langId == 1)
                                    {{"Update"}}
                                    @else($langId == 2)
                                    {{"আপডেট"}}
                                    @endif
                                </button>

                                    <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                        @if($langId == 1)
                                        {{"Back"}}
                                        @else($langId == 2)
                                        {{" পিছনে"}}
                                        @endif
                                    </a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection