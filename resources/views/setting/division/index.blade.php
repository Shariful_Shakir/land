@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif
                <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management d-inline">

                                @if($langId == 1)
                                {{"Division"}}
                                @else($langId == 2)
                                {{"বিভাগ"}}
                                @endif
                            </h3>

                        </div>

                        <div class="col-6">
                            <a href="{{ route('divisions.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">
                             <span><i class="fas fa-plus"> </i> </span>
                                @if($langId == 1)
                                {{"New Division"}}
                                @else($langId == 2)
                                {{"নতুন বিভাগ"}}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>

                                <th class="user-thead">
                                    @if($langId == 1)
                                    {{"ID"}}
                                    @else($langId == 2)
                                    {{"আইডি"}}
                                    @endif
                                </th>

                                <!-- <td>Division Name (English)</td> -->

                                <th class="user-thead">
                                    @if($langId == 1)
                                    {{"Division Name (English)"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম (ইংরেজী)"}}
                                    @endif
                                </th>

                                <!-- <td>Division Name (Bangla)</td> -->
                                <th class="user-thead">
                                    @if($langId == 1)
                                    {{"Division Name"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম (বাংলায়)"}}
                                    @endif
                                </th>

                                <!-- <td>Bbs_code</td> -->

                                <!-- <th class="user-thead">
                                    @if($langId == 1)
                                    {{"Bbs_code"}}
                                    @else($langId == 2)
                                    {{"বি বি এস কোড"}}
                                    @endif
                                </th> -->

                                <!-- <td colspan=2>Actions</td> -->

                                <th class="user-thead">
                                    @if($langId == 1)
                                    {{"Actions"}}
                                    @else($langId == 2)
                                    {{"কার্যক্রম"}}
                                    @endif
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($divisions as $division)
                            <tr>
                                <td>{{$division->id}}</td>
                                <td>{{$division->division_name_eng}}</td>
                                <td>{{$division->division_name_bng}}</td>
                                <!-- <td>{{$division->bbs_code}}</td> -->
                                <!-- <td>
                                    <a href="{{ route('divisions.edit', $division->id)}}" class="btn btn-primary">Edit</a>
                                </td> -->
                                <td>

                                    <form action="{{ route('divisions.destroy', $division->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn is-light btn-outline-warning cust-usr-btn" type="button" title="Edit" href="{{ route('divisions.edit', $division->id)}}">
                                            <i class="fas fa-user-edit"></i>
                                        </a>
                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>

                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
