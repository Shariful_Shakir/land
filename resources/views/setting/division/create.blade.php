@extends('layouts.app')

@section('content')


<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management text-center">

                        @if($langId == 1)
                        {{"Add Division"}}
                        @else($langId == 2)
                        {{"বিভাগ যোগ করুন"}}
                        @endif
                    </h3>
                    <hr>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" action="{{ route('divisions.store') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="division_name_eng">
                                  
                                         @if($langId == 1)
                                    {{"Division Name (English):"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম (ইংরেজী):"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="division_name_eng" />
                            </div>


                            <div class="form-group col-md-4">
                                <label for="division_name_bng">

                                         @if($langId == 1)
                                    {{"Division Name (Bangla):"}}
                                    @else($langId == 2)
                                    {{"বিভাগের নাম (বাংলা):"}}
                                    @endif
                                </label>
                                <input type="text" class="form-control" name="division_name_bng" />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="bbs_code">
                                        @if($langId == 1)
                                    {{"BBS Code (Bangla):"}}
                                    @else($langId == 2)
                                    {{"বি বি এস কোড (বাংলা):"}}
                                    @endif
                                </label>

                                <input type="text" class="form-control" name="bbs_code" />
                            </div>

                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-outline-primary update-buttn">
                                    @if($langId == 1)
                                    {{"Add Division"}}
                                    @else($langId == 2)
                                    {{"বিভাগ যোগ করুন"}}
                                    @endif
                                </button>
                                   <!-- <div class="col-12 text-right"> -->
                          <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                            @if($langId == 1)
                            {{"Back"}}
                            @else($langId == 2)
                            {{" পিছনে"}}
                            @endif
                        </a>
                  <!-- </div> -->
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection