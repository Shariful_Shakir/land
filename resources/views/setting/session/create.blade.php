@extends('layouts.app')

@section('content')

<div class="graph-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="user-section">
                    <h3 class="user-management">

                        @if($langId == 1)
                        {{"Session"}}
                        @else($langId == 2)
                        {{"সেশন"}}
                        @endif
                    </h3>
                    <hr>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="post" action="{{ route('sessions.store') }}">
                        @csrf

                      <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="title_eng">
                                         @if($langId == 1)
                                {{"Session (English)"}}
                                @else($langId == 2)
                                {{"সেশন (ইংরেজী)"}}
                                @endif
                            </label>
                            <input type="text" class="form-control" name="title_eng" />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="  title_bng">
                                 @if($langId == 1)
                                {{" Session (Bangla)"}}
                                @else($langId == 2)
                                {{"সেশন (বাংলা)"}}
                                @endif
                            </label>
                            <input type="text" class="form-control" name="  title_bng" />
                        </div>
                           <div class="col-12 text-center">
                            <button type="submit" class="btn btn-outline-primary update-buttn">
                                @if($langId == 1)
                                {{"Add Batch No"}}
                                @else($langId == 2)
                                {{"সেশন যোগ করুন"}}
                                @endif
                            </button>

                            <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn">
                                    @if($langId == 1)
                                    {{"Back"}}
                                    @else($langId == 2)
                                    {{" পিছনে"}}
                                    @endif
                                </a>
                        </div>
                      </div>

                     
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection