@extends('layouts.app')

@section('content')

<div class="graph-section repo-filt-min">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif

                <div class="user-section">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="user-management">

                                @if($langId == 1)
                                {{"Session"}}
                                @else($langId == 2)
                                {{"সেশন"}}
                                @endif
                            </h3>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('sessions.create')}}" class="creat-usrbtn float-right btn mt-0" type="button">
                              <span><i class="fas fa-plus"> </i> </span>
                                @if($langId == 1)
                                {{"New Session"}}
                                @else($langId == 2)
                                {{"নতুন সেশন"}}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="user-section mt-0">
                    <table class="table table-bordered report-table table-hover">
                        <thead>
                            <tr>
                                <th class="user-thead">
                                          @if($langId == 1)
                                {{"ID"}}
                                @else($langId == 2)
                                {{"আইডি"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                                            
                                          @if($langId == 1)
                                {{"Session (English)"}}
                                @else($langId == 2)
                                {{"সেশন (ইংরেজী)"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                   @if($langId == 1)
                                {{" Session (Bangla)"}}
                                @else($langId == 2)
                                {{"সেশন (বাংলা)"}}
                                @endif
                                </th>
                                <th class="user-thead">
                                   @if($langId == 1)
                                {{"Actions"}}
                                @else($langId == 2)
                                {{"কার্যক্রম"}}
                                @endif
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Sessions as $Session)
                            <tr>
                                <td>{{$Session->id}}</td>
                                <td>{{$Session->title_eng}}</td>
                                <td>{{$Session->title_bng}}</td>

                              
                                 <td>
                                    <form action="{{ route('sessions.destroy', $Session->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('sessions.edit', $Session->id)}}" class="btn is-light btn-outline-warning cust-usr-btn">

                                            <i class="fas fa-user-edit"></i>

                                        </a>

                                        <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


        @endsection