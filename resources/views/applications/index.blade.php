@extends('layouts.app')
@section('title', 'Appications')
@section('content')

<div class="graph-section">
     <div class="container">
          <div class="row">
               <div class="user-section">
                    <div class="row">
                         <div class="col-6">
                              <h3 class="user-management"></h3>
                         </div>

                         <a href="{{ route('applications.create') }}"  class="btn btn-info float-right ">
                              <span class="langBn">ডাটা যোগ</span>
                              <span class="langEn">Add Data</span>
                         </a>
                              <div class="dropdown">
                                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  File Download
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="{{route('applications.pdf')}}" target="blank">PDF</a>
                                  <a class="dropdown-item" href="#">CSV</a>
                                </div>
                              </div>
                    </div>
               </div>
          </div>
          @if (session('status'))
          <div class="alert alert-success" role="alert">
               {{ session('status') }}
          </div>
          @endif
          <div class="user-section">
               <form>
                    <div class="form-row">
                         <div class="col-3">
                              <input class="custom-select" type="Number" name="contact_no" placeholder="phone Number">
                         </div>

                            <div class="col-3">
                              <select class="custom-select mr-sm-2 cust-width" id="division_id"
                                name="division_id" required>                      
                                @if($langId == 1)
                                <option selected value=""> Division Name</option>
                                {!! getSelectOptions($data['division_en'], old('division_id')) !!}
                                @endif
                                @if($langId == 2)
                                <option selected value=""> বিভাগের নাম</option>
                                {!! getSelectOptions($data['division_bn'], old('division_id')) !!}
                                @endif                      
                              </select>
                              <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                            </div>

                            <div class="col-3">
                              <select class="custom-select mr-sm-2 cust-width" id="district_id"
                                name="district_id" required>                      
                                @if($langId == 1)
                                <option selected value=""> Distract Name</option>
                                {!! getSelectOptions($data['district_en'], old('district_id')) !!}
                                @endif
                                @if($langId == 2)
                                <option selected value=""> জেলার নাম </option>
                                {!! getSelectOptions($data['district_bn'], old('district_id')) !!}
                                @endif                      
                              </select>
                              <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                            </div>

                            <div class="col-3">
                              <select class="custom-select mr-sm-2 cust-width" id="upazila_id"
                                name="upazila_id" required>                      
                                @if($langId == 1)
                                <option selected value=""> Upazila Name</option>
                                {!! getSelectOptions($data['upazilas_en'], old('upazila_id')) !!}
                                @endif
                                @if($langId == 2)
                                <option selected value=""> উপজেলার নাম </option>
                                {!! getSelectOptions($data['upazilas_bn'], old('upazila_id')) !!}
                                @endif                      
                              </select>
                              <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                            </div>
                    </div>
               </form>
          </div>
          <div class="row">
               <div class="card">
                    <div class="card-content">
                         <table class="table table-bordered">
                              <thead>
                                   <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">
                                             <span class="langBn">ট্রাকিং নং</span>
                                             <span class="langEn">Traking No</span>
                                        </th>
                                        <th scope="col">
                                             <span class="langBn">আবেদনকারীর নাম</span>
                                             <span class="langEn">Applicant Name</span>
                                        </th>
                                        <th scope="col">
                                             <span class="langBn">উপজেলা</span>
                                             <span class="langEn">Upazila</span>
                                        </th>
                                        <th scope="col">
                                             <span class="langBn">ই-মেইল</span>
                                             <span class="langEn">E-mail</span>
                                        </th>
                                        <th scope="col">
                                             <span class="langBn">মোবাইল নং</span>
                                             <span class="langEn">Moblie No</span>
                                        </th>
                                        <th scope="col">
                                             <span class="langBn">এ্যাকশন</span>
                                             <span class="langEn">Action</span>
                                        </th>
                                   </tr>
                              </thead>
                              <tbody>
                                   @php $i= 0; @endphp
                                   @forelse($applications as $row)
                                   <tr>
                                        <th scope="row">{{ ++$i }}</th>
                                        <td> {{ $row->tracking_no }} </td>
                                        <td> {{ $row->name }} </td>
                                        <td> {{ isset($row->permanent_upazila_id) ? $data['upazilas_en'][$row->permanent_upazila_id] : '' }} </td>
                                        <td> {{ $row->email }} </td>
                                        <td> {{ $row->contact_no }} </td>
                                        <td>
                                             <form class="delete" action="{{ route('applications.destroy', $row->id)}}" method="post">
                                                  <a href="{{ route('application.pdf', $row->id) }}" class="btn is-light btn-outline-info cust-usr-btn" target="blank">PDF</i></a>
                                                  <a href="{{ route('applications.show', $row->id) }}" class="btn is-outlined m-r-5 btn-outline-success cust-usr-btn"><i class="fas fa-eye"></i></a>                                                  
                                                  <a href="{{ route('applications.edit', $row->id) }}" class="btn is-light btn-outline-warning cust-usr-btn"><i class="fas fa-user-edit"></i></a>
                                                  
                                                  @csrf
                                                  @method('DELETE')
                                                  <button class="btn is-light btn-outline-danger cust-usr-btn" type="submit"><i class="fas fa-trash-alt"></button>
                                             </form>
                                        </td>
                                   </tr>
                                   @empty
                                   <tr> <td colspan="7" class="text-center"> কোন ডাটা নাই </td> </tr>
                                   @endforelse
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
@endsection
@section('extraJS')

<script>
          
    $(document).on('change', '#division_id', function() {   
        var DivissionId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DivissionId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getDistrictInfos/' + DivissionId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                var district = '<option value="" >Select District</option>';
                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });




$(document).on('change', '#district_id', function() {   
        var DistrictId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DistrictId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getUpazliaInfo/' + DistrictId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                var upazila = '<option value="" >Select Upazila</option>';
                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });


function showdata(){

  var Div = $(this).val();
  //alert(Div);
}


$(document).ready(function() {
  
  $('#selectAllDivision').on('click',function(){
    if(this.checked) {
        $('.divisionCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.divisionCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });
  $('#selectAllDistrict').on('click',function(){
    if(this.checked) {
        $('.districtCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.districtCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });
  $('#selectAllUpazila').on('click',function(){
    if(this.checked) {
        $('.upazilaCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.upazilaCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });




//console.log('upazilaId : ' + upazilaId);
ajaxCallCB(url, data, function(response) {
//console.log(response);
if (langId == 1) {
var division_name = (typeof response.data.division_name_eng != 'undefined') ?
response.data.division_name_eng : '';
var district_name = (typeof response.data.district_name_eng != 'undefined') ?
response.data.district_name_eng : '';
} else {
var division_name = (typeof response.data.division_name_bng != 'undefined') ?
response.data.division_name_bng : '';
var district_name = (typeof response.data.district_name_bng != 'undefined') ?
response.data.district_name_bng : '';
}
// $('#division_title').html('<strong>' + division_name + '</strong>');
// $('#district_title').html('<strong>' + district_name + '</strong>');
$('#division').val(division_name);
$('#district').val(district_name);
//console.log('response : ' + response);
});
$('body').on('keydown', '#email', function() {
if (e.keyCode === 9) {
var email = $('#email').val();
email = email.trim();
// if (email == "") {
//     e.preventDefault();
// }
var re =/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
return re.test(email);
}
});

$('#contact_no, #contact_no_confirmation').on('keyup', function() {
var contact_no = $(this).val();
if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
$(this).val('');
}
if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
$(this).val('');
}
if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
contact_no[2] == 2)) {
$(this).val('');
}
});

// New JS By Asad
var address_check = $('#address_check');
$(address_check).prop('disabled', false);
$('.click').click(function() {
if ($(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled')) {
$(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled', false);
} else {
$(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled', true);
$('#permanent_division').val('');
$('#permanent_district').val('');
$('#permanent_upazila_id').val('');
$('#permanent_union_id').val('');
$('#permanent_village_id').val('');
$('#permanent_post_code_id').val('');
}
});
});
function password_set_attribute() {
}
$(document).ready(function() {
$(":input[data-inputmask-mask]").inputmask();
$(":input[data-inputmask-alias]").inputmask();
$(":input[data-inputmask-regex]").inputmask("Regex");
});
$(document).ready(function() {
if (langId == 1) {
$('.langEn').show();
$('.langBn').hide();
$('.BngBtn').hide();
$('.EngBtn').show();
} else {
$('.langBn').show();
$('.langEn').hide();
$('.EngBtn').hide();
$('.BngBtn').show();
}
if (langId == 2) {
$('#name').addClass('bng-input');
$('#name').removeAttr('oninput');
$('#father_name').addClass('bng-input');
$('#father_name').removeAttr('oninput');
$('#mother_name').addClass('bng-input');
$('#mother_name').removeAttr('oninput');
$('#contact_no').addClass('eng-input');
//$('#contact_no').removeAttr('oninput');
$('#contact_no_confirmation').addClass('eng-input');
//$('#contact_no_confirmation').removeAttr('oninput');
$('#nationality').addClass('bng-input');
$('#national_id').addClass('eng-input');
//$('#national_id').removeAttr('oninput');
$('#national_id_confirmation').addClass('eng-input');
//$('#national_id_confirmation').removeAttr('oninput');
$('#training_subject_description').addClass('bng-input');
$('#it_knowledge').addClass('bng-input');
$('#communication_skill').addClass('bng-input');
$('#present_village_id').addClass('bng-input');
$('#permanent_village_id').addClass('bng-input');
$('#present_post_code_id').addClass('bng-input');
$('#permanent_post_code_id').addClass('bng-input');
}
});
</script>
@endsection
<!-- For Vue Js -->
@section('scripts')
<script>
var app = new Vue({
el: '#app',
data: {
auto_password: true,
rolesSelected: [{!! old('roles') ? old('roles') : '' !!}],
divisionSelected: [{!! old('division_sp_id') ? old('division_sp_id') : '' !!}],
districtSelected: [{!! old('district_sp_id') ? old('district_sp_id') : '' !!}],
upazilaSelected: [{!! old('upazila_sp_id') ? old('upazila_sp_id') : '' !!}]
}
});

function submit_forms(){
var x = document.querySelectorAll(".multiForms");
for (let i = 0; i < x.length; i++) {
x[i].submit();
}
}
</script>
@endsection