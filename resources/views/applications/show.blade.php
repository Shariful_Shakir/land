@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Show Applications</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3 font-weight-bold">
                            <span class="langBn">ট্রাকিং নম্বর</span>
                            <span class="langEn">Traking No</span>
                        </div>
                        <div class="col-9">
                            {{ $application->tracking_no }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 font-weight-bold">
                            <span class="langBn">বিভাগের নাম</span>
                            <span class="langEn">Division Name</span>
                        </div>
                        <div class="col-9">
                            @if(isset($application->permanent_division_id))
                            @if($langId == 1)
                            {{$data['division_en'][$application->permanent_division_id]}}
                            @elseif($langId == 2)
                            {{$data['division_bn'][$application->permanent_division_id]}}
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 font-weight-bold">
                            <span class="langBn">জেলার নাম</span>
                            <span class="langEn">Districat Name</span>
                        </div>
                        <div class="col-9">
                            @if(isset($application->permanent_district_id))
                            @if($langId == 1)
                            {{$data['district_en'][$application->permanent_district_id]}}
                            @elseif($langId == 2)
                            {{$data['district_bn'][$application->permanent_district_id]}}
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 font-weight-bold">
                            <span class="langBn">উপজেলার নাম</span>
                            <span class="langEn">Upazila Name</span>
                        </div>
                        <div class="col-9">
                            @if(isset($application->permanent_upazila_id))
                            @if($langId == 1)
                            {{$data['upazilas_en'][$application->permanent_upazila_id]}}
                            @elseif($langId == 2)
                            {{$data['upazilas_bn'][$application->permanent_upazila_id]}}
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 font-weight-bold">
                            <span class="langBn">আবেদনকারীর নাম</span>
                            <span class="langEn">Applicant Name</span>
                        </div>
                        <div class="col-9">
                            {{ $application->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 font-weight-bold">
                            <span class="langBn">পিতা/স্বামীর নাম</span>
                            <span class="langEn">Father/Husband Name</span>
                        </div>
                        <div class="col-9">
                            {{ $application->father_name }}
                        </div>
                    </div>
                    <div class="row"> <div class="col-3 font-weight-bold">
                        <span class="langBn">মায়ের নাম</span>
                        <span class="langEn">Mother Name</span>
                        </div> <div class="col-9"> {{ $application->mother_name }} </div> </div>
                        <!-- <div class="row"> <div class="col-3 font-weight-bold">স্থায়ী ঠিকানা</div> <div class="col-9"> {{ $application->permanent_address }} </div> </div> -->
                        <div class="row"> <div class="col-3 font-weight-bold">
                            <span class="langBn">ব্যাচ নং</span>
                            <span class="langEn">Batch No</span>
                        </div> <div class="col-9">
                        @if(isset($application->batch_no))
                        @if($langId == 1)
                        {{$data['batch_en'][$application->batch_no]}}
                        @elseif($langId == 2)
                        {{$data['batch_bn'][$application->batch_no]}}
                        @endif
                        @endif
                    </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold">
                        <span class="langBn">সেশন</span>
                        <span class="langEn">Session No</span>
                    </div> <div class="col-9">
                    @if(isset($application->session_id))
                    @if($langId == 1)
                    {{$data['sessions_en'][$application->session_id]}}
                    @elseif($langId == 2)
                    {{$data['sessions_bn'][$application->session_id]}}
                    @endif
                    @endif
                </div> </div>
                <div class="row"> <div class="col-3 font-weight-bold">
                    <span class="langBn">জাতীয় পরিচয়পত্র নং</span>
                    <span class="langEn">National Id No</span>
                    </div> <div class="col-9"> {{ $application->national_id }} </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold">
                        <span class="langBn">যে বিষয়ে দক্ষতা আছে তার সংক্ষিপ্ত বিবারণ</span>
                        <span class="langEn">Any Skill</span>
                        </div> <div class="col-9"> {{ $application->skill_summary }} </div> </div>
                        <div class="row"> <div class="col-3 font-weight-bold">
                            <span class="langBn">আইটি জ্ঞানের বিবরণ (যদি থাকে)</span>
                            <span class="langEn">IT Skill(if have) </span>
                            </div> <div class="col-9"> {{ $application->it_knowledge }} </div> </div>
                            <div class="row"> <div class="col-3 font-weight-bold">
                                <span class="langBn">যোগাযোগের দক্ষতা (যদি থাকে)</span>
                                <span class="langEn">Communication skills (if have)</span>
                                </div> <div class="col-9"> {{ $application->communication_skill }} </div> </div>
                                <div class="row"> <div class="col-3 font-weight-bold">
                                    <span class="langBn">যে সকল বিষয়ে প্রশিক্ষণ গ্রহণ করতে ইচ্ছুক তার বর্ণনা</span>
                                    <span class="langEn">To receive training</span>
                                    </div> <div class="col-9"> {{ $application->training_subject_description }} </div> </div>
                                    <div class="row"> <div class="col-3 font-weight-bold">
                                        <span class="langBn">আত্নকর্মী হতে ইচ্ছুক কিনা</span>
                                        <span class="langEn">Self-employed</span>
                                    </div> <div class="col-9">
                                    @if(isset($application->is_self_employed))
                                    @if($langId == 1)
                                    {{$data['is_self_employed_en'][$application->is_self_employed]}}
                                    @elseif($langId == 2)
                                    {{$data['is_self_employed_bn'][$application->is_self_employed]}}
                                    @endif
                                    @endif
                                </div> </div>
                                <!-- <div class="row"> <div class="col-3 font-weight-bold"> Interested Work's Description </div> <div class="col-9"> {{ $application->interested_work_description }} </div> </div> -->
                                <div class="row"> <div class="col-3 font-weight-bold">
                                    <span class="langBn">লিঙ্গ</span>
                                    <span class="langEn">Gender</span>
                                </div> <div class="col-9">
                                @if(isset($application->gender))
                                @if($langId == 1)
                                {{$data['gender_en'][$application->gender]}}
                                @elseif($langId == 2)
                                {{$data['gender_bn'][$application->gender]}}
                                @endif
                                @endif
                            </div> </div>
                            <div class="row"> <div class="col-3 font-weight-bold">
                                <span class="langBn">জন্ম তারিখ</span>
                                <span class="langEn">Birth-Date</span>
                                </div> <div class="col-9"> {{ $application->date_of_birth }} </div> </div>
                                <div class="row"> <div class="col-3 font-weight-bold">
                                    <span class="langBn">শিক্ষাগত যোগ্যতা</span>
                                    <span class="langEn">Educational Qualification</span>
                                </div> <div class="col-9">
                                @if(isset($application->educational_qualification))
                                @if($langId == 1)
                                {{$data['educational_qualification_en'][$application->educational_qualification]}}
                                @elseif($langId == 2)
                                {{$data['educational_qualification_bn'][$application->educational_qualification]}}
                                @endif
                                @endif
                            </div> </div>
                            <div class="row"> <div class="col-3 font-weight-bold">
                                <span class="langBn">রক্তের গ্রুপ</span>
                                <span class="langEn">Blood Group</span>
                            </div> <div class="col-9">
                            @if(isset($application->blood_group))
                            @if($langId == 1)
                            {{$data['blood_groups_en'][$application->blood_group]}}
                            @elseif($langId == 2)
                            {{$data['blood_groups_bn'][$application->blood_group]}}
                            @endif
                            @endif
                        </div> </div>
                        <div class="row"> <div class="col-3 font-weight-bold">
                            <span class="langBn">ধর্ম</span>
                            <span class="langEn">Religion</span>
                        </div> <div class="col-9">
                        @if(isset($application->religion))
                        @if($langId == 1)
                        {{$data['religions_en'][$application->religion]}}
                        @elseif($langId == 2)
                        {{$data['religions_bn'][$application->religion]}}
                        @endif
                        @endif
                    </div> </div>
                    <div class="row"> <div class="col-3 font-weight-bold">
                        <span class="langBn">জাতীয়তা</span>
                        <span class="langEn">Nationality</span>
                        </div> <div class="col-9"> {{ $application->nationality }} </div> </div>
                        <div class="row"> <div class="col-3 font-weight-bold">
                            <span class="langBn">ই-মেইল</span>
                            <span class="langEn">E-mail</span>
                            </div> <div class="col-9"> {{ $application->email }} </div> </div>
                            <div class="row"> <div class="col-3 font-weight-bold">
                                <span class="langBn">মোবাইল নং</span>
                                <span class="langEn">Mobile</span>
                                </div> <div class="col-9"> {{ $application->contact_no }} </div> </div>
                                <div class="row mt-3">
                                    <div class="col-3 font-weight-bold">
                                        <span class="langBn">ছবি</span>
                                        <span class="langEn">Photo</span>
                                    </div>
                                    <div class="col-9">
                                        @if(isset($application->img) && !empty($application->img))
                                        @php $imgName = $application->img; @endphp
                                        @else
                                        @php $imgName = "user.png"; @endphp
                                        @endif
                                        {{-- $imgName --}}
                                        <img src="{{ asset('img/uploads/' . $imgName) }}" alt="user" style="width:160px; height: 200px;">
                                    </div>
                                </div>
                                
                                <a type="submit" href="{{ url()->previous() }}" class="btn float-right btn-outline-primary back-btn mt-3">
                                @if($langId == 1)
                                {{"Back"}}
                                @else($langId == 2)
                                {{" পিছনে"}}
                                @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection