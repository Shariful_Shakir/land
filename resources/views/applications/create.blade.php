@extends('layouts.app')
@section('title', 'Appications')

@section('content')
@if (session('status'))
<div class="alert alert-success alert-dismissible" role="alert">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>{{ session('status') }}</strong>
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif

<div class="updateData">
    @php if($langId == 2)
        $ph = " ট্রাকিং নম্বর / মোবাইল নম্বর";
    else
        $ph = " Tracking / Contact no.";
    @endphp
    <form action="{{ url('applications/edit') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="text" class="{{($langId==2)?'bng-input':'eng-input'}}" maxlength="11" name="update_by_trackingNo" id="update_by_trackingNo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ old('update_by_trackingNo')}}" required oncopy="return false" oncut="return false" onpaste="return false" autocomplete="off" placeholder="{{$ph}}">
    <input type='submit' value='Update' style="display:none;">
    </form>
</div>
@php
$editStatus = '';
if(Request::segment(2) == 'edit'){
    $editStatus = 'edit';
}
@endphp
@if(isset($data) && !empty($data))
<form action="{{ route('applications.store') }}" class="needs-validation" id="insertForm" method="POST" onsubmit="return validateForm('{{$editStatus}}');" enctype="multipart/form-data"
    novalidate>
    @csrf
    @if(Request::segment(2) == 'edit')
    <input type="hidden" name="id" value="{{$applications[0]['id']}}">
    @endif
    <div class="container-fluid cust-form-bg entry-form py-2" style="background: #cdf6e8;">

        <div class="form section-1">

            <div class="row">
                <div class="col-xs-12 offset-md-1 col-md-3">
                    <div class="form-group row">
                        <label class="col-sm-6 col-md-4 col-form-label text-right">
                            <span class="langBn"> ট্রাকিং নম্বর</span>
                            <span class="langEn"> Tracking No. </span>
                            <span class="text-danger"> * </span>
                        </label>
                        <div class="col-sm-12 col-md-5">
                            <input type="text" class="form-control"
                                id="tracking_no" name="tracking_no" value="{{ old('tracking_no', !empty($applications[0]['tracking_no'])? ($langId==2)?cv_en_to_bn($applications[0]['tracking_no']):$applications[0]['tracking_no'] : '') }}"
                                required autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>
                {{-- <div class="col-xs-4 offset-md-1 col-md-3">

                </div> --}}

            </div>
        </br>
            {{-- <div class="divider"></div> --}}
            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">আবেদনকারীর
                                নাম</span><span class="langEn">Applicant Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="name" name="name"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');"
                                value="{{ old('name',!empty($applications[0]['name'])? $applications[0]['name'] : '') }}" required autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পিতা/স্বামীর
                                নাম</span><span class="langEn">Father's/Husband's Name</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="father_name"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="father_name"
                                value="{{ old('father_name',!empty($applications[0]['father_name'])? $applications[0]['father_name'] : '') }}" required autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">মায়ের
                                নাম</span><span class="langEn">Mother's Name</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="mother_name"
                                oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="mother_name"
                                value="{{ old('mother_name',!empty($applications[0]['mother_name'])? $applications[0]['mother_name'] : '') }}" required autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">মোবাইল
                                নং</span><span class="langEn">Mobile number</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <!-- <input type="text" class="form-control" id="contact_no" /> -->
                            <input type="text" class="form-control" required id="contact_no" minlength="11"
                                maxlength="11" name="contact_no" onfocusout="this.setAttribute('type','password');"
                                onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"

                                value="{{ old('contact_no',!empty($applications[0]['contact_no'])? ($langId==2)?cv_en_to_bn($applications[0]['contact_no']):$applications[0]['contact_no'] : '') }}" required oncopy="return false" oncut="return false"
                                onpaste="return false" autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label">মোবাইল নং<span class="text-danger">
                        * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control">
                        </div>
                    </div> -->
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পুনরায় মোবাইল
                                নং</span><span class="langEn">Mobile number</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="contact_no_confirmation" minlength="11"
                                maxlength="11" name="contact_no_confirmation"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="{{ old('contact_no_confirmation',!empty($applications[0]['contact_no'])? ($langId==2)?cv_en_to_bn($applications[0]['contact_no']):$applications[0]['contact_no'] : '') }}" required oncopy="return false"
                                oncut="return false" onpaste="return false" autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">রক্তের
                                গ্রুপ</span><span class="langEn">Blood group</span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-" id="blood_group" name="blood_group">
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['blood_groups_en'], old('blood_group',!empty($applications[0]['blood_group'])? $applications[0]['blood_group'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['blood_groups_bn'], old('blood_group',!empty($applications[0]['blood_group'])? $applications[0]['blood_group'] : '')) !!}
                                @php } @endphp
                            </select>

                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">ই-মেইল</span><span class="langEn">Email</span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="email" class="form-control eng-input" id="email" name="email"
                                value="{{ old('email',!empty($applications[0]['email'])? $applications[0]['email'] : '') }}" data-inputmask-alias="email" data-val="true"
                                 onfocusout="this.setAttribute('type','password');"
                                onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                                oncopy="return false" oncut="return false" onpaste="return false" autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>

                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">কনফার্ম
                                ই-মেইল</span><span class="langEn">Confirm Email</span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="email" class="form-control eng-input" id="email_confirmation"
                                oncopy="return false" oncut="return false" onpaste="return false"
                                name="email_confirmation" value="{{ old('email_confirmation',!empty($applications[0]['email'])? $applications[0]['email'] : '') }}" autocomplete="off">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">লিঙ্গ</span><span class="langEn">gender</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" name="gender" id="gender" required>
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['gender_en'], old('gender',!empty($applications[0]['gender'])? $applications[0]['gender'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['gender_bn'], old('gender',!empty($applications[0]['gender'])? $applications[0]['gender'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
@php $type=''; if(Request::segment(2)=='edit') $type='date'; else $type='text';@endphp
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জন্ম
                                তারিখ</span><span class="langEn">Birth Day</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7"><style>.entry-form .form-control{line-height: 100% !important;}</style>
                            <input type="{{$type}}" class="form-control " id="date_of_birth" required name="date_of_birth"
                                onfocusout="this.setAttribute('type','password');"
                                onfocus="this.setAttribute('type','date');"
                                data-inputmask="'yearrange': { 'minyear': '1971'}" data-val="true"
                                data-val-required="Required" oncopy="return false" oncut="return false"
                                onpaste="return false" value="{{ old('date_of_birth',!empty($applications[0]['date_of_birth'])? $applications[0]['date_of_birth'] : '') }}" autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জন্ম
                                তারিখ</span><span class="langEn">Birth Day</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="{{$type}}" class="form-control" id="confirm_date_of_birth" required
                                name="confirm_date_of_birth"  onfocus="this.setAttribute('type','date');"
                                data-inputmask="'yearrange': { 'minyear': '1971'}" data-val="true"
                                data-val-required="Required" value="{{ old('confirm_date_of_birth', !empty($applications[0]['date_of_birth'])? $applications[0]['date_of_birth'] : '') }}"
                                autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">জাতীয়তা</span><span class="langEn">Nationality</span><span
                                class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="nationality" name="nationality"
                                value="{{old('nationality',!empty($applications[0]['nationality'])? $applications[0]['nationality'] : '')}}" autocomplete="off" required>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জাতীয় পরিচয়পত্র
                                নং</span><span class="langEn">National ID</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="national_id" minlength="10"
                                maxlength="17"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                oncopy="return false" oncut="return false" onpaste="return false" name="national_id"
                                value="{{ old('national_id',!empty($applications[0]['national_id'])? ($langId==2)?cv_en_to_bn($applications[0]['national_id']):$applications[0]['national_id'] : '') }}" required
                                onfocusout="this.setAttribute('type','password');"
                                onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                                autocomplete="off">
                            <!-- <div class="invalid-feedback">
                                এই ক্ষেত্রটি সর্বনিম্ন ১০ ডিজিট এবং সর্বোচ্চ ১৭ ডিজিট পূরণ করুন.
                            </div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জাতীয় পরিচয়পত্র
                                নং</span><span class="langEn">National ID</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" required id="national_id_confirmation"
                                minlength="10" maxlength="17"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                name="national_id_confirmation" value="{{ old('national_id_confirmation',!empty($applications[0]['national_id'])? ($langId==2)?cv_en_to_bn($applications[0]['national_id']):$applications[0]['national_id'] : '') }}" required
                                autocomplete="off">
                            <!-- <div class="invalid-feedback">
                                এই ক্ষেত্রটি সর্বনিম্ন ১০ ডিজিট এবং সর্বোচ্চ ১৭ ডিজিট পূরণ করুন.
                            </div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">ধর্ম</span><span class="langEn">Religion</span><span class="text-danger">
                                *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" id="religion" name="religion" required>
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['religions_en'], old('religion',!empty($applications[0]['religion'])? $applications[0]['religion'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['religions_bn'], old('religion',!empty($applications[0]['religion'])? $applications[0]['religion'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="form section-2 ">
            <h6 class="langBn">বর্তমান ঠিকানাঃ</h6>
            <h6 class="langEn">Present Address</h6>
            <div class="divider"></div>


            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">বিভাগের
                                নাম</span><span class="langEn">Division Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <div id="division_title">
                                <input type="text" id="division" required name="present_division_id"
                                    class="form-control" value="{{ old('present_division_id',!empty($applications[0]['present_division_id'])? $applications[0]['present_division_id'] : '') }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জেলার
                                নাম</span><span class="langEn">District Name</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <div id="district_title">
                                <input type="text" id="district" name="present_district_id" class="form-control"
                                    required value="{{ old('present_district_id',!empty($applications[0]['present_district_id'])? $applications[0]['present_district_id'] : '') }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                @role('super_admin|division_admin|district_admin')

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">উপজেলার
                                নাম</span><span class="langEn">Upazila Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2 cust-width" id="upazila_id" name="present_upazila_id"
                                required>
                                <!-- @if($langId == 1)
                                {!! getSelectOptions($data['upazilas_en'], old('upazila_id')) !!}
                                @endif
                                @if($langId == 2)
                                {!! getSelectOptions($data['upazilas_bn'], old('upazila_id')) !!}
                                @endif -->
                                 @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['upazilas_en'], old('present_upazila_id',!empty($applications[0]['present_upazila_id'])? $applications[0]['present_upazila_id'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['upazilas_bn'], old('present_upazila_id',!empty($applications[0]['present_upazila_id'])? $applications[0]['present_upazila_id'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                @endrole
                @role('data_entry_user')

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">উপজেলার
                                নাম</span><span class="langEn">Upazila Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2 cust-width" id="upazila_id_data_entry"
                                name="present_upazila_id" required>
                                <!-- @if($langId == 1)
                                @foreach($data['upazilas_en'] as $key=>$upazila)
                                "<option value="{{$key}}" selected>{!! $upazila !!}</option>"
                                @endforeach
                                @endif
                                @if($langId == 2)
                                @foreach($data['upazilas_bn'] as $key=>$upazila)
                                "<option value="{{$key}}" selected>{!! $upazila !!}</option>"
                                @endforeach
                                @endif -->
                                 @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data_p['upazilas_en'], old('present_upazila_id',!empty($applications[0]['present_upazila_id'])? $applications[0]['present_upazila_id'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data_p['upazilas_bn'], old('present_upazila_id',!empty($applications[0]['present_upazila_id'])? $applications[0]['present_upazila_id'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>
                @endrole

            </div>

            <div class="row">
                <!-- @php
                 $presentUnionOldValue = old('present_union_id');
                 $selected = 'selected';
                 if($presentUnionOldValue == ''){
                     $presentUnionOldValue = "old('present_union_id')";
                     $selected = '';
                 }
                @endphp -->
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ইউনিয়ন
                                নাম</span><span class="langEn">Union Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" id="union_id" name="present_union_id" required>
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">গ্রাম/বাসা
                                নং</span><span class="langEn">Village/House No</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input class="form-control" id="present_village_id" type="text" name="present_village_id"
                                value="{{ old('present_village_id',!empty($applications[0]['present_village_id'])? $applications[0]['present_village_id'] : '') }}" required autocomplete="off" required>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পোষ্ট
                                কোড</span><span class="langEn">Post Code</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input class="form-control" id="present_post_code_id" type="text"
                                name="present_post_code_id" value="{{old('present_post_code_id',!empty($applications[0]['present_post_code_id'])? $applications[0]['present_post_code_id'] : '')}}" autocomplete="off"
                                required>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

        </div>

        @php $do_btn = ''; if($langId == 2) {$do_btn = 'do-btn-ban';} else {$do_btn = 'do-btn';} @endphp
        @php $do_lang = ''; if($langId == 2) {$do_lang = 'langBn';} else {$do_lang = 'langEn';} @endphp
        <div class="form section-3">
            <h6 class="{{$do_lang}}">@if($langId == 2) স্থায়ী ঠিকানাঃ @else Permanent Address: @endif
                <span class="{{$do_btn}}">
                    <span><input type="checkbox" class="click" id="address_check">@if($langId == 2) ঐ @else Do @endif</span>
                </span>
            </h6>

            <!-- <h6 class="langEn">Permanent Address:
                <span class="do-btn">
                    <span><input type="checkbox" class="click" id="address_check">Do</span>
                </span>
            </h6> -->

            <div class="divider-two"></div>

            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right boltu"><span class="langBn">বিভাগের
                                নাম</span><span class="langEn">Division Name</span><span class="text-danger"> *
                            </span></label>
                        <!-- <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" placeholder="বিভাগের নাম">
                                    </div> -->
                        <div class="col-sm-12 col-md-7">
                            <div id="division_title">
                                <input type="text" id="permanent_division" name="permanent_division_id"
                                    class="form-control" value="{{old('permanent_division_id', !empty($applications[0]['permanent_division_id'])? $applications[0]['permanent_division_id'] : '')}}" disabled
                                    autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">জেলার
                                নাম</span><span class="langEn">District Name</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <div id="district_title">
                                <input type="text" id="permanent_district" name="permanent_district_id"
                                    class="form-control" value="{{ old('permanent_district_id',!empty($applications[0]['permanent_district_id'])? $applications[0]['permanent_district_id'] : '') }}" disabled
                                    autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right">
                            <span class="langBn">উপজেলার নাম</span>
                            <span class="langEn">Upazila Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2 cust-width" id="permanent_upazila_id"
                                name="permanent_upazila_id" required>
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['upazilas_en'], old('permanent_upazila_id',!empty($applications[0]['permanent_upazila_id'])? $applications[0]['permanent_upazila_id'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['upazilas_bn'], old('permanent_upazila_id',!empty($applications[0]['permanent_upazila_id'])? $applications[0]['permanent_upazila_id'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ইউনিয়ন
                                নাম</span><span class="langEn">Union Name</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" id="permanent_union_id" name="permanent_union_id" required>
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">গ্রাম/বাসা
                                নং</span><span class="langEn">Village/House No</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input class="form-control" id="permanent_village_id" type="text"
                                name="permanent_village_id" value="{{ old('permanent_village_id',!empty($applications[0]['permanent_village_id'])? $applications[0]['permanent_village_id'] : '') }}" required
                                autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">পোষ্ট
                                কোড</span><span class="langEn">Post Code</span><span class="text-danger">
                                * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input class="form-control" id="permanent_post_code_id" type="text"
                                name="permanent_post_code_id" value="{{old('permanent_post_code_id',!empty($applications[0]['permanent_post_code_id'])? $applications[0]['permanent_post_code_id'] : '')}}"
                                autocomplete="off" required>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="divider-three"> </div>
            </div>

        </div>

        <div class="form section-4">

            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">ব্যাচ
                                নং</span><span class="langEn">Batch No</span><span class="text-danger"> *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" name="batch_no" id="batch_no" required>
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['batch_en'], old('batch_no',!empty($applications[0]['batch_no'])? $applications[0]['batch_no'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['batch_bn'], old('batch_no',!empty($applications[0]['batch_no'])? $applications[0]['batch_no'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span
                                class="langBn">সেশন</span><span class="langEn">Session</span><span class="text-danger">
                                *
                            </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" id="session_id" name="session_id" required>
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['sessions_en'], old('session_id',!empty($applications[0]['session_id'])? $applications[0]['session_id'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['sessions_bn'], old('session_id',!empty($applications[0]['session_id'])? $applications[0]['session_id'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">শিক্ষাগত
                                যোগ্যতা</span><span class="langEn">Educational Qualification</span><span
                                class="text-danger"> * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" name="educational_qualification"
                                id="educational_qualification" required>
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['educational_qualification_en'],
                                old('educational_qualification',!empty($applications[0]['educational_qualification'])? $applications[0]['educational_qualification'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['educational_qualification_bn'],
                                old('educational_qualification',!empty($applications[0]['educational_qualification'])? $applications[0]['educational_qualification'] : '')) !!}
                                @php } @endphp
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right">
                            <!--কিনা--><span class="langBn">আত্মকর্মী হতে ইচ্ছুক</span>
                            <span class="langEn">Self-employed</span></label>
                        <div class="col-sm-12 col-md-7">
                            <select class="custom-select mr-sm-2" name="is_self_employed" id="is_self_employed"
                                value="{{ old('is_self_employed') }}">
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['is_self_employed_en'],
                                old('is_self_employed',!empty($applications[0]['is_self_employed'])? $applications[0]['is_self_employed'] : '')) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['is_self_employed_bn'],
                                old('is_self_employed',!empty($applications[0]['is_self_employed'])? $applications[0]['is_self_employed'] : '')) !!}
                                @php } @endphp
                            </select>
                        </div>
                    </div>

                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right">
                            <!--তার সংক্ষিপ্ত
                                        বিবারণ--><span class="langBn">যে বিষয়ে দক্ষতা আছে</span><span
                                class="langEn">Any Skill</span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" id="training_subject_description"
                                name="training_subject_description" value="{{ old('training_subject_description',!empty($applications[0]['training_subject_description'])? $applications[0]['training_subject_description'] : '' ) }}"
                                autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right">
                            <!--(যদি থাকে)--><span class="langBn">আইটি জ্ঞানের বিবরণ</span><span class="langEn">IT
                                knowledge details</span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" id="it_knowledge" name="it_knowledge"
                                value="{{ old('it_knowledge', !empty($applications[0]['it_knowledge'])? $applications[0]['it_knowledge'] : '') }}" autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn">যোগাযোগের
                                দক্ষতা (যদি থাকে)</span><span class="langEn">Communication skills (if any)</label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" id="communication_skill" name="communication_skill"
                                value="{{ old('communication_skill', !empty($applications[0]['communication_skill'])? $applications[0]['communication_skill'] : '') }}" autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right">
                            <!--<span>যে সকল বিষয়ে  ইচ্ছুক</span> -->
                            <span class="langBn">প্রশিক্ষণ গ্রহণ করতে ইচ্ছুক</span><span class="langEn">To receive
                                training</span>
                        </label>
                        <div class="col-sm-12 col-md-7">
                            <input type="text" class="form-control" id="training_subject_description"
                                name="training_subject_description" value="{{ old('training_subject_description',!empty($applications[0]['training_subject_description'])? $applications[0]['training_subject_description'] : '') }}"
                                autocomplete="off">
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn"> ছবি আপলোড করুন
                            </span><span class="langEn">Upload Photo</span><span class="text-danger"> * </span></label>
                        <div class="col-sm-12 col-md-7">
                            <input type="file" id="img_file" name="image" class="cust-file-type" value="{{old('image', !empty($applications[0]['img'])? $applications[0]['img'] : '')}}" required>
                            @if($langId == 1)
                                <p class="extensionStyle">(jpg, jpeg, png)</p>
                            @elseif($langId == 2)
                                <p class="extensionStyle">(jpg, jpeg, png)</p>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-5 col-form-label text-right"><span class="langBn"> ফাইল সংযুক্ত
                                করুন </span><span class="langEn">Upload File</span></label>
                        <div class="col-sm-12 col-md-7">
                            <!-- <input type="file" id="file" name="file_name" class="form-control"> -->
                            <input type="file" id="file" name="file" class="cust-file-type" value="{{old('file', !empty($applications[0]['file_name']) ? $applications[0]['file_name'] : '')}}">
                            @if($langId == 1)
                                <p class="extensionStyle">(doc, docx, xlsx, pdf)</p>
                            @elseif($langId == 2)
                                <p class="extensionStyle">(doc, docx, xlsx, pdf)</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-4 btns text-center">

                    @if(Request::segment(2) == 'edit')
                    <button class="btn btn-primary btn-sm"> Update </button>
                    <button type="reset" class="btn btn-warning btn-sm text-white" onclick="location.href='{{ route('applications.create') }}'"> Cancel </button>
                    @else
                    <button class="btn btn-primary btn-sm"> Submit </button>
                    <button type="reset" class="btn btn-warning btn-sm text-white"> Reset </button>
                    @endif
                </div>
            </div>
        </div>
        <!-- <div class="btns float-right my-3">
                            <button class="btn btn-primary btn-sm"> Submit </button>
                            <button class="btn btn-warning btn-sm"> Reset </button>
                        </div> -->
    </div>
</form>
@endif
@endsection
@section('extraJS')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
    integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="{{ asset('assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
    rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
<link rel="stylesheet" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
<script src="{{ asset('js/form-validation.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script src="{{ asset('js/dataEntryFormValidation.js') }}"></script>


<script>


    $(document).ready(function() {
        var editSegment = "{{Request::segment(2)}}";
        if(editSegment == 'edit'){
            $('#insertForm').attr('action', '{{ url('applications/update') }}');
            $('#img_file').removeAttr('required');
            $('.stylebutton').attr('title','Language change not right now.');
            $('.stylebutton').attr('data-toggle','tooltip');
            $('.stylebutton').attr('disabled','disabled');
            $('[data-toggle="tooltip"]').tooltip();
          var presentUpazillaId = $('#upazila_id').val();
          var permanentUpazilaId = $('#permanent_upazila_id').val();
        }else{
            $('.stylebutton').removeAttr('disabled');
            var presentUpazillaId = "{{old('present_upazila_id')}}";
            var permanentUpazilaId = "{{old('permanent_upazila_id')}}";
        }

        if(presentUpazillaId != '') {
            upazillaInfos(presentUpazillaId);
        }

        if(permanentUpazilaId != ''){
            permanentUpazialInfo(permanentUpazilaId);
        }



    $('#upazila_id_data_entry').on('keydown', function() {
        var url = "{{ url('/getUpazliaInfos') }}";
        var upazilaId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        ajaxCallCB(url, data, function(response) {
            if (langId == 1) {
                var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                    response.data.division_name_eng : '';
                var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                    response.data.district_name_eng : '';
            } else {
                var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                    response.data.division_name_bng : '';
                var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                    response.data.district_name_bng : '';
            }
            $('#division').val(division_name);
            $('#district').val(district_name);
        });
    });


    $('#upazila_id_data_entry').on('keydown', function() {
            var url = "{{ url('/getUnionInfos') }}";
            var upazilaId = $(this).val();
            var union_name = '';
            var data = {
                "_token": "{{ csrf_token() }}",
                upazila_id: upazilaId
            };

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'json'
            }).done(function(response) {
                var union = '';
                union += '<option value=""></option>';
                $.each(response, function(i, item) {
                    if (langId == 1) {
                        union += '<option value="' + response[i].id + '">' + response[i]
                            .union_name_eng + '</option>';
                    } else {
                        union += '<option value="' + response[i].id + '">' + response[i]
                            .union_name_bng + '</option>';
                    }
                });
                $('#union_id').html(union);
            });
        });


    $('#upazila_id').on('change', function() {
        var upazilaId = $(this).val();
        upazillaInfos(upazilaId);
    });


    function upazillaInfos(upazilaId)
    {
        var url = "{{ url('/getUpazliaInfos') }}";
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        ajaxCallCB(url, data, function(response) {
            if (langId == 1) {
                var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                    response.data.division_name_eng : '';
                var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                    response.data.district_name_eng : '';
            } else {
                var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                    response.data.division_name_bng : '';
                var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                    response.data.district_name_bng : '';
            }
            $('#division').val(division_name);
            $('#district').val(district_name);
        });

        //--############ FOR Load Union Data---------------------------
        var url = "{{ url('/getUnionInfos') }}";
        var union_name = '';
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var union = '';
            var selected = '';
            union += '<option value=""></option>';
            if(editSegment == 'edit'){
                var oldValue = "{{!empty($applications[0]['present_union_id']) ? $applications[0]['present_union_id'] : ''}}";
            }else{
                var oldValue = '{{old("present_union_id")}}';
            }

            console.log(oldValue);
            $.each(response, function(i, item) {
                if(oldValue == response[i].id){
                    selected = 'selected';
                }else{
                    selected = '';
                }
                if (langId == 1) {
                    union += '<option '+selected+' value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option '+selected+' value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#union_id').html(union);
        });
    }


    $('#union_id ').on('change', function() {
        var url = "{{ url('/getPostalCode') }}";
        var union_id = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            union_id: union_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var post_code = (typeof response.data[0].postcode != 'undefined') ? response.data[0]
                .postcode : '';
            if (post_code) {
                $('#present_post_code_id').val(post_code);
            } else {
                $('#present_post_code_id').val('');
            }
        });
    });
    // ------------permanent--------------
    $('#permanent_upazila_id').on('change', function() {
        var upazilaId = $(this).val();
        permanentUpazialInfo(upazilaId);
    });

    function permanentUpazialInfo(upazilaId){
        var url = "{{ url('/getUpazliaInfos') }}";
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        ajaxCallCB(url, data, function(response) {
            if (langId == 1) {
                var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                    response.data.division_name_eng : '';
                var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                    response.data.district_name_eng : '';
            } else {
                var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                    response.data.division_name_bng : '';
                var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                    response.data.district_name_bng : '';
            }
            $('#permanent_division').val(division_name);
            $('#permanent_district').val(district_name);
        });
        //---########## For load Permanent Union Data
        var url = "{{ url('/getUnionInfos') }}";
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var union = '';
            var selected = '';
            if(editSegment == 'edit'){
                var perOldValue = "{{!empty($applications[0]['permanent_union_id']) ? $applications[0]['permanent_union_id'] : ''}}";
            }else{
                var perOldValue = "{{old('permanent_union_id')}}";
            }

            union += '<option value=""></option>';
            $.each(response, function(i, item) {
                if(perOldValue == response[i].id){
                    selected = 'selected';
                }else{
                    selected = '';
                }
                if (langId == 1) {
                    union += '<option '+selected+' value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option '+selected+' value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#permanent_union_id').html(union);
        });
    }

    $('#permanent_union_id ').on('change', function() {
        var url = "{{ url('/getPostalCode') }}";
        var union_id = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            union_id: union_id
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var post_code = (typeof response.data[0].postcode != 'undefined') ? response.data[0]
                .postcode : '';
            if (post_code) {
                $('#permanent_post_code_id').val(post_code);
            } else {
                $('#permanent_post_code_id').val('');
            }
        });
    });
    //------------permanent---------------
    $('body').on('keydown', '#tracking_no', function(e) {
        if (e.keyCode === 9) {
            var tracking_no = $('#tracking_no').val();
            if (tracking_no == "") {
                e.preventDefault();
            }
            if(tracking_no != "" && editSegment != 'edit'){
                var data = {
                    "_token": "{{ csrf_token() }}",
                    data: {"value":tracking_no, "column":"tracking_no"},
                };
                uniqueCheck(data);
            }
        }
    });
    $('body').on('keydown', '#name', function(e) {
        if (e.keyCode === 9) {
            var name = $('#name').val();
            name = name.trim();
            if (name == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#union_id', function(e) {
        if (e.keyCode === 9) {
            var union_id = $('#union_id').val();
            if (union_id == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#permanent_union_id', function(e) {
        if (e.keyCode === 9) {
            var permanent_union_id = $('#permanent_union_id').val();
            if (permanent_union_id == "") {
                e.preventDefault();
            }
        }
    });

    $('#name').on('keyup', function() {
        var name = $(this).val();
        if (typeof name[0] != 'undefined' && (name[0] == (event.charCode == 32) || name[0] == '.' ||
                name[0] == '-')) {
            $(this).val('');
        }
    });
    //onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'
    $('body').on('keydown', '#father_name', function(e) {
        if (e.keyCode === 9) {
            var father_name = $('#father_name').val();
            father_name = father_name.trim();
            if (father_name == "") {
                e.preventDefault();
            }
        }
    });
    $('#father_name').on('keyup', function() {
        var father_name = $(this).val();
        if (typeof father_name[0] != 'undefined' && (father_name[0] == (event.charCode == 32) ||
                father_name[0] == '.' || father_name[0] == '-')) {
            $(this).val('');
        }
    });
    $('body').on('keydown', '#mother_name', function(e) {
        if (e.keyCode === 9) {
            var mother_name = $('#mother_name').val();
            mother_name = mother_name.trim();
            if (mother_name == "") {
                e.preventDefault();
            }
        }
    });
    $('#mother_name').on('keyup', function() {
        var mother_name = $(this).val();
        if (typeof mother_name[0] != 'undefined' && (mother_name[0] == (event.charCode == 32) ||
                mother_name[0] == '.' || mother_name[0] == '-')) {
            $(this).val('');
        }
    });
    $('body').on('keydown', '#contact_no', function(e) {
        if (e.keyCode === 9) {
            var contact_no = $('#contact_no').val();
            if (contact_no == "" || contact_no.length != 11) {
                e.preventDefault();
            }
            if(contact_no != ""  && editSegment != 'edit'){
                var data = {
                    "_token": "{{ csrf_token() }}",
                    data: {"value":contact_no, "column":"contact_no"},
                };
                uniqueCheck(data);
            }
        }
    });

    $('body').on('keydown', '#contact_no_confirmation', function(e) {
        if (e.keyCode === 9) {
            var contact_no = $('#contact_no').val();
            var contact_no_confirmation = $('#contact_no_confirmation').val();
            if (contact_no_confirmation == "" || contact_no != contact_no_confirmation) {
                e.preventDefault();
            }
            if (contact_no != contact_no_confirmation) {
                $("#contact_no").focus();
                $("#contact_no_confirmation").val('');
            }
        }
    });
    $('body').on('keydown', '#national_id', function(e) {
        if (e.keyCode === 9) {
            var national_id = $('#national_id').val();
            if (national_id == "" || national_id.length != 10 && national_id.length != 13 && national_id
                .length != 17) {
                e.preventDefault();
            }
            if(national_id != ""  && editSegment != 'edit'){
                var data = {
                    "_token": "{{ csrf_token() }}",
                    data: {"value":national_id, "column":"national_id"},
                };
                uniqueCheck(data);
            }
        }
    });
    $('body').on('keydown', '#national_id_confirmation', function(e) {
        if (e.keyCode === 9) {
            var national_id = $('#national_id').val();
            var national_id_confirmation = $('#national_id_confirmation').val();
            if (national_id_confirmation == "" || national_id != national_id_confirmation) {
                e.preventDefault();
            }
            if (national_id != national_id_confirmation) {
                $("#national_id").focus();
                $("#national_id_confirmation").val('');
            }
        }
    });
    $('body').on('keydown', '#religion', function(e) {
        if (e.keyCode === 9) {
            var religion = $('#religion').val();
            if (religion == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#nationality', function(e) {
        if (e.keyCode === 9) {
            var nationality = $('#nationality').val();
            nationality = nationality.trim();
            if (nationality == "" && langId == 2) {
                e.preventDefault();
                $(this).val('বাংলাদেশী');
            }
            if (nationality == "" && langId == 1) {
                e.preventDefault();
                $(this).val('Bangladeshi');
            }
        }
    });
    $('#nationality').on('focusout', function() {
        if ($(this).val() == "" && langId == 1) {
            $(this).val('Bangladeshi');
        }
        if ($(this).val() == "" && langId == 2) {
            $(this).val('বাংলাদেশী ');
        }
    });
    $('#nationality').on('focusin', function() {
        if (langId == 1) {
            $(this).val('Bangladeshi');
        }
        if (langId == 2) {
            $(this).val('বাংলাদেশী ');
        }
    });
    $('body').on('keydown', '#email', function(e) {
        if (e.keyCode === 9) {
            var email = $('#email').val();
            email = email.trim();
            if(email != ""  && editSegment != 'edit'){
                var data = {
                    "_token": "{{ csrf_token() }}",
                    data: {"value":email, "column":"email"},
                };
                uniqueCheck(data);

                // if (email == "") {
                //     e.preventDefault();
                // }
                var re =
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        }
    });
    $('body').on('keydown', '#email_confirmation', function(e) {
        if (e.keyCode === 9) {
            var email = $('#email').val();
            var email_confirmation = $('#email_confirmation').val();
            // if (email_confirmation == "" || email != email_confirmation) {
            //     e.preventDefault();
            // }
            if (email != email_confirmation) {
                $("#email").focus();
                $("#email_confirmation").val('');
                e . preventDefault();
            }
        }
    });
    $('body').on('keydown', '#date_of_birth', function(e) {
        if (e.keyCode === 9) {
            var date_of_birth = $('#date_of_birth').val();
            //date_of_birth = date_of_birth.trim();
            var input_date = date_of_birth;
            date_of_birth = date_of_birth.replace(/\D/g, '');
            if (date_of_birth == "" || date_of_birth.length != 8) {
                e.preventDefault();
            }
            var date = new Date();
            // var dd = date.getDate();
            // var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();
            // if (dd < 10) {
            //     dd = '0' + dd;
            // }
            // if (mm < 10) {
            //     mm = '0' + mm;
            // }
            //current_date = dd + '/' + mm + '/' + yyyy;
            year1 = input_date.split('/');
            year2 = yyyy;
            if (year1[2] > year2) {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#confirm_date_of_birth', function(e) {
        if (e.keyCode === 9) {
            var date_of_birth = $('#date_of_birth').val();
            var confirm_date_of_birth = $('#confirm_date_of_birth').val();
            date_of_birth = date_of_birth.replace(/\D/g, '');
            confirm_date_of_birth = confirm_date_of_birth.replace(/\D/g, '');
            if (confirm_date_of_birth == "" || date_of_birth != confirm_date_of_birth) {
                e.preventDefault();
            }
            if (date_of_birth != confirm_date_of_birth) {
                $("#date_of_birth").focus();
                $("#confirm_date_of_birth").val('');
            }
            //  var date = new Date();
            //  var yyyy = date.getFullYear();
            //  year1 = input_date.split('/');
            //  year2 = yyyy;
            //  if(year1[2] > year2){
            //     e.preventDefault();
            // }
        }
    });
    $('body').on('keydown', '#gender', function(e) {
        if (e.keyCode === 9) {
            var gender = $('#gender').val();
            if (gender == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#batch_no', function(e) {
        if (e.keyCode === 9) {
            var batch_no = $('#batch_no').val();
            if (batch_no == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#session_id', function(e) {
        if (e.keyCode === 9) {
            var session_id = $('#session_id').val();
            if (session_id == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#upazila_id', function(e) {
        if (e.keyCode === 9) {
            var upazila_id = $('#upazila_id').val();
            if (upazila_id == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#present_village_id', function(e) {
        if (e.keyCode === 9) {
            var present_village_id = $('#present_village_id').val();
            if (present_village_id == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#permanent_village_id', function(e) {
        if (e.keyCode === 9) {
            var permanent_village_id = $('#permanent_village_id').val();
            if (permanent_village_id == "") {
                e.preventDefault();
            }
        }
    });
    $('body').on('keydown', '#img_file', function(e) {
        if (e.keyCode === 9) {
            var img_file = $('#img_file').val();
            if (img_file == "") {
                e.preventDefault();
            }
        }
    });
    // $('body').on('keydown', '#session_id', function(e) {
    //     if (e.keyCode === 9) {
    //         var session_id = $('#session_id').val();
    //         if(session_id == ""){
    //             e.preventDefault();
    //         }
    //     }
    // });
    $('body').on('keydown', '#educational_qualification', function(e) {
        if (e.keyCode === 9) {
            var educational_qualification = $('#educational_qualification').val();
            if (educational_qualification == "") {
                e.preventDefault();
            }
        }
    });

    $(document).on('keydown', function(e){
        if (e.keyCode === 9) {
            $("#file").focus(function(){
                $("#submitbtn").removeAttr('disabled');
            });
        }
    });
    $("#btndiv").mouseover(function(){
        $("#submitbtn").removeAttr('disabled');
    });
    // $('body').on('keydown', '#is_self_employed', function(e) {
    //     if (e.keyCode === 9) {
    //         var is_self_employed = $('#is_self_employed').val();
    //         if(is_self_employed == ""){
    //             e.preventDefault();
    //         }
    //     }
    // });
    $('#contact_no').on('keyup', function() {
        if(langId == 2){
             var contact_no = $('#contact_no').val();
            contact_no = cv_bn_to_en_js(contact_no);
            if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
                $('#contact_no').val('');
            }

            if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
                $('#contact_no').val('');
            }
            if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                contact_no[2] == 2)) {
                $('#contact_no').val('');
            }
        }else{
            var contact_no = $(this).val();
            if ((typeof contact_no[0] != 'undefined' && contact_no[0] != 0)) {
                $(this).val('');
            }
            if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
                $(this).val('');
            }
            if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                    contact_no[2] == 2)) {
                $(this).val('');
            }
        }
    });

    $('#contact_no_confirmation').on('keyup', function() {
        if(langId == 2){
             var contact_no = $('#contact_no_confirmation').val();
            contact_no = cv_bn_to_en_js(contact_no);
            if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
                $('#contact_no_confirmation').val('');
            }

            if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
                $('#contact_no_confirmation').val('');
            }
            if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                contact_no[2] == 2)) {
                $('#contact_no_confirmation').val('');
            }
        }else{
            var contact_no = $(this).val();
            if ((typeof contact_no[0] != 'undefined' && contact_no[0] != 0)) {
                $(this).val('');
            }
            if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
                $(this).val('');
            }
            if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                    contact_no[2] == 2)) {
                $(this).val('');
            }
        }
    });

    $('#national_id, #national_id_confirmation').on('keyup', function() {
        var national_id = $(this).val();
        if (typeof national_id[0] != 'undefined' && national_id[0] == 0) {
            $(this).val('');
        }
    });
    // New JS By Asad
    // var address_check = $('#address_check');
    // $(address_check).prop('disabled', false);
    $('.click').click(function() {
        if ($(
                "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
            )
            .prop('disabled')) {
            $(
                    "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                )
                .prop('disabled', false);
        } else {
            $(
                    "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                )
                .prop('disabled', true);
            $('#permanent_division').val('');
            $('#permanent_district').val('');
            $('#permanent_upazila_id').val('');
            $('#permanent_union_id').val('');
            $('#permanent_village_id').val('');
            $('#permanent_post_code_id').val('');
        }
    });
});

function password_set_attribute() {}
$(document).ready(function() {
    $(":input[data-inputmask-mask]").inputmask();
    $(":input[data-inputmask-alias]").inputmask();
    $(":input[data-inputmask-regex]").inputmask("Regex");
});
$(document).ready(function() {
    if (langId == 1) {

        $('#name').addClass('eng-input');
        $('#name').removeAttr('oninput');
        //$('#tracking_no').attr('type', 'text');
        $('#tracking_no').addClass('eng-input');

        $('#father_name').addClass('eng-input');

        $('#mother_name').addClass('eng-input');

        $('#contact_no').addClass('eng-input');
        $('#contact_no_confirmation').addClass('eng-input');
        $('#nationality').addClass('eng-input');

        $('#national_id').addClass('eng-input');
        $('#national_id_confirmation').addClass('eng-input');

        $('#training_subject_description').addClass('eng-input');
        $('#it_knowledge').addClass('eng-input');
        $('#communication_skill').addClass('eng-input');

        $('#present_village_id').addClass('eng-input');
        $('#permanent_village_id').addClass('eng-input');

        $('#present_post_code_id').addClass('eng-input');
        $('#permanent_post_code_id').addClass('eng-input');


        $('.langEn').show();
        $('.langBn').hide();
        $('.BngBtn').hide();
        $('.EngBtn').show();
    } else {
        $('.langBn').show();
        $('.langEn').hide();
        $('.EngBtn').hide();
        $('.BngBtn').show();
    }
    if (langId == 2) {
        $('#name').addClass('bng-input');
        $('#name').removeAttr('oninput');

        $('#tracking_no').removeAttr('oninput');
        //$('#tracking_no').attr('type', 'text');
        $('#tracking_no').addClass('bng-input');

        $('#father_name').addClass('bng-input');
        $('#father_name').removeAttr('oninput');

        $('#mother_name').addClass('bng-input');
        $('#mother_name').removeAttr('oninput');

        $('#contact_no').addClass('bng-input');
        $('#contact_no').removeAttr('oninput');
        $('#contact_no_confirmation').addClass('bng-input');
        $('#contact_no_confirmation').removeAttr('oninput');

        $('#nationality').addClass('bng-input');

        $('#national_id').addClass('bng-input');
        $('#national_id').removeAttr('oninput');
        $('#national_id_confirmation').addClass('bng-input');
        $('#national_id_confirmation').removeAttr('oninput');

        $('#training_subject_description').addClass('bng-input');
        $('#it_knowledge').addClass('bng-input');
        $('#communication_skill').addClass('bng-input');

        $('#present_village_id').addClass('bng-input');
        $('#permanent_village_id').addClass('bng-input');

        $('#present_post_code_id').addClass('bng-input');
        $('#permanent_post_code_id').addClass('bng-input');
        $('#update_by_trackingNo').removeAttr('oninput');

    }else{
        $('#tracking_no').attr('type', 'number');
    }
});

function uniqueCheck(data){
        var selector = data.data.column;
        selector = "#"+selector+"";
        $.ajax({
            type: "POST",
            url: "{{url('check-unique')}}",
            data: data,
            success: function(response){
                if(response){
                    if (langId == 1) {
                        alert('Already exist try another');
                        $(selector).val('');
                        $(selector).focus();
                    } else {
                        alert('ইতিমধ্যে বিদ্যমান অন্য চেষ্টা করুন');
                        $(selector).val('');
                        $(selector).focus();
                    }
                }
            }
        });
    }
</script>
@endsection
