@extends('layouts.app')
@section('title', 'Appications')
@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif
<form action="{{ route('applications.update', $applications->id) }}" method="POST">
    @method('PATCH')
    @csrf
    <div class="container-fluid cust-form-bg" style="background: #cdf6e8;">
        <div class="form section-1 p-2">
            <div class="row">
                <div class="col-xs-12 offset-md-1 col-md-3">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right">
                            <span class="langBn"> ট্রাকিং নম্বর </span>
                            <span class="langEn"> Tracking No. </span>
                            <span class="text-danger"> * </span>
                        </label>
                        <div class="col-sm-12 col-md-4">
                            <input type="number" onkeydown="return event.keyCode !== 69" class="form-control"
                            id="tracking_no" name="tracking_no" value="{{ $applications->tracking_no }}"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            required autocomplete="off">
                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">আবেদনকারীর
                            নাম</span><span class="langEn">Applicant Name</span><span class="text-danger"> *
                        </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control" id="name" name="name"
                            oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');"
                            value="{{ $applications->name }}" required autocomplete="off">
                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">পিতা/স্বামীর
                            নাম</span><span class="langEn">Father's/Husband's Name</span><span class="text-danger">
                        * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control" id="father_name"
                            oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="father_name"
                            value="{{ $applications->father_name }}" required autocomplete="off">
                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">মায়ের
                            নাম</span><span class="langEn">Mother's Name</span><span class="text-danger">
                        * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control" id="mother_name"
                            oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');" name="mother_name"
                            value="{{ $applications->mother_name }}" required autocomplete="off">
                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">মোবাইল
                            নং</span><span class="langEn">Mobile number</span><span class="text-danger">
                        * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <!-- <input type="text" class="form-control" id="contact_no" /> -->
                            <input type="text" class="form-control" id="contact_no" minlength="11" maxlength="11"
                            name="contact_no" onfocusout="this.setAttribute('type','password');"
                            onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="{{ $applications->contact_no }}" required oncopy="return false" oncut="return false" onpaste="return false" autocomplete="off">
                            <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label">মোবাইল নং<span class="text-danger">
                        * </span></label>
                        <div class="col-sm-12 col-md-8">
                            <input type="text" class="form-control">
                        </div>
                    </div> -->
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">পূর্নরায় মোবাইল নং</span><span class="langEn">Mobile number</span><span class="text-danger">
                    * </span></label>
                    <div class="col-sm-12 col-md-8">
                        <input type="text" class="form-control" id="contact_no_confirmation" minlength="11"
                        maxlength="11" name="contact_no_confirmation"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                        value="{{ $applications->contact_no }}" required oncopy="return false" oncut="return false" onpaste="return false" autocomplete="off">
                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">রক্তের
                        গ্রুপ</span><span class="langEn">Blood group</span></label>
                        <div class="col-sm-12 col-md-8">
                            <select class="custom-select mr-sm-" id="blood_group" name="blood_group">
                                @php if($langId == 1){ @endphp
                                {!! getSelectOptions($data['blood_groups_en'], $applications->blood_group) !!}
                                @php } @endphp
                                @php if($langId == 2){ @endphp
                                {!! getSelectOptions($data['blood_groups_bn'], $applications->blood_group) !!}
                                @php } @endphp
                            </select>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-4 col-form-label text-right"><span
                            class="langBn">ই-মেইল</span><span class="langEn">Email</span></label>
                            <div class="col-sm-12 col-md-8">
                                <input type="email" class="form-control eng-input" id="email" name="email"
                                value="{{ $applications->email }}" data-inputmask-alias="email" data-val="true"
                                data-val-required="Required" onfocusout="this.setAttribute('type','password');"
                                onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                                oncopy="return false" oncut="return false" onpaste="return false" autocomplete="off">
                                <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">কনফার্ম
                                ই-মেইল</span><span class="langEn">Confirm Email</span></label>
                                <div class="col-sm-12 col-md-8">
                                    <input type="email" class="form-control eng-input" id="email_confirmation"
                                    oncopy="return false" oncut="return false" onpaste="return false"
                                    name="email_confirmation" value="{{ $applications->email }}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label class="col-sm-12 col-md-4 col-form-label text-right"><span
                                    class="langBn">লিঙ্গ</span><span class="langEn">gender</span><span class="text-danger"> * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" name="gender" id="gender" required>
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['gender_en'], $applications->gender) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['gender_bn'], $applications->gender) !!}
                                            @php } @endphp
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">জন্ম
                                        তারিখ</span><span class="langEn">Birth Day</span><span class="text-danger">
                                    * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control " id="date_of_birth" name="date_of_birth"
                                        onfocusout="this.setAttribute('type','password');"
                                        onfocus="this.setAttribute('type','text');" data-inputmask-alias="dd-mm-yyyy"
                                        data-inputmask="'yearrange': { 'minyear': '1971'}" data-val="true"
                                        data-val-required="Required" oncopy="return false" oncut="return false"
                                        onpaste="return false" value="{{ $applications->date_of_birth }}" required autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">জন্ম
                                        তারিখ</span><span class="langEn">Birth Day</span><span class="text-danger">
                                    * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control " id="confirm_date_of_birth"
                                        name="confirm_date_of_birth" data-inputmask-alias="dd-mm-yyyy"
                                        data-inputmask="'yearrange': { 'minyear': '1971'}" data-val="true"
                                        data-val-required="Required" value="{{ $applications->date_of_birth }}" required
                                        autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span
                                        class="langBn">জাতীয়তা</span><span class="langEn">Nationality</span><span
                                        class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" id="nationality" name="nationality"
                                        value="{{ $applications->nationality }}" autocomplete="off" required>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <span class="langBn">জাতীয় পরিচয়পত্র নং</span>
                                        <span class="langEn">National ID</span>
                                        <span class="text-danger"> * </span>
                                </label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" id="national_id" minlength="10" maxlength="17"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        oncopy="return false" oncut="return false" onpaste="return false" name="national_id"
                                        value="{{ $applications->national_id }}" required
                                        onfocusout="this.setAttribute('type','password');"
                                        onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                                        autocomplete="off">
                                        <div class="invalid-feedback">এই ক্ষেত্রটি সর্বনিম্ন ১০ ডিজিট এবং সর্বোচ্চ ১৭ ডিজিট পূরণ
                                        করুন.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">জাতীয় পরিচয়পত্র
                                        নং</span><span class="langEn">National ID</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" id="national_id_confirmation" minlength="10"
                                        maxlength="17"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        name="national_id_confirmation"
                                        value="{{ $applications->national_id }}" required autocomplete="off">
                                        <div class="invalid-feedback">এই ক্ষেত্রটি সর্বনিম্ন ১০ ডিজিট এবং সর্বোচ্চ ১৭ ডিজিট পূরণ
                                        করুন.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span
                                        class="langBn">ধর্ম</span><span class="langEn">Religion</span><span class="text-danger">
                                        *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" id="religion" name="religion" required>
                                            @if($langId == 1)
                                            {!! getSelectOptions($data['religions_en'], $applications->religion) !!}
                                            @else($langId == 2){ @endphp
                                            {!! getSelectOptions($data['religions_bn'], $applications->religion) !!}
                                            @endif                                           
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form section-2">
                        <h6 class="langBn">বর্তমান ঠিকানাঃ</h6>
                        <h6 class="langEn">Present Address</h6>
                        <div class="divider"> </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">বিভাগের
                                        নাম</span><span class="langEn">Division Name</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div id="division_title">
                                            <input type="text" id="division" name="present_division_id" class="form-control"
                                            value="{{ $data['division_en'][$applications->present_division_id] }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">জেলার
                                        নাম</span><span class="langEn">District Name</span><span class="text-danger">
                                    * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div id="district_title">
                                            <input type="text" id="district" name="present_district_id" class="form-control"
                                            value="{{ $data['district_en'][$applications->present_district_id] }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">উপজেলার
                                        নাম</span><span class="langEn">Upazila Name</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2 cust-width" id="upazila_id" name="present_upazila_id" required>
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['upazilas_en'], $applications->present_upazila_id) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['upazilas_bn'], $applications->present_upazila_id) !!}
                                            @php } @endphp
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">ইউনিয়ন
                                        নাম</span><span class="langEn">Union Name</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" id="union_id" name="present_union_id"
                                            value="{{ old('present_union_id') }}" required>
                                            {!! getSelectOptions($data['union_en'], $applications->present_union_id) !!}
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">গ্রাম/বাসা
                                        নং</span><span class="langEn">Village/House No</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input class="form-control" id="present_village_id" type="text" name="present_village_id"
                                        value="{{ $applications->present_village_id }}" required autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">পোষ্ট
                                        কোড</span><span class="langEn">Post Code</span><span class="text-danger">
                                    * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input class="form-control" id="present_post_code_id" type="text"
                                        name="present_post_code_id" value="{{ $applications->present_post_code_id }}" autocomplete="off" required>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form section-3">
                        <h6 class="langBn">স্থায়ী ঠিকানাঃ
                        <span class="do-btn mb-2">
                            <span><input type="checkbox" class="click" id="address_check">ঐ</span>
                        </span>
                        </h6>
                        <h6 class="langEn">Permanent Address:
                        <span class="do-btn mb-2">
                            <span><input type="checkbox" class="click" id="address_check">Do</span>
                        </span>
                        </h6>
                        <div class="divider-two"></div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right boltu"><span class="langBn">বিভাগের
                                        নাম</span><span class="langEn">Division Name</span><span class="text-danger"> *
                                    </span></label>
                                    <!-- <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" placeholder="বিভাগের নাম">
                                    </div> -->
                                    <div class="col-sm-12 col-md-8">
                                        
                                        <div id="division_title">
                                            <input type="text" id="permanent_division" name="permanent_division_id"
                                            class="form-control" value="{{ $data['division_en'][$applications->permanent_division_id] }}" disabled
                                            autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">জেলার
                                        নাম</span><span class="langEn">District Name</span><span class="text-danger">
                                    * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <div id="district_title">
                                            <input type="text" id="permanent_district" name="permanent_district_id"
                                            class="form-control" value="{{ $data['district_en'][$applications->permanent_district_id] }}" disabled
                                            autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">উপজেলার
                                        নাম</span><span class="langEn">Upazila Name</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2 cust-width" id="permanent_upazila_id"
                                            name="permanent_upazila_id" required>
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['upazilas_en'], $applications->permanent_upazila_id) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['upazilas_bn'], $applications->permanent_upazila_id) !!}
                                            @php } @endphp
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">ইউনিয়ন
                                        নাম</span><span class="langEn">Union Name</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" id="permanent_union_id" name="permanent_union_id"
                                            value="{{ old('permanent_union_id') }}" required>
                                            {!! getSelectOptions($data['union_en'], $applications->permanent_union_id) !!}
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">গ্রাম/বাসা
                                        নং</span><span class="langEn">Village/House No</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input class="form-control" id="permanent_village_id" type="text"
                                        name="permanent_village_id" value="{{ $applications->permanent_village_id }}" required
                                        autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">পোষ্ট
                                        কোড</span><span class="langEn">Post Code</span><span class="text-danger">
                                    * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input class="form-control" id="permanent_post_code_id" type="text"
                                        name="permanent_post_code_id" value="{{ $applications->permanent_post_code_id }}"
                                        autocomplete="off" required>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider-three"> </div>
                        </div>
                    </div>
                    <div class="form section-4">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">ব্যাচ
                                        নং</span><span class="langEn">Batch No</span><span class="text-danger"> *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" name="batch_no" id="batch_no" required>
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['batch_en'], $applications->batch_no) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['batch_bn'], $applications->batch_no) !!}
                                            @php } @endphp
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span
                                        class="langBn">সেশন</span><span class="langEn">Session</span><span class="text-danger">
                                        *
                                    </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" id="session_id" name="session_id" required>
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['sessions_en'], $applications->session_id) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['sessions_bn'], $applications->session_id) !!}
                                            @php } @endphp
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">শিক্ষাগত
                                        যোগ্যতা</span><span class="langEn">Educational Qualification</span><span
                                    class="text-danger"> * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" name="educational_qualification"
                                            id="educational_qualification" required>
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['educational_qualification_en'], $applications->educational_qualification) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['educational_qualification_bn'], $applications->educational_qualification) !!}
                                            @php } @endphp
                                        </select>
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <!--কিনা--><span class="langBn">আত্নকর্মী হতে ইচ্ছুক</span><span
                                    class="langEn">Self-employed</span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <select class="custom-select mr-sm-2" name="is_self_employed" id="is_self_employed"
                                            value="{{ old('is_self_employed') }}">
                                            @php if($langId == 1){ @endphp
                                            {!! getSelectOptions($data['IsSelfEmployed_en'], $applications->is_self_employed) !!}
                                            @php } @endphp
                                            @php if($langId == 2){ @endphp
                                            {!! getSelectOptions($data['IsSelfEmployed_bn'], $applications->is_self_employed) !!}
                                            @php } @endphp
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <!--তার সংক্ষিপ্ত
                                        বিবারণ--><span class="langBn">যে বিষয়ে দক্ষতা আছে</span><span class="langEn">Any
                                    Skill</span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" id="training_subject_description"
                                        name="training_subject_description" value="{{ $applications->training_subject_description }}"
                                        autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <!--(যদি
                                        থাকে)--><span class="langBn">আইটি জ্ঞানের বিবরণ</span><span class="langEn">IT knowledge
                                    details</span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" id="it_knowledge" name="it_knowledge"
                                        value="{{ $applications->it_knowledge }}" autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn">যোগাযোগের
                                    দক্ষতা (যদি থাকে)</span><span class="langEn">Communication skills (if any)</label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" class="form-control" id="communication_skill" name="communication_skill"
                                        value="{{ $applications->communication_skill }}" autocomplete="off">
                                        <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right">
                                        <!--<span>যে সকল বিষয়ে  ইচ্ছুক</span> -->
                                        <span class="langBn">প্রশিক্ষণ গ্রহণ করতে</span><span class="langEn">To receive
                                    training</span>
                                </label>
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" class="form-control" id="training_subject_description"
                                    name="training_subject_description" value="{{ $applications->training_subject_description }}"
                                    autocomplete="off">
                                    <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group row">
                                <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn"> ছবি আপলোড করুন
                                    </span><span class="langEn">Upload Photo</span><span class="text-danger"> * </span></label>
                                    <div class="col-sm-12 col-md-8">
                                        <input type="file" id="img_file" name="img" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-md-4 col-form-label text-right"><span class="langBn"> ফাইল সংযুক্ত
                                        করুন </span><span class="langEn">Upload File</span></label>
                                        <div class="col-sm-12 col-md-8">
                                            <input type="file" id="file" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4 btns text-center">
                                    <button class="btn btn-primary btn-sm"> Update </button>
                                    <button type="reset" class="btn btn-warning btn-sm text-white"> Reset </button>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="btns float-right my-3">
                            <button class="btn btn-primary btn-sm"> Submit </button>
                            <button class="btn btn-warning btn-sm"> Reset </button>
                        </div> -->
                    </div>
                </form>
                @endsection
                @section('extraJS')
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
                    integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
                    <link href="{{ asset('assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
                        rel="stylesheet">
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
                        <link rel="stylesheet" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">
                        <script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/moment.js') }}"></script>
                        <script src="{{ asset('assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.full.min.js"></script>
                        <script src="{{ asset('js/form-validation.js') }}"></script>
                        <script src="{{ asset('js/common.js') }}"></script>
                        <script>
                        
                        $(document).ready(function() {
                        $('#communication_skill').on('focus', function() {
                        // var e = $.Event("keypress");
                        // e.which = 65;
                        // $("#communication_skill").trigger(e);
                        //if($("#communication_skill").trigger($.Event("keypress", { keyCode: 86 }))){
                        //    console.log('1');
                        // }
                        });
                        //$('.EngBtn').hide();
                        // $("#upazila_id").select2({
                        //  theme: "bootstrap",
                        //  //theme: "bootstrap4"
                        //  allowClear: true
                        // });
                        // $("#session_id").select2({
                        //  theme: "bootstrap",
                        //  allowClear: true
                        // });
                        // $("#blood_group").select2({
                        //  theme: "bootstrap",
                        //  allowClear: true
                        // });
                        // $("#religion").select2({
                        //  theme: "bootstrap",
                        //  allowClear: true
                        // });
                        // $(".datepicker").datetimepicker({
                        //        //format: 'yyyy-mm-dd',
                        //        //minView: 2,
                        //        format: 'DD-MM-YYYY',
                        //  // autoclose:true,
                        //  // endDate: "today",
                        //  // maxDate: today
                        //    });
                        $('#upazila_id').on('change', function() {
                        var url = "{{ url('/getUpazliaInfos') }}";
                        var upazilaId = $(this).val();
                        var data = {
                        upazila_id: upazilaId
                        };
                        //console.log('upazilaId : ' + upazilaId);
                        ajaxCallCB(url, data, function(response) {
                        //console.log(response);
                        if (langId == 1) {
                        var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                        response.data.division_name_eng : '';
                        var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                        response.data.district_name_eng : '';
                        } else {
                        var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                        response.data.division_name_bng : '';
                        var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                        response.data.district_name_bng : '';
                        }
                        // $('#division_title').html('<strong>' + division_name + '</strong>');
                        // $('#district_title').html('<strong>' + district_name + '</strong>');
                        $('#division').val(division_name);
                        $('#district').val(district_name);
                        //console.log('response : ' + response);
                        });
                        });
                        $('#upazila_id').on('change', function() {
                        var url = "{{ url('/getUnionInfos') }}";
                        var upazilaId = $(this).val();
                        var union_name = '';
                        var data = {
                        upazila_id: upazilaId
                        };
                        $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        dataType: 'json'
                        }).done(function(response) {
                        //console.log(response);
                        var union = '';
                        union += '<option value=""></option>';
                        //console.log(union_name);
                        $.each(response, function(i, item) {
                        if (langId == 1) {
                        union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                        } else {
                        union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                        }
                        });
                        $('#union_id').html(union);
                        });
                        });
                        $('#union_id ').on('change', function() {
                        var url = "{{ url('/getPostalCode') }}";
                        var union_id = $(this).val();
                        var data = {
                        union_id: union_id
                        };
                        $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        dataType: 'json'
                        }).done(function(response) {
                        var post_code = (typeof response.data[0].postcode != 'undefined') ? response.data[0]
                        .postcode : '';
                        if (post_code) {
                        $('#present_post_code_id').val(post_code);
                        } else {
                        $('#present_post_code_id').val('');
                        }
                        });
                        });
                        // ------------permanent--------------
                        $('#permanent_upazila_id').on('change', function() {
                        var url = "{{ url('/getUpazliaInfos') }}";
                        var upazilaId = $(this).val();
                        var data = {
                        upazila_id: upazilaId
                        };
                        //console.log('upazilaId : ' + upazilaId);
                        ajaxCallCB(url, data, function(response) {
                        // var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                        //     response.data.division_name_eng : '';
                        // var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                        //     response.data.district_name_eng : '';
                        if (langId == 1) {
                        var division_name = (typeof response.data.division_name_eng != 'undefined') ?
                        response.data.division_name_eng : '';
                        var district_name = (typeof response.data.district_name_eng != 'undefined') ?
                        response.data.district_name_eng : '';
                        } else {
                        var division_name = (typeof response.data.division_name_bng != 'undefined') ?
                        response.data.division_name_bng : '';
                        var district_name = (typeof response.data.district_name_bng != 'undefined') ?
                        response.data.district_name_bng : '';
                        }
                        // $('#division_title').html('<strong>' + division_name + '</strong>');
                        // $('#district_title').html('<strong>' + district_name + '</strong>');
                        $('#permanent_division').val(division_name);
                        $('#permanent_district').val(district_name);
                        //console.log('response : ' + response);
                        });
                        });
                        $('#permanent_upazila_id').on('change', function() {
                        var url = "{{ url('/getUnionInfos') }}";
                        var upazilaId = $(this).val();
                        var data = {
                        upazila_id: upazilaId
                        };
                        $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        dataType: 'json'
                        }).done(function(response) {
                        var union = '';
                        union += '<option value=""></option>';
                        $.each(response, function(i, item) {
                        // union += '<option value="' + response[i].id + '">' + response[i]
                        //.union_name_bng + '</option>';
                        if (langId == 1) {
                        union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                        } else {
                        union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                        }
                        });
                        $('#permanent_union_id').html(union);
                        });
                        });
                        $('#permanent_union_id ').on('change', function() {
                        var url = "{{ url('/getPostalCode') }}";
                        var union_id = $(this).val();
                        var data = {
                        union_id: union_id
                        };
                        $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        dataType: 'json'
                        }).done(function(response) {
                        var post_code = (typeof response.data[0].postcode != 'undefined') ? response.data[0]
                        .postcode : '';
                        if (post_code) {
                        $('#permanent_post_code_id').val(post_code);
                        } else {
                        $('#permanent_post_code_id').val('');
                        }
                        });
                        });
                        //------------permanent---------------
                        $('body').on('keydown', '#tracking_no', function(e) {
                        if (e.keyCode === 9) {
                        var tracking_no = $('#tracking_no').val();
                        if (tracking_no == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#name', function(e) {
                        if (e.keyCode === 9) {
                        var name = $('#name').val();
                        name = name.trim();
                        if (name == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#union_id', function(e) {
                        if (e.keyCode === 9) {
                        var union_id = $('#union_id').val();
                        if (union_id == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#permanent_union_id', function(e) {
                        if (e.keyCode === 9) {
                        var permanent_union_id = $('#permanent_union_id').val();
                        if (permanent_union_id == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        // $('body').on('keydown', '#blood_group', function() {
                        //     if (e.keyCode === 9) {
                        //         var blood_group = $('#blood_group').val();
                        //         // if (blood_group == "") {
                        //         //     e.preventDefault();
                        //         // }
                        //     }
                        // });
                        $('#name').on('keyup', function() {
                        var name = $(this).val();
                        if (typeof name[0] != 'undefined' && (name[0] == (event.charCode == 32) || name[0] == '.' ||
                        name[0] == '-')) {
                        $(this).val('');
                        }
                        });
                        //onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))'
                        $('body').on('keydown', '#father_name', function(e) {
                        if (e.keyCode === 9) {
                        var father_name = $('#father_name').val();
                        father_name = father_name.trim();
                        if (father_name == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('#father_name').on('keyup', function() {
                        var father_name = $(this).val();
                        if (typeof father_name[0] != 'undefined' && (father_name[0] == (event.charCode == 32) ||
                        father_name[0] == '.' || father_name[0] == '-')) {
                        $(this).val('');
                        }
                        });
                        $('body').on('keydown', '#mother_name', function(e) {
                        if (e.keyCode === 9) {
                        var mother_name = $('#mother_name').val();
                        mother_name = mother_name.trim();
                        if (mother_name == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('#mother_name').on('keyup', function() {
                        var mother_name = $(this).val();
                        if (typeof mother_name[0] != 'undefined' && (mother_name[0] == (event.charCode == 32) ||
                        mother_name[0] == '.' || mother_name[0] == '-')) {
                        $(this).val('');
                        }
                        });
                        $('body').on('keydown', '#contact_no', function(e) {
                        if (e.keyCode === 9) {
                        var contact_no = $('#contact_no').val();
                        if (contact_no == "" || contact_no.length != 11) {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#contact_no_confirmation', function(e) {
                        if (e.keyCode === 9) {
                        var contact_no = $('#contact_no').val();
                        var contact_no_confirmation = $('#contact_no_confirmation').val();
                        if (contact_no_confirmation == "" || contact_no != contact_no_confirmation) {
                        e.preventDefault();
                        }
                        if (contact_no != contact_no_confirmation) {
                        $("#contact_no").focus();
                        $("#contact_no_confirmation").val('');
                        }
                        }
                        });
                        $('body').on('keydown', '#national_id', function(e) {
                        if (e.keyCode === 9) {
                        var national_id = $('#national_id').val();
                        if (national_id == "" || national_id.length != 10 && national_id.length != 13 && national_id
                        .length != 17) {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#national_id_confirmation', function(e) {
                        if (e.keyCode === 9) {
                        var national_id = $('#national_id').val();
                        var national_id_confirmation = $('#national_id_confirmation').val();
                        if (national_id_confirmation == "" || national_id != national_id_confirmation) {
                        e.preventDefault();
                        }
                        if (national_id != national_id_confirmation) {
                        $("#national_id").focus();
                        $("#national_id_confirmation").val('');
                        }
                        }
                        });
                        $('body').on('keydown', '#religion', function(e) {
                        if (e.keyCode === 9) {
                        var religion = $('#religion').val();
                        if (religion == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#nationality', function(e) {
                        if (e.keyCode === 9) {
                        var nationality = $('#nationality').val();
                        nationality = nationality.trim();
                        if (nationality == "" && langId == 2) {
                        e.preventDefault();
                        $(this).val('বাংলাদেশী');
                        }
                        if (nationality == "" && langId == 1) {
                        e.preventDefault();
                        $(this).val('Bangladeshi');
                        }
                        }
                        });
                        $('#nationality').on('focusout', function() {
                        if ($(this).val() == "" && langId == 1) {
                        $(this).val('Bangladeshi');
                        }
                        if ($(this).val() == "" && langId == 2) {
                        $(this).val('বাংলাদেশী ');
                        }
                        });
                        $('#nationality').on('focusin', function() {
                        if (langId == 1) {
                        $(this).val('Bangladeshi');
                        }
                        if (langId == 2) {
                        $(this).val('বাংলাদেশী ');
                        }
                        });
                        $('body').on('keydown', '#email', function() {
                        if (e.keyCode === 9) {
                        var email = $('#email').val();
                        email = email.trim();
                        // if (email == "") {
                        //     e.preventDefault();
                        // }
                        var re =/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        return re.test(email);
                        }
                        });
                        $('body').on('keydown', '#email_confirmation', function() {
                        if (e.keyCode === 9) {
                        var email = $('#email').val();
                        var email_confirmation = $('#email_confirmation').val();
                        if (email_confirmation == "" || email != email_confirmation) {
                        e.preventDefault();
                        }
                        if (email != email_confirmation) {
                        $("#email").focus();
                        $("#email_confirmation").val('');
                        }
                        }
                        });
                        $('body').on('keydown', '#date_of_birth', function(e) {
                        if (e.keyCode === 9) {
                        var date_of_birth = $('#date_of_birth').val();
                        //date_of_birth = date_of_birth.trim();
                        var input_date = date_of_birth;
                        date_of_birth = date_of_birth.replace(/\D/g, '');
                        if (date_of_birth == "" || date_of_birth.length != 8) {
                        e.preventDefault();
                        }
                        var date = new Date();
                        // var dd = date.getDate();
                        // var mm = date.getMonth() + 1;
                        var yyyy = date.getFullYear();
                        // if (dd < 10) {
                        //     dd = '0' + dd;
                        // }
                        // if (mm < 10) {
                        //     mm = '0' + mm;
                        // }
                        //current_date = dd + '/' + mm + '/' + yyyy;
                        year1 = input_date.split('/');
                        year2 = yyyy;
                        if (year1[2] > year2) {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#confirm_date_of_birth', function(e) {
                        if (e.keyCode === 9) {
                        var date_of_birth = $('#date_of_birth').val();
                        var confirm_date_of_birth = $('#confirm_date_of_birth').val();
                        date_of_birth = date_of_birth.replace(/\D/g, '');
                        confirm_date_of_birth = confirm_date_of_birth.replace(/\D/g, '');
                        if (confirm_date_of_birth == "" || date_of_birth != confirm_date_of_birth) {
                        e.preventDefault();
                        }
                        if (date_of_birth != confirm_date_of_birth) {
                        $("#date_of_birth").focus();
                        $("#confirm_date_of_birth").val('');
                        }
                        //  var date = new Date();
                        //  var yyyy = date.getFullYear();
                        //  year1 = input_date.split('/');
                        //  year2 = yyyy;
                        //  if(year1[2] > year2){
                        //     e.preventDefault();
                        // }
                        }
                        });
                        $('body').on('keydown', '#gender', function(e) {
                        if (e.keyCode === 9) {
                        var gender = $('#gender').val();
                        if (gender == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#batch_no', function(e) {
                        if (e.keyCode === 9) {
                        var batch_no = $('#batch_no').val();
                        if (batch_no == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#session_id', function(e) {
                        if (e.keyCode === 9) {
                        var session_id = $('#session_id').val();
                        if (session_id == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#upazila_id', function(e) {
                        if (e.keyCode === 9) {
                        var upazila_id = $('#upazila_id').val();
                        if (upazila_id == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#present_village_id', function(e) {
                        if (e.keyCode === 9) {
                        var present_village_id = $('#present_village_id').val();
                        if (present_village_id == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#permanent_village_id', function(e) {
                        if (e.keyCode === 9) {
                        var permanent_village_id = $('#permanent_village_id').val();
                        if (permanent_village_id == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        $('body').on('keydown', '#img_file', function(e) {
                        if (e.keyCode === 9) {
                        var img_file = $('#img_file').val();
                        if(img_file == ""){
                        e.preventDefault();
                        }
                        }
                        });
                        // $('body').on('keydown', '#session_id', function(e) {
                        //     if (e.keyCode === 9) {
                        //         var session_id = $('#session_id').val();
                        //         if(session_id == ""){
                        //             e.preventDefault();
                        //         }
                        //     }
                        // });
                        $('body').on('keydown', '#educational_qualification', function(e) {
                        if (e.keyCode === 9) {
                        var educational_qualification = $('#educational_qualification').val();
                        if (educational_qualification == "") {
                        e.preventDefault();
                        }
                        }
                        });
                        // $('body').on('keydown', '#is_self_employed', function(e) {
                        //     if (e.keyCode === 9) {
                        //         var is_self_employed = $('#is_self_employed').val();
                        //         if(is_self_employed == ""){
                        //             e.preventDefault();
                        //         }
                        //     }
                        // });
                        $('#contact_no, #contact_no_confirmation').on('keyup', function() {
                        var contact_no = $(this).val();
                        if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
                        $(this).val('');
                        }
                        if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
                        $(this).val('');
                        }
                        if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                        contact_no[2] == 2)) {
                        $(this).val('');
                        }
                        });
                        $('#national_id, #national_id_confirmation').on('keyup', function() {
                        var national_id = $(this).val();
                        if (typeof national_id[0] != 'undefined' && national_id[0] == 0) {
                        $(this).val('');
                        }
                        });
                        // New JS By Asad
                        var address_check = $('#address_check');
                        $(address_check).prop('disabled', false);
                        $('.click').click(function() {
                        if ($(
                        "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                        )
                        .prop('disabled')) {
                        $(
                        "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                        )
                        .prop('disabled', false);
                        } else {
                        $(
                        "#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
                        )
                        .prop('disabled', true);
                        $('#permanent_division').val('');
                        $('#permanent_district').val('');
                        $('#permanent_upazila_id').val('');
                        $('#permanent_union_id').val('');
                        $('#permanent_village_id').val('');
                        $('#permanent_post_code_id').val('');
                        }
                        });
                        });
                        function password_set_attribute() {
                        }
                        $(document).ready(function() {
                        $(":input[data-inputmask-mask]").inputmask();
                        $(":input[data-inputmask-alias]").inputmask();
                        $(":input[data-inputmask-regex]").inputmask("Regex");
                        });
                        $(document).ready(function() {
                        if (langId == 1) {
                        $('.langEn').show();
                        $('.langBn').hide();
                        $('.BngBtn').hide();
                        $('.EngBtn').show();
                        } else {
                        $('.langBn').show();
                        $('.langEn').hide();
                        $('.EngBtn').hide();
                        $('.BngBtn').show();
                        }
                        if (langId == 2) {
                        $('#name').addClass('bng-input');
                        $('#name').removeAttr('oninput');
                        $('#father_name').addClass('bng-input');
                        $('#father_name').removeAttr('oninput');
                        $('#mother_name').addClass('bng-input');
                        $('#mother_name').removeAttr('oninput');
                        $('#contact_no').addClass('eng-input');
                        //$('#contact_no').removeAttr('oninput');
                        $('#contact_no_confirmation').addClass('eng-input');
                        //$('#contact_no_confirmation').removeAttr('oninput');
                        $('#nationality').addClass('bng-input');
                        $('#national_id').addClass('eng-input');
                        //$('#national_id').removeAttr('oninput');
                        $('#national_id_confirmation').addClass('eng-input');
                        //$('#national_id_confirmation').removeAttr('oninput');
                        $('#training_subject_description').addClass('bng-input');
                        $('#it_knowledge').addClass('bng-input');
                        $('#communication_skill').addClass('bng-input');
                        $('#present_village_id').addClass('bng-input');
                        $('#permanent_village_id').addClass('bng-input');
                        $('#present_post_code_id').addClass('bng-input');
                        $('#permanent_post_code_id').addClass('bng-input');
                        }
                        });
                        
                        </script>
                        @endsection