@extends('layouts.app')

@section('content')

@if (session('status'))
<div class="notification is-success">
    {{ session('status') }}
</div>
@endif

<div class="log-box-full bg-white forget-min">
    <div class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="log-box">
                        <div class="login-box lg-bx-shdw">
                            <div class="box bg-white p-5 border-rt">
                                <div class="log-left">
                                    <img src="{{ asset('img/Youth+Development+Icon.png')}}" alt="images">
                                </div>
                            </div>

                            <div class="box bg-white p-5">

                                <form action="{{route('password.email')}}" method="POST" role="form">
                                    {{csrf_field()}}

                                    <h4 class="log-text margin-t-fifteen">Forgot Password?</h4>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-12 col-form-label purple-color">Email Address :
                                        </label>
                                        <div class="col-sm-12">

                                            <input class="form-control cust-log-cnt input {{$errors->has('email') ? 'is-danger' : ''}}" type="text" name="email" id="email" placeholder="name@example.com" value="{{old('email')}}">
                                        </div>
                                        @if ($errors->has('email'))
                                        <p class="help is-danger">{{$errors->first('email')}}</p>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12 log-btn mt-3">
                                            <button class="btn log-userbtn">Get Reset Link</button>
                                        </div>
                                    </div>
                                    
                                    <div class="log-back">
                                    <a href="{{route('login')}}">
                                      <i class="fa fa-caret-left m-r-10"></i> 
                                          <span>
                                          Back to Login
                                          </span>
                                    
                                    </a>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection