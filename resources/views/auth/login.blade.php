@extends('layouts.app')
@section('content')
@php

 $langId = Cookie::get('langId');

$data['division_en'] = DB::table('geo_divisions')->pluck('division_name_eng', 'id')->toArray();
$data['division_bn'] = DB::table('geo_divisions')->pluck('division_name_bng', 'id')->toArray();
$data['district_en'] = DB::table('geo_districts')->pluck('district_name_eng', 'id')->toArray();
$data['district_bn'] = DB::table('geo_districts')->pluck('district_name_bng', 'id')->toArray();
$data['upazilas_en'] = DB::table('geo_upazilas')->pluck('upazila_name_eng', 'id')->toArray();
$data['upazilas_bn'] = DB::table('geo_upazilas')->pluck('upazila_name_bng', 'id')->toArray();

 @endphp
<div class="log-box-full">
    <div class="container-fluid log-min">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="log-box">

                        <div class="login-box">

                            <div class="box bg-white p-5 border-rt">
                                <div class="log-left">




        <form class="registration-form" action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="registration-title">
            <h3 class="text-center">
             @if($langId == 1)
                {{"Registration"}}
                @else($langId == 2)
                {{"Registration"}}
                @endif
            </h3>
        </div>
        <div class="registration-body">
            <div class="form-group">
                <label for="">
                    @if($langId == 1)
                    {{"Full Name"}}
                    @else($langId == 2)
                    {{"Full Name"}}
                    @endif
                    <span class="text-danger"> *
                            </span>
                </label>
                <input style="text-align: center" class="input {{$errors->has('name') ? 'is-danger' : ''}} form-control" type="text" name="name"
                    id="name" value="{{old('name')}}" oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');"
                    required autocomplete="off">
                @if ($errors->has('name'))
                <p class="help is-danger">{{$errors->first('name')}}</p>
                @endif

            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Division Name"}}
                            @else($langId == 2)
                            {{"Division Name"}}
                            @endif
                            <span class="text-danger"> *
                            </span>
                        </label>
                        <select class="custom-select mr-sm-2 cust-width" id="division_id" name="division_id" required>
                             @if($langId == 1)
                            {!! getSelectOptions($data['division_en'], old('division_id')) !!}
                            @endif
                            @if($langId == 2)
                            {!! getSelectOptions($data['division_bn'], old('division_id')) !!}
                            @endif
                        </select>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"District Name"}}
                            @else($langId == 2)
                            {{"District Name"}}
                            @endif
                            <span class="text-danger"> *
                            </span>
                        </label>
                        <select class="custom-select mr-sm-2" id="district_id" name="district_id"
                            value="{{ old('district_id') }}" required>
                        </select>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Upazila Name"}}
                            @else($langId == 2)
                            {{"Upazila Name"}}
                            @endif
                            <span class="text-danger"> *
                            </span>
                        </label>
                        <select class="custom-select mr-sm-2" id="upazila_id" name="upazila_id"
                            value="{{ old('upazila_id') }}" required>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <!-- <label for="">
                            @if($langId == 1)
                            {{"Mobile No"}}
                            @else($langId == 2)
                            {{"Mobile No"}}
                            @endif
                        </label>
                        <input form-controlt type="text" class="form-control" id="contact_no" minlength="11" maxlength="11" name="contact_no"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="{{ old('contact_no') }}" required oncopy="return false" oncut="return false"
                            onpaste="return false" autocomplete="off"> -->
                            <label>Union Name<span class="text-danger"> *
                            </span></label>
                            <select class="custom-select mr-sm-2" id="union_id" name="union_id" required>
                            </select>
                            <!-- <div class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</div> -->
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Mobile No"}}
                            @else($langId == 2)
                            {{"Mobile No"}}
                            @endif
                            <span class="text-danger"> *
                            </span>
                        </label>
                        <input class="input {{$errors->has('contact_no') ? 'is-danger' : ''}} form-control" type="text"
                            name="contact_no" id="contact_no" value="{{old('contact_no')}}"
                            data-val="true" required autocomplete="off">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"National ID"}}
                            @else($langId == 2)
                            {{"National ID"}}
                            @endif
                            <span class="text-danger"> *
                            </span>
                        </label>
                        <input style="text-align: center" class="input {{$errors->has('nid') ? 'is-danger' : ''}} form-control" type="text"
                            name="nid" id="nid" value="{{old('nid')}}"
                            data-val="true" required autocomplete="off">

                    </div>
                </div>
            </div>



            <span class="btn btn-success is-success is-outlined is-fullwidth m-t-30 w-100" id="registrationBtn">
                    @if($langId == 1)
                    {{"Register"}}
                    @else($langId == 2)
                    {{"Register"}}
                    @endif
            </span>
            <h6 class="form-foot">
                <!-- @if($langId == 1)
                    {{"Already have an Account?"}}
                    @else($langId == 2)
                    {{"আপনার কি কোন অ্যাকাউন্ট আছে?"}}
                    @endif -->
            </h6>
            <a href="{{route('login')}}" class="is-muted">
                 <!-- @if($langId == 1)
                    {{"LogIn"}}
                    @else($langId == 2)
                    {{"লগইন"}}
                    @endif -->
                </a>
        </div>
    </form>


                                </div>
                            </div>

                            <div class="box bg-white p-5">

                                <form action="{{route('login')}}" method="POST" role="form">
                                    @csrf
                                    <h4 class="log-text">
                                        @if($langId == 1)
                                        {{"Log In"}}
                                        @else($langId == 2)
                                        {{"Log In"}}
                                        @endif
                                    </h4>
                                    <div class="form-group row">
                                        <label for="nid" class="col-sm-12 col-form-label purple-color">
                                        @if($langId == 1)
                                        {{"User ID"}}
                                        @else($langId == 2)
                                        {{"User ID"}}
                                        @endif
                                        </label>
                                        <div class="col-sm-12">
                                            <input
                                                class="form-control cust-log-cnt input {{$errors->has('nid') ? 'is-danger' : ''}}"
                                                type="text" name="nid" value="{{old('nid')}}"
                                                placeholder=" user id" autocomplete="off">
                                        </div>
                                        @if ($errors->has('nid'))
                                        <p class="help is-danger">{{$errors->first('nid')}}</p>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="Password" class="col-sm-12 col-form-label purple-color">
                                            @if($langId == 1)
                                            {{"Password"}}
                                            @else($langId == 2)
                                            {{"Password"}}
                                            @endif
                                        </label>
                                        <div class="col-sm-12">
                                            <input class="form-control cust-log-cnt input {{$errors->has('password') ? 'is-danger' : ''}}"
                                                type="password" name="password" id="password"
                                                placeholder=" password">
                                        </div>
                                        @if ($errors->has('password'))
                                        <p class="help is-danger">{{$errors->first('password')}}</p>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-6 custm-check">
                                            <b-checkbox name="remember" class="form-check-input m-t-20 remember">
                                                <!-- @if($langId == 1)
                                                {{"Remember Me"}}
                                                @else($langId == 2)
                                                {{"মনে রাখুন"}}
                                                @endif -->
                                            </b-checkbox>

                                        </div>
                                        <div class="col-6 purple-color">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 log-btn mt-4">
                                            <button class="btn log-userbtn">
                                            @if($langId == 1)
                                            {{"LogIn"}}
                                            @else($langId == 2)
                                            {{"LogIn"}}
                                            @endif
                                            </button>
                                        </div>
                                    </div>

                                    <div class="form-group forget-inline">
                                        <a href="{{route('password.request')}}" class="forget-pass">
                                        <!-- @if($langId == 1)
                                        {{"Forget Password?"}}
                                        @else($langId == 2)
                                        {{"আপনি কি পাসওয়ার্ড ভুলে গেছেন"}}
                                        @endif -->
                                        </a>
                                        <!-- <span class="other-wise">or</span> -->
                                    </div>
                                    <div class="form-group form-foot">
                                        <a href="{{route('register')}}" class="register">
                                        <!-- @if($langId == 1)
                                        {{"Register Now"}}
                                        @else($langId == 2)
                                        {{"রেজিট্রেশন"}}
                                        @endif -->
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
    <script>
    $(document).ready(function(){

        $('#registrationBtn').on('click', function(){
            var name = $('#name').val();
            var division_id = $('#division_id').val();
            var district_id = $('#district_id').val();
            var upazila_id = $('#upazila_id').val();
            var union_id = $('#union_id').val();
            var contact_no = $('#contact_no').val();
            var nid = $('#nid').val();
            var data = {
                "_token": "{{ csrf_token() }}",
                name: name,
                division_id: division_id,
                district_id: district_id,
                upazila_id: upazila_id,
                union_id: union_id,
                contact_no: contact_no,
                nid: nid,
            };
            $.ajax({
                type:'POST',
                url: "{{ url('register-user') }}",
                data:data,
                success: function(res){
                    $('#name').val('');
                    $('#division_id').val('');
                    $('#district_id').val('');
                    $('#upazila_id').val('');
                    $('#union_id').val('');
                    $('#contact_no').val('');
                    $('#nid').val('');
                    var al = "Registration Success ! Please Login. \n Your Password: "+ res;
                    alert(al);

                },
                error: function(){

                }
            });
        });


    $(document).on('change', '#division_id', function() {
        var DivissionId = $(this).val();
         var data = {
            "_token": "{{ csrf_token() }}",
            divissionId: DivissionId
        };
        //alert(DivissionId);
        $.ajax({
            type: "POST",
            url: "{{ url('getDistrictInfos') }}",
            data: data,
            success: function(data) {
                //console.log(data);
                if(langId == 1){
                    var district = '<option value="" ></option>';
                }else{
                    var district = '<option value="" ></option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });

    $(document).on('change', '#district_id', function() {
        var DistrictId = $(this).val();
        var data = {
            "_token": "{{ csrf_token() }}",
            districtId: DistrictId
        };
        //alert(DistrictId);
        $.ajax({
            type: "post",
            url: "{{ url('getUpazliaInfo') }}",
            data: data,
            success: function(data) {
                //console.log(data);

                if(langId == 1){
                    var upazila = '<option value="" ></option>';
                }else{
                    var upazila = '<option value="" ></option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });

    $(document).on('change', '#upazila_id', function(){
        //--############ FOR Load Union Data---------------------------
        var upazilaId = $(this).val();
        var url = "{{ url('/getUnionInfos') }}";
        var union_name = '';
        var data = {
            "_token": "{{ csrf_token() }}",
            upazila_id: upazilaId
        };
        console.log('upazila_id' + upazila_id);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json'
        }).done(function(response) {
            var union = '';
            var selected = '';
            union += '<option value=""></option>';
            // if(editSegment == 'edit'){
            //     var oldValue = "{{!empty($applications[0]['present_union_id']) ? $applications[0]['present_union_id'] : ''}}";
            // }else{
            //     var oldValue = '{{old("present_union_id")}}';
            // }

            $.each(response, function(i, item) {
                // if(oldValue == response[i].id){
                //     selected = 'selected';
                // }else{
                //     selected = '';
                // }
                if (langId == 1) {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_eng + '</option>';
                } else {
                    union += '<option value="' + response[i].id + '">' + response[i]
                        .union_name_bng + '</option>';
                }
            });
            $('#union_id').html(union);
        });
    });

});
    </script>
@endsection
