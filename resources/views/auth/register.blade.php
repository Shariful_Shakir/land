@extends('layouts.app')
@section('content')

<div class="registration-bg">
    <form class="registration-form" action="{{route('register')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="registration-title">
            <h3 class="text-center"> 
             @if($langId == 1)
                {{"Registration"}}
                @else($langId == 2)
                {{"রেজিট্রেশন"}}
                @endif
            </h3>
        </div>
        <div class="registration-body">
            <div class="form-group">
                <label for="">
                    @if($langId == 1)
                    {{"Full Name"}}
                    @else($langId == 2)
                    {{"নাম"}}
                    @endif
                </label>
                <input class="input {{$errors->has('name') ? 'is-danger' : ''}} form-control" type="text" name="name"
                    id="name" value="{{old('name')}}" oninput="this.value = this.value.replace(/[^A-Za-z .-]/g, '');"
                    required>
                @if ($errors->has('name'))
                <p class="help is-danger">{{$errors->first('name')}}</p>
                @endif
                <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Division Name"}}
                            @else($langId == 2)
                            {{"বিভাগের নাম"}}
                            @endif
                        </label>
                        <select class="custom-select mr-sm-2 cust-width" id="division_id" name="division_id" required>
                            @if($langId == 1)
                            {!! getSelectOptions($data['division_en'], old('division_id')) !!}
                            @endif
                            @if($langId == 2)
                            {!! getSelectOptions($data['division_bn'], old('division_id')) !!}
                            @endif
                        </select>
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"District Name"}}
                            @else($langId == 2)
                            {{"জেলার নাম"}}
                            @endif
                        </label>
                        <select class="custom-select mr-sm-2" id="district_id" name="district_id"
                            value="{{ old('district_id') }}" required>
                        </select>
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Upazila Name"}}
                            @else($langId == 2)
                            {{"উপজেলার নাম"}}
                            @endif
                        </label>
                        <select class="custom-select mr-sm-2" id="upazila_id" name="upazila_id"
                            value="{{ old('upazila_id') }}" required>
                        </select>
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Mobile No"}}
                            @else($langId == 2)
                            {{"মোবাইল নং"}}
                            @endif
                        </label>
                        <input form-controlt type="text" class="form-control" id="contact_no" minlength="11" maxlength="11" name="contact_no" onfocusout="this.setAttribute('type','password');" onfocus="this.setAttribute('type','text');" onblur="password_set_attribute();"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="{{ old('contact_no') }}" required oncopy="return false" oncut="return false"
                            onpaste="return false" autocomplete="off">
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Email"}}
                            @else($langId == 2)
                            {{"ই-মেইল"}}
                            @endif
                        </label>
                        <input class="input {{$errors->has('email') ? 'is-danger' : ''}} form-control" type="text"
                            name="email" id="email" value="{{old('email')}}" data-inputmask-alias="email"
                            data-val="true" required>

                        @if ($errors->has('email'))
                        <p class="help is-danger">{{$errors->first('email')}}</p>
                        @endif
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Password"}}
                            @else($langId == 2)
                            {{"পাসওর্য়াড"}}
                            @endif
                        </label>
                        <!-- <inpu form-controlt type="text" class="form-control" name="password" id="password" v-if="!auto_password" placeholder=""> -->
                        <input class="input {{$errors->has('password') ? 'is-danger' : ''}} form-control" type="password" name="password" id="password" required>
                        @if ($errors->has('password'))
                        <p class="help is-danger">{{$errors->first('password')}}</p>
                        @endif
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="">
                            @if($langId == 1)
                            {{"Re-Password"}}
                            @else($langId == 2)
                            {{"পূর্নরায় পাসওর্য়াড"}}
                            @endif
                        </label>
                        <!-- <inpu form-controlt type="text" class="form-control" name="password" id="password" v-if="!auto_password" placeholder=""> -->
                        <input class="input {{$errors->has('password_confirmation') ? 'is-danger' : ''}} form-control"
                            type="password" name="password_confirmation" id="password_confirmation" required>
                        @if ($errors->has('password_confirmation'))
                        <p class="help is-danger">{{$errors->first('password_confirmation')}}</p>
                        @endif
                        <small class="invalid-feedback">অনুগ্রহ করে এই ঘরটি পুরন কর</small>
                    </div>
                </div>
            </div>
            <button class="btn btn-success is-success is-outlined is-fullwidth m-t-30 w-100">
                    @if($langId == 1)
                    {{"Register"}}
                    @else($langId == 2)
                    {{"রেজিট্রেশন করুন"}}
                    @endif
            </button>
            <h6 class="form-foot">
                @if($langId == 1)
                    {{"Already have an Account?"}}                   
                    @else($langId == 2)
                    {{"আপনার কি কোন অ্যাকাউন্ট আছে?"}}
                    @endif
            </h6>
            <a href="{{route('login')}}" class="is-muted">
                 @if($langId == 1)
                    {{"LogIn"}}                   
                    @else($langId == 2)
                    {{"লগইন"}}
                    @endif
                </a>
        </div>
    </form>
</div>

@endsection

@section('extraJS')
<script>
    $(document).on('change', '#division_id', function() {   
        var DivissionId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DivissionId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getDistrictInfos/' + DivissionId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                if(langId == 1){
                    var district = '<option value="" >Select District</option>';
                }else{
                    var district = '<option value="" >সিলেক্ট জেলা</option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_eng + '</option>';
                    } else {
                    district += '<option value="' + data[i].id + '">' + data[i]
                    .district_name_bng + '</option>';
                    }
                }
                //alert(op);
                $("#district_id").html(district);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });


$(document).on('change', '#district_id', function() {   
        var DistrictId = $(this).val();
        var APP_URL = {!! json_encode(url('/')) !!}
        //alert(DistrictId);
        $.ajax({
            type: "GET",
            url: APP_URL + '/getUpazliaInfo/' + DistrictId,
            dataType: "JSON",
            success: function(data) {
                //console.log(data);
                
                if(langId == 1){
                    var upazila = '<option value="" >Select Upazila</option>';
                }else{
                    var upazila = '<option value="" >সিলেক্ট উপজেলা</option>';
                }

                for (var i = 0; i < data.length; i++) {
                    if (langId == 1) {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_eng + '</option>';
                    } else {
                    upazila += '<option value="' + data[i].id + '">' + data[i]
                    .upazila_name_bng + '</option>';
                    }
                }
                //alert(upazila);
                $("#upazila_id").html(upazila);
                //div.find('.stoppagePoint').append(op);
            },
            error: function() {
                alert('Data Not Found');
            }
        });
    });


$(document).ready(function() {
  
  $('#selectAllDivision').on('click',function(){
    if(this.checked) {
        $('.divisionCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.divisionCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });
  $('#selectAllDistrict').on('click',function(){
    if(this.checked) {
        $('.districtCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.districtCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });
  $('#selectAllUpazila').on('click',function(){
    if(this.checked) {
        $('.upazilaCheckBox:input:checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.upazilaCheckBox:input:checkbox').each(function() {
            this.checked = false;                       
        });
    }
  });




//console.log('upazilaId : ' + upazilaId);
ajaxCallCB(url, data, function(response) {
//console.log(response);
if (langId == 1) {
var division_name = (typeof response.data.division_name_eng != 'undefined') ?
response.data.division_name_eng : '';
var district_name = (typeof response.data.district_name_eng != 'undefined') ?
response.data.district_name_eng : '';
} else {
var division_name = (typeof response.data.division_name_bng != 'undefined') ?
response.data.division_name_bng : '';
var district_name = (typeof response.data.district_name_bng != 'undefined') ?
response.data.district_name_bng : '';
}
// $('#division_title').html('<strong>' + division_name + '</strong>');
// $('#district_title').html('<strong>' + district_name + '</strong>');
$('#division').val(division_name);
$('#district').val(district_name);
//console.log('response : ' + response);
});
$('body').on('keydown', '#email', function() {
if (e.keyCode === 9) {
var email = $('#email').val();
email = email.trim();
// if (email == "") {
//     e.preventDefault();
// }
var re =/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
return re.test(email);
}
});

    $('#contact_no, #contact_no_confirmation').on('keyup', function() {
        var contact_no = $(this).val();
        if (typeof contact_no[0] != 'undefined' && contact_no[0] != 0) {
            $(this).val('');
        }
        if (typeof contact_no[1] != 'undefined' && contact_no[1] != 1) {
            $(this).val('');
        }
        if (typeof contact_no[2] != 'undefined' && (contact_no[2] == 0 || contact_no[2] == 1 ||
                contact_no[2] == 2)) {
            $(this).val('');
        }
    });

// New JS By Asad
var address_check = $('#address_check');
$(address_check).prop('disabled', false);
$('.click').click(function() {
if ($(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled')) {
$(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled', false);
} else {
$(
"#permanent_union_id, #permanent_village_id, #permanent_post_code_id, #permanent_upazila_id"
)
.prop('disabled', true);
$('#permanent_division').val('');
$('#permanent_district').val('');
$('#permanent_upazila_id').val('');
$('#permanent_union_id').val('');
$('#permanent_village_id').val('');
$('#permanent_post_code_id').val('');
}
});
});
function password_set_attribute() {
}
$(document).ready(function() {
$(":input[data-inputmask-mask]").inputmask();
$(":input[data-inputmask-alias]").inputmask();
$(":input[data-inputmask-regex]").inputmask("Regex");
});
$(document).ready(function() {
if (langId == 1) {
$('.langEn').show();
$('.langBn').hide();
$('.BngBtn').hide();
$('.EngBtn').show();
} else {
$('.langBn').show();
$('.langEn').hide();
$('.EngBtn').hide();
$('.BngBtn').show();
}
if (langId == 2) {
$('#name').addClass('bng-input');
$('#name').removeAttr('oninput');
$('#father_name').addClass('bng-input');
$('#father_name').removeAttr('oninput');
$('#mother_name').addClass('bng-input');
$('#mother_name').removeAttr('oninput');
$('#contact_no').addClass('eng-input');
//$('#contact_no').removeAttr('oninput');
$('#contact_no_confirmation').addClass('eng-input');
//$('#contact_no_confirmation').removeAttr('oninput');
$('#nationality').addClass('bng-input');
$('#national_id').addClass('eng-input');
//$('#national_id').removeAttr('oninput');
$('#national_id_confirmation').addClass('eng-input');
//$('#national_id_confirmation').removeAttr('oninput');
$('#training_subject_description').addClass('bng-input');
$('#it_knowledge').addClass('bng-input');
$('#communication_skill').addClass('bng-input');
$('#present_village_id').addClass('bng-input');
$('#permanent_village_id').addClass('bng-input');
$('#present_post_code_id').addClass('bng-input');
$('#permanent_post_code_id').addClass('bng-input');
}
});
</script>
@endsection