<?php

use Illuminate\Database\Seeder;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $religions = [
			['title_eng' => 'Islam', 'title_bng' => 'ইসলাম'], 
			['title_eng' => 'Hindu', 'title_bng' => 'হিন্দু'], 
			['title_eng' => 'Buddha', 'title_bng' => 'বৈদ্ধ'], 
			['title_eng' => 'Christian', 'title_bng' => 'খ্রিক্ষ্টান'], 
			['title_eng' => 'Other', 'title_bng' => 'অন্যান্য']
			
		];
		
		DB::table('religions')->insert($religions);
    }
}
