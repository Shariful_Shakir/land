<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sessions = [
            ['title_eng' => '2015-16', 'title_bng' => '২০১৫-১৬'], 
            ['title_eng' => '2016-17', 'title_bng' => '২০১৬-১৭'], 
            ['title_eng' => '2017-18', 'title_bng' => '২০১৭-১৮'], 
            ['title_eng' => '2018-19', 'title_bng' => '২০১৮-১৯'], 
            ['title_eng' => '2019-20', 'title_bng' => '২০১৯-২০']
        ];
		
		DB::table('sessions')->insert($sessions);
    }
}
