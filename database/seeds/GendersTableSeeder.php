<?php

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = [
			['title_eng' => 'Male', 'title_bng' => 'পুরুষ'], 
			['title_eng' => 'Female', 'title_bng' => 'মহিলা'],  
			['title_eng' => 'Other', 'title_bng' => 'অন্যান্য']
			
		];
		
		DB::table('genders')->insert($genders);
    }
}
