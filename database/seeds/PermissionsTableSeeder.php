<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'UserController@index',
                'display_name_en' => 'Index Users',
                'display_name_bn' => 'ইউজার ইনডেক্স দেখতে পারবে',
                'created_at' => '2019-09-18 12:46:13',
                'updated_at' => '2019-09-18 13:52:24',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'UserController@create',
                'display_name_en' => 'Create Users',
                'display_name_bn' => 'ইউজার তৈরী করতে পারবে',
                'created_at' => '2019-09-18 12:46:13',
                'updated_at' => '2019-09-18 13:49:11',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'UserController@store',
                'display_name_en' => 'Store Users',
                'display_name_bn' => 'ইউজার স্টোর করতে পারবে',
                'created_at' => '2019-09-18 12:46:13',
                'updated_at' => '2019-09-18 13:50:18',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'UserController@show',
                'display_name_en' => 'Show Users',
                'display_name_bn' => 'ইউজারকে দেখতে পারবে',
                'created_at' => '2019-09-18 12:46:13',
                'updated_at' => '2019-09-18 13:51:50',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'UserController@edit',
                'display_name_en' => 'Edit User',
                'display_name_bn' => 'ইউজার ইডিট',
                'created_at' => '2019-09-18 12:46:14',
                'updated_at' => '2019-09-18 13:53:56',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'UserController@update',
                'display_name_en' => 'Update User',
                'display_name_bn' => 'ইউজার আপডেট',
                'created_at' => '2019-09-18 12:46:14',
                'updated_at' => '2019-09-18 13:54:47',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'UserController@destroy',
                'display_name_en' => 'Delete User',
                'display_name_bn' => 'ইউজার ডিলিট',
                'created_at' => '2019-09-18 12:46:14',
                'updated_at' => '2019-09-18 13:55:32',
            ),
            7 => 
            array (
                'id' => 13,
                'name' => 'ApplicationController@index',
                'display_name_en' => 'Application Index',
                'display_name_bn' => 'আবেদনের ইনডেক্স',
                'created_at' => '2019-09-18 14:38:01',
                'updated_at' => '2019-09-18 14:38:50',
            ),
            8 => 
            array (
                'id' => 14,
                'name' => 'ApplicationController@create',
                'display_name_en' => 'Create Application',
                'display_name_bn' => 'আবেদন তৈরী করতে পারবে',
                'created_at' => '2019-09-18 14:43:38',
                'updated_at' => '2019-09-18 14:43:38',
            ),
            9 => 
            array (
                'id' => 15,
                'name' => 'ApplicationController@store',
                'display_name_en' => 'Store Application',
                'display_name_bn' => 'আবেদন স্টোর করতে পারবে',
                'created_at' => '2019-09-18 14:45:34',
                'updated_at' => '2019-09-18 14:45:34',
            ),
            10 => 
            array (
                'id' => 16,
                'name' => 'ApplicationController@show',
                'display_name_en' => 'Show Application',
                'display_name_bn' => 'আবেদন দেখতে পারবে',
                'created_at' => '2019-09-18 14:46:33',
                'updated_at' => '2019-09-18 14:46:33',
            ),
            11 => 
            array (
                'id' => 17,
                'name' => 'ApplicationController@edit',
                'display_name_en' => 'Edit Application',
                'display_name_bn' => 'আবেদন ইডিট করতে পারবে',
                'created_at' => '2019-09-18 14:47:34',
                'updated_at' => '2019-09-18 14:47:34',
            ),
            12 => 
            array (
                'id' => 18,
                'name' => 'ApplicationController@update',
                'display_name_en' => 'Update Application',
                'display_name_bn' => 'আবেদন আপডেট করতে পারবে',
                'created_at' => '2019-09-18 14:48:40',
                'updated_at' => '2019-09-18 14:48:40',
            ),
            13 => 
            array (
                'id' => 19,
                'name' => 'ApplicationController@destroy',
                'display_name_en' => 'Delete Application',
                'display_name_bn' => 'আবেদন ডিলিট করতে পারবে',
                'created_at' => '2019-09-18 14:50:07',
                'updated_at' => '2019-09-18 14:50:07',
            ),
            14 => 
            array (
                'id' => 24,
                'name' => 'AttachmentController@index',
                'display_name_en' => 'Index Attachment',
                'display_name_bn' => 'সংযুক্তির ইনডেক্স',
                'created_at' => '2019-09-18 14:55:08',
                'updated_at' => '2019-09-18 14:55:08',
            ),
            15 => 
            array (
                'id' => 25,
                'name' => 'AttachmentController@create',
                'display_name_en' => 'Create Attachment',
                'display_name_bn' => 'সংযুক্তির তৈরী করতে পারবে',
                'created_at' => '2019-09-18 14:56:27',
                'updated_at' => '2019-09-18 14:56:27',
            ),
            16 => 
            array (
                'id' => 26,
                'name' => 'AttachmentController@store',
                'display_name_en' => 'Store Attachment',
                'display_name_bn' => 'সংযুক্তি স্টোর করতে পারবে',
                'created_at' => '2019-09-18 14:57:44',
                'updated_at' => '2019-09-18 14:57:44',
            ),
            17 => 
            array (
                'id' => 27,
                'name' => 'AttachmentController@show',
                'display_name_en' => 'Show Attachment',
                'display_name_bn' => 'সংযুক্তির দেখতে পারবে',
                'created_at' => '2019-09-18 15:02:15',
                'updated_at' => '2019-09-18 15:02:15',
            ),
            18 => 
            array (
                'id' => 28,
                'name' => 'AttachmentController@edit',
                'display_name_en' => 'Edit Attachment',
                'display_name_bn' => 'সংযুক্তির মডিফাই করতে পারবে',
                'created_at' => '2019-09-18 15:03:16',
                'updated_at' => '2019-09-18 15:03:16',
            ),
            19 => 
            array (
                'id' => 29,
                'name' => 'AttachmentController@update',
                'display_name_en' => 'Update Attachment',
                'display_name_bn' => 'সংযুক্তি আপডেট করতে পারবে',
                'created_at' => '2019-09-18 15:04:05',
                'updated_at' => '2019-09-18 15:04:05',
            ),
            20 => 
            array (
                'id' => 30,
                'name' => 'AttachmentController@destroy',
                'display_name_en' => 'Delete Attachment',
                'display_name_bn' => 'সংযুক্তি ডিলিট করতে পারবে',
                'created_at' => '2019-09-18 15:04:47',
                'updated_at' => '2019-09-18 15:04:47',
            ),
            21 => 
            array (
                'id' => 31,
                'name' => 'RoleController@index',
                'display_name_en' => 'Index Role',
                'display_name_bn' => 'রোল ইনডেক্স',
                'created_at' => '2019-09-18 15:09:15',
                'updated_at' => '2019-09-19 11:03:26',
            ),
            22 => 
            array (
                'id' => 32,
                'name' => 'RoleController@create',
                'display_name_en' => 'Create Role',
                'display_name_bn' => 'রোল তৈরী করতে পারবে',
                'created_at' => '2019-09-18 15:11:06',
                'updated_at' => '2019-09-19 11:03:58',
            ),
            23 => 
            array (
                'id' => 33,
                'name' => 'RoleController@store',
                'display_name_en' => 'Store Role',
                'display_name_bn' => 'রোল স্টোর করতে পারবে',
                'created_at' => '2019-09-18 15:12:04',
                'updated_at' => '2019-09-19 11:04:25',
            ),
            24 => 
            array (
                'id' => 34,
                'name' => 'RoleController@edit',
                'display_name_en' => 'Edit Role',
                'display_name_bn' => 'রোল মডিফাই করতে পারবে',
                'created_at' => '2019-09-18 15:13:10',
                'updated_at' => '2019-09-19 11:05:12',
            ),
            25 => 
            array (
                'id' => 35,
                'name' => 'RoleController@update',
                'display_name_en' => 'Update Role',
                'display_name_bn' => 'রোল আপডেট করতে পারবে',
                'created_at' => '2019-09-18 15:14:06',
                'updated_at' => '2019-09-19 11:05:28',
            ),
            26 => 
            array (
                'id' => 36,
                'name' => 'RoleController@show',
                'display_name_en' => '‍Show Role',
                'display_name_bn' => 'রোল দেখতে পারবে',
                'created_at' => '2019-09-18 15:15:01',
                'updated_at' => '2019-09-19 11:07:26',
            ),
            27 => 
            array (
                'id' => 37,
                'name' => 'ReportController@filter',
                'display_name_en' => 'Report Filter',
                'display_name_bn' => 'রির্পোট ফিল্টার',
                'created_at' => '2019-09-18 15:16:48',
                'updated_at' => '2019-09-19 11:13:43',
            ),
            28 => 
            array (
                'id' => 38,
                'name' => 'ReportController@reportApplication',
                'display_name_en' => 'Report Application',
                'display_name_bn' => 'রির্পোট আবেদনপ্রত্র',
                'created_at' => '2019-09-18 15:19:04',
                'updated_at' => '2019-09-19 11:15:04',
            ),
            29 => 
            array (
                'id' => 39,
                'name' => 'ReportController@attachFilter',
                'display_name_en' => 'Attach Filter',
                'display_name_bn' => 'সংযুক্তি ফিল্টার',
                'created_at' => '2019-09-18 15:36:28',
                'updated_at' => '2019-09-19 11:18:58',
            ),
            30 => 
            array (
                'id' => 40,
                'name' => 'ReportController@importExportView',
                'display_name_en' => 'Report Import Export',
                'display_name_bn' => 'রির্পোট এর্মোপ্ট এক্সর্পোট',
                'created_at' => '2019-09-18 15:37:30',
                'updated_at' => '2019-09-19 11:21:44',
            ),
            31 => 
            array (
                'id' => 41,
                'name' => 'ReportController@export',
                'display_name_en' => 'Report Excel Export',
                'display_name_bn' => 'রির্পোট এক্সেল',
                'created_at' => '2019-09-18 15:38:42',
                'updated_at' => '2019-09-19 11:23:00',
            ),
            32 => 
            array (
                'id' => 42,
                'name' => 'ReportController@import',
                'display_name_en' => 'Report File Import',
                'display_name_bn' => 'রির্পোট এমর্পোট',
                'created_at' => '2019-09-18 15:40:43',
                'updated_at' => '2019-09-19 11:25:08',
            ),
            33 => 
            array (
                'id' => 43,
                'name' => 'HomeController@index',
                'display_name_en' => 'Index Home',
                'display_name_bn' => 'ইনডেক্স হোম',
                'created_at' => '2019-09-18 15:42:02',
                'updated_at' => '2019-09-19 11:36:45',
            ),
            34 => 
            array (
                'id' => 44,
                'name' => 'HomeController@applications',
                'display_name_en' => 'Applications Home',
                'display_name_bn' => 'হোম আবেদন',
                'created_at' => '2019-09-18 15:42:54',
                'updated_at' => '2019-09-19 11:29:32',
            ),
            35 => 
            array (
                'id' => 45,
                'name' => 'HomeController@personalInfo',
                'display_name_en' => 'Personal Information Home',
                'display_name_bn' => 'হোম পারসোনাল ইনফরমেশন',
                'created_at' => '2019-09-18 15:47:43',
                'updated_at' => '2019-09-19 11:29:57',
            ),
            36 => 
            array (
                'id' => 46,
                'name' => 'HomeController@attachedInfo',
                'display_name_en' => 'Attached Information Home',
                'display_name_bn' => 'হোম সংযুক্তি ইনফরমেশন',
                'created_at' => '2019-09-18 15:53:00',
                'updated_at' => '2019-09-19 11:30:24',
            ),
            37 => 
            array (
                'id' => 71,
                'name' => 'PermissionController@index',
                'display_name_en' => 'Index Permission',
                'display_name_bn' => 'পারমিশন ইনডেক্স',
                'created_at' => '2019-09-19 10:08:28',
                'updated_at' => '2019-09-19 10:08:28',
            ),
            38 => 
            array (
                'id' => 72,
                'name' => 'PermissionController@create',
                'display_name_en' => 'Create Permission',
                'display_name_bn' => 'পারমিশন এড করতে পারবে',
                'created_at' => '2019-09-19 10:09:45',
                'updated_at' => '2019-09-19 10:09:45',
            ),
            39 => 
            array (
                'id' => 73,
                'name' => 'PermissionController@store',
                'display_name_en' => 'Store Permission',
                'display_name_bn' => 'পারমিশন স্টোর করতে পারবে',
                'created_at' => '2019-09-19 10:11:03',
                'updated_at' => '2019-09-19 10:11:03',
            ),
            40 => 
            array (
                'id' => 74,
                'name' => 'PermissionController@show',
                'display_name_en' => 'Show Permission',
                'display_name_bn' => 'পারমিশন দেখতে পারবে',
                'created_at' => '2019-09-19 10:11:50',
                'updated_at' => '2019-09-19 10:11:50',
            ),
            41 => 
            array (
                'id' => 75,
                'name' => 'PermissionController@edit',
                'display_name_en' => 'Edit Permission',
                'display_name_bn' => 'পারমিশন মডিফাই করতে পারবে',
                'created_at' => '2019-09-19 10:12:31',
                'updated_at' => '2019-09-19 10:12:31',
            ),
            42 => 
            array (
                'id' => 76,
                'name' => 'PermissionController@update',
                'display_name_en' => 'Update Permission',
                'display_name_bn' => 'পারমিশন আপডেট করতে পারবে',
                'created_at' => '2019-09-19 10:13:13',
                'updated_at' => '2019-09-19 10:13:13',
            ),
        ));
        
        
    }
}