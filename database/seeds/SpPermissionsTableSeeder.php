<?php

use Illuminate\Database\Seeder;

class SpPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sp_permissions')->delete();
        
        \DB::table('sp_permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 4,
                'division_id' => 5,
                'district_id' => 6,
                'upazila_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}