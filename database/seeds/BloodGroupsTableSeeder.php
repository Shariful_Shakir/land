<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BloodGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blood_groups = [
			['title_eng' => 'A Positive', 'title_bng' => 'এ-প্রোজেটিভ'], 
			['title_eng' => 'B Positive', 'title_bng' => 'বি-প্রোজেটিভ'], 
			['title_eng' => 'O Positive', 'title_bng' => 'ও-প্রোজেটিভ'], 
			['title_eng' => 'AB Positive', 'title_bng' => 'এবি-প্রোজেটিভ'], 
			['title_eng' => 'A Negative', 'title_bng' => 'এ-নেগেটিভ'], 
			['title_eng' => 'B Negative', 'title_bng' => 'বি-নেগেটিভ'], 
			['title_eng' => 'O Negative', 'title_bng' => 'ও-নেগেটিভ'], 
			['title_eng' => 'AB Negative', 'title_bng' => 'এবি-নেগেটিভ']
		];
		
		DB::table('blood_groups')->insert($blood_groups);
    }
}
