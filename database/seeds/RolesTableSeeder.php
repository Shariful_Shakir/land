<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'super_admin',
                'display_name_en' => 'Super Admin',
                'display_name_bn' => 'সুপার এ্যাডমিন',
                'create_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2019-09-18 12:46:13',
                'updated_at' => '2019-09-18 12:46:13',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'division_admin',
                'display_name_en' => 'Division Admin',
                'display_name_bn' => 'বিভাগ এ্যাডমিন',
                'create_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2019-09-18 12:46:15',
                'updated_at' => '2019-09-18 12:46:15',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'district_admin',
                'display_name_en' => 'District Admin',
                'display_name_bn' => 'জেলা এ্যাডমিন',
                'create_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2019-09-18 12:46:16',
                'updated_at' => '2019-09-18 12:46:16',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'upazila_admin',
                'display_name_en' => 'Upazila Admin',
                'display_name_bn' => 'উপাজেলা এ্যাডমিন',
                'create_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2019-09-18 12:46:17',
                'updated_at' => '2019-09-18 12:46:17',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'data_entry_user',
                'display_name_en' => 'Data Entry User',
                'display_name_bn' => 'ডাটা এন্টি ইউজার',
                'create_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2019-09-18 12:46:18',
                'updated_at' => '2019-09-18 12:46:18',
            ),
        ));
        
        
    }
}