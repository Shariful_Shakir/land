<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(SessionsTableSeeder::class);
		$this->call(BloodGroupsTableSeeder::class);
		$this->call(ReligionsTableSeeder::class);
        $this->call(GeoUnionsTableSeeder::class);
        $this->call(GeoDivisionsTableSeeder::class);
        $this->call(GeoDistrictsTableSeeder::class);
        $this->call(GeoUpazilasTableSeeder::class);
        $this->call(EducationalQualificationsTableSeeder::class);
        $this->call(BatchsTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(isSelfEmployedTableSeeder::class);
        $this->call(LaratrustSeeder::class);
        $this->call(ApplicationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SpPermissionsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);        
        $this->call(PermissionRoleTableSeeder::class);
    }
}
