<?php

use Illuminate\Database\Seeder;

class isSelfEmployedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $is_self_employed = [
            ['title_eng' => 'Yes', 'title_bng' => 'হ্যাঁ'], 
            ['title_eng' => 'No', 'title_bng' => 'না']
        ];
		
		DB::table('is_self_employeds')->insert($is_self_employed);
    }
}
