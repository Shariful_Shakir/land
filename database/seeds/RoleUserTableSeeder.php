<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_user')->delete();
        
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'role_id' => 1,
                'team_id' => NULL,
                'user_type' => 'App\\User',
            ),
            1 => 
            array (
                'user_id' => 2,
                'role_id' => 2,
                'team_id' => NULL,
                'user_type' => 'App\\User',
            ),
            2 => 
            array (
                'user_id' => 3,
                'role_id' => 3,
                'team_id' => NULL,
                'user_type' => 'App\\User',
            ),
            3 => 
            array (
                'user_id' => 4,
                'role_id' => 4,
                'team_id' => NULL,
                'user_type' => 'App\\User',
            ),
            4 => 
            array (
                'user_id' => 5,
                'role_id' => 5,
                'team_id' => NULL,
                'user_type' => 'App\\User',
            ),
        ));
        
        
    }
}