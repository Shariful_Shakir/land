<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('users')->delete();

        \DB::table('users')->insert(array(
            0 => array(
                'id' => 1,
                'name_en' => 'Super Admin',
                'name_bn' => 'Super Admin',
                'division_id' => 3,
                'district_id' => 4,
                'upazila_id' => 8,
                'email' => 'super_admin@app.com',
                'password' => '$2y$10$NdrkgTrAOYnZuEm8KrRdQ.yCzx.3koEZ4pvxQKgiZAxgtSDFo5Wvy',
                'contact_no' => '1446896313',
                'api_token' => '46836055fe9217fff02a1205876977ad6581b0df34b5e0a1a2917ddd13ee',
                'remember_token' => null,
                'created_at' => '2019-07-30 12:03:01',
                'updated_at' => '2019-07-30 12:03:01',
            ),
            1 => array(
                'id' => 2,
                'name_en' => 'Shariful Islam',
                'name_bn' => 'Shariful Islam',
                'division_id' => 3,
                'district_id' => 21,
                'upazila_id' => 7,
                'email' => 'shariful@gmail.com',
                'password' => '$2y$10$NdrkgTrAOYnZuEm8KrRdQ.yCzx.3koEZ4pvxQKgiZAxgtSDFo5Wvy',
                'contact_no' => '01773592770',
                'api_token' => null,
                'remember_token' => null,
                'created_at' => '2019-08-04 14:01:01',
                'updated_at' => '2019-08-04 14:01:01',
            ),
        ));

    }
}
