<?php

use Illuminate\Database\Seeder;

class EducationalQualificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $educational_qualifications = [
			['title_eng' => 'JSC/JDC', 'title_bng' => 'জেএসসি/ জিডিসি'], 
			['title_eng' => 'SSC/Dakhal', 'title_bng' => 'এসএসসি/দাখিল'],  
			['title_eng' => 'HSC/Alim', 'title_bng' => 'এইচএসসি/আলিম'],
			['title_eng' => 'Deploma', 'title_bng' => 'ডিপ্লোমা'],
			['title_eng' => 'BSC/BBA/Bcom', 'title_bng' => 'বিএসসি/বিবিএ/বিকম'],
			['title_eng' => 'MSC/MBA/Master', 'title_bng' => 'এমএসসি/এমবিএ/মাস্টার'],
			
		];
		
		DB::table('educational_qualifications')->insert($educational_qualifications);
    }
}
