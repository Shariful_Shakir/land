<?php

use Illuminate\Database\Seeder;

class GeoDivisionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('geo_divisions')->delete();
        
        \DB::table('geo_divisions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'division_name_eng' => 'Barisal',
                'division_name_bng' => 'বরিশাল',
                'bbs_code' => '10',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'division_name_eng' => 'Chittagong',
                'division_name_bng' => 'চট্টগ্রাম',
                'bbs_code' => '20',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'division_name_eng' => 'Dhaka',
                'division_name_bng' => 'ঢাকা',
                'bbs_code' => '30',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'division_name_eng' => 'Khulna',
                'division_name_bng' => 'খুলনা',
                'bbs_code' => '40',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'division_name_eng' => 'Rajshahi',
                'division_name_bng' => 'রাজশাহী',
                'bbs_code' => '50',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'division_name_eng' => 'Rangpur',
                'division_name_bng' => 'রংপুর',
                'bbs_code' => '60',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'division_name_eng' => 'Sylhet',
                'division_name_bng' => 'সিলেট',
                'bbs_code' => '70',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'division_name_eng' => 'Mymensingh',
                'division_name_bng' => 'ময়মনসিংহ',
                'bbs_code' => '45',
                'status' => 3,
                'created_by' => 0,
                'modified_by' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}