<?php

use Illuminate\Database\Seeder;

class BatchsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batchs = [
			['title_eng' => 'Batch -1', 'title_bng' => 'ব্যাচ নং -১'], 
			['title_eng' => 'Batch -2', 'title_bng' => 'ব্যাচ নং -২'], 
			['title_eng' => 'Batch -3', 'title_bng' => 'ব্যাচ নং -৩'], 
			['title_eng' => 'Batch -4', 'title_bng' => 'ব্যাচ নং -৪']
		];
		
		DB::table('batch_numbers')->insert($batchs);
    }
}
