<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogicInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logic_informations', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('tracking_no');
			$table->unsignedSmallInteger('division_id');
			$table->unsignedSmallInteger('district_id');
			$table->unsignedSmallInteger('upazila_id');
			$table->string('name', 100);
			$table->string('father_or_husband_name', 100);
			$table->text('present_address');
			$table->unsignedInteger('office_id');
			$table->date('joining_date');
			$table->date('end_date');
			$table->double('finance_amount', 13, 3);
			$table->enum('is_dropout', ['Yes', 'No']);
			$table->tinyInteger('status')->comment('0=Pending, 1=Delete, 2=Inactive, 3=active, 4=PreviousActive')->default(3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logic_informations');
    }
}
