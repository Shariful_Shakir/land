<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_divisions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('division_name_eng', 50);
            $table->string('division_name_bng', 100);
            $table->string('bbs_code', 4);
            $table->tinyInteger('status')->comment('0=Pending, 1=Delete, 2=Inactive, 3=active, 4=PreviousActive')->default(3);
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            //$table->timestamps();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_divisions');
    }
}
