<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tracking_no')->default(0);
            $table->string('name', 100);
            $table->string('father_name', 100);
            $table->string('man_woman_attachment', 100);
            $table->unsignedSmallInteger('present_division_id')->default(0);
            $table->unsignedSmallInteger('present_district_id')->default(0);
            $table->unsignedSmallInteger('present_upazila_id')->default(0);
            $table->unsignedSmallInteger('present_union_id')->default(0);
            $table->text('present_village_id')->nullable();
            $table->text('present_post_code_id');
            $table->unsignedSmallInteger('attached_office')->default(0);
            $table->date('joining_date');
            $table->date('last_joining_date');
            $table->float('recived_money', 8, 2);             
            $table->string('drop_out');
            $table->tinyInteger('status')->comment('0=Pending, 1=Delete, 2=Inactive, 3=active, 4=PreviousActive')->default(3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
