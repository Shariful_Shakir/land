<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoUnionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_unions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('geo_division_id')->default(0);
            $table->unsignedTinyInteger('geo_district_id');
            $table->unsignedInteger('geo_upazila_id');
            $table->string('division_bbs_code', 4)->nullable();
            $table->string('district_bbs_code', 4)->nullable();
            $table->string('upazila_bbs_code', 4)->nullable();
            $table->string('union_name_eng', 50);
            $table->string('union_name_bng', 100);
            $table->string('bbs_code', 4);
            $table->unsignedInteger('postcode')->nullable();
            $table->tinyInteger('status')->comment('0=Pending, 1=Delete, 2=Inactive, 3=active, 4=PreviousActive')->default(3);
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            //$table->timestamps();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_unions');
    }
}
