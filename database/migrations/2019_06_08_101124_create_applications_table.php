<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('tracking_no')->default(0);
			$table->string('name', 100);
			$table->string('father_name', 100);
			$table->string('mother_name', 100);
			$table->unsignedSmallInteger('present_division_id')->default(0);
			$table->unsignedSmallInteger('present_district_id')->default(0);
			$table->unsignedSmallInteger('present_upazila_id')->default(0);
			$table->unsignedSmallInteger('present_union_id')->default(0);
			$table->text('present_village_id')->nullable();
			$table->unsignedSmallInteger('present_post_code_id')->default(0);
			$table->unsignedSmallInteger('permanent_division_id')->default(0);
			$table->unsignedSmallInteger('permanent_district_id')->default(0);
			$table->unsignedSmallInteger('permanent_upazila_id')->default(0);
			$table->unsignedSmallInteger('permanent_union_id')->default(0);
			$table->text('permanent_village_id')->nullable();
			$table->unsignedSmallInteger('permanent_post_code_id')->default(0);
			$table->unsignedSmallInteger('batch_no')->default(0);
			$table->unsignedSmallInteger('session_id')->default(0);
			$table->unsignedBigInteger('national_id')->default(0);
			$table->text('skill_summary')->nullable();
			$table->text('it_knowledge')->nullable();
			$table->text('communication_skill')->nullable();
			$table->text('training_subject_description')->nullable();
			//$table->enum('is_self_employed', ['Yes', 'No']);
			$table->string('is_self_employed')->nullable();
			$table->text('interested_work_description')->nullable();
			//$table->enum('gender', ['Male', 'Female', 'Others']);
			$table->string('gender');
			$table->date('date_of_birth');
			$table->string('educational_qualification')->nullable();
			$table->string('blood_group')->nullable();
			$table->string('religion');
			//$table->enum('blood_group', ['A+', 'B+', 'O+', 'AB+', 'A-', 'B-', 'O-', 'AB-']);
			//$table->enum('religion', ['Islam', 'Hindu', 'Buddha', 'Christian', 'Other', 'ইসলাম', 'হিন্দু', 'বৈদ্ধ', 'খ্রিক্ষ্টান', 'অন্যান্য']);
			$table->string('nationality');
			$table->string('contact_no', 13);
			$table->string('email')->nullable();
			$table->string('img')->default(0);
			$table->tinyInteger('status')->comment('0=Pending, 1=Delete, 2=Inactive, 3=active, 4=PreviousActive')->default(3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
