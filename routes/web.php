<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
 */

Route::get('/', function () {
    //return view('welcome');
    Cookie::queue(Cookie::make('langId', 1, 3600));
    return redirect(route('login'));
});

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');

Route::middleware(['HasPermission'])->group(function () {
    Route::resource('applications', 'ApplicationController');
    Route::resource('attachments', 'AttachmentController');
    Route::get('/personal-info', 'ReportController@applicationsReport');
    Route::any('/report-application', 'ReportController@reportApplication')->name('report.applications');
    Route::get('report/filter', 'ReportController@filter')->name('report.filter');
    Route::get('/attached-report', 'ReportController@attachFilter')->name('attached.report');
    Route::post('/application-pdf/{id}', 'ReportController@reportApplication')->name('application.pdf');
    Route::post('/application-print/{id}', 'ReportController@reportApplication')->name('application.print');
});

// Khotian Route
Route::get('/khotian-data-entry', 'KhotianController@create');
Route::post('/khotian/store', 'KhotianController@store');
Route::get('/khotian/report/', 'KhotianController@show');
Route::post('/khotian/report/', 'KhotianController@findKhotian');
Route::any('/user-khotian-info', 'KhotianController@userKhotianInfo');

Route::get('/edit-dag-report/{id}', 'KhotianController@editDagReport');
Route::post('/store-dag-report', 'KhotianController@storeDagReport');

Route::get('/edit-owner-report/{id}', 'KhotianController@editOwnerReport');
Route::post('/store-owner-report', 'KhotianController@storeOwnerReport');

Route::get('/otp', 'OtpController@index');
Route::post('/otp-send', 'OtpController@sendOtp');
Route::post('/otp-verify', 'OtpController@verifyOtp');

Route::post('/getDivisionInfos', 'UserController@getDivisionInformation');
Route::post('/getDistrictInfos', 'UserController@getDistrictInformation')->name('getDistrictInfos');
Route::post('/getUpazliaInfo', 'UserController@getUpazliaInformation');
Route::post('/getDistrictData', 'UserController@getDistrictData');
Route::post('/getUpazilaData', 'UserController@getUpazilaData');
Route::post('/getUnionInfos', 'UserController@getUnionInformation');

Route::post('/getUpazliaInfos', 'ApplicationController@getUpazliaInformation')->name('applications.getUpazliaInfos');
// Route::post('/getUnionInfos', 'ApplicationController@getUnionInformation');
Route::post('/getPostalCode', 'ApplicationController@getPostalCodeInformation');
// Route::resource('applications', 'ApplicationController')->middleware('role:superadmin|divisionadmin|admin|editor');

// Route::resource('work-status', 'WorkStatusController');

Route::get('/application/apply', 'ApplicationController@create')->name('application.apply');
Route::post('/applications/edit', 'ApplicationController@edit');
Route::post('/applications/update', 'ApplicationController@update');

Route::get('/change-language/{id}', 'LanguageController@changeLang');
Route::post('/check-unique', 'ApplicationController@check_unique');

//Route::get('/applications-pdf', 'ReportController@applicationPdf')->name('application.pdf');
Route::post('/sp-permission', 'UserController@sp_permission_store')->name('sp_permission');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::post('register-user', 'UserController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//End Auth

//User Profile Route

Route::get('user/profile', 'UserProfileController@index')->name('user.profile');
Route::get('user/profile/edit', 'UserProfileController@edit')->name('user.profile.edit');

Route::resource('divisions', 'DivisionController');
Route::resource('districts', 'DistrictController');
Route::resource('upazilas', 'UpazilaController');
Route::resource('unions', 'UnionController');
Route::resource('bloodGroups', 'BloodGroupController');
Route::resource('genders', 'GenderController');
Route::resource('religions', 'ReligionController');
Route::resource('batchs', 'BatchController');
Route::resource('sessions', 'SessionController');
Route::resource('educationalQualifications', 'EducationalQualificationController');

Route::prefix('manage')->group(function () {
// Route::prefix('manage')->group(function () {
    Route::middleware(['HasPermission'])->group(function () {
        Route::get('/', 'ManageController@index');
        Route::get('/dashboard', 'ManageController@dashboard')->name('manage.dashboard');
        Route::resource('/users', 'UserController');
        Route::resource('/permissions', 'PermissionController', ['except' => 'destroy']);
        Route::resource('/roles', 'RoleController', ['except' => 'destroy']);
    });

});
