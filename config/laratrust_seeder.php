<?php

return [
    'role_structure' => [
        'super_admin' => [
            'users' => 'c,r,u,d',
            'acl' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'division_admin' => [
            'users' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'district_admin' => [
            'users' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'upazila_admin' => [
            'users' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'data_entry_user' => [
            'profile' => 'r,u'
        ],
    ],
    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
