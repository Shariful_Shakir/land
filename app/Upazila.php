<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
    protected $table = 'geo_upazilas';

    protected $fillable = ['upazila_name_eng', 'upazila_name_bng', 'bbs_code', 'geo_division_id', 'geo_district_id', 'created_by'];

    public function union()
    {
        return $this->hasMany('App\Union', 'geo_upazila_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo('App\Division', 'geo_division_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo('App\District', 'geo_district_id', 'id');
    }

    protected function getGeoInformationsById($upazila_id = null)
    {
        $rtrArr = [];
        if (!empty($upazila_id)) {

            $upazila = Upazila::select('id', 'geo_division_id', 'geo_district_id', 'upazila_name_eng', 'upazila_name_bng')->where('id', $upazila_id)
                ->with([
                    'district' => function ($q) {
                        return $q->select(['id', 'district_name_eng', 'district_name_bng']);
                    },
                    'division' => function ($q) {
                        return $q->select(['id', 'division_name_eng', 'division_name_bng']);
                    },
                ])
                ->first();
            //echo $district->division->name_en;
            //dd($upazila_id);
            //$rtrArr[];

            if (!empty($upazila)) {
                $rtrArr['division_id'] = $upazila->division['id'];
                // $rtrArr['division_name_eng'] = $upazila->district->division->division_name_eng;
                // $rtrArr['division_name_bng'] = $upazila->district->division->division_name_bng;
                $rtrArr['district_id'] = $upazila->district->id;
                $rtrArr['district_name_eng'] = $upazila->district->district_name_eng;
                $rtrArr['district_name_bng'] = $upazila->district->district_name_bng;
                $rtrArr['upazila_name_eng'] = $upazila->upazila_name_eng;
                $rtrArr['upazila_name_bng'] = $upazila->upazila_name_bng;
            }

            //pr($rtrArr);
            //dd($upazila->toArray());
        }

        return $rtrArr;
    }
}
