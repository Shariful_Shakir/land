<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
		'tracking_no', 'father_name', 'man_woman_attachment', 'present_division_id', 'present_district_id', 'present_upazila_id','present_union_id', 
		'present_post_code_id', 'attached_office', 'joining_date', 'last_joining_date', 'amount'
	];

	public function division()
	{
		return $this->belongsTo('App\Division', 'division_id');
	}
	
	public function district()
	{
		return $this->belongsTo('App\District', 'district_id');
	}
	
	public function upazila()
	{
		return $this->belongsTo('App\Upazila', 'upazila_id');
	}
	
	public function scopeSearch($query, $search_txt)
    {
        $query->orWhere('name', 'like', '%'.$search_txt.'%' );
        $query->orWhere('email', 'like', '%'.$search_txt.'%' );
		
		$search_txt_date = date('Y-m-d H:i', strtotime($search_txt));
		$search_txt_date = str_replace(":00","", $search_txt_date);
		$search_txt_date = str_replace("00","", $search_txt_date);
		$query->orWhere('created_at', 'like', '%'.$search_txt_date.'%' );
		
    }
}
