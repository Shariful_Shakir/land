<?php 
function pr($dt)
{
	echo '<pre>';
	print_r($dt);
	echo '<pre>';
}

function vd($dt)
{
	echo '<pre>';
	var_dump($dt);
	echo '<pre>';
}


function isLocal()
{
	$rtrVal = 0;
	$baseUrl = url('/');
	//if(strpos($baseUrl, 'localhost') !== false || strpos($baseUrl, 'ems') !== false) {
	if(strpos($baseUrl, 'localhost') !== false) {
		$rtrVal = 1;
	} 

	return $rtrVal;
}

function isIeclServer()
{
	$rtrVal = 0;
	$baseUrl = url('/');
	//if(strpos($baseUrl, 'localhost') !== false || strpos($baseUrl, 'ems') !== false) {
	if(strpos($baseUrl, '118.179.193.221') !== false) {
		$rtrVal = 1;
	} 

	return $rtrVal;
}

//------------ START : CUSTOM ASSET FUNCTION -----------------------------------
function custom_asset($path, $secure = null)
{
	if(!isLocal() && !isIeclServer()) {
		$path = 'public/'.$path; 
	}
	return asset($path, $secure);
}

//------------ END : CUSTOM ASSET FUNCTION -----------------------------------



//GET CONVERT ENGLISH TO BANGLA
//===============================================================

function cv_en_to_bn($input) {
    $english = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "am", "pm", "AM", "PM", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "Jan", "Feb", "March", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Year", "Month", "Day", "year", "month", "day");
    $bangla = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০', 'পূর্বাহ্ন', 'অপরাহ্ন', 'পূর্বাহ্ন', 'অপরাহ্ন', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', 'বুধবার', 'বৃহস্পতিবার', 'শুক্রবার', 'বছর', 'মাস', 'দিন', 'বছর', 'মাস', 'দিন');
    $converted = str_replace($english, $bangla, $input);
    return $converted;
}


//===============================================================
//GET CONVERT BANGLA TO ENGLISH
//===============================================================

function cv_bn_to_en($input) {
    $bangla = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০', 'পূর্বাহ্ন', 'অপরাহ্ন', 'পূর্বাহ্ন', 'অপরাহ্ন', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর', 'শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', 'বুধবার', 'বৃহস্পতিবার', 'শুক্রবার', 'বছর', 'মাস', 'দিন');
    $english = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "am", "pm", "AM", "PM", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "Jan", "Feb", "March", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Year", "Month", "Day");
    $converted = str_replace($bangla, $english, $input);
    return $converted;
}

function viewNumberByLang($value, $langId = 1, $after_decimal = 2)
{
	if (!empty($value)) {
        if ($langId == 2) {
            $val = cv_en_to_bn(number_format($value, $after_decimal, '.', ','));
        } else {
            $val = number_format($value, $after_decimal, '.', ',');
        }
    } else {
        // if ($langId == 2) {
            // $val = '০.০০';
        // } else {
            // $val = '0.00';
        // }
		$val = '';
    }
    return $val;
}

function shortText($text, $chars_limit)
{
    // Check if length is larger than the character limit
    if (strlen($text) > $chars_limit) {
        // If so, cut the string at the character limit
        $new_text = mb_substr($text, 0, $chars_limit);
        // Trim off white space
        $new_text = trim($new_text);
        // Add at end of text ...
        return $new_text . "...";
    } else {
		return $text;
    }
}

function convertToLang($number, $langId = 1)
{
	switch($langId) {
		case 1: 
		return $number;
		
		case 2: 
		return cv_en_to_bn($number);
		
		default:
		return $number;
	}
}



function getCommonLabelTxt($commonLabels, $labelKey)
{
	$rtrTxt = '';
	if(isset($commonLabels[$labelKey]) && !empty($commonLabels[$labelKey])) {
	    $rtrTxt = $commonLabels[$labelKey];
	}
	return $rtrTxt;
}

function getColorCodes()
{
	$colorCodes = [
		'#2f7ed8', /* '#0d233a', */ '#8bbc21', /* '#910000', */ '#1aadce', '#492970', '#f28f43', '#77a1e5'/* , '#c42525' */, '#a6c96a', '#910000', '#0d233a',
		'#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1', '#c42525',
		//'#95CEFF', '#5C5C61', '#A9FF96', '#FFBC75', '#999EFF', '#FF7599', '#FDEC6D', '#44A9A8', '#FF7474', '#AAFFFA',
		//'#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92',
	];
	
	return $colorCodes;
}

function getFontSize() 
{
	$fontSize = array(
		'' => '', '6'=>'6','8'=>'8','10'=>'10','12'=>'12','14'=>'14','16'=>'16',
		'18'=>'18','20'=>'20','22'=>'22','24'=>'24','26'=>'26','28'=>'28',
		'30'=>'30','32'=>'32','34'=>'34','36'=>'36','38'=>'38','40'=>'40',
		'42'=>'42','44'=>'44','46'=>'46','48'=>'48','50'=>'50'
	);
	return $fontSize;
}
function getFontWeight()
{
	$fontWeight = array(
		'normal'=>'normal', 'bold'=>'bold', 'bolder'=>'bolder', 'lighter'=>'lighter', '100'=>'100', '200'=>'200', '300'=>'300', '400'=>'400', '500'=>'500', '600'=>'600', '700'=>'700', '800'=>'800', '900'=>'900', 'initial'=>'initial'
	);
	return $fontWeight;
}
function getFontSizeForSecondBand() 
{
	$fontSize = array(
		'6'=>'6','8'=>'8','10'=>'10', '12'=>'12','14'=>'14','16'=>'16','18'=>'18'
	);
	return $fontSize;
}
function getFontFamily() 
{
	$fontFamily = array(
		'' => '',
		'Arial'=>'Arial',
		'Comic Sans MS'=>'Comic Sans MS',
		'Georgia'=>'Georgia',
		'Georgia Ref'=>'Georgia Ref',
		'Giorgio Sans'=>'Giorgio Sans',
		'Helvetica'=>'Helvetica',
		'Lucida Grande'=>'Lucida Grande',
		'Lucida Sans Unicode'=>'Lucida Sans Unicode',
		'open_sansregular'=>'Open Sans Regular',
		'open_sanscondensed_light'=>'Open Sanscondensed Light',
		'Nikosh'=>'Nikosh',
		'NikoshBAN'=>'NikoshBAN',
		'kalpurush'=>'kalpurush',
		'solaimanlipi'=>'solaimanlipi',
		'Neuzeit S LT Std'=>'Neuzeit S LT Std',
		'robotoregular'=>'robotoregular',
		'sans-serif'=>'Sans-Serif',
		'Times New Roman'=>'Times New Roman',
		'Times New Roman Special'=>'Times New Roman Special',
		'Vrinda'=>'Vrinda',
		'Verdana'=>'Verdana',
		'Verdana Ref'=>'Verdana Ref',
	);
	/*$fontFamily = array(
		'Abadi MT Condensed'=>'Abadi MT Condensed',
		'Adobe Minion Web'=>'Adobe Minion Web',
		'Agency FB'=>'Agency FB',
		'Aharoni'=>'Aharoni',
		'Aldhabi'=>'Aldhabi',
		'Algerian'=>'Algerian',
		'Almanac MT'=>'Almanac MT',
		'American Uncial'=>'American Uncial',
		'Andale Mono'=>'Andale Mono',
		'Andalus'=>'Andalus',
		'Andy'=>'Andy',
		'Angsana New'=>'Angsana New',
		'AngsanaUPC'=>'AngsanaUPC',
		'Aparajita'=>'Aparajita',
		'Arabic Transparent'=>'Arabic Transparent',
		'Arabic Typesetting'=>'Arabic Typesetting',
		'Arial'=>'Arial',
		'Arial Black'=>'Arial Black',
		'Arial Narrow'=>'Arial Narrow',
		'Arial Narrow Special'=>'Arial Narrow Special',
		'Arial Rounded MT'=>'Arial Rounded MT',
		'Arial Special'=>'Arial Special',
		'Arial Unicode MS'=>'Arial Unicode MS',
		'Augsburger Initials'=>'Augsburger Initials',
		'Baskerville Old Face'=>'Baskerville Old Face',
		'Batang'=>'Batang',
		'BatangChe'=>'BatangChe',
		'Bauhaus 93'=>'Bauhaus 93',
		'Beesknees ITC'=>'Beesknees ITC',
		'Bell MT'=>'Bell MT',
		'Berlin Sans FB'=>'Berlin Sans FB',
		'Bernard MT Condensed'=>'Bernard MT Condensed',
		'Bickley Script'=>'Bickley Script',
		'Blackadder ITC'=>'Blackadder ITC',
		'Bodoni MT'=>'Bodoni MT',
		'Bodoni MT Condensed'=>'Bodoni MT Condensed',
		'Bon Apetit MT'=>'Bon Apetit MT',
		'Book Antiqua'=>'Book Antiqua',
		'Bookman Old Style'=>'Bookman Old Style',
		'Bookshelf Symbol'=>'Bookshelf Symbol',
		'Bradley Hand ITC'=>'Bradley Hand ITC',
		'Braggadocio'=>'Braggadocio',
		'BriemScript'=>'BriemScript',
		'Britannic'=>'Britannic',
		'Britannic Bold'=>'Britannic Bold',
		'Broadway'=>'Broadway',
		'Browallia New'=>'Browallia New',
		'BrowalliaUPC'=>'BrowalliaUPC',
		'Brush Script MT'=>'Brush Script MT',
		'Calibri'=>'Calibri',
		'Californian FB'=>'Californian FB',
		'Calisto MT'=>'Calisto MT',
		'Cambria'=>'Cambria',
		'Cambria Math'=>'Cambria Math',
		'Candara'=>'Candara',
		'Cariadings'=>'Cariadings',
		'Castellar'=>'Castellar',
		'Centaur'=>'Centaur',
		'Century'=>'Century',
		'Century Gothic'=>'Century Gothic',
		'Century Schoolbook'=>'Century Schoolbook',
		'Chiller'=>'Chiller',
		'Colonna MT'=>'Colonna MT',
		'Comic Sans MS'=>'Comic Sans MS',
		'Consolas'=>'Consolas',
		'Constantia'=>'Constantia',
		'Contemporary Brush'=>'Contemporary Brush',
		'Cooper Black'=>'Cooper Black',
		'Copperplate Gothic'=>'Copperplate Gothic',
		'Corbel'=>'Corbel',
		'Cordia New'=>'Cordia New',
		'CordiaUPC'=>'CordiaUPC',
		'Courier New'=>'Courier New',
		'Curlz MT'=>'Curlz MT',
		'DaunPenh'=>'DaunPenh',
		'David'=>'David',
		'Desdemona'=>'Desdemona',
		'DFKai-SB'=>'DFKai-SB',
		'DilleniaUPC'=>'DilleniaUPC',
		'Directions MT'=>'Directions MT',
		'DokChampa'=>'DokChampa',
		'Dotum'=>'Dotum',
		'DotumChe'=>'DotumChe',
		'Ebrima'=>'Ebrima',
		'Eckmann'=>'Eckmann',
		'Edda'=>'Edda',
		'Edwardian Script ITC'=>'Edwardian Script ITC',
		'Elephant'=>'Elephant',
		'Engravers MT'=>'Engravers MT',
		'Enviro'=>'Enviro',
		'Eras ITC'=>'Eras ITC',
		'Estrangelo Edessa'=>'Estrangelo Edessa',
		'EucrosiaUPC'=>'EucrosiaUPC',
		'Euphemia'=>'Euphemia',
		'Eurostile'=>'Eurostile',
		'FangSong'=>'FangSong',
		'Felix Titling'=>'Felix Titling',
		'Fine Hand'=>'Fine Hand',
		'Fixed Miriam Transparent'=>'Fixed Miriam Transparent',
		'Flexure'=>'Flexure',
		'Footlight MT'=>'Footlight MT',
		'Forte'=>'Forte',
		'Franklin Gothic'=>'Franklin Gothic',
		'Franklin Gothic Medium'=>'Franklin Gothic Medium',
		'FrankRuehl'=>'FrankRuehl',
		'FreesiaUPC'=>'FreesiaUPC',
		'Freestyle Script'=>'Freestyle Script',
		'French Script MT'=>'French Script MT',
		'Futura'=>'Futura',
		'Gabriola'=>'Gabriola',
		'Gadugi'=>'Gadugi',
		'Garamond'=>'Garamond',
		'Garamond MT'=>'Garamond MT',
		'Gautami'=>'Gautami',
		'Georgia'=>'Georgia',
		'Georgia Ref'=>'Georgia Ref',
		'Gigi'=>'Gigi',
		'Gill Sans MT'=>'Gill Sans MT',
		'Gill Sans MT Condensed'=>'Gill Sans MT Condensed',
		'Gisha'=>'Gisha',
		'Gloucester'=>'Gloucester',
		'Goudy Old Style'=>'Goudy Old Style',
		'Goudy Stout'=>'Goudy Stout',
		'Gradl'=>'Gradl',
		'Gulim'=>'Gulim',
		'GulimChe'=>'GulimChe',
		'Gungsuh'=>'Gungsuh',
		'GungsuhChe'=>'GungsuhChe',
		'Haettenschweiler'=>'Haettenschweiler',
		'Harlow Solid Italic'=>'Harlow Solid Italic',
		'Harrington'=>'Harrington',
		'High Tower Text'=>'High Tower Text',
		'Holidays MT'=>'Holidays MT',
		'Impact'=>'Impact',
		'Imprint MT Shadow'=>'Imprint MT Shadow',
		'Informal Roman'=>'Informal Roman',
		'IrisUPC'=>'IrisUPC',
		'Iskoola Pota'=>'Iskoola Pota',
		'JasmineUPC'=>'JasmineUPC',
		'Jokerman'=>'Jokerman',
		'Juice ITC'=>'Juice ITC',
		'KaiTi'=>'KaiTi',
		'Kalinga'=>'Kalinga',
		'Kartika'=>'Kartika',
		'Keystrokes MT'=>'Keystrokes MT',
		'Khmer UI'=>'Khmer UI',
		'Kino MT'=>'Kino MT',
		'KodchiangUPC'=>'KodchiangUPC',
		'Kokila'=>'Kokila',
		'Kristen ITC'=>'Kristen ITC',
		'Kunstler Script'=>'Kunstler Script',
		'Lao UI'=>'Lao UI',
		'Latha'=>'Latha',
		'LCD'=>'LCD',
		'Leelawadee'=>'Leelawadee',
		'Levenim MT'=>'Levenim MT',
		'LilyUPC'=>'LilyUPC',
		'Lucida Blackletter'=>'Lucida Blackletter',
		'Lucida Bright'=>'Lucida Bright',
		'Lucida Bright Math'=>'Lucida Bright Math',
		'Lucida Calligraphy'=>'Lucida Calligraphy',
		'Lucida Console'=>'Lucida Console',
		'Lucida Fax'=>'Lucida Fax',
		'Lucida Handwriting'=>'Lucida Handwriting',
		'Lucida Sans'=>'Lucida Sans',
		'Lucida Sans Typewriter'=>'Lucida Sans Typewriter',
		'Lucida Sans Unicode'=>'Lucida Sans Unicode',
		'Magneto'=>'Magneto',
		'Maiandra GD'=>'Maiandra GD',
		'Malgun Gothic'=>'Malgun Gothic',
		'Mangal'=>'Mangal',
		'Map Symbols'=>'Map Symbols',
		'Marlett'=>'Marlett',
		'Matisse ITC'=>'Matisse ITC',
		'Matura MT Script Capitals'=>'Matura MT Script Capitals',
		'McZee'=>'McZee',
		'Mead Bold'=>'Mead Bold',
		'Meiryo'=>'Meiryo',
		'Meiryo UI'=>'Meiryo UI',
		'Mercurius Script MT Bold'=>'Mercurius Script MT Bold',
		'Microsoft Himalaya'=>'Microsoft Himalaya',
		'Microsoft JhengHei'=>'Microsoft JhengHei',
		'Microsoft JhengHei UI'=>'Microsoft JhengHei UI',
		'Microsoft New Tai Lue'=>'Microsoft New Tai Lue',
		'Microsoft PhagsPa'=>'Microsoft PhagsPa',
		'Microsoft Sans Serif'=>'Microsoft Sans Serif',
		'Microsoft Tai Le'=>'Microsoft Tai Le',
		'Microsoft Uighur'=>'Microsoft Uighur',
		'Microsoft YaHei'=>'Microsoft YaHei',
		'Microsoft YaHei UI'=>'Microsoft YaHei UI',
		'Microsoft Yi Baiti'=>'Microsoft Yi Baiti',
		'MingLiU'=>'MingLiU',
		'MingLiU_HKSCS'=>'MingLiU_HKSCS',
		'MingLiU_HKSCS-ExtB'=>'MingLiU_HKSCS-ExtB',
		'MingLiU-ExtB'=>'MingLiU-ExtB',
		'Minion Web'=>'Minion Web',
		'Miriam'=>'Miriam',
		'Miriam Fixed'=>'Miriam Fixed',
		'Mistral'=>'Mistral',
		'Modern No. 20'=>'Modern No. 20',
		'Mongolian Baiti'=>'Mongolian Baiti',
		'Monotype Corsiva'=>'Monotype Corsiva',
		'Monotype Sorts'=>'Monotype Sorts',
		'Monotype.com'=>'Monotype.com',
		'MoolBoran'=>'MoolBoran',
		'MS Gothic'=>'MS Gothic',
		'MS LineDraw'=>'MS LineDraw',
		'MS Mincho'=>'MS Mincho',
		'MS Outlook'=>'MS Outlook',
		'MS PGothic'=>'MS PGothic',
		'MS PMincho'=>'MS PMincho',
		'MS Reference'=>'MS Reference',
		'MS UI Gothic'=>'MS UI Gothic',
		'MT Extra'=>'MT Extra',
		'MV Boli'=>'MV Boli',
		'Myanmar Text'=>'Myanmar Text',
		'Narkisim'=>'Narkisim',
		'New Caledonia'=>'New Caledonia',
		'News Gothic MT'=>'News Gothic MT',
		'Niagara'=>'Niagara',
		'Nirmala UI'=>'Nirmala UI',
		'NSimSun'=>'NSimSun',
		'Nyala'=>'Nyala',
		'OCR A Extended'=>'OCR A Extended',
		'OCRB'=>'OCRB',
		'OCR-B-Digits'=>'OCR-B-Digits',
		'Old English Text MT'=>'Old English Text MT',
		'Onyx'=>'Onyx',
		'Palace Script MT'=>'Palace Script MT',
		'Palatino Linotype'=>'Palatino Linotype',
		'Papyrus'=>'Papyrus',
		'Parade'=>'Parade',
		'Parchment'=>'Parchment',
		'Parties MT'=>'Parties MT',
		'Peignot Medium'=>'Peignot Medium',
		'Pepita MT'=>'Pepita MT',
		'Perpetua'=>'Perpetua',
		'Perpetua Titling MT'=>'Perpetua Titling MT',
		'Placard Condensed'=>'Placard Condensed',
		'Plantagenet Cherokee'=>'Plantagenet Cherokee',
		'Playbill'=>'Playbill',
		'PMingLiU'=>'PMingLiU',
		'PMingLiU-ExtB'=>'PMingLiU-ExtB',
		'Poor Richard'=>'Poor Richard',
		'Pristina'=>'Pristina',
		'Raavi'=>'Raavi',
		'Rage Italic'=>'Rage Italic',
		'Ransom'=>'Ransom',
		'Ravie'=>'Ravie',
		'RefSpecialty'=>'RefSpecialty',
		'Rockwell'=>'Rockwell',
		'Rockwell Condensed'=>'Rockwell Condensed',
		'Rockwell Extra Bold'=>'Rockwell Extra Bold',
		'Rod'=>'Rod',
		'Runic MT Condensed'=>'Runic MT Condensed',
		'Sakkal Majalla'=>'Sakkal Majalla',
		'Script MT Bold'=>'Script MT Bold',
		'Segoe Chess'=>'Segoe Chess',
		'Segoe Print'=>'Segoe Print',
		'Segoe Pseudo'=>'Segoe Pseudo',
		'Segoe Script'=>'Segoe Script',
		'Segoe UI'=>'Segoe UI',
		'Segoe UI Symbol'=>'Segoe UI Symbol',
		'Shonar Bangla'=>'Shonar Bangla',
		'Showcard Gothic'=>'Showcard Gothic',
		'Shruti'=>'Shruti',
		'Signs MT'=>'Signs MT',
		'SimHei'=>'SimHei',
		'Simplified Arabic'=>'Simplified Arabic',
		'Simplified Arabic Fixed'=>'Simplified Arabic Fixed',
		'SimSun'=>'SimSun',
		'SimSun-ExtB'=>'SimSun-ExtB',
		'Snap ITC'=>'Snap ITC',
		'Sports MT'=>'Sports MT',
		'Stencil'=>'Stencil',
		'Stop'=>'Stop',
		'Sylfaen'=>'Sylfaen',
		'Symbol'=>'Symbol',
		'Tahoma'=>'Tahoma',
		'Temp Installer Font'=>'Temp Installer Font',
		'Tempo Grunge'=>'Tempo Grunge',
		'Tempus Sans ITC'=>'Tempus Sans ITC',
		'Times New Roman'=>'Times New Roman',
		'Times New Roman Special'=>'Times New Roman Special',
		'Traditional Arabic'=>'Traditional Arabic',
		'Transport MT'=>'Transport MT',
		'Trebuchet MS'=>'Trebuchet MS',
		'Tunga'=>'Tunga',
		'Tw Cen MT'=>'Tw Cen MT',
		'Tw Cen MT Condensed'=>'Tw Cen MT Condensed',
		'Urdu Typesetting'=>'Urdu Typesetting',
		'Utsaah'=>'Utsaah',
		'Vacation MT'=>'Vacation MT',
		'Vani'=>'Vani',
		'Verdana'=>'Verdana',
		'Verdana Ref'=>'Verdana Ref',
		'Vijaya'=>'Vijaya',
		'Viner Hand ITC'=>'Viner Hand ITC',
		'Vivaldi'=>'Vivaldi',
		'Vixar ASCI'=>'Vixar ASCI',
		'Vladimir Script'=>'Vladimir Script',
		'Vrinda'=>'Vrinda',
		'Webdings'=>'Webdings',
		'Westminster'=>'Westminster',
		'Wide Latin'=>'Wide Latin',
		'Wingdings'=>'Wingdings',
	);*/
	return $fontFamily;
}

function getSDGLangArr($data)
{
	$langArr = [];
	if(!empty($data)) {
		foreach($data as $row) {
			$langArr[$row['language_id']] = $row;
		}
	}
	return $langArr;
}

function convertToMultiLang($data, $selectedLanguageId)
{
	if($selectedLanguageId === 2) {
		return cv_en_to_bn($data);
	}
	
	return $data;
}

function getNumberData($number, $selectedLanguageId=1, $point = 0)
{
	if(isset($number) && $selectedLanguageId == 2) {
		return cv_en_to_bn($number);
	}
	return $number;
}

function getSelectOptions($data, $selectedKey = '')
{
	//dd($selectedKey);

	$rtrOptions = '<option Style="display:none" value=""></option>';
	// echo 'SELECTED KEY : ' . $selectedKey;
	// die;

	if(!empty($data)) {
		foreach($data as $key => $val) {
			$selected = '';
			if(!empty($selectedKey) && $key == $selectedKey) {
				$selected = 'selected';
			}
			$rtrOptions .= '<option value="'. $key .'" '. $selected .'> ' . $val . ' </option>';
		}
	}
	
	return $rtrOptions;
}


// public function banglaDate()
// {
// 	 $banglaDate = '৩১ ডিসেম্বর, ২০১৪ ইং ১০:৫৭ মিঃ';

//     $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", "জানুয়ারী", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগষ্ট", "সেপ্টেম্বার", "অক্টোবার", "নভেম্বার", "ডিসেম্বার", ":", ",");

//     $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ":", ","); 

//     // convert all bangle char to English char 
//     $en_number = str_replace($search_array, $replace_array, $banglaDate);   

//     // remove unwanted char       
//     $end_date =  preg_replace('/[^A-Za-z0-9:\-]/', ' ', $en_number);

//     // convert date
//     $bangla_date = date("Y-m-d H:i ", strtotime($end_date));
// }