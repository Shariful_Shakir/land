<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Cookie;

class UniqueCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    
    private  $tableModelName;
    private  $fieldLabelTextEn;
    private  $fieldLabelTextBn;
    private  $columnName;
    private  $editId;
    public function __construct($tableModelName, $columnName, $fieldLabelTextEn, $fieldLabelTextBn, $editId)
    {
        $this->tableModelName = $tableModelName;
        $this->columnName = $columnName;
        $this->fieldLabelTextEn = $fieldLabelTextEn;
        $this->fieldLabelTextBn = $fieldLabelTextBn;
        $this->editId = $editId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $langId = Cookie::get('langId');
        if($langId == 2){
            $value = cv_bn_to_en($value);
        }

        $conditions = [];
        $conditions = [['status','!=', 1],[$this->columnName, $value]];
        if( ($this->editId != null) && !empty($tableModelName)  && !empty($columnName) ){
            $conditions = [['status','!=', 1],[$this->columnName, $value],['id','!=', $this->editId]];
        }
        
            
        //dd($conditions);
         if( !empty($value) && !empty($this->tableModelName) && !empty($this->columnName)){
             $existsData = $this->tableModelName::where($conditions)
            ->count();
            if($existsData > 0){
                return false;
            }
            
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $langId = Cookie::get('langId');
        $msg = '';

        if($langId ==1){
           $msg =  $this->fieldLabelTextEn.' has already been taken.';
        }else{
            $msg =  $this->fieldLabelTextBn.' পূর্বে সংরক্ষন করা হয়েছে ।';
        }

        return $msg;
        
    }
}
