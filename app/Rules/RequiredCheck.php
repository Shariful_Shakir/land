<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Cookie;

class RequiredCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    
    private  $fieldLabelTextEn;
    private  $fieldLabelTextBn;
    public function __construct($fieldLabelTextEn, $fieldLabelTextBn)
    {
        $this->fieldLabelTextEn = $fieldLabelTextEn;
        $this->fieldLabelTextBn = $fieldLabelTextBn;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(empty($value)){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $langId = Cookie::get('langId');
        $msg = '';

        if($langId ==1){
           $msg =  $this->fieldLabelTextEn.' must be required.';
        }else{
            $msg =  $this->fieldLabelTextBn.' অবশ্যই দিতে হবে ।';
        }

        return $msg;
        
    }
}
