<?php

namespace App\Exports;

//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

//class ExcelExport implements FromCollection
class ExcelExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
	
	public $excelData;
	
	function __construct($data)
	{
		$this->excelData = $data;
	}
	
    // public function collection()
    // {
        
    // }
	
	public function view(): View
    {
        return view($this->excelData['view'], [
            'data' => $this->excelData['data'],
            'langId' => $this->excelData['langId'],
            'applicationsReport' => $this->excelData['applicationsReport'],
            'is_excel' => 1,
        ]);
    }
}
