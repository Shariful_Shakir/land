<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Union;
use App\Upazila;
use App\District;
use App\Division;
use App\Session;
use App\BloodGroup;
use App\Religion;
use App\EducationalQualification;
use App\Gender;
use App\Batch;
use App\IsSelfEmployed;
use App\Application;
use Cookie;

class Report extends Model
{
    protected function dataList()
    {
    	$data = [];

	$data['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
	$data['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();
	$data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
	$data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
	$data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
	$data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
	$data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
	$data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
	$data['sessions_en'] = Session::pluck('title_eng', 'id')->toArray();
	$data['sessions_bn'] = Session::pluck('title_bng', 'id')->toArray();
	$data['blood_groups_en'] = BloodGroup::pluck('title_eng', 'id')->toArray();
	$data['blood_groups_bn'] = BloodGroup::pluck('title_bng', 'id')->toArray();
	$data['religions_en'] = Religion::pluck('title_eng', 'id')->toArray();
	$data['religions_bn'] = Religion::pluck('title_bng', 'id')->toArray();
	$data['educational_qualification_en'] = EducationalQualification::pluck('title_eng', 'id')->toArray();
	$data['educational_qualification_bn'] = EducationalQualification::pluck('title_bng', 'id')->toArray();
	$data['gender_en'] = Gender::pluck('title_eng', 'id')->toArray();
	$data['gender_bn'] = Gender::pluck('title_bng', 'id')->toArray();
	$data['batch_en'] = Batch::pluck('title_eng', 'id')->toArray();
	$data['batch_bn'] = Batch::pluck('title_bng', 'id')->toArray();
	$data['is_self_employed_en'] = IsSelfEmployed::pluck('title_eng', 'id')->toArray();
	$data['is_self_employed_bn'] = IsSelfEmployed::pluck('title_bng', 'id')->toArray();



		return $data;
    }

    protected function applicationData($conditions = [], $print=null)
    {
    	//dd($print);
		if(!empty($print)){
			$results = Application::where($conditions)->get();
		}else{
			$results = Application::where($conditions)->paginate(20);
		}
    	

    	return $results;
    }
}
