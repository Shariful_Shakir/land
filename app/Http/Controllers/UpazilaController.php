<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Division;
use App\District;
use App\Upazila;
use App\Application;
use Cookie;

class UpazilaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $upazilas = Upazila::all();

        return view('setting.upazila.index', compact('upazilas', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');
        //$datas = Division::all();

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        return view('setting.upazila.create', compact('langId', 'getNavData', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->toArray());

        $request->validate([
            'upazila_name_eng'=>'required',
            'upazila_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $upazila = new Upazila([
            'upazila_name_eng' => $request->get('upazila_name_eng'),
            'upazila_name_bng' => $request->get('upazila_name_bng'),            
            'geo_division_id' => $request->get('geo_division_id'),
            'geo_district_id' => $request->get('geo_district_id'),
            'bbs_code' => $request->get('bbs_code'),
            'created_by' => auth()->id()
            
        ]);
        $upazila->save();
        return redirect('/upazilas')->with('success', 'Upazila saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();

        $upazilas = Upazila::find($id);

        //dd($upazilas->toArray());

        return view('setting.upazila.edit', compact('upazilas', 'langId', 'getNavData', 'data')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'upazila_name_eng'=>'required',
            'upazila_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $upazila = Upazila::find($id);
        $upazila->upazila_name_eng =  $request->get('upazila_name_eng');
        $upazila->upazila_name_bng = $request->get('upazila_name_bng');
        $upazila->geo_division_id = $request->get('division_id');
        $upazila->geo_district_id = $request->get('district_id');
        $upazila->bbs_code = $request->get('bbs_code');
        $upazila->modified_by = $userId;
        $upazila->save();

        return redirect('/upazilas')->with('success', 'Upazila updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upazila = Upazila::find($id);
        $upazila->delete();

        return redirect('/upazilas')->with('success', 'upazila deleted!');
    }
}
