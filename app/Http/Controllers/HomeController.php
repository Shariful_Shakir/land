<?php
namespace App\Http\Controllers;

use App\Application;
use App\District;
use App\Division;
use App\Upazila;
use Cookie;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $dataEntryCounting = Application::groupBy('present_division_id', 'present_district_id', 'present_upazila_id')->selectRaw('count(id) as upazila_wise_dataEntry_count, present_division_id, present_district_id, present_upazila_id')->get();
        $divisions = Division::pluck('division_name_eng', 'id');
        $districts = District::pluck('district_name_eng', 'id');
        $upazilas = Upazila::pluck('upazila_name_eng', 'id');

        $chartMainArr = [];

        foreach ($dataEntryCounting as $decRow) {

            if (empty($chartMainArr[$decRow->present_division_id]['div_wise_data_entry_count'])) {
                $chartMainArr[$decRow->present_division_id]['div_wise_data_entry_count'] = 0;
            }
            $chartMainArr[$decRow->present_division_id]['div_wise_data_entry_count'] += $decRow->upazila_wise_dataEntry_count;
            $chartMainArr[$decRow->present_division_id]['name'] = $divisions[$decRow->present_division_id];

            if (empty($chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['dist_wise_data_entry_count'])) {
                $chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['dist_wise_data_entry_count'] = 0;
            }
            $chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['dist_wise_data_entry_count'] += $decRow->upazila_wise_dataEntry_count;
            $chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['name'] = $districts[$decRow->present_district_id];

            if (empty($chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['upazilla'][$decRow->present_upazila_id]['upz_wise_data_entry_count'])) {
                $chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['upazilla'][$decRow->present_upazila_id]['upz_wise_data_entry_count'] = 0;
            }
            $chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['upazilla'][$decRow->present_upazila_id]['upz_wise_data_entry_count'] += $decRow->upazila_wise_dataEntry_count;
            $chartMainArr[$decRow->present_division_id]['district'][$decRow->present_district_id]['upazilla'][$decRow->present_upazila_id]['name'] = $upazilas[$decRow->present_upazila_id];
        }

        //pr($chartMainArr);

        $chartMainData['series']['name'] = 'Division';
        $chartMainData['series']['colorByPoint'] = true;
        $s_i = 0;
        $dd_i = 0;
        foreach ($chartMainArr as $key => $divisionArr) {
            $chartMainData['series']['data'][$s_i]['name'] = $divisionArr['name'];
            $chartMainData['series']['data'][$s_i]['y'] = $divisionArr['div_wise_data_entry_count'];
            $drilldownId = strtolower($divisionArr['name']) . 'Division';
            $chartMainData['series']['data'][$s_i]['drilldown'] = $drilldownId;

            ++$s_i;

            if (!empty($divisionArr['district'])) {
                $dst_i = 0;
                $upz_i = 0;

                $chartMainData['drilldown']['series'][$dd_i]['id'] = $drilldownId;
                $chartMainData['drilldown']['series'][$dd_i]['name'] = 'District';
                $chartMainData['drilldown']['series'][$dd_i]['colorByPoint'] = true;

                $upzData = [];
                foreach ($divisionArr['district'] as $distKey => $districtArr) {
                    $chartMainData['drilldown']['series'][$dd_i]['data'][$dst_i]['name'] = $districtArr['name'];
                    $chartMainData['drilldown']['series'][$dd_i]['data'][$dst_i]['y'] = $districtArr['dist_wise_data_entry_count'];
                    $cDrilldownId = strtolower($districtArr['name']) . 'District';
                    $chartMainData['drilldown']['series'][$dd_i]['data'][$dst_i]['drilldown'] = $cDrilldownId;

                    ++$dst_i;

                    if (!empty($districtArr['upazilla'])) {

                        $upz_i = 0;
                        $upzData[$distKey]['name'] = $cDrilldownId;
                        foreach ($districtArr['upazilla'] as $upazilaArr) {
                            $upzData[$distKey]['data'][$upz_i][0] = $upazilaArr['name'];
                            $upzData[$distKey]['data'][$upz_i][1] = $upazilaArr['upz_wise_data_entry_count'];
                            ++$upz_i;

                        }

                    }

                }

                if (!empty($upzData)) {
                    foreach ($upzData as $udRow) {
                        ++$dd_i;
                        $chartMainData['drilldown']['series'][$dd_i]['id'] = $udRow['name'];
                        $chartMainData['drilldown']['series'][$dd_i]['name'] = 'Upazila';
                        $chartMainData['drilldown']['series'][$dd_i]['colorByPoint'] = true;
                        $chartMainData['drilldown']['series'][$dd_i]['data'] = $udRow['data'];
                    }
                }

                ++$dd_i;
            }

        }

        foreach ($chartMainArr as $key => $divisionArr) {
            $mapData['bangladesh'][$key]['region_name'] = $divisionArr['name'] . ' Division';
            $mapData['bangladesh'][$key]['region_id'] = strtolower($divisionArr['name']) . 'Division';
            $mapData['bangladesh'][$key]['total'] = $divisionArr['div_wise_data_entry_count'];

            if (!empty($divisionArr['district'])) {
                foreach ($divisionArr['district'] as $distKey => $districtArr) {
                    $mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['parent_region_id'] = 'Bangladesh';
                    $mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['region_name'] = $districtArr['name'] . ' District';
                    $mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['region_id'] = strtolower($districtArr['name']) . 'District';
                    $mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['total'] = $districtArr['dist_wise_data_entry_count'];

                    if (!empty($districtArr['upazilla'])) {
                        foreach ($districtArr['upazilla'] as $upzkey => $upazilaArr) {
                            $mapData[$mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['region_id']][$upzkey]['parent_region_id'] = strtolower($districtArr['name']) . 'Division';
                            $mapData[$mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['region_id']][$upzkey]['region_name'] = $upazilaArr['name'] . ' Upazila';
                            $mapData[$mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['region_id']][$upzkey]['region_id'] = strtolower($upazilaArr['name']) . 'Upazila';
                            $mapData[$mapData[$mapData['bangladesh'][$key]['region_id']][$distKey]['region_id']][$upzkey]['total'] = $upazilaArr['upz_wise_data_entry_count'];
                        }
                    }

                }
            }
        }

        $mapDataRs = [];
        if (!empty($mapData)) {
            foreach ($mapData as $key => $row) {
                $mapDataRs[$key] = array_values($row);
            }
        }
        //pr($chartMainData); die;
        // dd($mapData);
        // $data = json_encode($mapDataRs);
        // echo $data;die;
        $langId = Cookie::get('langId');
        return view('home')->with(compact('langId', 'getNavData', 'chartMainData', 'mapDataRs'));
    }

    // public function about()
    // {
    //     return view('about');
    // }

    public function applications()
    {
        return view('applications');
    }

    public function personalInfo()
    {
        return view('report.personal_info');
    }
    public function attachedInfo()
    {
        return view('report.attached_info');
    }

    public function about()
    {

        //$userData = User::get();

        //dd($userData->toArray());
        // instantiate and use the dompdf class

        // $pdf = PDF::loadView('applications.index')->setPaper('a4', 'portrait');

        // foreach ($userData as $key => $user) {
        //     $userName[$key]['userName'] = $user['name'];
        // }
        // //dd($userName);

        // return $pdf->stream($userName[$key]['userName'], '.pdf');

        //$pdf = PDF::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test For PDF and Excel work with Laravel work</h1>');

    }

}
