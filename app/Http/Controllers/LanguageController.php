<?php

namespace App\Http\Controllers;

use Cookie;

class LanguageController extends Controller
{
    public function changeLang($lang = null)
    {
        Cookie::queue(Cookie::make('langId', $lang, 3600));
        //dd(back());
        return redirect()->back();
    }
}
