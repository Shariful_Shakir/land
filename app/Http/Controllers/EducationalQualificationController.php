<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\EducationalQualification;
use App\Application;
use Cookie;

class EducationalQualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $EducationalQualifications = EducationalQualification::all();

        return view('setting.educational_qualification.index', compact('EducationalQualifications', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        return view('setting.educational_qualification.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$userId = auth()->id();
        // dd($userId);

        // $test = 1;
            $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $EducationalQualification = new EducationalQualification([
            'title_eng' => $request->get('title_eng'),
            'title_bng' => $request->get('title_bng'),
            'created_by' => auth()->id()
            
        ]);
        $EducationalQualification->save();
        return redirect('/educationalQualifications')->with('success', 'Educational Qualification saved!');
    }

   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $EducationalQualification = EducationalQualification::find($id);
        return view('setting.educational_qualification.edit', compact('EducationalQualification', 'langId', 'getNavData')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $EducationalQualification = EducationalQualification::find($id);
        $EducationalQualification->title_eng =  $request->get('title_eng');
        $EducationalQualification->title_bng = $request->get('title_bng');
        $EducationalQualification->modified_by = $userId;
        $EducationalQualification->save();

        return redirect('/educationalQualifications')->with('success', 'Educational Qualification updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $EducationalQualification = EducationalQualification::find($id);
        $EducationalQualification->delete();

        return redirect('/educationalQualifications')->with('success', 'Educational Qualification deleted!');
    }
}
