<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Union;
use App\Upazila;
use App\District;
use App\Division;
use App\Session;
use App\BloodGroup;
use App\Religion;
use App\EducationalQualification;
use App\Gender;
use App\Batch;
use App\IsSelfEmployed;
use Cookie;
use App\Role;
use App\Report;
use Laratrust;
use PDF;
use App\Attachment;

use App\Rules\RequiredCheck;
use App\Rules\UniqueCheck;

class AttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $getNavData = Application::getNavData();
       $langId = Cookie::get('langId');
       $title = 'Attachment List';

        if ($request->has('search')) {
            $attachments = Attachment::search($request->search)->where('status', '!=', 1)
                ->paginate(20);
        } else {
            $attachments = Attachment::where('status', '!=', 1);
            $attachments = $attachments->orderBy('id', 'desc')->paginate(20);
        }

        //dd($attachments->toArray());
        return view('attachments.index')->with(compact('title', 'attachments','langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $getNavData = Application::getNavData();

    $roles = Role::get();
    $langId = Cookie::get('langId');
    $userCheck = Auth::user();
    $superAdmin    = Laratrust::hasRole('super_admin');
    $divisionAdmin = Laratrust::hasRole('division_admin');
    $districtAdmin = Laratrust::hasRole('district_admin');
    $upazilaAdmin  = Laratrust::hasRole('upazila_admin');
    $dataentryUser = Laratrust::hasRole('data_entry_user');

    //dd($dataentryUser);

    $data = [];
    if($superAdmin == true){
        $title['title'] = 'Add';
        $data['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
    }elseif($divisionAdmin == true){
        $title['title'] = 'Add';
        $data['union_en'] = Union::where('geo_division_id', $userCheck['division_id'])->pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::where('geo_division_id', $userCheck['division_id'])->pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_bng', 'id')->toArray();
    }elseif($districtAdmin == true){
        $title['title'] = 'Add';
        $data['union_en'] = Union::where('geo_district_id', $userCheck['district_id'])->pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::where('geo_district_id', $userCheck['district_id'])->pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_bng', 'id')->toArray();
    }elseif($upazilaAdmin == true){
        $title['title'] = 'Add';
        $data['union_en'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
    }elseif($dataentryUser == true){
        $title['title'] = 'Add';
        $data['union_en'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
    }else{
        $title['title'] = 'Add';
        $data['union_en'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
    }

        $data['sessions_en'] = Session::pluck('title_eng', 'id')->toArray();
        $data['sessions_bn'] = Session::pluck('title_bng', 'id')->toArray();
        $data['blood_groups_en'] = BloodGroup::pluck('title_eng', 'id')->toArray();
        $data['blood_groups_bn'] = BloodGroup::pluck('title_bng', 'id')->toArray();
        $data['religions_en'] = Religion::pluck('title_eng', 'id')->toArray();
        $data['religions_bn'] = Religion::pluck('title_bng', 'id')->toArray();
        $data['educational_qualification_en'] = EducationalQualification::pluck('title_eng', 'id')->toArray();
        $data['educational_qualification_bn'] = EducationalQualification::pluck('title_bng', 'id')->toArray();
        $data['gender_en'] = Gender::pluck('title_eng', 'id')->toArray();
        $data['gender_bn'] = Gender::pluck('title_bng', 'id')->toArray();
        $data['batch_en'] = Batch::pluck('title_eng', 'id')->toArray();
        $data['batch_bn'] = Batch::pluck('title_bng', 'id')->toArray();
        $data['is_self_employed_en'] = IsSelfEmployed::pluck('title_eng', 'id')->toArray();
        $data['is_self_employed_bn'] = IsSelfEmployed::pluck('title_bng', 'id')->toArray();

        return view('attachments.create', compact('data','langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //dd($request->toArray());
        $langId = Cookie::get('langId');
        $getNavData = Application::getNavData();
        /*$request->validate([
            'tracking_no' => 'required|unique:attachments|max:20',            
            'father_name' => 'required|min:3|max:50',
            'man_woman_attachment' => 'required|min:3|max:50',
            // 'present_division_id' => 'required',
            //'present_district_id' => 'required',
            'present_upazila_id' => 'required',
            'present_union_id' => 'required',
            'present_village_id' => 'required',
            'present_post_code_id' => 'required|max:10',
            'attached_office' => 'required',
            //'joining_date' => 'required|date_format:Y-m-d',
            'joining_date' => 'required|date_format:d-m-Y',
            'last_joining_date' => 'required|date_format:d-m-Y',
            'recived_money' => 'required',
            'drop_out' => 'required|min:2|max:10',
           // 'amount' => 'required|numeric|between:0,99999999.99'

        ]);*/

        $request->validate([
                'tracking_no' => [new RequiredCheck('Tracking number', 'ট্রাকিং নম্বর'), new UniqueCheck('App\Attachment', 'tracking_no', 'Tracking number', 'ট্রাকিং নম্বর', 0 )],
                
                'father_name' => [new RequiredCheck("Father's/Husband's name", "পিতা/স্বামীর নাম")],
                
                'man_woman_attachment' => [new RequiredCheck("Male/Female Attachment", "পুরুষ এবং মহিলার সংযুক্তি")],
                
                'present_upazila_id' => [new RequiredCheck("Upazila Name", "উপজেলার নাম")],
                
                'present_union_id' => [new RequiredCheck("Union Name", "ইউনিয়ন নাম")],

                'present_village_id' => [new RequiredCheck("Village/House No", "গ্রাম/বাসা নং")],

                'present_post_code_id' => [new RequiredCheck("Post Code", "পোষ্ট কোড")],

                'attached_office' => [new RequiredCheck("Office Attachement", "সংযৃক্তি অফিস")],

                'joining_date' => [new RequiredCheck("Joining Date", "যোগদানের তারিখ")],
                
                'last_joining_date' => [new RequiredCheck("Last Joining  Date", "যোগদানের শেষ তারিখ")],

                'recived_money' => [new RequiredCheck("Recived Money", "জমাকৃত টাকা")],

                'drop_out' => [new RequiredCheck("Drop-Out?", "ডপ-আউট?")],
                
            ]);


        $attachment= new Attachment;
        //$attachment->date= date('Y-m-d', strtotime($request->get('date')));   
        $attachment->name='';   

        $attachment->tracking_no= $request->get('tracking_no'); 
        if($langId == 2){
            $attachment->tracking_no= cv_bn_to_en($request->get('tracking_no'));  
        } 
          
        $attachment->father_name=$request->get('father_name');   
        $attachment->man_woman_attachment=$request->get('man_woman_attachment');   
        $attachment->joining_date=date('Y-m-d', strtotime($request->get('joining_date')));   
        $attachment->last_joining_date =date('Y-m-d', strtotime($request->get('last_joining_date')));   
        $attachment->present_division_id= !empty($request->get('present_division_id')) ? $request->get('present_division_id') : 0;
        
        //$attachment->present_district_id=$request->get('present_district_id');   
        $attachment->present_district_id=!empty($request->get('present_upazila_id')) ? $request->get('present_upazila_id') : 0;   
        $attachment->present_upazila_id=$request->get('present_upazila_id');   
        $attachment->present_union_id=$request->get('present_union_id');   
        $attachment->present_village_id=$request->get('permanent_village_id');   
        $attachment->present_post_code_id=$request->get('present_post_code_id'); 
        
        $attachment->recived_money=$request->get('recived_money'); 
        if($langId == 2){
            $attachment->recived_money= cv_bn_to_en($request->get('recived_money')); 
        }
        $attachment->drop_out=$request->get('drop_out'); 
        if ($request->hasFile('attached_office')) {
            //dd($request);
            $file = $request->attached_office;
            $ext = $file->getClientOriginalExtension();
            $filename = date('YmdHis') . '-' . uniqid() . '.' . $ext;
            $file->storeAs('public/files/uploads', $filename);
            $file->move(public_path('files/uploads'), $filename);
//for Update
            //Storage::delete("public/pics/{$user->image}");
            //$user->image = $filename;
            $attachment['attached_office'] = $filename;
        }
        //dd($attachment->toArray());
        if($attachment->save()){
            if($langId == 1){
                return redirect('attachments/create')->with('success', 'Attachment  has been added.');
            }else{
                return redirect('attachments/create')->with('success', 'তথ্য সংরক্ষণ করা হয়েছে ।');
            }
        }

        if($langId == 1){
            return redirect('attachments/create')->with('finalError', 'Something went wrong, Please try again later.');
        }else{
           return redirect('attachments/create')->with('finalError', 'কোন সমস্যা দেখা দিয়েছে, কিছুক্ষন পর পুনরায় চেষ্টা করুন ।'); 
        }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function show(Attachment $application)
    {
        $getNavData = Application::getNavData();
        return view('attachments.show',compact('application', 'getNavData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function edit( $attachment)
    {
        $getNavData = Application::getNavData();
        //dd($attachment->toArray());
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        $data['is_drop'] = Attachment::pluck('is_dropout', 'id')->toArray();
        $attachments=Attachment::find($attachment);
        //dd( $attachments->toArray());
        return view('attachments.edit',compact('attachments','data', 'getNavData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $attachment)
    {
        $getNavData = Application::getNavData();
        //dd($request->toArray());
        $attachment=Attachment::find($attachment);
        //dd($attachment->toArray());
        $attachment->tracking_no = $request->get('tracking_no');
        // $attachment->division_id=$request->get('division_id');
        // $attachment->district_id=$request->get('district_id');
        $attachment->upazila_id=$request->get('upazila_id');
        $attachment->name=$request->get('name');
        $attachment->father_name=$request->get('father_name');
        $attachment->present_address=$request->get('present_address');
        $attachment->office_joined_date=$request->get('office_joined_date');
        $attachment->office_end_date=$request->get('office_end_date');
        $attachment->amount=$request->get('amount');
        //$attachment->is_dropout=$request->get('is_dropout');
        //$attachment->status=$request->get('status');
        $attachment->save();
        return redirect()->route('attachments.index')
                        ->with('success','Attachment Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attachment $attachment)
    {
        //
    }
}
