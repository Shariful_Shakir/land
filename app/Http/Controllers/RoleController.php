<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Permission;
use App\SpPermission;
use App\Division;
use App\District;
use App\Upazila;
use App\Application;
use Session;
use Cookie;
use Laratrust;

class RoleController extends Controller
{
public function __construct()
{
$this->middleware('auth');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
	$user = Auth::user();
	$userCheck = Auth::user();
	$allUser['name_en'] = User::pluck('name_en', 'id')->toArray();
	$allUser['name_bn'] = User::pluck('name_bn', 'id')->toArray();
	//dd($allUser);

	$getNavData = Application::getNavData();
	$roles = Role::get();
	$langId = Cookie::get('langId');
	//$roleUsers = RoleUser::get();

//Now this section take data from data of

	return view('manage.roles.index', compact('roles', 'langId', 'getNavData', 'user', 'allUser'));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
	$user = Auth::user();
	$getNavData = Application::getNavData();
	$langId = Cookie::get('langId');
	$permissions = Permission::all();
	$divisions = Division::all();
	$distracts = District::all();
	//dd($permissions->toArray());
return view('manage.roles.create', compact('permissions', 'divisions', 'langId', 'getNavData', 'user'));
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
	$getNavData = Application::getNavData();
	$langId = Cookie::get('langId');
	$this->validateWith([
	'display_name_en' => 'required|max:255',
	'display_name_bn' => 'required|max:255',
	'name' => 'required|max:100|alpha_dash|unique:roles'
	]);
	$role = new Role();
	$role->display_name_en = $request->display_name_en;
	$role->display_name_bn = $request->display_name_bn;
	$role->name = $request->name;
	$role->create_by = $request->create_by;
	$role->save();
	if ($request->permissions) {
	$role->syncPermissions(explode(',', $request->permissions));
	}
Session::flash('success', 'Successfully created the new '. $role->display_name_en . ' role in the database.');
return redirect()->route('roles.show', $role->id);
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
	$getNavData = Application::getNavData();
	$langId = Cookie::get('langId');
$role = Role::where('id', $id)->with('permissions')->first();
// return view('manage.roles.show')->withRole($role);
return view('manage.roles.show', compact('langId', 'role', 'getNavData'));

}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
	$getNavData = Application::getNavData();
	$langId = Cookie::get('langId');
$role = Role::where('id', $id)->with('permissions')->first();
$permissions = Permission::all();

// return view('manage.roles.edit')->withRole($role)->withPermissions($permissions);
return view('manage.roles.edit', compact('langId', 'role', 'permissions', 'getNavData'));
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
	$getNavData = Application::getNavData();
$this->validateWith([
'display_name_en' => 'required|max:255',
'display_name_bn' => 'required|max:255'
]);
$role = Role::findOrFail($id);
$role->display_name_en = $request->display_name_en;
$role->display_name_bn = $request->display_name_bn;
$role->save();
if ($request->permissions) {
$role->syncPermissions(explode(',', $request->permissions));
}
Session::flash('success', 'Successfully update the '. $role->display_name_en . ' role in the database.');
return redirect()->route('roles.show', $id);
}
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
//
}
}