<?php

namespace App\Http\Controllers;

use App\Application;
use App\Report;
use Cookie;
use Illuminate\Http\Request;
use PDF;
use App\Exports\ExcelExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Cookie::queue(Cookie::make('langId', 1, 3600));
        $langId = Cookie::get('langId');
    }

    public function filter()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $data = Report::dataList();

        return view('report.application_filter')->with(compact('langId', 'getNavData', 'data'));
    }

    public function reportApplication(Request $request, $fileType = '')
    {
        // echo $pdf;
        // echo $print;
        //dd($request->printStatus);

        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $conditions = [];
        if (isset($_POST['filter'])) {
            if (isset($request) && !empty($request)) {
                if (isset($request->present_division_id) && !empty($request->present_division_id)) {
                    $conditions['present_division_id'] = $request->present_division_id;
                }

                if (isset($request->present_district_id) && !empty($request->present_district_id)) {
                    $conditions['present_district_id'] = $request->present_district_id;
                }

                if (isset($request->present_upazila_id) && !empty($request->present_upazila_id)) {
                    $conditions['present_upazila_id'] = $request->present_upazila_id;
                }

                if (isset($request->batch_no) && !empty($request->batch_no)) {
                    $conditions['batch_no'] = $request->batch_no;
                }

                if (isset($request->session_id) && !empty($request->session_id)) {
                    $conditions['session_id'] = $request->session_id;
                }

                if (isset($request->contact_no) && !empty($request->contact_no)) {
                    $conditions['contact_no'] = $request->contact_no;
                }
            }
        }

        //dd($applicationsReport->toArray());
        $data = Report::dataList();
        //dd($request->get('print'));

        if (!empty($request->printStatus && $fileType == 'print')) {
            $applicationsReport = Report::applicationData($conditions, $request->printStatus);
            //dd($applicationsReport);
            return view('report.application_print')->with(compact('langId', 'getNavData', 'data', 'applicationsReport', 'conditions'));
        } else {
            $applicationsReport = Report::applicationData($conditions);
        }

        if ((isset($fileType) && !empty($fileType)) && $fileType == 'pdf') {
            $pdf = PDF::loadView('report.applicationPdf', compact('data', 'langId', 'getNavData', 'applicationsReport'))
                ->setPaper('A3', 'landscape');

            //dd($pdf);
            $fileName = 'file';
            return $pdf->stream($fileName, '.pdf');
        }

        if ((isset($fileType) && !empty($fileType)) && $fileType == 'excel') {

            libxml_use_internal_errors(true);
            $dataE['data'] = $data;
            $dataE['langId'] = $langId;
            $dataE['applicationsReport'] = $applicationsReport;
            $dataE['view'] = 'report.application_pdf';
            return Excel::download(new ExcelExport($dataE), 'application.xlsx');
        }

        return view('report.application_report')->with(compact('langId', 'getNavData', 'data', 'applicationsReport', 'conditions'));
    }

    // Report For Attachedment

    public function attachFilter()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $data = Report::dataList();

        return view('report.attach_filter')->with(compact('langId', 'getNavData', 'data'));
    }




    public function importExportView()
    {
        return view('import');
    }


    /**
     * @return \Illuminate\Support\Collection
     */

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */

    public function import()
    {
        Excel::import(new UsersImport, request()->file('file'));
        return back();
    }
}