<?php

namespace App\Http\Controllers;

use App\Khotian;
use App\Owner;
use Cookie;
use Illuminate\Http\Request;
use Redirect;
use Session;

class KhotianController extends Controller
{
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langId = Cookie::get('langId');

        return view('khotian.create', compact('langId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        foreach ($request->dag_no as $key => $value) {
            $khotian = new Khotian();
            $khotian->division_id = $request->input('division_id');
            $khotian->district_id = $request->input('district_id');
            $khotian->upazila_id = $request->input('upazila_id');
            $khotian->union_id = $request->input('union_id');
            $khotian->khotian_no = $request->input('khotian_no');
            $khotian->dag_no = $request['dag_no'][$key];
            $khotian->type = $request['class'][$key];
            $khotian->land_part = $request['area_of_land'][$key];
            $khotian->save();
        }
        foreach ($request->owner_name as $key => $value) {
            $land_owners = new Owner();
            $land_owners->division_id = $request->input('division_id');
            $land_owners->district_id = $request->input('district_id');
            $land_owners->upazila_id = $request->input('upazila_id');
            $land_owners->union_id = $request->input('union_id');
            $land_owners->khotian_no = $request->input('khotian_no');
            $land_owners->owner_name = $request['owner_name'][$key];
            $land_owners->father_name = $request['father_name'][$key];
            $land_owners->part = $request['part_of_land'][$key];
            $land_owners->save();
        }
        Session::flash('success', "Data Added Successfully.");
        return redirect(url('khotian-data-entry'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $langId = Cookie::get('langId');
        return view('khotian.report', compact('langId'));
    }

    public function findKhotian(Request $request)
    {
        $langId = Cookie::get('langId');
        $division_id = $request->division_id;
        $district_id = $request->district_id;
        $upazila_id = $request->upazila_id;
        $union_id = $request->union_id;

        $khotian_no = $request->khotian_no;
        $khotians = Khotian::where('khotian_no', $khotian_no)
            ->where('division_id', $division_id)
            ->where('district_id', $district_id)
            ->where('upazila_id', $upazila_id)
            ->where('union_id', $union_id)
            ->get()->toArray();
        $owners = Owner::where('khotian_no', $khotian_no)
            ->where('division_id', $division_id)
            ->where('district_id', $district_id)
            ->where('upazila_id', $upazila_id)
            ->where('union_id', $union_id)
            ->get()->toArray();
        $total_dag = count($khotians);
        $total_part = 0;
        foreach ($khotians as $khotian) {
            $total_part += (float) $khotian['land_part'];
        }
        //dd($khotians);
        if (!empty($khotians) && !empty($owners)) {
            return view('khotian.report', compact('langId', 'khotians', 'owners', 'total_dag', 'total_part'));
        } else {
            Session::flash('errors', "Data Not Found.");
            return Redirect::back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function userKhotianInfo(Request $request)
    {
        //dd($request->all());
        $khotian_no = $request->khotian_no;
        $langId = Cookie::get('langId');
        $division_id = Cookie::get('division_id');
        $district_id = Cookie::get('district_id');
        $upazila_id = Cookie::get('upazila_id');
        $union_id = Cookie::get('union_id');
        //dd($division_id);
        if (!empty($khotian_no)) {
            $khotians = Khotian::where('division_id', $division_id)
                ->where('district_id', $district_id)
                ->where('upazila_id', $upazila_id)
                ->where('union_id', $union_id)
                ->where('khotian_no', $khotian_no)
                ->get()->toArray();
            $owners = Owner::where('division_id', $division_id)
                ->where('district_id', $district_id)
                ->where('upazila_id', $upazila_id)
                ->where('union_id', $union_id)
                ->where('khotian_no', $khotian_no)
                ->get()->toArray();

        } else {
            $khotians = Khotian::where('division_id', $division_id)
                ->where('district_id', $district_id)
                ->where('upazila_id', $upazila_id)
                ->where('union_id', $union_id)
                ->get()->toArray();
            $owners = Owner::where('division_id', $division_id)
                ->where('district_id', $district_id)
                ->where('upazila_id', $upazila_id)
                ->where('union_id', $union_id)
                ->get()->toArray();
        }
        // $arr = [];
        // foreach ($khotians as $key => $khotian) {
        //$arr[$key]['khotian_no'] =
        //     $arr[$khotian['khotian_no']] = $khotian['dag_no'];
        // }
        //dd($khotians);
        $total_dag = count($khotians);
        $total_part = 0;
        foreach ($khotians as $khotian) {
            $total_part += (float) $khotian['land_part'];
        }

        return view('khotian.userKhotianInfo', compact('langId', 'khotians', 'owners', 'total_dag', 'total_part'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editOwnerReport(Request $report, $id)
    {
        $owner = Owner::find($id);
        return view('khotian.editOwnerReport', compact('owner'));
    }

    public function editDagReport(Request $report, $id)
    {
        $data = Khotian::find($id);
        return view('khotian.editDagReport', compact('data'));
    }
    public function storeDagReport(Request $request)
    {
        //dd($request->all());
        $khotian = Khotian::find($request->id);
        $khotian->dag_no = $request->dag_no;
        $khotian->type = $request->class;
        $khotian->land_part = $request->area_of_land;

        $khotian->save();

        return redirect(url('khotian/report'));
    }
    public function storeOwnerReport(Request $request)
    {
        //dd($request->all());
        $owner = Owner::find($request->id);
        $owner->owner_name = $request->owner_name;
        $owner->father_name = $request->father_name;
        $owner->part = $request->part_of_land;

        $owner->save();

        return redirect(url('khotian/report'));
    }
}
