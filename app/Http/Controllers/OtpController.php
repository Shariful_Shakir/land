<?php

namespace App\Http\Controllers;

use App\User;
use Cookie;
use Illuminate\Http\Request;

class OtpController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('otp.index');
    }
    public function sendOtp(Request $request)
    {
        $userId = Cookie::get('userID');
        $seed = str_split('123456789');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, 4) as $k) {
            $rand .= $seed[$k];
        }
        $user = User::where('contact_no', $request->contact_no)->where('nid', $userId)->first();
        //dd($user);
        $user->otp_code = $rand;

        if ($user->save()) {
            return $rand;
        } else {
            return "Somthing Errors. Please Try Again.";
        }

    }

    public function verifyOtp(Request $request)
    {
        $user = User::whereOtpCode($request->otp_code)->firstOrFail();
        //dd($user);
        if (!empty($user->otp_code)) {
            $user->otp_code = null;
            $user->save();
            return 'ok';
        } else {
            return 'Not verifyed';
        }

    }
}
