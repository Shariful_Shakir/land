<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Session;
use App\Application;
use Cookie;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $Sessions = Session::all();

        return view('setting.Session.index', compact('Sessions', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        return view('setting.Session.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$userId = auth()->id();
        // dd($userId);

        // $test = 1;
            $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $Session = new Session([
            'title_eng' => $request->get('title_eng'),
            'title_bng' => $request->get('title_bng'),
            'created_by' => auth()->id()
            
        ]);
        $Session->save();
        return redirect('/sessions')->with('success', 'Session saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $Session = Session::find($id);
        return view('setting.Session.edit', compact('Session', 'langId', 'getNavData')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $Session = Session::find($id);
        $Session->title_eng =  $request->get('title_eng');
        $Session->title_bng = $request->get('title_bng');
        $Session->modified_by = $userId;
        $Session->save();

        return redirect('/sessions')->with('success', 'Session updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Session = Session::find($id);
        $Session->delete();

        return redirect('/sessions')->with('success', 'Session deleted!');
    }
}
