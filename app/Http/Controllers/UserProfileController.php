<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\UserProfile;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\RoleUser;
use App\Division;
use App\District;
use App\Upazila;
use App\SpPermission;
use App\Application;
use Session;
use Cookie;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();

        $langId = Cookie::get('langId');
        $user = Auth::user();
        //dd($data->id);
       return view('manage.users.user_profile', compact('user', 'langId', 'data', 'getNavData'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(UserProfile $userProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserProfile $userProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserProfile  $userProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserProfile $userProfile)
    {
        //
    }
}
