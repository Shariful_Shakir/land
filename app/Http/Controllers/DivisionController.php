<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Division;
use App\Application;
use Cookie;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $divisions = Division::all();

        return view('setting.division.index', compact('divisions', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        return view('setting.division.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$userId = auth()->id();
        // dd($userId);

        // $test = 1;
            $request->validate([
            'division_name_eng'=>'required',
            'division_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $division = new Division([
            'division_name_eng' => $request->get('division_name_eng'),
            'division_name_bng' => $request->get('division_name_bng'),            
            'bbs_code' => $request->get('bbs_code'),
            'created_by' => auth()->id(),
            'modified_by' => auth()->id()
            
        ]);
        $division->save();
        return redirect('/divisions')->with('success', 'division saved!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $division = Division::find($id);
        return view('setting.division.edit', compact('division', 'langId', 'getNavData')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'division_name_eng'=>'required',
            'division_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $division = Division::find($id);
        $division->division_name_eng =  $request->get('division_name_eng');
        $division->division_name_bng = $request->get('division_name_bng');
        $division->bbs_code = $request->get('bbs_code');
        $division->modified_by = $userId;
        $division->save();

        return redirect('/divisions')->with('success', 'division updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = Division::find($id);
        $division->delete();

        return redirect('/divisions')->with('success', 'division deleted!');
    }
}
