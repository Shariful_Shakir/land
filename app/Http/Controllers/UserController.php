<?php

namespace App\Http\Controllers;

use App\Application;
use App\District;
use App\Division;
use App\Role;
use App\RoleUser;
use App\SpPermission;
use App\Upazila;
use App\Union;
use App\User;
use Cookie;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laratrust;

class UserController extends Controller
{
    private $superadmin;
    private $divisionadmin;
    private $districtadmin;
    private $dataentryuser;
    public function __construct()
    {
        //$this->middleware('auth');
        $langId = Cookie::get('langId');
        $getNavData = Application::getNavData();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $getNavData = Application::getNavData();
        $roles = Role::get();
        $langId = Cookie::get('langId');
        $roleUsers = RoleUser::get();

        // $roleUr = [];
        // foreach ($roleUsers as $key => $roleUser) {

        //     $roleUr[$key]['user_id'] = $roleUsers[$key]['user_id'];
        //     $roleUr[$key]['role_id'] = $roleUsers[$key]['role_id'];
        // }
        //dd($roleUr);

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        $superAdmin = Laratrust::hasRole('super_admin');
        $divisionAdmin = Laratrust::hasRole('division_admin');
        $districtAdmin = Laratrust::hasRole('district_admin');
        $upazilaAdmin = Laratrust::hasRole('upazila_admin');
        $dataentryUser = Laratrust::hasRole('data_entry_user');
        //Now this section take data from data of
        $userCheck = Auth::user();

        //dd($userCheck->toArray());
        //dd($superAdmin);
        $users = [];
        if ($superAdmin == true) {
            $users = User::orderBy('id', 'desc')
                ->paginate(10);
        } elseif ($divisionAdmin == true) {
            $otherDivision = SpPermission::where('user_id', $userCheck['id'])
                ->get();
            $divisionId = $userCheck['division_id'];
            foreach ($otherDivision as $key => $division) {
                $condition[$key] = $division['division_id'];
            }
            array_unshift($condition, $divisionId);

            if (!empty($otherDivision)) {
                $users = User::orderBy('id', 'desc')
                    ->whereIn('division_id', $condition)
                    ->paginate(10);
            } else {
                $condition = $userCheck['division_id'];
                $users = User::orderBy('id', 'desc')
                    ->where('division_id', $condition)
                    ->paginate(10);
            }
        } elseif ($districtAdmin == true) {
            $otherDistrict = SpPermission::where('user_id', $userCheck['id'])
                ->get();
            $districtId = $userCheck['district_id'];
            foreach ($otherDistrict as $key => $district) {
                $condition[$key] = $district['district_id'];
            }
            array_unshift($condition, $districtId);

            if (!empty($otherDistrict)) {
                $users = User::orderBy('id', 'desc')
                    ->whereIn('district_id', $condition)
                    ->paginate(10);
            } else {
                $condition = $userCheck['district_id'];
                $users = User::orderBy('id', 'desc')
                    ->where('district_id', $condition)
                    ->paginate(10);
            }
        } elseif ($upazilaAdmin == true) {
            $otherUpazila = SpPermission::where('user_id', $userCheck['id'])
                ->get();
            $upazilaId = $userCheck['upazila_id'];
            foreach ($otherUpazila as $key => $upazila) {
                $condition[$key] = $upazila['upazila_id'];
            }
            array_unshift($condition, $upazilaId);

            if (!empty($otherUpazila)) {
                $users = User::orderBy('id', 'desc')
                    ->whereIn('upazila_id', $condition)
                    ->paginate(10);
            } else {
                $condition = $userCheck['upazila_id'];
                $users = User::orderBy('id', 'desc')
                    ->where('upazila_id', $condition)
                    ->paginate(10);
            }
        } else {
            echo "Not Access";
        }

        //dd($users->toArray());
        //return view('manage.users.index')->withUsers($users);
        return view('manage.users.index', compact('users', 'data', 'langId', 'getNavData'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        //Role Send for view page
        // This role all work process than give data to send view page
        $roles = Role::get();
        $langId = Cookie::get('langId');
        $userCheck = Auth::user();
        //dd($userCheck['district_id']);

        $superAdmin = Laratrust::hasRole('super_admin');
        $divisionAdmin = Laratrust::hasRole('division_admin');
        $districtAdmin = Laratrust::hasRole('district_admin');
        $upazilaAdmin = Laratrust::hasRole('upazila_admin');
        $dataentryUser = Laratrust::hasRole('data_entry_user');
        //Now this section take data from data of
        //Data send for Division and district
        $data = [];
        if ($superAdmin == true) {
            $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
            $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
            $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
            $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($divisionAdmin == true) {
            $data['district_en'] = District::where('geo_division_id', $userCheck['division_id'])->pluck('district_name_eng', 'id')->toArray();
            $data['district_bn'] = District::where('geo_division_id', $userCheck['division_id'])->pluck('district_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($districtAdmin == true) {
            $data['upazilas_en'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($upazilaAdmin == true) {
            $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } else {
            echo "Not Access";
        }
        //dd($data);
        return view('manage.users.create', compact('roles', 'data', 'langId', 'getNavData'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->toArray());
        $superAdmin = Laratrust::hasRole('super_admin');
        $divisionAdmin = Laratrust::hasRole('division_admin');
        $districtAdmin = Laratrust::hasRole('district_admin');
        $upazilaAdmin = Laratrust::hasRole('upazila_admin');
        $dataentryUser = Laratrust::hasRole('data_entry_user');

        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');
        //dd($request->toArray());
        // $this->validate([
        //     'name_en'   => 'required|max:255',
        //     'name_bn'   => 'required|max:255',
        //     'password'  => 'nullable|confirmed|max:40'
        // ]);
        //dd($request->upazila_id);
        if (!empty($request->password)) {
            $password = trim($request->password);
        }
        //dd($request->upazila_id);
        $user = new User();
        //$user = $request->all();
        $user->name_en = $request->name_en;
        $user->name_bn = $request->name_bn;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->upazila_id = $request->upazila_id;
        $user->contact_no = $request->contact_no;
        $user->email = $request->email;
        $user->password = Hash::make($password);
        //dd($user);
        $user->save();
        if ($request->role) {
            $user->syncRoles($request->role);
        }

        //dd($request->toArray());

        if ($superAdmin == true) {
            if (!empty($request->division)) {
                foreach ($request->division as $division) {
                    if (!empty($request->district)) {
                        foreach ($request->district as $district) {
                            if (!empty($request->upazila)) {
                                foreach ($request->upazila as $upazila) {
                                    $data[] = [
                                        'user_id' => $user->id,
                                        'division_id' => $division,
                                        'district_id' => $district,
                                        'upazila_id' => $upazila,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($divisionAdmin == true) {
            if (!empty($request->district)) {
                foreach ($request->district as $district) {
                    if (!empty($request->upazila)) {
                        foreach ($request->upazila as $upazila) {
                            $data[] = [
                                'user_id' => $user->id,
                                'district_id' => $district,
                                'upazila_id' => $upazila,
                            ];
                        }
                    }
                }
            }
        }

        if ($districtAdmin == true) {
            if (!empty($request->upazila)) {
                foreach ($request->upazila as $upazila) {
                    $data[] = [
                        'user_id' => $user->id,
                        'upazila_id' => $upazila,
                    ];
                }
            }
        }

        if ($upazilaAdmin == true) {
            if (!empty($request->upazila_id)) {
                $upazila = $request->upazila_id;
                $data[] = [
                    'user_id' => $user->id,
                    'upazila_id' => $upazila,
                ];
            }
        }

        SpPermission::insert($data); // Eloquent approach

        return redirect()->route('users.show', $user->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');
        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['upazila_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazila_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        //dd($data);
        $user = User::where('id', $id)->with('roles')->first();
        //return view("manage.users.show")->withUser($user);
        //dd($user->toArray());
        return view('manage.users.show', compact('user', 'data', 'langId', 'getNavData'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        //Role Send for view page
        // This role all work process than give data to send view page
        $roles = Role::get();
        $langId = Cookie::get('langId');
        $userCheck = Auth::user();
        $user = User::where('id', $id)->first();
        $roleUsers = RoleUser::where('user_id', $id)->first();
        $permision = SpPermission::where('user_id', $id)->get();

        $superAdmin = Laratrust::hasRole('super_admin');
        $divisionAdmin = Laratrust::hasRole('division_admin');
        $districtAdmin = Laratrust::hasRole('district_admin');
        $upazilaAdmin = Laratrust::hasRole('upazila_admin');
        $dataentryUser = Laratrust::hasRole('data_entry_user');
        //Now this section take data from data of
        //Data send for Division and district
        $data = [];
        if ($superAdmin == true) {
            $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
            $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
            $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
            $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($divisionAdmin == true) {
            $data['district_en'] = District::where('geo_division_id', $userCheck['division_id'])->pluck('district_name_eng', 'id')->toArray();
            $data['district_bn'] = District::where('geo_division_id', $userCheck['division_id'])->pluck('district_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($districtAdmin == true) {
            $data['upazilas_en'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($upazilaAdmin == true) {
            $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } else {
            echo "Not Access";
        }

        return view('manage.users.edit', compact('user', 'data', 'roles', 'langId', 'getNavData', 'roleUsers', 'permision'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        //dd($request->toArray());

        $this->validateWith([
            'name_en' => 'required|max:255',
            'name_bn' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $id,
        ]);
        $user = User::findOrFail($id);
        $user->name_en = $request->name_en;
        $user->name_bn = $request->name_bn;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        //$user->syncRoles(explode(',', $request->roles));
        if ($request->role) {
            $user->syncRoles($request->role);
        }
        return redirect()->route('users.show', $id);
        // if () {
        //   return redirect()->route('users.show', $id);
        // } else {
        //   Session::flash('error', 'There was a problem saving the updated user info to the database. Try again later.');
        //   return redirect()->route('users.edit', $id);
        // }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sp_permission_store(Request $request)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        // $input = Request::get();
        // dd($request->toArray());

        $users = User::get();

        foreach ($users as $key => $user) {}
        //dd($id->id);
        //dd($request->toArray());
        $spPermission = new SpPermission();
        //dd($id);
        //$spPermission->user_id = $user->id;

        //$divisions = $request->division;
        //dd($divisions);
        //$districts = $request->district;
        // $upazilas = $request->upazila;

        // if(!empty($divisions)){
        //     //dd($divisions);
        //     foreach ($divisions as $key => $id) {
        //     //dd($id);

        //     $spPermission->division_id = $id;

        //     //dd($spPermission->division_id);
        //     $spPermission->save();
        //     unset($spPermission->division_id);
        // }

        // }

        $divisions = $request->division;
        $districts = $request->district;
        $upazilas = $request->upazila;

        //dd($request->toArray());

        $data = [];
        foreach ($divisions as $key => $division) {
            $data[] = [
                'division_id' => $division,
                'user_id' => $user->id,
            ];

            // foreach($districts as $key => $district) {
            //     $data[] = [
            //         'district' => $district
            //     ];

            // }
        }

        SpPermission::insert($data);

        // $services = $request->division;
        // //dd($services);
        // foreach($services as $service){
        //     //dd($service);
        //  SpPermission::create($service, );
        // }
        // $services = $request->input('division');
        // //dd($services);
        // foreach($services as $service){
        //     //dd($service);
        //  SpPermission::create($service);
        // }

        //dd($user->id);
        //$user = $request->all();
        // $spPermission->spPermission_id = $id->id;
        // $spPermission->division_id = $request->division;
        // $spPermission->district_id = $request->district;
        // $spPermission->upazila_id  = $request->upazila;
        //dd($spPermission);
        //$spPermission->save();
        return redirect()->route('users.show', $user->id);
    }

    public function getDistrictInformation(Request $request)
    {
        $districts = District::where('geo_division_id', $request->divissionId)->get();
        return response()->json($districts);
    }
    public function getUpazliaInformation(Request $request)
    {

        $upzilas = Upazila::where('geo_district_id', $request->districtId)->get();
        //dd($upzilas);
        return response()->json($upzilas);
    }

    public function getDistrictData(Request $request)
    {
        //dd($request->toArray());
        $districts = District::whereIn('geo_division_id', $request->divisionId)->get();
        //dd($districts->toArray());
        return response()->json($districts);
    }

    public function getUpazilaData(Request $request)
    {
        //dd($request->all());
        $upazilas = Upazila::whereIn('geo_district_id', $request->districtId)->get();
        //dd($upazilas->toArray());
        return response()->json($upazilas);
    }

    public function getUnionInformation(Request $request)
    {
        $langId = Cookie::get('langId');
        if (isset($request->upazila_id) && !empty($request->upazila_id)) {
            $union_name = ($langId == 1) ? 'union_name_eng' : 'union_name_bng';
            $unionInfo = Union::select('id', $union_name)->where('geo_upazila_id', $request->upazila_id)->get();
            return $unionInfo;
        }
    }

    public function register(Request $request)
    {
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
            . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            . '0123456789');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, 8) as $k) {
            $rand .= $seed[$k];
        }
        //dd($request->all());
        $user = new User();
        $user->name_en = $request->name;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->upazila_id = $request->upazila_id;
        $user->union_id = $request->union_id;
        $user->contact_no = $request->contact_no;
        $user->nid = $request->nid;
        $user->password = Hash::make($rand);
        //dd($user->toArray());

        if($user->save()){
            return $rand;
        }else{
            return "Somthing Errors. Please Try Again.";

        }
        
    }
}
