<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Religion;
use App\Application;
use Cookie;

class ReligionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $Religions = Religion::all();

        return view('setting.Religion.index', compact('Religions', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        return view('setting.Religion.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$userId = auth()->id();
        // dd($userId);

        // $test = 1;
            $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $Religion = new Religion([
            'title_eng' => $request->get('title_eng'),
            'title_bng' => $request->get('title_bng'),
            'created_by' => auth()->id()
            
        ]);
        $Religion->save();
        return redirect('/religions')->with('success', 'Religion saved!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $Religion = Religion::find($id);
        return view('setting.Religion.edit', compact('Religion', 'langId', 'getNavData')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $Religion = Religion::find($id);
        $Religion->title_eng =  $request->get('title_eng');
        $Religion->title_bng = $request->get('title_bng');
        $Religion->modified_by = $userId;
        $Religion->save();

        return redirect('/religions')->with('success', 'Religion updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Religion = Religion::find($id);
        $Religion->delete();

        return redirect('/religions')->with('success', 'Religion deleted!');
    }
}
