<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Division;
use App\District;
use App\Application;
use Cookie;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $districts = District::all();

        return view('setting.district.index', compact('districts', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');
        //$datas = Division::all();

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['division_bbs_code'] = Division::pluck('bbs_code', 'id')->toArray();
        
        //dd($datas->toArray());
        return view('setting.district.create', compact('langId', 'getNavData', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->toArray());
        $request->validate([
            'district_name_eng'=>'required',
            'district_name_bng'=>'required',
            'geo_division_id'=>'required',
            'bbs_code'=>'required'
        ]);

        $district = new District([
            'district_name_eng' => $request->get('district_name_eng'),
            'district_name_bng' => $request->get('district_name_bng'),            
            'geo_division_id' => $request->get('geo_division_id'),
            'bbs_code' => $request->get('bbs_code'),
            'created_by' => auth()->id()
            
        ]);
        $district->save();
        return redirect('/districts')->with('success', 'district saved!');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();

        $district = District::find($id);
        return view('setting.district.edit', compact('district', 'langId', 'getNavData', 'data')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'district_name_eng'=>'required',
            'district_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $district = District::find($id);
        $district->district_name_eng =  $request->get('district_name_eng');
        $district->district_name_bng = $request->get('district_name_bng');
        $district->geo_division_id = $request->get('division_id');
        $district->bbs_code = $request->get('bbs_code');
        $district->modified_by = $userId;
        $district->save();

        return redirect('/districts')->with('success', 'district updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $district = District::find($id);
        $district->delete();

        return redirect('/districts')->with('success', 'district deleted!');
    }
}
