<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\BloodGroup;
use App\Application;
use Cookie;

class BloodGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $BloodGroups = BloodGroup::all();

        return view('setting.blood_group.index', compact('BloodGroups', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        return view('setting.blood_group.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$userId = auth()->id();
        // dd($userId);

        // $test = 1;
            $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $BloodGroup = new BloodGroup([
            'title_eng' => $request->get('title_eng'),
            'title_bng' => $request->get('title_bng'),
            'created_by' => auth()->id()
            
        ]);

        $BloodGroup->save();
        return redirect('/bloodGroups')->with('success', 'Blood Group saved!');
    }

  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $BloodGroup = BloodGroup::find($id);
        //dd($BloodGroup->toArray());
        return view('setting.blood_group.edit', compact('BloodGroup', 'langId', 'getNavData')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $BloodGroup = BloodGroup::find($id);
        $BloodGroup->title_eng =  $request->get('title_eng');
        $BloodGroup->title_bng = $request->get('title_bng');
        $BloodGroup->modified_by = $userId;
        $BloodGroup->save();

        return redirect('/bloodGroups')->with('success', 'BloodGroup updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $BloodGroup = BloodGroup::find($id);
        $BloodGroup->delete();

        return redirect('/bloodGroups')->with('success', 'Blood Group deleted!');
    }
}
