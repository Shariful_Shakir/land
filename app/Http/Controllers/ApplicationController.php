<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Application;
use App\Batch;
use App\BloodGroup;
use App\District;
use App\Division;
use App\EducationalQualification;
use App\Gender;
use App\IsSelfEmployed;
use App\Religion;
use App\Role;
use App\Session;
use App\Union;
use App\Upazila;
use Cookie;
use Laratrust;
use PDF;

class ApplicationController extends Controller
{
/**
 * Create a new controller instance.
 *
 * @return void
 */
    private $superadmin;
    private $divisionadmin;
    private $districtadmin;
    private $dataentryuser;

    public function __construct()
    {
        $this->middleware('auth');
//Cookie::queue(Cookie::make('langId', 1, 3600));
        $langId = Cookie::get('langId');
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index(Request $request, $pdf = '')
    {
        $langId = Cookie::get('langId');
        $getNavData = Application::getNavData();
//Get all applications and pass it to the view
        //dd($pdf);

        $title = 'আবেদন তালিকা';
        $superAdmin = Laratrust::hasRole('super_admin');
        $divisionAdmin = Laratrust::hasRole('division_admin');
        $districtAdmin = Laratrust::hasRole('district_admin');
        $dataentryUser = Laratrust::hasRole('data_entry_user');

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
//Now this section take data from data of
        $userCheck = Auth::user();
//dd($userCheck->toArray());
        //dd($districtAdmin);
        $applications = [];
        if ($superAdmin == true) {
            if ($request->has('search')) {
                $applications = Application::search($request->search)
                    ->where('status', '!=', 1)
                    ->paginate(20);
            } else {
                $applications = Application::where('status', '!=', 1);
                $applications = $applications->orderBy('id', 'desc')->paginate(20);
            }
        } elseif ($divisionAdmin == true) {
            if ($request->has('search')) {
                $applications = Application::search($request->search)
                    ->where('status', '!=', 1)
                    ->where('present_division_id', $userCheck['division_id'])
                    ->orWhere('permanent_division_id', $userCheck['division_id'])
                    ->paginate(20);
            } else {
                $applications = Application::where('status', '!=', 1);
                $applications = $applications->orderBy('id', 'desc')
                    ->where('present_division_id', $userCheck['division_id'])
                    ->orWhere('permanent_division_id', $userCheck['division_id'])
                    ->paginate(20);
            }
        } elseif ($districtAdmin == true) {
            if ($request->has('search')) {
                $applications = Application::search($request->search)
                    ->where('status', '!=', 1)
                    ->where('present_district_id', $userCheck['district_id'])
                    ->orWhere('permanent_district_id', $userCheck['district_id'])
                    ->paginate(20);
            } else {
                $applications = Application::where('status', '!=', 1);
                $applications = $applications->orderBy('id', 'desc')
                    ->where('present_district_id', $userCheck['district_id'])
                    ->orWhere('permanent_district_id', $userCheck['district_id'])
                    ->paginate(20);
            }
        } else {
            echo "Not Access";
        }

        if ($pdf == 'pdf') {
            $pdf = PDF::loadView('applications.index', compact('data', 'applications', 'langId'))
                ->setPaper('a4', 'portrait');
            //dd($pdf);
            $userName = 'file';
            return $pdf->stream($userName . '.pdf');
        }

        return view('applications.index')->with(compact('title', 'applications', 'data', 'langId', 'getNavData'));
    }
/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function create()
    {
        $getNavData = Application::getNavData();

        $roles = Role::get();
        $langId = Cookie::get('langId');
        $userCheck = Auth::user();
        $superAdmin = Laratrust::hasRole('super_admin');
        $divisionAdmin = Laratrust::hasRole('division_admin');
        $districtAdmin = Laratrust::hasRole('district_admin');
        $upazilaAdmin = Laratrust::hasRole('upazila_admin');
        $dataentryUser = Laratrust::hasRole('data_entry_user');

//dd($dataentryUser);

        $data = [];
        if ($superAdmin == true) {
            $title['title'] = 'Add';
            $data['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
            $data['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($divisionAdmin == true) {
            $title['title'] = 'Add';
            $data['union_en'] = Union::where('geo_division_id', $userCheck['division_id'])->pluck('union_name_eng', 'id')->toArray();
            $data['union_bn'] = Union::where('geo_division_id', $userCheck['division_id'])->pluck('union_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('geo_division_id', $userCheck['division_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($districtAdmin == true) {
            $title['title'] = 'Add';
            $data['union_en'] = Union::where('geo_district_id', $userCheck['district_id'])->pluck('union_name_eng', 'id')->toArray();
            $data['union_bn'] = Union::where('geo_district_id', $userCheck['district_id'])->pluck('union_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('geo_district_id', $userCheck['district_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($upazilaAdmin == true) {
            $title['title'] = 'Add';
            $data['union_en'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_eng', 'id')->toArray();
            $data['union_bn'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } elseif ($dataentryUser == true) {
            $title['title'] = 'Add';
            $data['union_en'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_eng', 'id')->toArray();
            $data['union_bn'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
        } else {
            $title['title'] = 'Add';
            $data['union_en'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_eng', 'id')->toArray();
            $data['union_bn'] = Union::where('geo_upazila_id', $userCheck['upazila_id'])->pluck('union_name_bng', 'id')->toArray();
            $data['upazilas_en'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_eng', 'id')->toArray();
            $data['upazilas_bn'] = Upazila::where('id', $userCheck['upazila_id'])->pluck('upazila_name_bng', 'id')->toArray();
        }

        $data['sessions_en'] = Session::pluck('title_eng', 'id')->toArray();
        $data['sessions_bn'] = Session::pluck('title_bng', 'id')->toArray();
        $data['blood_groups_en'] = BloodGroup::pluck('title_eng', 'id')->toArray();
        $data['blood_groups_bn'] = BloodGroup::pluck('title_bng', 'id')->toArray();
        $data['religions_en'] = Religion::pluck('title_eng', 'id')->toArray();
        $data['religions_bn'] = Religion::pluck('title_bng', 'id')->toArray();
        $data['educational_qualification_en'] = EducationalQualification::pluck('title_eng', 'id')->toArray();
        $data['educational_qualification_bn'] = EducationalQualification::pluck('title_bng', 'id')->toArray();
        $data['gender_en'] = Gender::pluck('title_eng', 'id')->toArray();
        $data['gender_bn'] = Gender::pluck('title_bng', 'id')->toArray();
        $data['batch_en'] = Batch::pluck('title_eng', 'id')->toArray();
        $data['batch_bn'] = Batch::pluck('title_bng', 'id')->toArray();
        $data['is_self_employed_en'] = IsSelfEmployed::pluck('title_eng', 'id')->toArray();
        $data['is_self_employed_bn'] = IsSelfEmployed::pluck('title_bng', 'id')->toArray();

        $data_p['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
        $data_p['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();
        $data_p['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data_p['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();

//dd($data);
        return view('applications.create', compact('data', 'langId', 'getNavData', 'data_p'));
//return response()->json($arr);
    }
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    public function store(Request $request)
    {
        //dd($request->all());
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        if ($langId == 1) {
            $messsages = array(
                'tracking_no.required' => 'Tracking number must be given.',
                'tracking_no.unique' => 'Tracking number must be unique.',
                'tracking_no.max' => 'No more than 25 trucking numbers can be provided.',

                'name.required' => 'Name must be given.',
                'name.min' => 'Name must be more than 3 characters long.',
                'name.max' => 'Name must be less than 50 characters long.',

                'father_name.required' => "Father's/Husband's name must be given.",
                'father_name.min' => "Father's/Husband's name must be more than 3 characters long.",
                'father_name.max' => "Father's/Husband's name must be less than 50 characters long.",

                'mother_name.required' => "Mother's name must be given.",
                'mother_name.min' => "Mother's name must be more than 3 characters long.",
                'mother_name.max' => "Mother's name must be less than 50 characters long.",

                'national_id.required' => 'National ID must be given.',
                'national_id.unique' => 'National ID must be unique.',
                'national_id.min' => 'National ID must be more than 10 and less than 17 digit.',
                'national_id.max' => 'National ID must be more than 10 and less than 17 digit.',

                'contact_no.required' => 'Contact Number must be given.',
                'contact_no.unique' => 'Contact Number must be unique.',
                'contact_no.min' => 'Contact Number must be minimum 11 digit.',
                'contact_no.max' => 'Contact Number must be maximum 11 digit.',

                'email.unique' => 'Email must be Unique',

                'image.required' => 'Photo must be given',
                'image.mimes' => 'Photo type must be .jpg, .jpeg, .png',
                'image.max' => 'Photo size maximum 2 mb.',

                'file.mimes' => 'File type must be .doc, .docx, .xlsx, .pdf',
                'file.max' => 'File size maximum 10 mb.',

                'educational_qualification.required' => 'Educational Qualification must be given.',
                'nationality.required' => 'Nationality must be given.',
                'religion.required' => 'Religion must be given.',
                'date_of_birth.required' => 'Date of birth must be given.',
                'gender.required' => 'Gender must be given.',
                'session_id.required' => 'Session must be given.',
                'batch_no.required' => 'Batch No must be given.',
            );
        } else {
            $messsages = array(
                'tracking_no.required' => 'ট্রাকিং নম্বর অবশ্যই দিতে হবে।',
                'tracking_no.unique' => 'ট্র্যাকিং নম্বরটি অবশ্যই অনন্য হবে।',
                'tracking_no.max' => 'ট্রাকিং নম্বর ২০ এর বেশি দেয়া যাবে না।',

                'name.required' => 'নাম অবশ্যই দিতে হবে।',
                'name.min' => 'নাম অবশ্যই ৩ অক্ষর এর বেশি হতে হবে।',
                'name.max' => 'নাম অবশ্যই ৫০ অক্ষর এর কম হতে হবে।',

                'father_name.required' => 'বাবার / স্বামীর নাম অবশ্যই দিতে হবে।',
                'father_name.min' => 'পিতার / স্বামীর নাম অবশ্যই ৩ টি অক্ষরের বেশি হতে হবে।',
                'father_name.max' => 'পিতার / স্বামীর নাম অবশ্যই ৫০ টি অক্ষরের কম হতে হবে।',

                'mother_name.required' => 'মায়ের নাম অবশ্যই দিতে হবে।',
                'mother_name.min' => 'মায়ের নাম অবশ্যই ৩ টি অক্ষরের বেশি দিতে হবে।',
                'mother_name.max' => 'মায়ের নাম অবশ্যই ৫০ টি অক্ষরের কম হতে হবে।',

                'national_id.required' => 'জাতীয় আইডি অবশ্যই দিতে হবে।',
                'national_id.unique' => 'জাতীয় আইডি অবশ্যই অনন্য হবে।',
                'national_id.min' => 'জাতীয় আইডি 10 এর বেশি এবং 17 ডিজিটের চেয়ে কম হওয়া উচিত।',
                'national_id.max' => 'জাতীয় আইডি 10 এর বেশি এবং 17 ডিজিটের চেয়ে কম হওয়া উচিত।',

                'contact_no.required' => 'যোগাযোগের নম্বরটি অবশ্যই দিতে হবে।',
                'contact_no.unique' => 'যোগাযোগের নম্বরটি অবশ্যই অনন্য হবে।',
                'contact_no.min' => 'যোগাযোগের নম্বরটি ন্যূনতম ১১ ডিজিটের হতে হবে।',
                'contact_no.max' => 'যোগাযোগের সংখ্যাটি সর্বাধিক ১১ ডিজিটের হতে হবে।',

                'email.unique' => 'ইমেল অবশ্যই অনন্য হতে হবে।',

                'image.required' => 'ছবি অবশ্যই দিতে হবে।',
                'image.mimes' => 'ছবি এর টাইপ অবশ্যই .jpg, .png, .jpeg হতে হবে।',
                'image.max' => 'ছবি আকার সর্বাধিক ২ এমবি।',

                'file.mimes' => 'ফাইল এর টাইপ অবশ্যই .doc, .docx, .xlsx, .pdf হতে হবে।',
                'file.max' => 'ফাইলের আকার সর্বাধিক ১০ এমবি।',

                'educational_qualification.required' => 'শিক্ষাগত যোগ্যতা দিতে হবে।',
                'nationality.required' => 'জাতীয়তা দিতে হবে।',
                'religion.required' => 'ধর্ম অবশ্যই দিতে হবে।',
                'date_of_birth.required' => 'জন্ম তারিখ অবশ্যই দিতে হবে।',
                'gender.required' => 'লিঙ্গ দিতে হবে।',
                'session_id.required' => 'সেশন অবশ্যই দিতে হবে।',
                'batch_no.required' => 'ব্যাচ নম্বর অবশ্যই দিতে হবে।',

            );
        }

        $rules = array(
            'tracking_no' => 'required|unique:applications|max:20',
            'name' => 'required|string|min:3|max:50',
            'father_name' => 'required|string|min:3|max:50',
            'mother_name' => 'required|string|min:3|max:50',
            'national_id' => 'required|unique:applications|min:10|max:17',
            'educational_qualification' => 'required|min:1|max:255',
            'contact_no' => 'required|unique:applications|min:11|max:11',
            'email' => 'nullable|unique:applications|max:40',
            'image' => 'required|mimes:jpeg,png,jpg|max:2048',
            'file' => 'mimes:doc,docx,xlsx,pdf|max:10240',
            'nationality' => 'required',
            'religion' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'session_id' => 'required',
            'batch_no' => 'required',
        );

        $applications = $request->all();
        if ($langId == 2) {
            $applications['tracking_no'] = cv_bn_to_en($applications['tracking_no']);
            $applications['contact_no'] = cv_bn_to_en($applications['contact_no']);
            $applications['national_id'] = cv_bn_to_en($applications['national_id']);

        }

        $validator = Validator::make($applications, $rules, $messsages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $presentGeoInfo = Upazila::getGeoInformationsById($request->present_upazila_id);
        $permanentGeoInfo = Upazila::getGeoInformationsById($request->permanent_upazila_id);
        if (empty($request->permanent_upazila_id)) {
            $applications['present_division_id'] = $presentGeoInfo['division_id'];
            $applications['permanent_division_id'] = $presentGeoInfo['division_id'];
            $applications['present_district_id'] = $presentGeoInfo['district_id'];
            $applications['permanent_district_id'] = $presentGeoInfo['district_id'];
            $applications['permanent_upazila_id'] = $applications['present_upazila_id'];
            $applications['permanent_union_id'] = $applications['present_union_id'];
            $applications['permanent_village_id'] = $applications['present_village_id'];
            $applications['permanent_post_code_id'] = $applications['present_post_code_id'];
        } else {
            $applications['present_division_id'] = $presentGeoInfo['division_id'];
            $applications['permanent_division_id'] = $permanentGeoInfo['division_id'];
            $applications['present_district_id'] = $presentGeoInfo['district_id'];
            $applications['permanent_district_id'] = $permanentGeoInfo['district_id'];
        }
        $applications['date_of_birth'] = date('Y-m-d', strtotime($applications['date_of_birth']));

        if ($request->hasFile('image')) {
            $image = $request->image;
            $ext = $image->getClientOriginalExtension();
            $imgname = date('YmdHis') . '-' . uniqid() . '.' . $ext;
            $image->storeAs('public/img/uploads', $imgname);
            $image->move(public_path('img/uploads'), $imgname);
            //for Update
            //Storage::delete("public/pics/{$user->image}");
            //$user->image = $filename;
            $applications['img'] = $imgname;
        }

        if ($request->hasFile('file')) {
            $file = $request->file;
            $ext = $file->getClientOriginalExtension();
            $filename = date('YmdHis') . '-' . uniqid() . '.' . $ext;
            $file->storeAs('public/files/uploads', $filename);
            $file->move(public_path('files/uploads'), $filename);
            //for Update
            //Storage::delete("public/pics/{$user->image}");
            //$user->image = $filename;
            $applications['file_name'] = $filename;
        }
        Application::create($applications);
        if ($langId == 2) {
            $request->session()->flash('status', 'তথ্য সফলভাবে সংরক্ষিত হয়েছে');
        } else {
            $request->session()->flash('status', 'Data Save Successfully');
        }

        return redirect()->route('applications.create')
            ->with('success', 'Application created successfully.');
    }

/**
 * Display the specified resource.
 *
 * @param  \App\Application  $application
 * @return \Illuminate\Http\Response
 */
    public function show(Application $application)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');
        $data['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['sessions_en'] = Session::pluck('title_eng', 'id')->toArray();
        $data['sessions_bn'] = Session::pluck('title_bng', 'id')->toArray();
        $data['blood_groups_en'] = BloodGroup::pluck('title_eng', 'id')->toArray();
        $data['blood_groups_bn'] = BloodGroup::pluck('title_bng', 'id')->toArray();
        $data['religions_en'] = Religion::pluck('title_eng', 'id')->toArray();
        $data['religions_bn'] = Religion::pluck('title_bng', 'id')->toArray();
        $data['educational_qualification_en'] = EducationalQualification::pluck('title_eng', 'id')->toArray();
        $data['educational_qualification_bn'] = EducationalQualification::pluck('title_bng', 'id')->toArray();
        $data['gender_en'] = Gender::pluck('title_eng', 'id')->toArray();
        $data['gender_bn'] = Gender::pluck('title_bng', 'id')->toArray();
        $data['batch_en'] = Batch::pluck('title_eng', 'id')->toArray();
        $data['batch_bn'] = Batch::pluck('title_bng', 'id')->toArray();
        $data['is_self_employed_en'] = IsSelfEmployed::pluck('title_eng', 'id')->toArray();
        $data['is_self_employed_bn'] = IsSelfEmployed::pluck('title_bng', 'id')->toArray();
        //dd($application->toArray());
        return view('applications.show', compact('application', 'langId', 'data', 'getNavData'));
    }
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Application  $application
 * @return \Illuminate\Http\Response
 */
    public function edit(Request $request)
    {
        $langId = Cookie::get('langId');

        if($langId == 2){
            $value = cv_bn_to_en($request->update_by_trackingNo);
        }else{
            $value = $request->update_by_trackingNo;
        }
        
        if (!empty($value) && strlen($value) == '11') {
            $column = "contact_no";
        } else {
            $column = "tracking_no";
        }
        $getNavData = Application::getNavData();
        $roles = Role::get();
        
        $userCheck = Auth::user();
        $data['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['sessions_en'] = Session::pluck('title_eng', 'id')->toArray();
        $data['sessions_bn'] = Session::pluck('title_bng', 'id')->toArray();
        $data['blood_groups_en'] = BloodGroup::pluck('title_eng', 'id')->toArray();
        $data['blood_groups_bn'] = BloodGroup::pluck('title_bng', 'id')->toArray();
        $data['religions_en'] = Religion::pluck('title_eng', 'id')->toArray();
        $data['religions_bn'] = Religion::pluck('title_bng', 'id')->toArray();
        $data['educational_qualification_en'] = EducationalQualification::pluck('title_eng', 'id')->toArray();
        $data['educational_qualification_bn'] = EducationalQualification::pluck('title_bng', 'id')->toArray();
        $data['gender_en'] = Gender::pluck('title_eng', 'id')->toArray();
        $data['gender_bn'] = Gender::pluck('title_bng', 'id')->toArray();
        $data['batch_en'] = Batch::pluck('title_eng', 'id')->toArray();
        $data['batch_bn'] = Batch::pluck('title_bng', 'id')->toArray();
        // $data['IsSelfEmployed_en'] = IsSelfEmployed::pluck('title_eng', 'id')->toArray();
        // $data['IsSelfEmployed_bn'] = IsSelfEmployed::pluck('title_bng', 'id')->toArray();
        $data['is_self_employed_en'] = IsSelfEmployed::pluck('title_eng', 'id')->toArray();
        $data['is_self_employed_bn'] = IsSelfEmployed::pluck('title_bng', 'id')->toArray();
        $applications = Application::where($column, $value)->get()->toArray();

        if (empty($applications)) {
            if ($langId == 2) {
                $request->session()->flash('status', 'তথ্য পাওয়া যায়নি');
            } else {
                $request->session()->flash('status', 'Data not found');
            }
            return redirect()->route('applications.create');
        }

        return view('applications.create', compact('applications', 'data', 'langId', 'getNavData'));
    }
/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \App\Application  $application
 * @return \Illuminate\Http\Response
 */
    public function update(Request $request)
    {

        $langId = Cookie::get('langId');

        if ($langId == 1) {
            $messsages = array(
                'tracking_no.required' => 'Tracking number must be given.',
                'tracking_no.unique' => 'Tracking number must be unique.',
                'tracking_no.max' => 'No more than 25 trucking numbers can be provided.',

                'name.required' => 'Name must be given.',
                'name.min' => 'Name must be more than 3 characters long.',
                'name.max' => 'Name must be less than 50 characters long.',

                'father_name.required' => "Father's/Husband's name must be given.",
                'father_name.min' => "Father's/Husband's name must be more than 3 characters long.",
                'father_name.max' => "Father's/Husband's name must be less than 50 characters long.",

                'mother_name.required' => "Mother's name must be given.",
                'mother_name.min' => "Mother's name must be more than 3 characters long.",
                'mother_name.max' => "Mother's name must be less than 50 characters long.",

                'national_id.required' => 'National ID must be given.',
                'national_id.unique' => 'National ID must be unique.',
                'national_id.min' => 'National ID must be more than 10 and less than 17 digit.',
                'national_id.max' => 'National ID must be more than 10 and less than 17 digit.',

                'contact_no.required' => 'Contact Number must be given.',
                'contact_no.unique' => 'Contact Number must be unique.',
                'contact_no.min' => 'Contact Number must be minimum 11 digit.',
                'contact_no.max' => 'Contact Number must be maximum 11 digit.',

                'email.unique' => 'Email must be Unique',

                'image.required' => 'Photo must be given',
                'image.mimes' => 'Photo type must be .jpg, .jpeg, .png',
                'image.max' => 'Photo size maximum 2 mb.',

                'file.mimes' => 'File type must be .doc, .docx, .xlsx, .pdf',
                'file.max' => 'File size maximum 10 mb.',

                'educational_qualification.required' => 'Educational Qualification must be given.',
                'nationality.required' => 'Nationality must be given.',
                'religion.required' => 'Religion must be given.',
                'date_of_birth.required' => 'Date of birth must be given.',
                'gender.required' => 'Gender must be given.',
                'session_id.required' => 'Session must be given.',
                'batch_no.required' => 'Batch No must be given.',
            );
        } else {
            $messsages = array(
                'tracking_no.required' => 'ট্রাকিং নম্বর অবশ্যই দিতে হবে।',
                'tracking_no.unique' => 'ট্র্যাকিং নম্বরটি অবশ্যই অনন্য হবে।',
                'tracking_no.max' => 'ট্রাকিং নম্বর ২০ এর বেশি দেয়া যাবে না।',

                'name.required' => 'নাম অবশ্যই দিতে হবে।',
                'name.min' => 'নাম অবশ্যই ৩ অক্ষর এর বেশি হতে হবে।',
                'name.max' => 'নাম অবশ্যই ৫০ অক্ষর এর কম হতে হবে।',

                'father_name.required' => 'বাবার / স্বামীর নাম অবশ্যই দিতে হবে।',
                'father_name.min' => 'পিতার / স্বামীর নাম অবশ্যই ৩ টি অক্ষরের বেশি হতে হবে।',
                'father_name.max' => 'পিতার / স্বামীর নাম অবশ্যই ৫০ টি অক্ষরের কম হতে হবে।',

                'mother_name.required' => 'মায়ের নাম অবশ্যই দিতে হবে।',
                'mother_name.min' => 'মায়ের নাম অবশ্যই ৩ টি অক্ষরের বেশি দিতে হবে।',
                'mother_name.max' => 'মায়ের নাম অবশ্যই ৫০ টি অক্ষরের কম হতে হবে।',

                'national_id.required' => 'জাতীয় আইডি অবশ্যই দিতে হবে।',
                'national_id.unique' => 'জাতীয় আইডি অবশ্যই অনন্য হবে।',
                'national_id.min' => 'জাতীয় আইডি 10 এর বেশি এবং 17 ডিজিটের চেয়ে কম হওয়া উচিত।',
                'national_id.max' => 'জাতীয় আইডি 10 এর বেশি এবং 17 ডিজিটের চেয়ে কম হওয়া উচিত।',

                'contact_no.required' => 'যোগাযোগের নম্বরটি অবশ্যই দিতে হবে।',
                'contact_no.unique' => 'যোগাযোগের নম্বরটি অবশ্যই অনন্য হবে।',
                'contact_no.min' => 'যোগাযোগের নম্বরটি ন্যূনতম ১১ ডিজিটের হতে হবে।',
                'contact_no.max' => 'যোগাযোগের সংখ্যাটি সর্বাধিক ১১ ডিজিটের হতে হবে।',

                'email.unique' => 'ইমেল অবশ্যই অনন্য হতে হবে।',

                'image.required' => 'ছবি অবশ্যই দিতে হবে।',
                'image.mimes' => 'ছবি এর টাইপ অবশ্যই .jpg, .png, .jpeg হতে হবে।',
                'image.max' => 'ছবি আকার সর্বাধিক ২ এমবি।',

                'file.mimes' => 'ফাইল এর টাইপ অবশ্যই .doc, .docx, .xlsx, .pdf হতে হবে।',
                'file.max' => 'ফাইলের আকার সর্বাধিক ১০ এমবি।',

                'educational_qualification.required' => 'শিক্ষাগত যোগ্যতা দিতে হবে।',
                'nationality.required' => 'জাতীয়তা দিতে হবে।',
                'religion.required' => 'ধর্ম অবশ্যই দিতে হবে।',
                'date_of_birth.required' => 'জন্ম তারিখ অবশ্যই দিতে হবে।',
                'gender.required' => 'লিঙ্গ দিতে হবে।',
                'session_id.required' => 'সেশন অবশ্যই দিতে হবে।',
                'batch_no.required' => 'ব্যাচ নম্বর অবশ্যই দিতে হবে।',

            );
        }

        $rules = array(
            'tracking_no' => 'required|max:20|unique:applications,tracking_no,' . $request['id'],
            'name' => 'required|string|min:3|max:50',
            'father_name' => 'required|string|min:3|max:50',
            'mother_name' => 'required|string|min:3|max:50',
            'national_id' => 'required|confirmed|unique:applications,national_id,' . $request['id'] . '|min:10|max:17',
            'educational_qualification' => 'required|min:1|max:255',
            'contact_no' => 'required|confirmed|unique:applications,contact_no,' . $request['id'] . '|min:11|max:11',
            'email' => 'nullable|confirmed|unique:applications,email,' . $request['id'] . '|max:40',
            'image' => 'mimes:jpeg,png,jpg|max:2048',
            'file' => 'mimes:doc,docx,xlsx,pdf|max:10240',
            'nationality' => 'required',
            'religion' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'session_id' => 'required',
            'batch_no' => 'required',
        );

        $data = $request->all();
        if ($langId == 2) {
            $request->tracking_no = cv_bn_to_en($data['tracking_no']);
            $request->contact_no = cv_bn_to_en($data['contact_no']);
            $request->national_id = cv_bn_to_en($data['national_id']);
        }
        //dd($request->all());
        $validator = Validator::make($data, $rules, $messsages);
        if ($validator->fails()) {
            return redirect()
                ->route('applications.create')
                ->withErrors($validator);
        }

        $application = Application::find($request->id);

        $application->tracking_no = $request->input('tracking_no');
        $application->name = $request->input('name');
        $application->father_name = $request->input('father_name');
        $application->mother_name = $request->input('mother_name');
        $application->contact_no = $request->input('contact_no');
        $application->blood_group = $request->input('blood_group');
        $application->email = $request->input('email');
        $application->gender = $request->input('gender');
        $application->date_of_birth = date('Y-m-d', strtotime($request->input('date_of_birth')));
        $application->nationality = $request->input('nationality');
        $application->national_id = $request->input('national_id');
        $application->religion = $request->input('religion');

        $presentGeoInfo = Upazila::getGeoInformationsById($request->input('present_upazila_id'));
        $permanentGeoInfo = Upazila::getGeoInformationsById($request->input('permanent_upazila_id'));

        $application['present_division_id'] = $presentGeoInfo['division_id'];
        $application['present_district_id'] = $presentGeoInfo['district_id'];
        $application['present_upazila_id'] = $request->input('present_upazila_id');
        $application['present_union_id'] = $request->input('present_union_id');
        $application['present_village_id'] = $request->input('present_village_id');
        $application['present_post_code_id'] = $request->input('present_post_code_id');

        if (empty($request->input('permanent_upazila_id'))) {
            $application['permanent_division_id'] = $presentGeoInfo['division_id'];
            $application['permanent_district_id'] = $presentGeoInfo['district_id'];
            $application['permanent_upazila_id'] = $request->input('present_upazila_id');
            $application['permanent_union_id'] = $request->input('present_union_id');
            $application['permanent_village_id'] = $request->input('present_village_id');
            $application['permanent_post_code_id'] = $request->input('present_post_code_id');
        } else {
            $application['permanent_division_id'] = $permanentGeoInfo['division_id'];
            $application['permanent_district_id'] = $permanentGeoInfo['district_id'];
            $application['permanent_upazila_id'] = $request->input('permanent_upazila_id');
            $application['permanent_union_id'] = $request->input('permanent_union_id');
            $application['permanent_village_id'] = $request->input('permanent_village_id');
            $application['permanent_post_code_id'] = $request->input('permanent_post_code_id');
        }

        $application->educational_qualification = $request->input('educational_qualification');
        $application->is_self_employed = $request->input('is_self_employed');
        $application->batch_no = $request->input('batch_no');
        $application->session_id = $request->input('session_id');
        $application->training_subject_description = $request->input('training_subject_description');
        $application->it_knowledge = $request->input('it_knowledge');
        $application->communication_skill = $request->input('communication_skill');
        //dd($request->file);

        if (empty($request->image) || $request->image == null) {
            $application['img'] = $application->img;
        } else {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $ext = $image->getClientOriginalExtension();
                $imgname = date('YmdHis') . '-' . uniqid() . '.' . $ext;
                $image->storeAs('public/img/uploads', $imgname);
                $image->move(public_path('img/uploads'), $imgname);
                $application['img'] = $imgname;
            }
        }
        if (empty($request->file) || $request->file == null) {
            $application['file_name'] = $application->file_name;
        } else {
            if ($request->hasFile('file')) {
                $file = $request->file;
                $ext = $file->getClientOriginalExtension();
                $filename = date('YmdHis') . '-' . uniqid() . '.' . $ext;
                $file->storeAs('public/img/uploads', $filename);
                $file->move(public_path('img/uploads'), $filename);
                $application['file_name'] = $filename;
            }
        }
        if ($langId == 2) {
            $application->tracking_no = cv_bn_to_en($request->input('tracking_no'));
            $application->contact_no = cv_bn_to_en($request->input('contact_no'));
            $application->national_id = cv_bn_to_en($request->input('national_id'));

        }

        //dd($application);
        if ($application->save()) {
            if ($langId == 2) {
                $request->session()->flash('status', 'তথ্য সফলভাবে আপডেট হয়েছে।');
            } else {
                $request->session()->flash('status', 'Data Update Succesfully.');
            }
        } else {
            if ($langId == 2) {
                $request->session()->flash('status', 'তথ্য ডেটা আপডেট হয়নি।');
            } else {
                $request->session()->flash('status', 'Data Not Updated.');
            }

        }

        return redirect()->route('applications.create')
            ->with('success', 'Application updated successfully');
    }
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Application  $application
 * @return \Illuminate\Http\Response
 */
    public function destroy(Application $application)
    {
        $getNavData = Application::getNavData();
        $application->delete();
        return redirect()->route('applications.index')
            ->with('success', 'Application deleted successfully');
    }
    public function getUpazliaInformation(Request $request)
    {
        $rtrArr = [];
        if (isset($request->upazila_id) && !empty($request->upazila_id)) {
            $upazilaInfo = Upazila::select('geo_district_id', 'upazila_name_eng', 'upazila_name_bng')->where('id', $request->upazila_id)->first();
            $rtrArr = District::getDistrictInformation($upazilaInfo->geo_district_id);
            $rtrArr['upazila_code'] = $upazilaInfo->upazilaid;
            $rtrArr['upazila_name_en'] = $upazilaInfo->name_en;
            $rtrArr['upazila_name'] = $upazilaInfo->name;
        }
        $status = true;
        return response()->json(['success' => $status, 'data' => $rtrArr]);
    }
    public function getUnionInformation(Request $request)
    {
        $langId = Cookie::get('langId');
        if (isset($request->upazila_id) && !empty($request->upazila_id)) {
            $union_name = ($langId == 1) ? 'union_name_eng' : 'union_name_bng';
            $unionInfo = Union::select('id', $union_name)->where('geo_upazila_id', $request->upazila_id)->get();
            return $unionInfo;
        }
    }
    public function getPostalCodeInformation(Request $request)
    {
        if (isset($request->union_id) && !empty($request->union_id)) {
            $post_code = Union::select('postcode')->where('id', $request->union_id)->get();
            $status = true;
            return response()->json(['success' => $status, 'data' => $post_code]);
        }
    }
    public function check_unique(Request $request)
    {
        $value = cv_bn_to_en($request->data['value']);
        $column = $request->data['column'];

        if (!empty($value) && !empty($column)) {
            $is_exist = Application::select($column)->where($column, $value)->get();
            if (!empty($is_exist)) {
                return $is_exist[0][$column];
            } else {
                return 0;
            }
        }
    }

}
