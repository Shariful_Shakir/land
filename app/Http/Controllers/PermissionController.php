<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Application;
use Session;
use Cookie;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $langId = Cookie::get('langId');
        $getNavData = Application::getNavData();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $getNavData = Application::getNavData();
      $langId = Cookie::get('langId');
      //$permissions = Permission::all()->where('division_id', $division_id);
      $permissions = Permission::all();
      //return view('manage.permissions.index')->withPermissions($permissions);
      return view('manage.permissions.index', compact('permissions', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $getNavData = Application::getNavData();
      $langId = Cookie::get('langId');

      //return view('manage.permissions.create');
      return view('manage.permissions.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->toArray());

      $getNavData = Application::getNavData();
      $langId = Cookie::get('langId');

        $this->validateWith([
          'display_name_en' => 'required|max:255',
          'display_name_bn' => 'required|max:255',
          'name' => 'required|max:255|unique:permissions,name'
        ]);

        $permission = new Permission();
        $permission->name = $request->name;
        $permission->display_name_en = $request->display_name_en;
        $permission->display_name_bn = $request->display_name_bn;
        $permission->save();

        Session::flash('success', 'Permission has been successfully added');
        return redirect()->route('permissions.index');

      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $getNavData = Application::getNavData();
      $langId = Cookie::get('langId');

      $permission = Permission::findOrFail($id);
      //return view('manage.permissions.show')->withPermission($permission);
      return view('manage.permissions.show', compact('langId', 'getNavData', 'permission'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $getNavData = Application::getNavData();
      $langId = Cookie::get('langId');

      $permission = Permission::findOrFail($id);
      //return view('manage.permissions.edit')->withPermission($permission);
      return view('manage.permissions.edit', compact('langId', 'getNavData', 'permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $getNavData = Application::getNavData();
      $langId = Cookie::get('langId');

      $this->validateWith([
        'display_name_en' => 'required|max:255',
        'display_name_bn' => 'required|max:255'
      ]);
      $permission = Permission::findOrFail($id);
      $permission->display_name_en = $request->display_name_en;
      $permission->display_name_bn = $request->display_name_bn;
      $permission->name = $request->name;
      $permission->save();

      Session::flash('success', 'Updated the '. $permission->display_name_en . ' permission.');
      return redirect()->route('permissions.index', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // $article = Permission::findOrFail($id);
      // $article->delete();
      // return view('dashboard')->with([
      //   'flash_message' => 'Deleted',
      //   'flash_message_important' => false
      // ]);
    }
}
