<?php
namespace App\Http\Controllers\Auth;

use App\District;
use App\Division;
use App\Http\Controllers\Controller;
use App\Upazila;
use App\User;
use Cookie;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
/*
|--------------------------------------------------------------------------
| Register Controller
|--------------------------------------------------------------------------
|
| This controller handles the registration of new users as well as their
| validation and creation. By default this controller uses a trait to
| provide this functionality without requiring any additional code.
|
 */
    use RegistersUsers;
/**
 * Where to redirect users after registration.
 *
 * @var string
 */
    protected $redirectTo = '/home';
/**
 * Create a new controller instance.
 *
 * @return void
 */
    public function __construct()
    {
//$this->middleware('guest');
    }
/**
 * Get a validator for an incoming registration request.
 *
 * @param  array  $data
 * @return \Illuminate\Contracts\Validation\Validator
 */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'division_id' => ['required', 'numeric', 'max:255'],
            'district_id' => ['required', 'numeric', 'max:255'],
            'upazila_id' => ['required', 'numeric', 'max:255'],
            'contact_no' => ['required', 'max:11'],
// 'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:6', 'confirmed'],
            'nid' => ['required'],
        ]);
    }
/**
 * Create a new user instance after a valid registration.
 *
 * @param  array  $data
 * @return \App\User
 */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'division_id' => $data['division_id'],
            'district_id' => $data['district_id'],
            'upazila_id' => $data['upazila_id'],
            'contact_no' => $data['contact_no'],
            //'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

/**
 * Show the application registration form.
 *
 * @return \Illuminate\Http\Response
 */
    public function showRegistrationForm()
    {
        $langId = Cookie::get('langId');
        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        $data['upazilas_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazilas_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        //$region=Region::all();
        //dd($data);

        return view('auth.register', compact('data', 'langId'));
    }

}
