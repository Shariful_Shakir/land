<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Gender;
use App\Application;
use Cookie;

class GenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $Genders = Gender::all();

        return view('setting.Gender.index', compact('Genders', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        return view('setting.Gender.create', compact('langId', 'getNavData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$userId = auth()->id();
        // dd($userId);

        // $test = 1;
            $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $Gender = new Gender([
            'title_eng' => $request->get('title_eng'),
            'title_bng' => $request->get('title_bng'),
            'created_by' => auth()->id()
            
        ]);
        $Gender->save();
        return redirect('/genders')->with('success', 'Gender saved!');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $Gender = Gender::find($id);
        return view('setting.Gender.edit', compact('Gender', 'langId', 'getNavData')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'title_eng'=>'required',
            'title_bng'=>'required'
        ]);

        $Gender = Gender::find($id);
        $Gender->title_eng =  $request->get('title_eng');
        $Gender->title_bng = $request->get('title_bng');
        $Gender->modified_by = $userId;
        $Gender->save();

        return redirect('/genders')->with('success', 'Gender updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Gender = Gender::find($id);
        $Gender->delete();

        return redirect('/genders')->with('success', 'Gender deleted!');
    }
}
