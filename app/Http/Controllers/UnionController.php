<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Division;
use App\District;
use App\Upazila;
use App\Union;
use App\Application;
use Cookie;

class UnionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');

        $unions = Union::all();

        return view('setting.union.index', compact('unions', 'langId', 'getNavData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');
        //$datas = Division::all();

        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        $data['upazila_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazila_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        return view('setting.union.create', compact('langId', 'getNavData', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->toArray());

        $request->validate([
            'union_name_eng'=>'required',
            'union_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $unions = new Union([
            'union_name_eng' => $request->get('union_name_eng'),
            'union_name_bng' => $request->get('union_name_bng'),            
            'geo_division_id' => $request->get('geo_division_id'),
            'geo_district_id' => $request->get('geo_district_id'),
            'geo_upazila_id' => $request->get('geo_upazila_id'),
            'bbs_code' => $request->get('bbs_code'),
            'created_by' => auth()->id()
            
        ]);

        $unions->save();
        return redirect('/unions')->with('success', 'Union saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getNavData = Application::getNavData();
        $langId = Cookie::get('langId');


        $data['division_en'] = Division::pluck('division_name_eng', 'id')->toArray();
        $data['division_bn'] = Division::pluck('division_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        $data['district_en'] = District::pluck('district_name_eng', 'id')->toArray();
        $data['district_bn'] = District::pluck('district_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        $data['upazila_en'] = Upazila::pluck('upazila_name_eng', 'id')->toArray();
        $data['upazila_bn'] = Upazila::pluck('upazila_name_bng', 'id')->toArray();
        //dd($datas->toArray());
        $data['union_en'] = Union::pluck('union_name_eng', 'id')->toArray();
        $data['union_bn'] = Union::pluck('union_name_bng', 'id')->toArray();

        $unions = Union::find($id);
        //dd($unions->toArray());
        return view('setting.union.edit', compact('unions', 'langId', 'getNavData', 'data')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;

        $request->validate([
            'union_name_eng'=>'required',
            'union_name_bng'=>'required',
            'bbs_code'=>'required'
        ]);

        $union = Union::find($id);
        $union->union_name_eng =  $request->get('union_name_eng');
        $union->union_name_bng = $request->get('union_name_bng');
        $union->geo_division_id = $request->get('division_id');
        $union->geo_district_id = $request->get('district_id');
        $union->geo_upazila_id = $request->get('upazila_id');
        $union->bbs_code = $request->get('bbs_code');
        $union->modified_by = $userId;
        $union->save();

        return redirect('/unions')->with('success', 'union updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upazila = Union::find($id);
        $upazila->delete();

        return redirect('/unions')->with('success', 'Union deleted!');
    }
}
