<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use Auth;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->toArray());
        $user = Auth::user();
        $controllerAction = class_basename(Route::currentRouteAction());
        //dd($user->toArray());
        if($user->can($controllerAction)){
            return $next($request);
        }
        else {
            return redirect()->back()->with('flash_warning', 'you are not allowed for requesting page');
        }

    }
}
