<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Khotian extends Model
{
    public $fillable = ['khotian_no', 'dag_no', 'land_part', 'type'];
    public $timestamps = false;
}
