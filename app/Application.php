<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use PDF;

class Application extends Model
{
	protected $fillable = [
		'tracking_no', 'present_division_id', 'permanent_division_id', 'present_district_id', 'permanent_district_id', 'present_upazila_id', 'permanent_upazila_id', 'name', 'father_name', 'mother_name', 'present_village_id', 'present_union_id', 'present_post_code_id', 'permanent_village_id', 'permanent_union_id', 'permanent_post_code_id',
		'batch_no', 'session_id', 'national_id', 'skill_summary', 'it_knowledge', 'communication_skill', 'training_subject_description',
		'is_self_employed', 'interested_work_description', 'gender', 'date_of_birth', 'educational_qualification', 'blood_group', 'religion',
		'nationality', 'contact_no', 'email', 'img', 'file_name'
	];
	
	public function division()
	{
		return $this->belongsTo('App\Division', 'division_id');
	}
	
	public function district()
	{
		return $this->belongsTo('App\District', 'district_id');
	}
	
	public function upazila()
	{
		return $this->belongsTo('App\Upazila', 'upazila_id');
	}
	
	public function scopeSearch($query, $search_txt)
{
$query->orWhere('name', 'like', '%'.$search_txt.'%' );
$query->orWhere('email', 'like', '%'.$search_txt.'%' );
		
		$search_txt_date = date('Y-m-d H:i', strtotime($search_txt));
		$search_txt_date = str_replace(":00","", $search_txt_date);
		$search_txt_date = str_replace("00","", $search_txt_date);
		$query->orWhere('created_at', 'like', '%'.$search_txt_date.'%');
		
}

protected function getNavData()
{

	$navDivisionData = Application::distinct('present_division_id')->count('present_division_id');
	$navDistrictData = Application::distinct('present_district_id')->count('present_district_id');
	$navUpazilaData = Application::distinct('present_upazila_id')->count('present_upazila_id');
	$allNavData = Application::count('id');

	$arr = [];

	$arr['navDivision'] = $navDivisionData;
	$arr['navDistrict'] = $navDistrictData;
	$arr['navUpazila'] = $navUpazilaData;
	$arr['allNavData'] = $allNavData;

	return $arr;
	//dd($arr);
}

}