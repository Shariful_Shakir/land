<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = 'geo_divisions';

	protected $fillable = ['division_name_eng', 'division_name_bng', 'bbs_code', 'created_by', 'modified_by'];

    public function district()
    {
    	return hasMany('App\District', 'geo_division_id', 'id');
    }

    public function upazila()
    {
    	return hasMany('App\Upazila', 'geo_division_id', 'id');
    }

    public function union()
	{
		return $this->hasMany('App\Union', 'geo_division_id', 'id');
	}


	protected function getDivisionInformation($divisionCode)
	{
		$district = Division::select('id', 'division_name_eng', 'division_name_bng')
		->where('id', $divisionCode)->first();

		//echo $district->division->name_en;	
		//dd($district->toArray());
		$rtrArr['division_code'] = $division->id;
		$rtrArr['division_name_eng'] = $division->division_name_eng;
		$rtrArr['division_name_bng'] = $division->division_name_bng;
		//dd($rtrArr);
		return $rtrArr;
	}
}
