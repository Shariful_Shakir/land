<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationalQualification extends Model
{
    protected $fillable = ['title_eng', 'title_bng'];
}
