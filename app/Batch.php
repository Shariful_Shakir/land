<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $table = 'batch_numbers';

    protected $fillable = ['title_eng', 'title_bng'];
}
