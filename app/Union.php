<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Union extends Model
{
	protected $table = 'geo_unions';

	protected $fillable = ['union_name_eng', 'union_name_bng', 'bbs_code', 'geo_division_id', 'geo_district_id', 'created_by', 'geo_upazila_id'];
	
    public function division()
	{
		return $this->belongsTo('App\Division', 'geo_division_id', 'divid');
	}
	
	public function district()
	{
		return $this->belongsTo('App\District', 'geo_district_id', 'id');
	}

	public function upazila()
	{
		return $this->belongsTo('App\Upazila', 'geo_upazila_id', 'id');
	}

	protected function getUnionsInfomationr($union_id)
	{
		$unions = Union::select('id', 'geo_upazila_id', 'union_name_eng', 'union_name_bng')->where('id', $union_id)
			->with([
				'upazila' => function($q) {
					return $q->select('id', 'upazila_name_eng', 'upazila_name_bng')->with([
					'district' => function($q) {
						return $q->select('id', 'district_name_eng', 'district_name_bng')
							->with([
								'division' => function($q) {
									return $q->select('id', 'division_name_eng', 'division_name_bng');
								}
							]);
						}
					]);
				}
			])
			->first();

		$rtrArr['division_id'] = $unions->$upazila->district->division->id;
		$rtrArr['division_name_eng'] = $unions->$upazila->district->division->division_name_eng;
		$rtrArr['division_name_bng'] = $unions->$upazila->district->division->division_name_bng;
		$rtrArr['district_id'] = $unions->$upazila->district->id;
		$rtrArr['district_name_eng'] = $unions->$upazila->district->district_name_eng;
		$rtrArr['district_name_bng'] = $unions->$upazila->district->district_name_bng;
		$rtrArr['upazila_id'] = $unions->$upazila->id;
		$rtrArr['upazila_name_eng'] = $unions->$upazila->upazila_name_eng;
		$rtrArr['upazila_name_bng'] = $unions->$upazila->upazila_name_bng;
		$rtrArr['union_name_eng'] = $unions->union_name_eng;
		$rtrArr['union_name_bng'] = $unions->union_name_bng;

		//dd($rtrArr);

		return $rtrArr;
		
	}
	
}
