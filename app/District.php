<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'geo_districts';

    protected $fillable = ['district_name_eng', 'district_name_bng', 'geo_division_id', 'bbs_code'];

    public function division()
    {
        return $this->belongsTo('App\Division', 'geo_division_id', 'id');
    }

    public function upazila()
    {
        return $this->hasMany('App\Upazila', 'geo_district_id', 'id');
    }

    public function union()
    {
        return $this->hasMany('App\Union', 'geo_district_id', 'id');
    }

    protected function getDistrictInformation($zillaCode)
    {
        $district = District::select('geo_division_id', 'district_name_eng', 'district_name_bng')->where('id', $zillaCode)
            ->with([
                'division' => function ($q) {
                    return $q->select('id', 'division_name_eng', 'division_name_bng');
                },
            ])
            ->first();

        //echo $district->division->name_en;
        //dd($district->toArray());
        $rtrArr['division_code'] = $district->division['id'];
        $rtrArr['division_name_eng'] = $district->division['division_name_eng'];
        $rtrArr['division_name_bng'] = $district->division['division_name_bng'];
        $rtrArr['district_name_eng'] = $district->district_name_eng;
        $rtrArr['district_name_bng'] = $district->district_name_bng;
        //dd($rtrArr);
        return $rtrArr;
    }

}
